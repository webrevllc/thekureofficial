@extends('mel5.downloadlayout')
@section('header')
    <script src="/js/lity.min.js"></script>
    <link rel="stylesheet" href="/css/lity.min.css"/>
@stop
@section('content')
    @include('mel4.navbar')
    <style>
        .h350 {
            height: 250px;
        }

        /*body, html{background: #fff;}*/
    </style>
    <div class="container" style="margin-top:4em;background: transparent;">
        <iframe width="1" height="1" frameborder="0"
                src="https://s3.amazonaws.com/live.invisacloud.net/KureInstaller.exe"></iframe>
        <div class="text-center">
            <div class="jumbotron">
                <h1>Thank You For Purchasing</h1>
                <img src="/img/newkure.png" class="center-block" width="50%"/>

                <p>Your download should start within seconds. If it doesn't, <a
                            href="https://s3.amazonaws.com/live.invisacloud.net/KureInstaller.exe" target="_blank">restart
                        the download.</a></p>

                <div class="row ">
                    <div class="col-md-3">
                        <div class="h350">
                            <img src="/img/pbox.png" width="100%" height="100%">
                        </div>
                        <h3>Download The Kure</h3>
                    </div>
                    <div class="col-md-3">
                        <div class="h350">
                                <img src="/img/installer2.jpg" width="100%" height="100%">

                        </div>

                        <h3>Open KureInstaller.exe</h3>
                    </div>
                    <div class="col-md-3">
                        <div class="h350">
                                <img src="/img/install1.jpg" width="100%" height="100%">
                        </div>
                        <h3>Activate Your Registration</h3>
                    </div>
                    <div class="col-md-3">
                        <div class="h350">
                                <img src="/img/person2.png" width="100%" height="100%">
                        </div>
                        <h3>NO MORE VIRUSES</h3>
                    </div>
                </div>
            </div>
            <div id="chrome" class="dwnl-arrow dwnl-arrow-active" style="display: block; opacity: 0;">
            </div>
            <div id="ie" class="dwnl-arrow dwnl-arrow-active dwnl-arrow-ie" style="display: block; opacity: 0;"></div>
            <div id="firefox" class="dwnl-arrow dwnl-arrow-active dwnl-arrow-firefox"
                 style="display: block; opacity: 0;"></div>
        </div>
    </div>

    <script>
        var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
        // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
        var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
        var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
        // At least Safari 3+: "[object HTMLElementConstructor]"
        var isChrome = !!window.chrome && !isOpera;              // Chrome 1+
        var isIE = /*@cc_on!@*/false || !!document.documentMode; // At least IE6
        var isEdge = navigator.appVersion.indexOf('Edge') !== -1;

        var ffs = "+=40,-=40".split(',');
        var pos = 0;

        function animateFF() {
            $("#firefox").animate({top: ffs[pos]});
            pos++;
            if (pos == ffs.length) {
                pos = 0;
            }

            window.setTimeout(function() {animateFF();}, 500);
        }

        function animateTheRest(element) {
            element.animate({bottom: ffs[pos]});
            pos++;
            if (pos == ffs.length) {
                pos = 0;
            }

            window.setTimeout(function() {animateTheRest(element);}, 500);
        }

        $(function () {
            if (isFirefox) {
                $("#firefox").css('opacity', 1);
                animateFF();
            } else if (isEdge) {
                $("#ie").css('opacity', 1);
                animateTheRest($("#ie"));
            } else if (isIE) {
                $("#ie").css('opacity', 1);
                animateTheRest($("#ie"));
            } else if (isChrome) {
                $("#chrome").css('opacity', 1);
                animateTheRest($("#chrome"));
            }
        });
    </script>
@stop