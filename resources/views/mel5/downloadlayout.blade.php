<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="author" content="The Kure">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!-- CSS -->
    <link href="/css/app.css" rel="stylesheet"/>
    {{--<link href="css/libs.css" rel="stylesheet"/>--}}
    <link href="/css/jquery-ui.css" rel="stylesheet"/>
    <link href="/css/styles.css?1" rel="stylesheet"/>
    <link href="https://bootswatch.com/cerulean/bootstrap.min.css?12" rel="stylesheet"/>


    <!--JS-->
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="/js/jquery-ui.js"></script>
    {{--<script src="js/sweetalert-dev.js"></script>--}}
    {{--<script src="js/scripts.js"></script>--}}
    <title>The Kure!</title>
    <style>
        .helper
        {
            position:fixed;
            width:150px;
            background:blue;
            bottom:0;
            right:0;
            padding:15px;
            text-align: center;
        }
    </style>
    @yield('header')
</head>

<body>
@yield('content')
@include('flash')
@yield('footer')


<div class="helper">
    <a href="/support" style="color:#fff;">Questions?</a>
</div>


</body>

</html>
