@extends('mel5.homelayout')

@section('content')

    <div class="navbar navbar-inverse navbar-fixed-top ">
        <a id="purchase_link" href="/purchase">
            <img id="purchase" src="img/headerbannerkure22.png?4" class="hidden-xs" style="width:100%;"/>
            <img src="/img/newkure.png" width="80%" class="center-block visible-xs"/>
            <h3 class="text-center visible-xs" style="color:#fff;">Order Now!</h3>
        </a>
    </div>

<div class="tech-wrapper">
    <div style="margin-top:170px;" class="hidden-xs"></div>
    <div class="container" >
        <div class="row">
            <div class="blackish " style="text-align: center;">
                <img src="/img/newkure.png" style="width:90%;" class="center-block">
            </div>
        </div>
        <div class="row">
            {{--<img src="/img/mcintelbanner.jpg" width="100%"/>--}}
        </div>
        <div class="row">
            <div class="orangebox " style="text-align: center; max-width:100%;">
                <h1 class="hidden-xs">NEVER EVER, EVER GET A VIRUS AGAIN! GUARANTEED!!</h1>
                <h4 class="visible-xs">NEVER EVER, EVER GET A VIRUS AGAIN! GUARANTEED!!</h4>
            </div>
        </div>
        <div class="row">
            <div class="greenbox " style="text-align: center;">
                {{--<h1>LIMITED TIME, ABSOLUTELY FREE 30 DAY TRIAL</h1>--}}
                <h1>$49.95 per pc One time, NO annual fees!!!</h1>
                <h1>IF YOU DON'T LOVE THE KURE SEND IT BACK AND YOU OWE NOTHING. WE'LL EVEN COVER THE POSTAGE TO RETURN IT</h1>
            </div>
        </div>
        <div class="row">
            {{--<div class="col-xs-12">--}}
                {{--<img src="/img/kure100.png" width="100%" class="center-block" style="border:5px solid black; border-radius:15px;margin:10px;"/>--}}
            {{--</div>--}}
            <div class="col-xs-12">
                <img src="/img/kure1000.png" width="100%" class="center-block" style="border:5px solid black; border-radius:15px;margin:10px;"/>
            </div>

        </div>

    </div>

    <div class="container" >
        <div class="row">
            <div class="col-xs-12 col-md-6 top-2">
                <img width="100%" src="/img/kureproductlifetime.png?1"/>
            </div>
            <div class="col-xs-12 col-md-6" style="margin-top: .5em;">
                <div class=" text-center">
                    <h3 style="margin:0px;">AS SEEN ON THE SAVINGS SHOPPING CHANNEL</h3>
                    {{--<h3 style="margin:0px;">THE SAVINGS SHOPPING CHANNEL</h3>--}}
                </div>

                <iframe style="margin-top: 6px; " width="100%" height="320" class="fancy-border" src="https://www.youtube.com/embed/N4Ge7xYafP0?autoplay=1&loop=1" frameborder="0" allowfullscreen></iframe>
                {{--<iframe style="margin-top: 6px; " width="100%" height="320" class="fancy-border" src="https://www.youtube.com/embed/UDdfh5tO7kU?autoplay=1&loop=1vq=hd1080" frameborder="0" allowfullscreen></iframe>--}}
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-8 top-2">
                <img src="img/vault.jpg" class="fancy-border" width="100%" style="margin-bottom: 3em;"/>
                <div class="col-md-6 col-xs-12">
                    <img src="img/windows10.png" class="fancy-border" width="100%"/>
                </div>
                <div class="col-md-6 col-xs-12 top-2">
                    <div class="md-green-text fancy-border text-center" >
                        <h3>Work on your PC just like you always have - ONLY FASTER! You'll fall in love with your computer <b>ALL OVER AGAIN!</b></h3>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 top-2">
                <div class="text-center orangebox" style="margin-bottom: 2em;">
                    <h2 class="hidden-xs">TURN IT OFF TURN IT ON AND THE VIRUSES ARE GONE!</h2>
                    <h3 class="visible-xs">TURN IT OFF TURN IT ON AND THE VIRUSES ARE GONE!</h3>
                </div>
                <div class="md-black-text fancy-border text-center" >
                    <h3>It's like an artificial intelligence. It knows what documents you have created. It knows what pictures you've imported. It knows which updates you've downloaded. It saves all of them, and only destroys the viruses. How's that for INTELLIGENT?</h3>
                </div>
            </div>
        </div>

        <div class="row" style="margin-top: 2em; text-align: center;padding:10px;">
            <div class="col-xs-12 col-md-4 md-black-text fancy-border">
                <h2>Over Two Million PCs protected daily by the commercial version of The Kure.</h2>
                <h4>JUST SOME OF OUR COMMERCIAL CUSTOMERS:</h4>
                <h4>COLUMBIA UNIVERSITY</h4>
                <h4>PENN STATE UNIVERSITY</h4>
                <h4>FLORIDA STATE UNIVERSITY</h4>
                <h4>U.S. OLYMPIC COMMITTEE</h4>
                <h4>HURLBURT AIR FORCE BASE</h4>
                <h4>CATHOLIC CHARITIES</h4>
                <h4>AIR FORCE ACADEMY</h4>
                <h4>U.S. NAVAL HOSPITAL</h4>
            </div>
            <div class="col-xs-12 col-md-7 col-md-offset-1 table-responsive fancy-border">
                <img src="/img/table.png" width="100%"/>
                {{--<table class="table table-condensed" style="text-align: center; border-radius: 10px">--}}
                    {{--<tr>--}}
                        {{--<th style="text-align: center;">VIRUS TYPES</th>--}}
                        {{--<th style="color: red;text-align: center;">Traditional--}}
                            {{--Antivirus EFFECTIVENESS--}}
                        {{--</th>--}}
                        {{--<th style="color: green;text-align: center;">The Kure--}}
                            {{--EFFECTIVENESS--}}
                        {{--</th>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td class="virus-type-cell">WORMS</td>--}}
                        {{--<td class="antivirus-table-cell">25%</td>--}}
                        {{--<td class="kure-table-cell">100%</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td class="virus-type-cell">TROJAN HORSE</td>--}}
                        {{--<td class="antivirus-table-cell">25%</td>--}}
                        {{--<td class="kure-table-cell">100%</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td class="virus-type-cell">BOOT VIRUSES</td>--}}
                        {{--<td class="antivirus-table-cell">25%</td>--}}
                        {{--<td class="kure-table-cell">100%</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td class="virus-type-cell">MACRO VIRUSES</td>--}}
                        {{--<td class="antivirus-table-cell">25%</td>--}}
                        {{--<td class="kure-table-cell">100%</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td class="virus-type-cell">MALWARE</td>--}}
                        {{--<td class="antivirus-table-cell">25%</td>--}}
                        {{--<td class="kure-table-cell">100%</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td class="virus-type-cell">RANSOMWARE</td>--}}
                        {{--<td class="antivirus-table-cell">25%</td>--}}
                        {{--<td class="kure-table-cell">100%</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td class="virus-type-cell">AVERAGE ON ALL VIRUSES</td>--}}
                        {{--<td class="antivirus-table-cell" style="font-size: 2em;">25%</td>--}}
                        {{--<td class="kure-table-cell" style="font-size: 2em;">100%</td>--}}
                    {{--</tr>--}}
                {{--</table>--}}
                <div class="text-center">
                    <a href="/purchase" class="btn btn-success btn-block btn-lg" style="text-align: center;"><h1>ORDER NOW</h1><small>100% SECURE 256-BIT SSL</small></a>
                </div>
            </div>
        </div>
        {{--<div class="row" style="margin-top: 4em; text-align: center;">--}}
            {{--<div class="col-xs-12 " style="border:3px solid black; border-radius:10px;">--}}
                {{--<h2>17 YEARS IN BUSINESS</h2>--}}
                {{--<h2>2 MILLION COMMERCIAL PC'S PROTECTED</h2>--}}
                {{--<h1><span style="font-size:1.8em;">0</span> VIRUSES!</h1>--}}
            {{--</div>--}}
        {{--</div>--}}
        <div class="row">
            <div class="col-xs-12 text-center">
                <img src="img/coffee.jpeg" width="100%"/>
            </div>
        </div>
        <div class="row" style="margin-top: 2em;">
            <div class="row greenbox" >
                <div class="col-xs-12 text-center">
                    <h2 class="hidden-xs">It's so EASY to use! Just install The KURE and never worry about viruses again! ORDER NOW!</h2>
                    <h3 class="visible-xs">It's so EASY to use! Just install The KURE and never worry about viruses again! ORDER NOW!</h3>
                </div>
            </div>

            <div class="row top-2" style="padding: 10px;">
                {{--<div class="col-xs-12 col-md-4 ">--}}
                    {{--<img src="img/person3.png" width="80%" class="fancy-border"/>--}}
                    {{--<p class="sm-black-text">When you install The Kure, the first thing it does is check--}}
                        {{--your system for viruses, spyware, etc. It will then clean your drives to ensure that The Kure is--}}
                        {{--installed on a perfectly clean system, so that every time you turn your system off and on again,--}}
                        {{--the system will return to a perfectly clean and pristine condition. No other software does that!</p>--}}
                {{--</div>--}}
                <div class="col-xs-12 col-md-4 ">
                    <img src="img/person2.png" width="80%" class="fancy-border"/>
                    <p class="sm-black-text">The Kure protects your most important files on your PC, 24 hours a day, 7 days
                        a
                        week,
                        365 days a year. All your files...<br/>Pictures, Videos, Music, tax returns, stock portfolios, back
                        accounts,
                        credit card accounts, usernames and passwords, social security numbers, e-mails, receipts...<br/>
                        NO MORE VIRUSES, EVER!!!</p>
                </div>
                <div class="col-xs-12 col-md-offset-4 col-md-4">
                    <img src="img/nicelady.PNG" width="80%" class="fancy-border"/>
                    <p class="sm-black-text">
                        5 Star Rated <img src="img/stars.PNG"/>
                    </p>

                    <p class="sm-black-text">
                        Patent Pending Technology
                    </p>

                    <p class="sm-black-text">
                        Used by U.S. Military
                    </p>

                    <p class="sm-black-text">
                        The ONLY safe way to surf the web... the ONLY way!!!
                    </p>

                    <p class="sm-black-text">
                        Cannot be defeated by new viruses!
                    </p>

                    <p class="sm-black-text">
                        Two million satisfied users
                    </p>

                    <p class="sm-black-text">
                        20 year old company serving Banking, Hospitals, and Government
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="text-center">
                    <a href="/purchase" class="btn btn-success btn-block btn-lg" style="text-align: center;"><h1>ORDER NOW!</h1><small>100% SECURE 256-BIT SSL</small></a>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <img src="/img/crossman.png?1" width="100%" class="top-2"/>
            </div>
            <div class="col-md-5">
                <img src="/img/kevin.png" width="100%" class="top-2"/>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <img src="/img/jan2.png" width="100%" class="top-2"/>
            </div>
        </div>

        {{--<div class="row">--}}
            {{--<div class="greenbox">--}}
                {{--<h3 class="text-center">LIMITED TIME BONUS OFFER WHEN YOU ORDER THE KURE</h3>--}}
                {{--<h3 class="text-center">CYBERSAFEPC IDENTITY THEFT SOFTWARE $19.95/YR VALUE ... FREE!!!</h3>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row" style="text-align: center; margin-top: 2em;">--}}
            {{--<div class="col-xs-12">--}}
                {{--<p class="md-red-text">SOMEONE GETS THEIR IDENTITY STOLEN--}}
                    {{--EVERY 2 SECONDS--}}
                    {{--<BR/>IDENTITY THEFT IS THE LARGEST PROPERTY CRIME IN THE U.S.</p>--}}

                {{--<p class="md-black-text">Hackers can steal your keystrokes as you type on your--}}
                    {{--PC. They can steal your:</p>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="row" style="text-align: center; margin-top: 2em;">--}}
            {{--<div class="col-xs-12 col-md-6">--}}
                {{--<ul style="text-align: left;" class="md-black-text">--}}
                    {{--<li>Usernames</li>--}}
                    {{--<li>Passwords</li>--}}
                    {{--<li>Account numbers</li>--}}
                    {{--<li>Credit Card information</li>--}}
                    {{--<li>Social Security numbers, and more!</li>--}}
                {{--</ul>--}}
            {{--</div>--}}
            {{--<div class="col-xs-12 col-md-6">--}}
                {{--<img src="img/identitytheft.PNG" width="100%"/>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="row" style="margin-top: 4em; text-align: center;">--}}
            {{--<div class="col-xs-12 col-md-6">--}}
                {{--<h2 >They can spy on your husband, wife, and--}}
                    {{--children through the webcam--}}
                    {{--on your computer and post videos of them on the web.<br/><br/>They can listen to your most private--}}
                    {{--conversations--}}
                    {{--through the microphone on your PC.</h2>--}}
            {{--</div>--}}
            {{--<div class="col-xs-12 col-md-6">--}}
                {{--<img src="img/dressing2.png" width="100%"/>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="row" style="margin-top: 4em; text-align: center;">--}}
            {{--<div class="col-xs-12 col-md-6">--}}
                {{--<p class="md-black-text">Identity theft is the number one property crime in America</p>--}}
                {{--<img src="img/cyber2.png" width="60%" class="center-block"/>--}}
                {{--<p class="md-black-text" style="margin-top: 1em;">CyberSafePC protects your PC from hackers attempting to invade your privacy, steal your money, and ruin your credit. It will even alert you by text or email that an attempt on your system was blocked!</p>--}}



            {{--</div>--}}
            {{--<div class="col-xs-12 col-md-6 center-block">--}}
                {{--<p class="big-blue-text">A $19.95/yr VALUE ABSOLUTELY FREE WITH THE KURE</p>--}}
                {{--<img src="img/cybersafe.jpg" width="50%"/>--}}
            {{--</div>--}}
        {{--</div>--}}


        <div class="row orangebox text-center" >
            <div class="col-xs-12">
                <h1>STOP THROWING CASH IN THE TRASH</h1>
                <h2 class="hidden-xs">Why pay $50/yr for ANTIVIRUS that works only 25% of the time?</h2>
                <h3 class="visible-xs">Why pay $50/yr for ANTIVIRUS that works only 25% of the time?</h3>
                <h2 class="hidden-xs"><b>$49.95 per pc One time, NO annual fees!!! WE WON'T CHARGE YOU A PENNY. WE'LL EVEN COVER THE POSTAGE TO RETURN IT!</b></h2>
                <h3 class="visible-xs"><b>$49.95 per pc One time, NO annual fees!!! WE WON'T CHARGE YOU A PENNY. WE'LL EVEN COVER THE POSTAGE TO RETURN IT!</b></h3>
            </div>
        </div>



        <div class="row " style="margin-top: 4em; text-align: center;">
            <div class="col-xs-12 col-md-offset-1 col-md-2">
                <img src="img/clients/murphy.png" width="100%" class="fancy-border"/>
                <br/><Br/>

                <p>MURPHY NORMAN</p>

                <p>&quot;I had Antivirus software and it was really frustrating me. You need
                    to fire your Antivirus company and hire THE KURE!&quot;</p>
            </div>
            <div class="col-xs-12 col-md-2">
                <img src="img/clients/michael.jpg" width="100%" class="fancy-border"/>
                <br/><Br/>

                <p>MICHAEL VANDIVER</p>

                <p>&quot;I got hit by Ransomware and lost everything on my pc. Now I have both the KURE
                    and CYBERSAFEPC. They are both awesome. They really do what they say they do.
                    You have to protect yourself.&quot;</p>
            </div>
            <div class="col-xs-12 col-md-2">
                <img src="img/clients/porsche.jpg" width="100%" class="fancy-border"/>
                <br/><Br/>

                <p>PORSCHE PARCHER</p>

                <p>&quot;I was skeptical at first about The KURE. Then I heard that it was being used
                    in Universities and Government facilities, so I gave it a try. It's an awesome program!&quot;</p>
            </div>
            <div class="col-xs-12 col-md-2">
                <img src="img/clients/keith.jpg" width="100%" class="fancy-border"/>
                <br/><Br/>

                <p>KEITH MURPHY</p>

                <p>&quot;I work as an expert in the Internet security space. But we were getting viruses on our home
                    network.
                    I've gotten rid of all my Antivirus software and all we now use is THE KURE&quot;</p>
            </div>
            <div class="col-xs-12 col-md-2">
                <img src="img/clients/maria.jpg" width="100%" class="fancy-border"/>
                <br/><Br/>

                <p>MARIA SARDINAS</p>

                <p>&quot;It doesn't matter what Antivirus we used, we were always getting one thing or another.
                    But since I installed THE KURE, we've had nothing. Now we are protected.&quot;</p>
            </div>
        </div>
        <div class="row" style="margin-top: 4em; text-align: center;">
            <div class="col-xs-12 col-md-4">
                <img src="img/novirus3.png" width="100%"/>
            </div>
            <div class="col-xs-12 col-md-4 greenbox" style="margin:0px;">
                <h3>Use The KURE for 30 full days. At the end of the 30 days, if you are not absolutely in love with the savings, the security and the peace of mind you get with The KURE, just send it back, no questions asked and you owe nothing. And, we'll even cover the postage to return it!</h3>
            </div>
            <div class="col-xs-12 col-md-4 ">
                {{--<img src="img/trialguar2.png" width="100%"/>--}}
                <img src="img/1000gurantee.png" width="100%"/>
            </div>
        </div>

        <div class="row" style="margin-top: 4em; text-align: center;">
            <div class="col-xs-12">
                <p class="big-green-text">$49.95 per pc One time, NO annual fees!!!</p>
                <a class="btn btn-success" href="/purchase">ORDER NOW</a>
            </div>
        </div>
        <div class="row">
            <div class=" container top-2" style="background: transparent;">
                <nav class="navbar navbar-inverse ">
                    <div style="background: transparent;">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="/purchase" style="padding:10px;"><img src="/img/usabased.png" style="width:222px;"></a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li style="margin-right:80px;"><a class="btn btn-warning" href="/watch">Watch The Demo</a></li>
                                <li><a href="/about">About Us</a></li>
                                <li><a href="/support">Support</a></li>
                                <li><a href="/contact">Contact Us</a></li>
                                <li><a href="/faq">FAQ</a></li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>
            </div>
        </div>
    </div>

</div>



{{--<style>--}}
    {{--.timer{--}}
        {{--position:fixed;--}}
        {{--left:-400px;--}}
        {{--top:50%;--}}
        {{--background:#fff;--}}
        {{--padding:10px;--}}
        {{--width:400px;--}}
        {{---webkit-border-radius:10px;--}}
        {{---moz-border-radius:10px;--}}
        {{--border-radius:10px;--}}
        {{--border:3px solid lime;--}}
    {{--}--}}
    {{--.timer h3{--}}
        {{--margin:0px;--}}
        {{--padding:0px;--}}
    {{--}--}}
    {{--#theCount{--}}
        {{--color:lime;--}}
    {{--}--}}
{{--</style>--}}
    {{--<div class="timer text-center hidden-xs">--}}
        {{--<h3>Free Trials End Soon! Only</h3>--}}
        {{--<h3 id="getting-started"></h3>--}}
        {{--<h3>Left!</h3>--}}
    {{--</div>--}}


    {{--<div class="container blackbox top-2" style="padding:20px;">--}}
        {{--<div class="top-2 top-2">--}}
            {{--<div class="col-xs-12 col-sm-2 text-center" >--}}
                {{--<a href="/about" style="background:#000;padding:10px;font-size: 1.7em; color:#fff;">ABOUT</a>--}}
            {{--</div>--}}
            {{--<div class="col-xs-12 col-sm-2 text-center" >--}}
                {{--<a href="/faq" style="background:#000;padding:10px;font-size: 1.7em;color:#fff;">FAQ</a>--}}
            {{--</div>--}}
            {{--<div class="col-xs-12 col-sm-2 text-center">--}}
                {{--<a href="/support" style="background:#000;padding:10px;font-size: 1.7em;color:#fff;">SUPPORT</a>--}}
            {{--</div>--}}
            {{--<div class="col-xs-12 col-sm-2 text-center" >--}}
                {{--<a href="/contact" style="background:#000;padding:10px;font-size: 1.7em;color:#fff;">CONTACT</a>--}}
            {{--</div>--}}
            {{--<div class="col-xs-12 col-sm-4 text-center" >--}}
                {{--<img src="/img/flag1.png" style="width:30%" class="center-block"/>--}}
                {{--<p class="text-center">A Proud American Company</p>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--<script>--}}
{{--//    $(function() {--}}
{{--//        getCountdown();--}}
{{--//    });--}}
{{--//--}}
{{--//    function getCountdown() {--}}
{{--//        $.ajax({--}}
{{--//            type: "GET",--}}
{{--//            url: "/get_countdown",--}}
{{--//            success: function(data){--}}
{{--//                console.log(data);--}}
{{--//                $("#theCount").html(data);--}}
{{--//            }--}}
{{--//        });--}}
{{--//    }--}}

    {{--$(document).ready(function(){--}}
        {{--var div = $(".timer");--}}
        {{--div.animate({left: '50px'}, 2000);--}}
        {{--div.animate({opacity: '1'}, 10000);--}}
        {{--div.animate({left: '-4000px'}, 2000);--}}
    {{--});--}}

{{--</script>--}}

    {{--<script type="text/javascript">--}}
    {{--$("#getting-started")--}}
        {{--.countdown("2015/12/04", function(event) {--}}
            {{--$(this).text(--}}
                    {{--event.strftime('%D days %H:%M:%S hours')--}}
            {{--);--}}
        {{--});--}}
    {{--</script>--}}
@stop
