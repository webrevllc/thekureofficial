<head>
    <link href="/css/app.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class=" row col-md-6 col-md-offset-3" style="margin-top:50px;">

        <form method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label for="email">Email address:</label>
                <input type="email" class="form-control" name="email" placeholder="Email">
            </div>
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name" placeholder="Person's Name">
            </div>
            <div class="form-group">
                <label for="message">Message:</label>
                <textarea class="form-control" cols="10" name="mymessage"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>

</body>