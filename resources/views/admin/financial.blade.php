@extends('admin.layout')

@section('header')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/dark-hive/jquery-ui.css">
    <style>
        .ui-datepicker-month, .ui-datepicker-year, .ui-datepicker-month option,  .ui-datepicker-year option {
            color: #000;
        }
    </style>
@stop
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Financial Data</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">

        <div class="col-lg-6 ">
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped" id="myTable">
                    <tr>
                        <th></th>
                        <th class="text-right">Sales</th>
                        <th class="text-right">Refunds</th>
                    </tr>
                    <tr>
                        <td>Today</td>
                        <td class="text-right">${{todayDollars()}}</td>
                        <td class="text-right">${{todayRefunds()}}</td>
                    </tr>
                    <tr>
                        <td>This Week</td>
                        <td class="text-right">${{weekDollars()}}</td>
                        <td class="text-right">${{weekRefunds()}}</td>
                    </tr>
                    <tr>
                        <td>This Month</td>
                        {{--<td class="text-right">${{monthDollars()}}</td>--}}
                        {{--<td class="text-right">${{monthRefunds()}}</td>--}}
                    </tr>
                    <tr>
                        <td>This Quarter</td>
                        {{--<td class="text-right">${{quarterDollars()}}</td>--}}
                        {{--<td class="text-right">${{quarterRefunds()}}</td>--}}
                    </tr>
                    <tr>
                        <td>This Year</td>
                        {{--<td class="text-right">${{yearDollars()}}</td>--}}
                        {{--<td class="text-right">${{yearRefunds()}}</td>--}}
                    </tr>
                    <tr>
                        <td>Total</td>
                        {{--<td class="text-right">${{totalSum()}}</td>--}}
                        {{--<td class="text-right">${{totalRefunds()}}</td>--}}
                    </tr>
                </table>
            </div>
            <!-- /.table-responsive -->
            <div class="well">
                <h3>Custom</h3>
                <form class="form-inline" method="post" action="/admin/financial">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="exampleInputName2">From</label>
                        <input type="text" class="form-control" id="from" name="fromDate" value="{{old('fromDate')}}"/>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail2">To</label>
                        <input type="text" class="form-control" id="to" name="toDate" value="{{old('toDate')}}"/>
                    </div>
                    <button type="submit" class="btn btn-info">Select these Dates</button>
                </form>
            </div>

            @if($customDateDollars != '')
                <div class="col-md-6">
                    <div class="alert alert-success" role="alert">
                        Total collected from selected dates: ${{$customDateDollars}}<br>
                        Total refunds from selected dates: ${{$getCustomDateRefunds}}
                    </div>
                </div>
            @endif
        </div>
        <div class="clearfix"></div>
    </div>
        <!-- /.row -->
    <script src="https://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script>
        $(function() {
            $( "#from" ).datepicker({
                defaultDate: "0",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd'
            });
            $( "#to" ).datepicker({
                defaultDate: "0",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd'
            });
        });
    </script>

@stop