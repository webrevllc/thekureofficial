@extends('admin.layout')
@section('header')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/dark-hive/jquery-ui.css">
    <style>
        .ui-datepicker-month, .ui-datepicker-year, .ui-datepicker-month option,  .ui-datepicker-year option {
            color: #000;
        }
    </style>
@stop
@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Pete Vs. Mel</h1>
            <h4>View Optimizely data <a href="http://app.optimizely.com/l/Ei3TIy?token=c81951f81d049841be59#" target="_blank" class="btn btn-primary">CLICK ME</a></h4>
        </div>
    </div>

    <form class="form-inline" method="post" action="/admin/pvm">
        {{csrf_field()}}
        <div class="form-group">
            <label for="exampleInputName2">From</label>
            <input type="text" class="form-control" id="from" name="fromDate" value="{{old('fromDate')}}"/>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail2">To</label>
            <input type="text" class="form-control" id="to" name="toDate" value="{{old('toDate')}}"/>
        </div>
        <button type="submit" class="btn btn-info">Select these Dates</button>
    </form>

    <div class="col-md-8">
        <div class="col-md-6">
            <h1 style="font-size: 150px; text-align: center" >{{$pete}}</h1>
            <h1 style="font-size: 75px; text-align: center">Pete's Site</h1>
        </div>
        <div class="col-md-6">
            <h1 style="font-size: 150px; text-align: center">{{$mel}}</h1>
            <h1 style="font-size: 75px; text-align: center">Mel's Site</h1>
        </div>
    </div>

    <script src="https://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script>
        $(function() {
            $( "#from" ).datepicker({
                defaultDate: "0",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd'
            });
            $( "#to" ).datepicker({
                defaultDate: "0",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd'
            });
        });
    </script>
@stop