<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>The Kure - Admin</title>

    <!-- Bootstrap Core CSS -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/css/admin/metis.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/css/admin/admin.css" rel="stylesheet">
    <link href="/css/libs.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://bootswatch.com/spacelab/bootstrap.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-latest.js"></script>
    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>--}}
    <script src="/js/jquery-ui.js"></script>
    <script src="/js/sweetalert-dev.js"></script>
    <script src="/js/admin/metis.js"></script>
    <script src="/js/admin/admin.js"></script>


    @yield('header')

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs-3.3.5/dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,cr-1.2.0,fh-3.0.0,r-1.0.7,sc-1.3.0/datatables.min.css"/>

    <script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,cr-1.2.0,fh-3.0.0,r-1.0.7,sc-1.3.0/datatables.min.js"></script>

</head>

<body>

<div id="wrapper">

    @include('admin.navbar')

    <div id="page-wrapper">

        @yield('content')

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

@include('flash')

@yield('footer')
</body>

</html>
