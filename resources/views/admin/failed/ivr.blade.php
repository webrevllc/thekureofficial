@extends('admin.layout')

@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            <h4>{{ Session::get('success') }}</h4>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger" role="alert">
            <h4>{{ Session::get('error') }}</h4>
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Orders</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> All Orders
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-8 ">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped" id="myTable">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Amount</th>
                                        <th>Customer Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Date Created</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($leads as $f)
                                        <tr>
                                            <td>{{$f->id}}</td>
                                            <td>${{$f->amount}}</td>
                                            <td>{{ucwords(strtolower($f->name))}}</td>
                                            <td>{{$f->email}}</td>
                                            <td>{{$f->phone}}</td>
                                            <td>{{date("F j, Y H:i:s", strtotime($f->created_at))}}</td>
                                            <td><a href="/admin/ivr_lead/{{$f->id}}" class="btn btn-danger "><i class="fa fa-trash-o"></i></a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.col-lg-4 (nested) -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
    </div>
    <!-- /.row -->

    <script>
        $(document).ready(function(){
            $('#myTable').DataTable({
                "iDisplayLength": 50,
                "aaSorting": []
            });
        });
    </script>

@stop