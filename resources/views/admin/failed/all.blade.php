@extends('admin.layout')

@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            <h4>{{ Session::get('success') }}</h4>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger" role="alert">
            <h4>{{ Session::get('error') }}</h4>
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Orders</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> All Orders
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-8 ">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped" id="myTable">
                                    <thead>
                                    <tr>
                                        <th>Update Billing</th>
                                        <th>Process Failed Subscription</th>
                                        <th>Failed ID</th>
                                        <th>Reason</th>
                                        <th>User Name</th>
                                        <th>Phone</th>
                                        <th>Original Order Date</th>
                                        <th>Amount</th>
                                        <th>Cancel Subscription</th>
                                        <th>Remove From List</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($fs as $f)
                                        <tr>
                                            <td><a href="/admin/user/edit/{{$f->user->id}}" class="btn btn-info "><i class="fa fa-pencil-square"></i></a></td>
                                            <td><a href="/admin/chargefailedsubscription/{{$f->id}}" class="btn btn-success"><i class="fa fa-recycle"></i></a></td>
                                            <td>{{$f->id}}</td>
                                            <td>{{$f->reason}}</td>
                                            <td>{{$f->user->name}}</td>
                                            <td>{{$f->user->phone}}</td>
                                            <td>{{date('F j, Y, g:i a', strtotime($f->subscription->created_at))}}</td>
                                            <td>${{number_format($f->amount, 2)}}</td>
                                            <td><a href="/admin/cancelFailed/{{$f->subscription->id}}" class="btn btn-danger "><i class="fa fa-trash-o"></i></a></td>
                                            <td><a href="/admin/removeFailed/{{$f->subscription->id}}" class="btn btn-warning "><i class="fa fa-rocket"></i></a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.col-lg-4 (nested) -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
    </div>
    <!-- /.row -->

    <script>
        $(document).ready(function(){
            $('#myTable').DataTable({
                "iDisplayLength": 50,
                "aaSorting": []
            });
        });
    </script>

@stop