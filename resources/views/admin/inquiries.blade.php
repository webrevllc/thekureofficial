@extends('admin.layout')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Inquiries</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> Outstanding Inquiries
                    <div class="pull-right">
                        {{count($inquiries)}}
                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th>Created At</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Message</th>
                                        <th>Respond Now</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($inquiries as $inquiry)
                                            <tr>
                                                <td>{{date("F j, Y g:i a", strtotime($inquiry->created_at))}}</td>
                                                <td>{{$inquiry->name}}</td>
                                                <td>{{$inquiry->phone}}</td>
                                                <td>{{$inquiry->email}}</td>
                                                <td>{{$inquiry->message}}</td>
                                                <td><a data-id="{{$inquiry->id}}" data-name="{{$inquiry->name}}" data-email="{{$inquiry->email}}" data-toggle="modal" data-target="#sendMessage"><i class="fa fa-2x fa-envelope-o"></i></a></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.col-lg-4 (nested) -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
    </div>
    <!-- /.row -->


    <!-- Modal -->
    <div class="modal fade" id="sendMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Reply to </h4>
                </div>
                <div class="modal-body">
                    <form method="post" >
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Recipient:</label>
                            <input type="text" class="form-control" id="recipient-name" name="email">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Message:</label>
                            <textarea class="form-control" id="message-text" name="message"></textarea>
                        </div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-send-o"></i> Send</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#sendMessage').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var name = button.data('name');
            var email = button.data('email');
            var id = button.data('id');
            var modal = $(this);
            modal.find('.modal-title').text('New message to ' + name);
            modal.find('#recipient-name').val(email);
            modal.find('.modal-body form').attr('action', '/admin/inquiries/' + id + '/message');
        })
    </script>

@stop