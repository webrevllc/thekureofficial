@extends('admin.layout')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Leads From The Website</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> All Leads
                    <div class="pull-right">
                        {{count($leads)}}
                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped" id="leadTable">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>City/State</th>
                                        <th>Email</th>
                                        <th>Phone Number</th>
                                        <th>Step</th>
                                        <th>Valid Credit Card?</th>
                                        <th>Has Converted?</th>
                                        <th>Email Sent?</th>
                                        <th>Created</th>
                                        <th>Updated</th>
                                        <th>Source</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($leads as $lead)
                                        <tr>
                                            <td>{{ucwords(strtolower($lead->first_name)) . ' ' . ucwords(strtolower($lead->last_name))}}</td>
                                            <td>{{ucwords(strtolower($lead->city)).', '. $lead->state}}</td>
                                            <td>{{$lead->email}}</td>
                                            <td>{{$lead->phone}}</td>
                                            <td>{{$lead->step}}</td>
                                            <td>{{$lead->auth_profile == '' ? 'No' : 'Yes'}}</td>
                                            <td>{{$lead->has_converted ? 'Yes' : 'No'}}</td>
                                            <td>{{$lead->email_sent ? 'Yes' : 'No'}}</td>
                                            <td>{{date('F j, Y, g:i a', strtotime($lead->created_at))}}</td>
                                            <td>{{date('F j, Y, g:i a', strtotime($lead->updated_at))}}</td>
                                            <td>{{$lead->source}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.col-lg-4 (nested) -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
    </div>
    <!-- /.row -->
    <script>
        $(document).ready(function(){
            $('#leadTable').DataTable({
                "iDisplayLength": 25,
                "aaSorting": []
            });
        });
    </script>


@stop