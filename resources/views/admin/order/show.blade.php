@extends('admin.layout')

@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            <h4>{{ Session::get('success') }}</h4>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger" role="alert">
            <h4>{{ Session::get('error') }}</h4>
        </div>
    @endif
    <div class="row">
        <div class="col-lg-6">
            <h1 class="page-header">
                <small><a href="/admin/customers/{{$order->user->id}}">{{$order->user->name}}</a></small>
                <br> Order: {{$order->invoice_number}}
                <small>{{date("F j, Y g:i a", strtotime($order->created_at))}} (EST)</small>
            </h1>
        </div>
        <div class="col-lg-6">
            <div class="btn-group pull-right" style="margin-top:40px;">

                {{--<button class="btn btn-success btn-sm" data-toggle="modal" data-target="#order"><i class="fa fa-question-circle"></i> Order/Customer not Found?</button>--}}
                <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#order"><i
                {{--<button class="btn btn-info btn-sm" data-toggle="modal" data-target="#addon"><i class="fa fa-plus"></i> Add On</button>--}}
                {{--<button class="btn btn-info" data-toggle="modal" data-target="#email"><i class="fa fa-envelope"></i> Email Receipt</button>--}}
                            class="fa fa-question-circle"></i> Order/Customer not Found?
                </button>
                <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#addon"><i class="fa fa-plus"></i>
                    Add On
                </button>
                {{--<button class="btn btn-info" data-toggle="modal" data-target="#email"><i class="fa fa-envelope"></i> Email Receipt</button>--}}
                @if($order->status == 'trial')
                    <a href="/admin/pdf?order={{$order->id}}" class="btn  btn-sm " ><i class="fa fa-print"></i> Print Welcome Letter</a>
                    <button class="btn btn-warning btn-sm {{$subscription->status == 'cancelled' ? '' : ''}}" {{$subscription->status == 'cancelled' ? '' : ''}}  data-toggle="modal" data-target="#partial"><i class="fa fa-space-shuttle"></i> Modify Trial Subscription</button>
                    <button class="btn btn-danger btn-sm {{$subscription->status == 'cancelled' ? '' : ''}}" {{$subscription->status == 'cancelled' ? '' : ''}} data-toggle="modal" data-target="#cancelTrial"><i class="fa fa-exclamation-triangle"></i> CANCEL</button>
                @else
                    <a href="/admin/pdf?order={{$order->id}}" class="btn  btn-sm " ><i class="fa fa-print"></i> Print Welcome Letter</a>
                    <button class="btn btn-warning btn-sm {{$subscription->status == 'cancelled' ? '' : ''}}" {{$subscription->status == 'cancelled' ? '' : ''}}  data-toggle="modal" data-target="#partial"><i class="fa fa-space-shuttle"></i> Partial Refund</button>
                    <button class="btn btn-danger btn-sm {{$subscription->status == 'cancelled' ? '' : ''}}" {{$subscription->status == 'cancelled' ? '' : ''}} data-toggle="modal" data-target="#refund"><i class="fa fa-exclamation-triangle"></i> Refund</button>
                @endif
            </div>
        </div>

    </div>
    <!-- /.row -->
    <!-- /.row -->
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-user fa-fw"></i> Order: {{$order->invoice_number}}
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th>Invoice Number</th>
                                        <th>Status</th>
                                        <th>Last 4</th>
                                        <th>Amount</th>
                                        <th>Date Ordered</th>
                                        <th>Source</th>
                                        <th>Shipped?</th>
                                        <th>Legacy?</th>
                                    </tr>
                                    </thead>
                                    <tr>
                                        <td>{{$order->invoice_number}}</td>
                                        <td>{{$order->status}}</td>
                                        <td>{{$order->last_4}}</td>

                                        <td>{{$order->amount == "Free Trial" ? "Free Trial" : "$".$order->amount}}</td>
                                        <td>{{date("F j, Y g:i a", strtotime($order->created_at))}}  (EST)</td>
                                        <td>{{$order->source}}</td>
                                        <td>{{$order->shipped ? date('F j, Y', strtotime($order->shipped_date)) . '  (EST) by ' . $order->shipped_by : 'Not yet'}}</td>
                                        <td>{{$order->user->legacy_id != null ? 'Yes' : 'No'}}</td>
                                    </tr>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.col-lg-4 (nested) -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <i class="fa fa-barcode fa-fw"></i> Serial Numbers For This Order
                    <div class="pull-right">
                        <b>{{count($serials)}}</b>
                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th>Serial Number</th>
                                        <th>Assigned</th>
                                        <th>Name</th>
                                    </tr>
                                    </thead>
                                    @foreach($serials as $serial)
                                        <tr>
                                            <td class="btnGetKurePassword"><a href="#">{{$serial->serial}}</a> <i class="fa fa-spinner fa-pulse"></i></td>
                                            <td>{{date('F j, Y', strtotime($serial->assigned_date))}}</td>
                                            <td>{{$serial->name}}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.col-lg-4 (nested) -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
    </div>


        <div class="row">
            @if(!empty ($subscription))
            <div class="col-xs-12 col-sm-6">
                <div class="panel {{$subscription->status == 'active' ? 'panel-success' : 'panel-danger'}}">
                    <div class="panel-heading">
                        @if($subscription->status == 'active')
                            <a href="/admin/paysubscription/{{$subscription->id}}" class="btn btn-info">PAY NOW</a>
                        @endif
                            <i class="fa fa-refresh fa-fw"></i> Related Subscription is <b>{{$subscription->status}}</b> {{$subscription->id}}
                        <div class="pull-right">
                            Next Process Date: <b>{{date('m-d-Y', strtotime($subscription->next_process_date))}}</b> - Amount: <b>${{number_format($subscription->amount, 2)}}</b>
                        </div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Quantity</th>
                                            <th>Price</th>
                                            <th>Total Paid</th>
                                        </tr>
                                        </thead>
                                        @foreach(json_decode($subscription->products) as $product)
                                            <tr>
                                                <td>{{$product->name}}</td>
                                                <td>{{$product->quantity}}</td>
                                                <td>${{$product->price/100}}</td>
                                                <td>${{number_format($product->quantity * $product->price/100, 2)}}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.col-lg-4 (nested) -->
                        </div>
                        <!-- /.row -->
                        {{--@if(! empty($change_orders))--}}
                            {{--<h2>Change Orders</h2>--}}
                            {{--@foreach($change_orders as $co)--}}
                                {{--Placed by: {{date('F j, Y, g:i a', strtotime($co->created_at))}}<br>--}}
                                {{--On:{{$co->user}}--}}
                            {{--@endforeach--}}
                        {{--@endif--}}
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
            @endif
            @if(!empty($order->refunds()->first()))
            <div class="col-xs-12 col-sm-6">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <i class="fa fa-exclamation-triangle fa-fw"></i> Refunds
                        {{--<div class="pull-right">--}}
                            {{--Next Process Date: <b>{{date('m-d-Y', strtotime($subscription->next_process_date))}}</b> ---}}
                            {{--Amount: <b>${{number_format($subscription->amount, 2)}}</b>--}}
                        {{--</div>--}}
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>

                                        <tr>
                                            <th>Refund Amount</th>
                                            <th>Date Refunded</th>
                                            <th>Refunded By</th>
                                            <th>Reason</th>
                                        </tr>
                                        </thead>
                                        @foreach($order->refunds as $refund)
                                            <tr>
                                                <td>${{number_format($refund->amount, 2)}}</td>
                                                <td>{{$refund->created_at}}</td>
                                                <td>{{$refund->refunded_by}}</td>
                                                <td>{{$refund->reason}}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.col-lg-4 (nested) -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
            @endif
        </div>



    {{--ADD ON MODAL--}}
    <!-- Modal -->
    <div class="modal fade" id="addon" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Subscription Add-On</h4>
                </div>
                <div class="modal-body">
                    <form action="/admin/subscription/{{$subscription->id}}/add-on" method="post">
                        <input type="hidden" name="order_id" value="{{$order->id}}"/>
                        {{csrf_field()}}
                        <table class="table">
                            @foreach($products as $p)
                                <tr>
                                    <td>{{$p->name}}</td>
                                    <td>${{$p->price/100}}/yr prorated</td>
                                    <td><input type="number" name="{{$p->api_code}}" min="0" value="0"/></td>
                                </tr>
                            @endforeach
                        </table>

                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Add-On To Current Subscription</button>
                        <a href="newOrder" class="btn btn-success">Create a New Order/Subscription</a>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{--EMAIL RECEIPT MODAL--}}
    <!-- Modal -->
    {{--<div class="modal fade" id="email" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">--}}
    {{--<div class="modal-dialog" role="document">--}}
    {{--<div class="modal-content">--}}
    {{--<div class="modal-header">--}}
    {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>--}}
    {{--<h4 class="modal-title" id="myModalLabel">Modal title</h4>--}}
    {{--</div>--}}
    {{--<div class="modal-body">--}}
    {{--...--}}
    {{--</div>--}}
    {{--<div class="modal-footer">--}}
    {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
    {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--PARTIAL REFUND MODAL--}}
    <!-- Modal -->
    <div class="modal fade" id="partial" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Change the Quantities</h4>
                </div>
                <div class="modal-body">
                    @if($order->status == 'trial')
                        <form action="/admin/subscription/{{$subscription->id}}/modify-trial" method="post">
                    @else
                        <form action="/admin/subscription/{{$subscription->id}}/partial" method="post">
                            <input type="hidden" name="order_id" value="{{$order->id}}"/>
                    @endif

                        {{csrf_field()}}
                        <table class="table table-condensed">
                            @foreach(json_decode($subscription->products) as $product)
                                <tr>
                                    <td>{{$product->name}}</td>
                                    <td><input type="text" class="form-control input-sm" value="{{$product->quantity}}"
                                               name="product[{{$product->id}}]"></td>
                                </tr>
                            @endforeach
                            <tr>
                                <td>Select Reason</td>
                                <td>
                                    <select name="reason" class="form-control">
                                        <option value="Misplaced Order">Misplaced Order</option>
                                        <option value="Changed Mind">Changed Mind</option>
                                        <option value="I don't want the product anymore">I don't want the product
                                            anymore
                                        </option>
                                        <option value="Other">Other</option>
                                    </select>
                                </td>
                            </tr>
                        </table>

                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

                        @if($order->status == 'trial')
                            <button type="submit" class="btn btn-primary">Change Subscription</button>
                        @else
                            <button type="submit" class="btn btn-primary">Issue Refund</button>
                        @endif
                    </form>
                </div>

            </div>
        </div>
    </div>


    {{--FULL REFUND MODAL--}}
    <!-- Modal -->
    <div class="modal fade" id="refund" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Issue Full Refund?</h4>
                </div>
                <div class="modal-body">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h3 class="panel-title">FULLY REFUND ${{$order->amount}}?</h3>
                        </div>
                        <div class="panel-body">
                            You are about to cancel this subscription and future payments for this order.
                            ${{$order->amount}} will be refunded to the customer within 3-5 business days.
                        </div>
                        <div class="panel-footer">
                            <form action="/admin/subscription/{{$subscription->id}}/full" method="POST">
                                <input type="hidden" name="order_id" value="{{$order->id}}"/>
                                {{csrf_field()}}
                                <button class="btn btn-block btn-danger" type="submit">ISSUE REFUND</button>
                            </form>
                            <button class="btn btn-block btn-success" type="button" data-dismiss="modal"
                                    aria-label="Close">Cancel
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <div class="modal fade" id="cancelTrial" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Cancel Trial Subscription?</h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                <h3 class="panel-title">THIS WILL CANCEL THE SUBSCRIPTION - THE CUSTOMER WILL NOT BE BILLED</h3>
                            </div>
                            <div class="panel-body">
                                You are about to cancel this subscription and future payments for this order.
                            </div>
                            <div class="panel-footer">
                                <form action="/admin/subscription/{{$subscription->id}}/cancel-trial" method="POST">
                                    {{csrf_field()}}
                                    <button class="btn btn-block btn-danger" type="submit">CANCEL FREE TRIAL</button>
                                </form>
                                <button class="btn btn-block btn-success" type="button" data-dismiss="modal" aria-label="Close">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    <div class="modal fade" id="order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload The Order/Customer</h4>
                </div>
                <div class="modal-body">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Upload The Customers Order</h3>
                        </div>
                        <div class="panel-body">
                            With some legacy customers their order was not uploaded properly, this will make it is.
                        </div>
                        <div class="panel-footer">
                            <form action="/admin/ordernotfound/{{$order->id}}" method="POST">
                                {{csrf_field()}}
                                <button class="btn btn-block btn-primary" type="submit">Upload The Order</button>
                            </form>
                            <form action="/admin/customernotfound/{{$order->id}}" method="POST">
                                {{csrf_field()}}
                                <button class="btn btn-block btn-info" type="submit">Upload The Customer</button>
                            </form>
                            <button class="btn btn-block btn-success" type="button" data-dismiss="modal"
                                    aria-label="Close">Cancel
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $('.fa-spinner').hide();
            $(".btnGetKurePassword a").click(function (e) {

                var serial = $(e.target).text();
                var button = $(this);
                $('.fa-spinner').show();
//                $(this).html("Loading...");

                $.ajax({
                    url: "/mykure/support/getKurePassword",
                    type: "POST",
                    data: "serial=" + serial,
                    success: function (result) {
                        if (result == "" || result == null) {
                            $('.fa-spinner').hide();
                            alert("No Kure password associated with this serial.");

                        } else {
                            $('.fa-spinner').hide();
                            alert("Your Kure password: " + result);
                        }
                    },
                    error: function () {
                        alert("Could not find password");
                        $('.fa-spinner').hide();
                    }
                });
            });
        });
    </script>
@stop