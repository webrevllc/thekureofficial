@extends('admin.layout')

@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            <h4>{{ Session::get('success') }}</h4>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger" role="alert">
            <h4>{{ Session::get('error') }}</h4>
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><small><a href="/admin/customers/{{$order->user->id}}">{{$order->user->name}}</a></small><br> Add-on for Subscription: {{$subscription->unique_id}} <small>{{date("F j, Y g:i a", strtotime($order->created_at))}}  (EST)</small></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <i class="fa fa-dollar fa-fw"></i> New Add-On Order
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Description</th>
                                        <th>Quantity</th>
                                    </tr>
                                    </thead>
                                        @foreach($prod as $p)
                                            <tr>
                                                 <td>{{$p->name}}</td>
                                                 <td>{{$p->description}}</td>
                                                 <td>{{$p->quantity}}</td>
                                            </tr>
                                        @endforeach
                                            <tr>
                                                <td></td>
                                                <td><b>Prorated Amount</b></td>
                                                <td>${{calculateProration($order, $prod)}}</td>
                                            </tr>
                                </table>
                            </div>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.col-lg-4 (nested) -->
                </div>
                <div class="panel-footer">
                    <button class="btn btn-success pull-right">Place Order</button>
                    <button class="btn btn-danger pull-right" style="margin-right:10px;">Cancel</button>
                    <div class="clearfix"></div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.panel-body -->
        </div>
    </div>

    @stop