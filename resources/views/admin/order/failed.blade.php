@extends('admin.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Dashboard</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> Failed IVR Orders
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Amount</th>
                                        <th>Date/Time Order Attempted</th>
                                        <th>Invoice Number</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($forders as $order)
                                        <tr>
                                            <td><a href="/admin/customers/{{$order->user->id}}">{{$order->user->name}}</a></td>
                                            <td>${{$order->amount}}</td>
                                            <td>{{date('F j, Y g:i a', strtotime($order->created_at))}}</td>
                                            <td>{{$order->invoice_number}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.col-lg-4 (nested) -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> Incomplete Web Orders
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Date/Time Order Attempted</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($corders as $iorder)
                                        <tr>
                                            <td><a href="/admin/customers/{{$iorder->id}}">{{$iorder->name}}</a></td>
                                            <td>{{date('F j, Y g:i a', strtotime($iorder->created_at))}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.col-lg-4 (nested) -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
        <!-- /.col-lg-8 -->

    </div>
    <!-- /.row -->


@stop