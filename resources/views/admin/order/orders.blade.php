@extends('admin.layout')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Orders</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> All Orders
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-6 ">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped" id="myTable">
                                    <thead>
                                    <tr>
                                        <th>Order ID</th>
                                        <th>Invoice Number</th>
                                        <th>Created At</th>
                                        <th>Amount</th>
                                        <th>Source</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orders as $order)
                                        <tr>
                                            <td class="text-right">{{$order->id}}</td>
                                            <td class="text-right"><a href="/admin/order/{{$order->id}}">{{$order->invoice_number}}</a></td>
                                            <td class="text-right">{{date("F j, Y, g:i a", strtotime($order->created_at))}}</td>
                                            @if($order->amount == "Free Trial")
                                                <td class="text-right">${{$order->subscription->amount}}</td>
                                            @else
                                                <td class="text-right">${{$order->amount}}</td>
                                            @endif
                                            @if($order->amount == "Free Trial")
                                                <td class="text-right">{{$order->source}}/Free Trial</td>
                                            @else
                                                <td class="text-right">{{$order->source}}</td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.col-lg-4 (nested) -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
    </div>
    <!-- /.row -->

    <script>
        $(document).ready(function(){
            $('#myTable').DataTable({
                "iDisplayLength": 50,
                "aaSorting": []
            });
        });
    </script>

@stop