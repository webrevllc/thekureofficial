@extends('admin.layout')

@section('content')
    <style>
        table{height:300px;overflow-y: scroll;display: block;}
    </style>
    <div class="row">
        <div class="col-lg-12">
            <div class="col-xs-12">
                <h1 class="page-header">Dashboard</h1>
                {{--<h4>View Optimizely data <a href="http://app.optimizely.com/l/Ei3TIy?token=c81951f81d049841be59#" target="_blank" class="btn btn-primary">CLICK ME</a></h4>--}}
            </div>
            {{--<div class="col-xs-5">--}}
                {{--<div class="form-inline" style="margin-top:30px;">--}}
                    {{--<div class="form-group">--}}
                        {{--<label for="exampleInputName2">Load a Schedule</label>--}}
                        {{--{!!  Form::select('Schedules', $schedules, $airdates[0]->schedule_id, array('id' => 'seCompanyID'))  !!}--}}
                        {{--<select class="form-control" id="loadsched">--}}
                            {{--@foreach($schedules as $schedule)--}}
                                {{--<option value="{{$schedule->id}}">{{$schedule->filename}}</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                    {{--</div>--}}
                    {{--<button data-toggle="modal" data-target="#uploader" class="btn btn-danger"><i class="fa fa-upload"></i> Upload Schedule</button>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <style>
        canvas{
            width: 100% !important;
            height: 300px !important;
        }
    </style>
    <div class="col-md-12">
        <h3>Totals</h3>
        <canvas id="all" width="100%" height="400px"></canvas>
    </div>
    {{--<div class="col-md-3" >--}}
        {{--<h3>Weekly Schedule</h3>--}}
        {{--<table class="table table-bordered" >--}}
            {{--<tr>--}}
                {{--<th>Date</th>--}}
                {{--<th>Day</th>--}}
                {{--<th>Time</th>--}}
                {{--<th>Spend</th>--}}
            {{--</tr>--}}
            {{--@foreach($airdates as $airdate)--}}
                {{--<tr>--}}
                    {{--<td>{{date('m-d', strtotime($airdate->airdateday))}}</td>--}}
                    {{--<td>{{$airdate->airdateday}}</td>--}}
                    {{--<td>{{$airdate->air_time}}</td>--}}
                    {{--<td>${{number_format($airdate->spend, 2)}}</td>--}}
                {{--</tr>--}}
            {{--@endforeach--}}
        {{--</table>--}}
    {{--</div>--}}

    <div class="col-md-12">
        <h3>Web Totals</h3>
        <canvas id="web" width="100%" height="300px"></canvas>
    </div>

    <div class="col-md-12">
        <h3>IVR Totals</h3>
        <canvas id="ivr" width="100%" height="300px"></canvas>
    </div>

    <div class="col-xs-12">
        <h3>Upcoming Renewals</h3>
        <canvas id="upcoming_renewals" width="100%" height="300px"></canvas>
    </div>

    <div class="modal fade" id="uploader" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Excel Schedule</h4>
                </div>
                <form enctype="multipart/form-data" action="/admin/schedule" method="post">
                    <div class="modal-body">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label>Upload Schedule</label>
                            <input type="file" name="schedule"/>
                        </div>
                        <div class="form-group">
                            <label>Name This File</label>
                            <input type="text" name="nameoffile" class="form-control" placeholder="today's date or something"/>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Upload</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
@stop

@section('footer')
    <script src="/js/admin/chart.min.js"></script>
    <script>
        (function(){
            var all = document.getElementById('all').getContext('2d');
            var web = document.getElementById('web').getContext('2d');
            var ivr = document.getElementById('ivr').getContext('2d');
            var upcomingRenewals = document.getElementById('upcoming_renewals').getContext('2d');
            var allChart = {
                labels: {!! $dates !!},
                datasets: [{
                    data: {!! $allTotals !!},
                    label: "All Totals",
                    fillColor : "rgba(255,0,0,0.0)",
                    strokeColor : "rgba(255,0,0,1;",
                    pointColor : "rgba(255,0,0,0.7)",
                    pointHighlightFill : "rgba(255,0,0,1)",
                    pointHighlightStroke : "rgba(255,0,0,1)"
                },
                    {
                        data: {!! $dailyWebTotals !!},
                        label: "Daily Web Totals",
                        fillColor : "rgba(0,255,0,0.0)",
                        strokeColor : "rgba(0,255,0,1)",
                        pointColor : "rgba(0,255,0,0.7)",
                        pointHighlightFill : "rgba(0,255,0,0.7)",
                        pointHighlightStroke : "rgba(0,255,0,0.7)"
                    },
                    {
                        data: {!! $dailyIVRTotal !!},
                        label: "Daily IVR Totals",
                        fillColor : "rgba(0,0,255,0.0)",
                        strokeColor : "rgba(0,0,255,1)",
                        pointColor : "rgba(0,0,255,0.7)",
                        pointHighlightFill : "rgba(0,0,255,0.7)",
                        pointHighlightStroke : "rgba(0,0,255,0.7)"
                    }]
            };
            var webChart = {
                labels: {!! $dates !!},
                datasets: [
                    {
                        data: {!! $dailyWebTrials !!},
                        label: "Daily Web Trial Orders",
                        fillColor : "rgba(0,255,0,0.0)",
                        strokeColor : "rgba(0,255,0,1)",
                        pointColor : "rgba(0,255,0,0.7)",
                        pointHighlightFill : "rgba(0,255,0,0.7)",
                        pointHighlightStroke : "rgba(0,255,0,0.7)"
                    },
                    {
                        data: {!! $dailyWebPaid !!},
                        label: "Daily Web Paid Orders",
                        fillColor : "rgba(0,0,255,0.0)",
                        strokeColor : "rgba(0,0,255,1)",
                        pointColor : "rgba(0,0,255,0.7)",
                        pointHighlightFill : "rgba(0,0,255,0.7)",
                        pointHighlightStroke : "rgba(0,0,255,0.7)"
                    }]
            };
            var ivrChart = {
                labels: {!! $dates !!},
                datasets: [
                    {
                        data: {!! $dailyIVRPaid !!},
                        label: "Daily IVR Paid Orders",
                        fillColor : "rgba(0,255,0,0.0)",
                        strokeColor : "rgba(0,255,0,1)",
                        pointColor : "rgba(0,255,0,0.7)",
                        pointHighlightFill : "rgba(0,255,0,0.7)",
                        pointHighlightStroke : "rgba(0,255,0,0.7)"
                    },
                    {
                        data: {!! $dailyIVRTrial !!},
                        label: "Daily IVR Trial Orders",
                        fillColor : "rgba(0,0,255,0.0)",
                        strokeColor : "rgba(0,0,255,1)",
                        pointColor : "rgba(0,0,255,0.7)",
                        pointHighlightFill : "rgba(0,0,255,0.7)",
                        pointHighlightStroke : "rgba(0,0,255,0.7)"
                    }]
            };

            var renewalChart = {
                labels: {!! $renewalDates !!},
                datasets: [
                    {
                        data: {!! $renewals !!},
                        label: "Upcoming Renewals",
                        fillColor : "rgba(0,255,0,1)",
                        strokeColor : "rgba(0,255,0,1)",
                        pointColor : "rgba(0,255,0,0.7)",
                        pointHighlightFill : "rgba(0,255,0,0.7)",
                        pointHighlightStroke : "rgba(0,255,0,0.7)"
                    }]
            };

            new Chart(all).Line(allChart, {multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"});
            new Chart(web).Line(webChart, {multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"});
            new Chart(ivr).Line(ivrChart, {multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"});
            new Chart(upcomingRenewals).Line(renewalChart, {multiTooltipTemplate: "<%= datasetLabel %> - <%= $value %>"});

        })();


        $('#loadsched').on('change', function() {
//            alert( this.value ); // or $(this).val()
            window.location.href = "/admin?schedule="+ this.value;

        });
    </script>
@stop