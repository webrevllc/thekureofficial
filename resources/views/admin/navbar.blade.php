
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/admin">The Kure Admin v2.0</a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">

            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    {{\Auth::user()->name}} <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    {{--<li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>--}}
                    {{--</li>--}}
                    {{--<li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>--}}
                    {{--</li>--}}
                    {{--<li class="divider"></li>--}}
                    <li>
                        <a href="/auth/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-search"></i> Search <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <form action="/admin/lookup" method="post">
                                        {{csrf_field()}}
                                        <label>Search by Customer Name</label>
                                        <input type="text" class="form-control input-sm" name="pname" placeholder="Customer name...">
                                        <button class="btn btn-info btn-sm btn-block" type="submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </form>
                                </div>
                                <!-- /input-group -->
                            </li>
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <form action="/admin/lookup-order" method="post">
                                        {{csrf_field()}}
                                        <label>Search by Order Number</label>
                                        <input type="text" class="form-control input-sm" name="ordernumber" placeholder="Order/Transaction Number...">
                                        <button class="btn btn-info btn-sm btn-block" type="submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </form>
                                </div>
                                <!-- /input-group -->
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>

                    <li>
                        <a href="/admin"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="/admin/shipments"><i class="fa fa-gift fa-fw"></i> Shipments</a>
                    </li>
                    <li>
                        <a href="/admin/customers"><i class="fa fa-users fa-fw"></i> Customers</a>
                    </li>
                    <li>
                        <a href="/admin/order"><i class="fa fa-bullseye fa-fw"></i> All Orders</a>
                    </li>
                    {{--<li>--}}
                        {{--<a href="/admin/pvm"><i class="fa fa-legal fa-fw"></i> Pete vs. Mel</a>--}}
                    {{--</li>--}}
                    <li>
                        <a href="/admin/leads"><i class="fa fa-flag fa-fw"></i> Leads</a>
                    </li>
                    <li>
                        <a href="/admin/failed_subs"><i class="fa fa-frown-o fa-fw"></i> Failed Subscriptions</a>
                    </li>
                    <li>
                        <a href="/admin/ivr_leads"><i class="fa fa-tasks fa-fw"></i> IVR Leads</a>
                    </li>
                    {{--<li>--}}
                        {{--<a href="/admin/failed"><i class="fa fa-exclamation-triangle fa-fw"></i> Failed/Incomplete Orders</a>--}}
                    {{--</li>--}}

                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>
