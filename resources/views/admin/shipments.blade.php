@extends('admin.layout')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Shipments</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> Orders that need to be shipped
                    <div class="pull-right">
                        {{count($orders)}}
                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th>Print</th>
                                        <th>Created At</th>
                                        {{--<th>Name</th>--}}
                                        <th>Invoice Number</th>
                                        <th>Amount</th>
                                        <th>Source</th>
                                        <th>Mark Shipped</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orders as $order)
                                        <tr>
                                            <td>
                                                <a href="/admin/pdf/{{$order->id}}" style="color:lightgreen"><i class="fa fa-2x fa-print"></i></a>
                                            </td>
                                            <td>{{date("F j, Y, g:i a", strtotime($order->created_at))}}</td>
                                            {{--<td>{{$order->user->name}}</td>--}}
                                            <td>{{$order->invoice_number}}</td>
                                            <td>{{$order->amount}}</td>
                                            <td>{{$order->source}}</td>
                                            <td><a href="shipments/marked/{{$order->id}}" style="color:red;"><i class="fa fa-2x fa-times"></i></a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.col-lg-4 (nested) -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
    </div>
    <!-- /.row -->


    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Trial Shipments</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> Orders that need to be shipped
                    <div class="pull-right">
                        {{count($trials)}}
                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th>Print</th>
                                        <th>Created At</th>
                                        {{--<th>Name</th>--}}
                                        <th>Invoice Number</th>
                                        <th>Amount</th>
                                        <th>Source</th>
                                        <th>Mark Shipped</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($trials as $order)
                                        <tr>
                                            <td>
                                                <a href="/admin/trialpdf/{{$order->id}}" style="color:lightgreen"><i class="fa fa-2x fa-print"></i></a>
                                            </td>
                                            <td>{{date("F j, Y, g:i a", strtotime($order->created_at))}}</td>
                                            {{--<td>{{$order->user->name}}</td>--}}
                                            <td>{{$order->invoice_number}}</td>
                                            <td>{{$order->amount}}</td>
                                            <td>{{$order->source}}</td>
                                            <td><a href="shipments/marked/{{$order->id}}" style="color:red;"><i class="fa fa-2x fa-times"></i></a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.col-lg-4 (nested) -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
    </div>
    <!-- /.row -->
@stop