@extends('admin.layout')

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            <h4>{{ Session::get('success') }}</h4>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger" role="alert">
            <h4>{{ Session::get('error') }}</h4>
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit User</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-user fa-fw"></i> Edit User: {{$user->name}}
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="form-horizontal" method="post">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="username" class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="username" name="username" value="{{$user->name}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" id="email" name="email" value="{{$user->email}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-primary">Edit Customers Info</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.col-lg-4 (nested) -->
                    </div>

                    <!-- /.row -->
                </div>
                <!-- /.panel-body -->
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-user fa-fw"></i> User Actions
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#receipt" aria-controls="receipt" role="tab" data-toggle="tab">Send Receipt</a></li>
                        <li role="presentation"><a href="#cc" aria-controls="cc" role="tab" data-toggle="tab">Add/Change Credit Card</a></li>
                        {{--<li role="presentation"><a href="#shipping" aria-controls="shipping" role="tab" data-toggle="tab">Add/Change Shipping Info</a></li>--}}
                        <li role="presentation"><a href="#sendcard" aria-controls="sendcard" role="tab" data-toggle="tab">Send New Membership Card(s)</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        @include('admin.customers.edittabs.receipt')
                        @include('admin.customers.edittabs.cc')
                        @include('admin.customers.edittabs.shipping')
                        @include('admin.customers.edittabs.sendcard')




                    </div>
                </div>
            </div>
        </div>
    </div>
@stop