@extends('admin.layout')

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            <h4>{{ Session::get('success') }}</h4>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger" role="alert">
            <h4>{{ Session::get('error') }}</h4>
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Create A New Order For {{$user->name}}
                <br><small>Email: {{$user->email}}</small>
                <br><small>Phone: {{$user->phone}}</small>
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- /.row -->
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <form method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <label>Trial or Paid Order?</label>
                    <div class="radio">
                        <label>
                            <input type="radio" name="orderType" id="trialOrder" value="trial" checked>
                            Trial Order
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="orderType" id="paidOrder" value="paid">
                            Paid Order
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="selector">How Many "Kures"?</label>
                    <select id="selector" class="form-control" name="quantity">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Add-ons</label>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="CSPC" name="addon[]">
                            <span id = "cspclabel">PC Cleaner On Steroids - $8.95</span>
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="HWMON" name="addon[]">
                            Hardware Monitoring - $4.95
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="PROC" name="addon[]">
                            Rush Processing - $1.99
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label>Shipping - $5.95</label>
                </div>
                <div class="form-group">
                    <span id="renewalDate"></span>
                    {{--<a href="#">Change This</a>--}}
                </div>

                <input type="submit" class="btn btn-success" />

            </form>
        </div>
    </div>

    <script>
        $('#renewalDate').text("This order will be charged on {{date('F j, Y', strtotime("+30 days"))}}");
        $('#paidOrder').change(function(){
           if($(this).is(':checked')){
               $('#cspc').prop('checked', true);
               $('#cspclabel').text('PC Cleaner On Steroids - FREE w/PAID ORDER');
               $('#renewalDate').text("This order will renew on {{date('F j, Y', strtotime("+365 days"))}}");
           }
        });
        $('#trialOrder').change(function(){
            if($(this).is(':checked')){
                $('#cspc').prop('checked', false);
                $('#cspclabel').text('PC Cleaner On Steroids - $8.95');
                $('#renewalDate').text("This order will be charged on {{date('F j, Y', strtotime("+30 days"))}}");
            }
        });

        function getFormattedDate(date) {
            var year = date.getFullYear();
            var month = (1 + date.getMonth()).toString();
            month = month.length > 1 ? month : '0' + month;
            var day = date.getDate().toString();
            day = day.length > 1 ? day : '0' + day;
            return year + '/' + month + '/' + day;
        }
    </script>
@stop