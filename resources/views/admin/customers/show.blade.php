@extends('admin.layout')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{$user->name}}
                <small style="font-size:.5em;">
                    <a class="btn btn-info" href="/admin/user/edit/{{$user->id}}">Edit &nbsp;<i class="fa fa-1x fa-pencil"></i></a>
                    <a class="btn btn-success" href="/admin/user/order/{{$user->id}}">Create A New Order &nbsp;<i class="fa fa-1x fa-shopping-cart"></i></a>
                </small>
                <br><small>Email: {{$user->email}}</small>
                <br><small>Phone: {{$user->phone}}</small>
                @if($address)
                    <br><small>Address: {{$address->address . ' ' . $address->city.', '. $address->state .' '. $address->zip}}</small>
                @endif
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- /.row -->
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-user fa-fw"></i> {{$user->name}}'s Orders
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                @foreach($user->orders as $order)
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th>Invoice Number</th>
                                        <th>Status</th>
                                        <th>Amount</th>
                                        <th>Date Ordered</th>
                                        <th>Source</th>
                                        <th>Shipped?</th>
                                        <th>Legacy?</th>
                                    </tr>
                                    </thead>

                                        <tr>
                                            <td><a href="/admin/order/{{$order->id}}">{{$order->invoice_number}}</a></td>
                                            <td>{{$order->status}}</td>
                                            <td>{{$order->amount == "Free Trial" ? "Free Trial" : "$".$order->amount}}</td>
                                            <td>{{date("F j, Y g:i a", strtotime($order->created_at))}} (EST)</td>
                                            <td>{{$order->source}}</td>
                                            <td>{{$order->shipped ? 'Yes!' : 'Not yet'}}</td>
                                            <td>{{$user->legacy_id != null ? 'Yes' : 'No'}}</td>
                                        </tr>
                                        <tr >

                                </table>
                                @endforeach
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.col-lg-4 (nested) -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
    </div>
@stop