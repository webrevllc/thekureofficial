<div role="tabpanel" class="tab-pane" id="sendcard">
    <h3>Select which cards to resend to the customer</h3>
    <form action="/admin/resendCards" method="post">
        {{csrf_field()}}
        @foreach($user->serials as $serial)
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="serial_id[]" value="{{$serial->id}}"> {{$serial->name}} - {{$serial->serial}}
                </label>
            </div>
        @endforeach
        <button type="submit" class="btn btn-primary">Queue To Be Re-Shipped</button>
    </form>
</div>