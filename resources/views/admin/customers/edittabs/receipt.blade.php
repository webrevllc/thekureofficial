<div role="tabpanel" class="tab-pane active" id="receipt">
    <h4>Select which order to send receipt for:</h4>
    <form action="/admin/send-receipt" method="POST">
        {{csrf_field()}}
        @foreach($user->orders as $order)
            <div class="radio">
                <label>
                    <input type="radio" name="order_id" id="optionsRadios1" value="{{$order->id}}" >
                    <b>{{$order->invoice_number}}</b>
                    <br>
                    Date Placed: {{date('F j, Y', strtotime($order->created_at))}}
                    <br>
                    @if($order->amount == 'Free Trial')
                        Amount: Free Trial
                    @else
                        Amount: ${{number_format($order->amount, 2)}}
                    @endif
                </label>
            </div>
        @endforeach
        <button type="submit" class="btn btn-primary">Send Receipt For Order</button>
    </form>

</div>