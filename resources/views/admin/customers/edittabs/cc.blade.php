<div role="tabpanel" class="tab-pane" id="cc">
    <h4>Enter New Credit Card Information</h4>
    <form class="form-horizontal" method="post" action="/admin/edit_cc/{{$user->id}}">
        {{csrf_field()}}
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">First Name</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" id="inputEmail3" name="fname">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Last Name</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" id="inputEmail3" name="lname">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Address</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" id="inputEmail3" name="add1">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Address 2</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" id="inputEmail3" name="add2">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Country</label>
            <div class="col-sm-3">
                <select class="form-control " tabindex="3" required id="country" name="country" onChange="getState(this.value);">
                    <option value="USA">USA</option>
                    <option value="Canada">Canada</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">City</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" id="inputEmail3" name="city">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">State</label>
            <div class="col-sm-3">
                <select name="state" id="state" class="form-control " required ></select>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Zip</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" id="inputEmail3" name="zip">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Credit Card</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" id="inputEmail3" name="card">
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label">Expiration Year</label>
            <div class="col-sm-3">
                <select name="year" id="year" class="form-control " required>
                    <option value="15">2015</option>
                    <option value="16">2016</option>
                    <option value="17">2017</option>
                    <option value="18">2018</option>
                    <option value="19">2019</option>
                    <option value="20">2020</option>
                    <option value="21">2021</option>
                    <option value="22">2022</option>
                    <option value="23">2023</option>
                    <option value="24">2024</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label">Expiration Month</label>
            <div class="col-sm-3">
                <select name="month" id="month" class="form-control  " required>
                    <option value="">month</option>
                    <option value="01">01 - January</option>
                    <option value="02">02 - February</option>
                    <option value="03">03 - March</option>
                    <option value="04">04 - April</option>
                    <option value="05">05 - May</option>
                    <option value="06">06 - June</option>
                    <option value="07">07 - July</option>
                    <option value="08">08 - August</option>
                    <option value="09">09 - September</option>
                    <option value="10" >10 - October</option>
                    <option value="11" selected>11 - November</option>
                    <option value="12">12 - December</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="cvc" class="col-sm-2 control-label">CVC</label>
            <div class="col-sm-3">
                <input type="password" class="form-control" id="cvc" name="cvc">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-success">Add New Card</button>
            </div>
        </div>
    </form>
</div>

<script>
    $(function() {
        getState("USA");
    });

    function getState(val) {
        console.log(val);
        $.ajax({
            type: "POST",
            url: "/get_state",
            data:'country='+val,
            success: function(data){
                console.log(data);
                $("#state").html(data);
            }
        });
    }
</script>