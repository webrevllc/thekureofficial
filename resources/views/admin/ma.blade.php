@extends('admin.layout')

@section('header')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/dark-hive/jquery-ui.css">
    <style>
        .ui-datepicker-month, .ui-datepicker-year, .ui-datepicker-month option,  .ui-datepicker-year option {
            color: #000;
        }
    </style>
@stop
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Orders Missing an Address</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">

        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Add Address</th>
                </thead>
                @foreach($array as $a)
                    <tr>
                        <td>{{$a->user->name}}</td>
                        <td>{{$a->user->phone}}</td>
                        <td><a class="btn btn-info">Add Address</a></td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="clearfix"></div>
    </div>


@stop