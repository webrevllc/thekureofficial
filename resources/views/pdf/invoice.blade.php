<html>
<head>

</head>
</html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link href="/css/app.css" rel="stylesheet">
<div class="container">
    <div class="row">
        <div class="well col-xs-10 col-sm-10 col-md-6 col-xs-offset-1 col-sm-offset-1 col-md-offset-3">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <address>
                        <strong>{{$order->user->name}}</strong>
                        <br>
                        {{$address->address}}
                        <br>
                        {{$address->city}}, {{$address->state}} {{$address->zip}}
                        <br>
                        <abbr title="Phone">P:</abbr> {{$order->user->phone}}
                    </address>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                    <img src="https://www.thekure.com/img/newkure.png" style="width:100%";/>
                    <p>
                        <em>Date: {{date("F j, Y, g:i a", strtotime($order->created_at))}}</em>
                    </p>
                    <p>
                        <em>Invoice Number #: {{$order->invoice_number}}</em>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="text-center">
                    <h1>Receipt</h1>
                </div>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th class="text-center">Price</th>
                        <th class="text-center">Total</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach(json_decode($order->products) as $product)
                        <tr>
                            <td class="col-md-9"><em>{{$product->name}}</em></td>
                            <td class="col-md-1" style="text-align: center"> {{$product->quantity}} </td>
                            <td class="col-md-1 text-center">${{number_format($product->price/100, 2) }}</td>
                            <td class="col-md-1 text-center">${{number_format(($product->price * $product->quantity)/100, 2) }}</td>
                        </tr>
                        @endforeach
                        {{--<tr>--}}
                            {{--<td>   </td>--}}
                            {{--<td>   </td>--}}
                            {{--<td class="text-right">--}}
                                {{--<p>--}}
                                    {{--<strong>Subtotal: </strong>--}}
                                {{--</p>--}}
                            {{--</td>--}}
                            {{--<td class="text-center">--}}
                                {{--<p>--}}
                                    {{--<strong>$6.94</strong>--}}
                                {{--</p>--}}
                            {{--</td>--}}
                        {{--</tr>--}}
                        <tr>
                            <td>   </td>
                            <td>   </td>
                            <td class="text-right"><h4><strong>Total: </strong></h4></td>
                            <td class="text-center text-danger"><h4><strong>${{number_format($order->amount, 2)}}</strong></h4></td>
                        </tr>
                    </tbody>
                    </table>
                    <button type="button" class="btn btn-success btn-lg btn-block">
                        Thank you for your order!
                    </button>
            </div>
        </div>
    </div>
</div>