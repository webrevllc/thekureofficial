<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sample Invoice</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    {{--<link href="/css/app.css" rel="stylesheet">--}}
    <style>
        .sincerely{
            padding-bottom: 0px;
            margin-bottom: 0px;}
        .pete{
            padding-top: 0px;
            margin-top:0px;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }

        h6{margin:0px;padding:0px;font-size:14px;}
        h5{
            font-size:15px;
            margin-top:5px;
            margin-bottom:5px;
            padding-top:5px;
            padding-bottom:5px;}
        .page-break {
            page-break-after: always;
        }
    </style>
</head>

<body>
<div class="container">
    <img src="data:image/png;base64,'.{{$code}}.'" style="position:absolute;right:140px;top:30px;"/>
    <div class="row">
        <div class="col-md-6 theheader">
            <br>
            <img src="https://www.thekure.com/img/newkure.png" style="width:30%;padding:10px;margin-left:-15px;" >
            <h6>512 Rudder Rd</h6>
            <h6>Fenton, MO 63026</h6>
            <br>
            <br>
            <br>
            <br>
            <br>
            <address>
                <strong>{{ucwords(strtolower($order->user->name))}}</strong>
                <br>
                {{$order->address->address}}
                <br>
                {{$order->address->city}}, {{$order->address->state}} {{$order->address->zip}}
                <br>
            </address>
        </div>
        <div class="col-xs-6 text-right">

        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <h5>Dear {{ucwords(strtolower($order->user->name))}}:</h5>
            <h5>Welcome to The Kure.</h5>
            <h5>We are honored that you have entrusted us with your computer security and look forward to years of keeping you protected.</h5>
            <h5>Up until now the Kure was only available for big business, government, and educational institutions. The Kure is a revolutionary next generation antivirus software that guarantees you will never ever get a virus again. We are so confident you will love it that we put a 30 day free trial offer to make sure you do, or you will not be charged a penny. If you choose to keep the Kure your credit card will be billed at the end of the trial period unless you cancel first.</h5>
            <h5>Unlike some products or services you find out there where the company hides behind their website or endless emails for support, we at Centurion Technologies (home of The Kure) want to hear from you whether you have good feedback or bad. We make it very easy to get ahold of us as you can see from our toll free number readily available at the bottom of this letter and on your membership card as well.</h5>
            <h5>Be it tech support or customer service, we are here for you during our normal respective business hours. Tech Support hours are 7 days per week from 8AM to 6PM EST. Customer Service hours are Monday through Friday 8AM to 6PM.</h5>
            <h5>You will also find links to our support throughout The Kure software application if you should need us. Again, we want you to know that Centurion Technologies, in partnership with McAfee (an Intel company), is most definitely serious and dedicated to your utmost satisfaction.</h5>
            <h5>We thank you once again for your free trial and/or purchase of The Kure and hope you will enjoy it and feel such a peace of mind that you will tell your family and friends about it!</h5>
            <h5 class="sincerely">Sincerely,</h5>
            <img src="https://www.thekure.com/img/pete.png" style="width:10%;padding:10px;margin-left:-15px;" >
            <h5 class="pete">Peter Spezza<br>President, Centurion Technologies</h5>
        </div>


    </div>

    <h5 style="text-align: center;color:#EF3E24;position:absolute;bottom:0;left:30%;">1-855-837-2617 | www.THEKURE.com</h5>
    <div class="page-break"></div>

    <div class="row">
        <div class="col-md-6">
            <img src="https://www.thekure.com/img/newkure.png" style="width:30%;padding:10px;margin-left:-15px;" >
        </div>
        <div class="col-md-6 text-right">
            <address>
                <strong>Customer Receipt</strong>
                <br>
                Transaction Number: <b>{{$order->invoice_number}}</b>
            </address>
        </div>
    </div>
    <br>
    <table style="width:100%" border="1">
        <thead>
        <tr>
            <th>Product</th>
            <th>Quantity</th>
            <th class="text-center">Price</th>
            <th class="text-center">Total</th>
        </tr>
        </thead>
        <tbody>
        @foreach(json_decode($order->products) as $product)
            <tr>
                <td class="col-md-9"><em>{{$product->name}}</em></td>
                <td class="col-md-1" style="text-align: center"> {{$product->quantity}} </td>
                <td class="col-md-1 text-center">${{number_format($product->price/100, 2) }}</td>
                <td class="col-md-1 text-center">${{number_format(($product->price * $product->quantity)/100, 2) }}</td>
            </tr>
        @endforeach
        <tr>
            <td>   </td>
            <td>   </td>
            <td class="text-right"><h4><strong>Total: </strong></h4></td>
            <td class="text-center text-danger"><h4><strong>${{number_format($order->subscription->amount, 2)}}</strong></h4></td>
        </tr>
        </tbody>
    </table>
    <div class="row">
        <h3>Installation Instructions</h3>
        <table border="0">
            <tr>
                <td><h5>1. On your Windows PC press and hold the Windows key and then press the letter "R".</h5></td>
                <td><img src="https://www.thekure.com/img/windowr.png"  width="200px"></td>
            </tr>
            <tr>
                <td><h5>2. In the "Run box type: "www.thekure.com/download" then click "OK".</h5></td>
                <td><img src="https://www.thekure.com/img/run.png" width="200px"></td>
            </tr>
            <tr>
                <td><h5>3. Install The Kure. When prompted enter your Transaction ID and Serial Number.</h5></td>
                <td><img src="https://www.thekure.com/img/install.jpg"  width="200px"></td>
            </tr>
        </table>

    </div>
    <div>
        <h5>By now you have certainly noticed that you received a personalized membership card as opposed to a USB drive. USB drives for software installations are only good for the installation and then basically become trash, not to mention they are ALL manufactured outside of the USA. Also, shipping a USB drive would add about $6.95 to your overall cost for a one time use only!</h5>
        <h5>We are a 100% USA based company and feel very strongly about manufacturing and designing all our products right here in the USA, so we have made a corporate decision to move away from USB shipments and pass along the savings to you while maintaining our 100% USA based manufacturing philosophy.</h5>
        <h5>We certainly hope that you appreciate our efforts to keep jobs in the USA and pass along savings to you along the way. Our goal is that you will love The Kure and our business philosophy so much that you will tell all your friends and family.</h5>
        <h5>Thank you for your support!</h5>

    </div>
</div>
<h5 style="text-align: center;color:#EF3E24;position:absolute;bottom:0;left:30%;">1-855-837-2617 | www.THEKURE.com</h5>
</body>
</html>