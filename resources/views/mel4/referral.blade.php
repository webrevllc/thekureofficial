@extends('mel4.homelayout')

@section('content')

        <div class="" style="padding-bottom: 4em; background:transparent;" >
            <div class="panel panel-info">
                <div class="panel-heading"><h3>Have a referral card? Great! Enter the "transaction number" of the referring order and click "Order"!</h3></div>
                <div class="panel-body">

                    @if(Session::has('success'))
                        <div class="alert alert-success" role="alert">
                            <h4>{{ Session::get('success') }}</h4>
                        </div>
                    @endif
                    @if(Session::has('error'))
                        <div class="alert alert-danger" role="alert">
                            <h4>{{ Session::get('error') }}</h4>
                        </div>
                    @endif
                    <form method="post" action="/referral">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="t">Transaction Number</label>
                            <input type="text" class="form-control" id="t" name="trans" placeholder="Enter The Transaction Number">
                        </div>
                        <button type="submit" class="pull-right btn btn-info tbn-lg">Order</button>
                    </form>
                </div>
            </div>


        </div>

@stop