<!DOCTYPE html>
<html lang="en">

<head>
    {{--<script src="//cdn.optimizely.com/js/3724870335.js"></script>--}}
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="author" content="The Kure">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!-- CSS -->
    <link href="css/app.css" rel="stylesheet"/>
    {{--<link href="css/libs.css" rel="stylesheet"/>--}}
    <link href="css/jquery-ui.css" rel="stylesheet"/>
    {{--<link href="css/styles.css" rel="stylesheet"/>--}}
    <link href="https://bootswatch.com/cerulean/bootstrap.min.css?1" rel="stylesheet"/>

    <!--JS-->
    <script src="https://code.jquery.com/jquery-latest.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <script src="js/jquery-ui.js"></script>
    {{--<script src="js/sweetalert-dev.js"></script>--}}
    {{--<script src="js/scripts.js"></script>--}}
    <title>The Kure!</title>
    <link href="/css/useradmin.css?1" rel="stylesheet">
    <style>
        .fixed{
            position: fixed;
            right:1%;
            bottom:0px;
        }
        body{padding-top:70px;}
    </style>
</head>

<body>
<div class="wrapper">
    @include('mel4.navbar')
    <div class="container">
        @yield('content')
    </div>
    <div class="push"></div>
</div>
@include('mel4.footer')
<a href="/pre-sales">
    <img src="/img/presales.png" class="fixed hidden-xs" style="width:200px;"/>
</a>
{{--<script type="text/javascript" src="https://sealserver.trustwave.com/seal.js?style=invert&code=b6bf09fa3d084ffe8d6550d51af43672"></script>--}}
@include('checkout2.mouseflow')
@include('mel4.tracking')
</body>
</html>
