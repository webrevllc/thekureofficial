
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', '{{env('TRACKING')}}', 'auto');
    ga('send', 'pageview');
</script>
<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(100892374); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/100892374ns.gif" /></p></noscript>
{{--<script type="text/javascript">--}}
    {{--var _mfq = _mfq || [];--}}
    {{--(function() {--}}
        {{--var mf = document.createElement("script");--}}
        {{--mf.type = "text/javascript"; mf.async = true;--}}
        {{--mf.src = "//cdn.mouseflow.com/projects/06afd77f-bf19-4af0-8c8d-ac16654cbeaf.js";--}}
        {{--document.getElementsByTagName("head")[0].appendChild(mf);--}}
    {{--})();--}}
{{--</script>--}}