@extends('mel4.homelayout')

@section('content')
    @include('mel4.navbar')

    <div class="container"  style="margin-top:4em;">
        <div class="col-md-8 col-md-offset-2" >
            <div class="row">
                <h1>Cancel Your Subscription</h1>
                <h3>If at ANY time you wish to cancel your FREE TRIAL subscription, simply fill out the form below.</h3>
                @if(Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        <h4>{{ Session::get('success') }}</h4>
                    </div>
                @endif
                @if(Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        <h4>{{ Session::get('error') }}</h4>
                    </div>
                @endif
            </div>
            <div class="row">
                <form action="/cancel" method="POST" style="margin:30px auto 60px;">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="department">Reason For Cancelling:</label>
                        <select name="department" id="department" class="form-control">
                            <option value="">--select--</option>
                            <option value="1">I changed my mind</option>
                            <option value="2">I couldn't install The Kure</option>
                            <option value="3">It's not what I wanted</option>
                            <option value="4">It didn't work for me</option>
                            <option value="5">Other</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" name="name" placeholder="name" value="{{old('name')}}" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="email" class="form-control" name="email" placeholder="email" value="{{old('email')}}" required>
                    </div>
                    <div class="form-group">
                        <label for="phone">Original Transaction Number:</label>
                        <input type="text" class="form-control" name="trans" placeholder="Transaction #" value="{{old('trans')}}">
                    </div>
                    <button type="submit" class="btn btn-danger">Cancel Your 30-Day Free Trial Now</button>
                </form>
            </div>

        </div>
    </div>
@stop