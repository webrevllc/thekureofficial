@extends('mel4.homelayout')

@section('content')
    <div class="panel panel-primary text-center" style="margin-top:10em;">
        <img src="img/newkure.png" width="50%" class="center-block"/>
        <h1>Need to get in touch with us? </h1>
        <h1>Feel free to chat with one of our live support representatives!</h1>
        <h4 style="text-shadow: 2px 2px 4px #000000; font-size: 8em;">
            <a href="http://www.kureconnect.com/">Click Here!</a>
        </h4>
    </div>
@stop