@extends('mel4.homelayout')

@section('content')
    <div class="" style="padding-bottom: 10em;">
        <h1 class="big-green-text center" style="background:#f2f2f2; padding:10px;">Frequently Asked Questions</h1>
        <div class="row">
            <div class="col-md-12">
                <div id="accordion">
                    <h3>What is The Kure?</h3>
                    <div>
                        <p>
                            Antivirus software just doesn't work anymore. It worked 20 years ago, but the Hackers
                            have just gotten too smart. The KURE is NOT antivirus software. With The Kure, you will
                            never need any antivirus software again. The Kure builds a safe-house, or a vault, on
                            your hard drive away from your operating system and data. Inside that vault you can open
                            up as many viruses as you want, then simply shut the computer off, turn it back on
                            again, the viruses are destroyed and your computer is back in pristine condition.
                        </p>
                        @include('mel4.smiles')
                    </div>

                    <h3>How does the FREE TRIAL work?</h3>

                    <div>
                        <p>
                            Try the Kure for 30 full days from the day you receive it. We won't charge you a penny for the 30 day trial.  At the end of the 30 days if you don't fall in love with the savings, security and peace of mind you get with the Kure, simply send it back, no questions asked and you owe nothing, because we didn't charge you a penny in the first
                            place. We're so sure you'll love it, we'll even cover the postage to return it.

                        </p>
                        @include('mel4.smiles')
                    </div>

                    <h3>How does the 30-day Money Back Guarantee work?</h3>

                    <div>
                        <p>
                            Use The KURE for 30 full days. At the end of the 30 days, if you are not absolutely in love with the savings, the security and the peace of mind you get with The KURE, just send it back, no questions asked, for a 100% refund including shipping and handling. And, we'll even cover the postage to return it!
                        </p>
                        @include('mel4.smiles')
                    </div>

                    <h3>How does the $1000 Guarantee work?</h3>

                    <div>
                        <p>
                            If you ever get a virus while using the KURE as prescribed, and we cannot remove it, we will replace your computer with one of equal or greater value, UP TO $1000!
                        </p>
                        @include('mel4.smiles')
                    </div>

                    <h3>Does the Kure use a lot of memory?</h3>
                    <div>
                        <p>
                            No not at all. The design of the Kure is very efficient. You will notice that very, very little of memory is used at all.
                        </p>
                        @include('mel4.smiles')
                    </div>
                    <h3>Will it slow down my computer?</h3>

                    <div>
                        <p>
                            On the contrary. You will find that your system will be far more efficient then it has been. Most computers are slowed down by undetected Malware. You will fall in love with your computer all over again. No other software does this.
                        </p>
                        @include('mel4.smiles')

                    </div>

                    <h3>Is the software safe to use?</h3>
                    <div>
                        <p>
                            The Kure software has been McAfee certified safe to use and load on your system. Our software has been tested, approved for Government and Military facilities. Over 2 million systems are running our software ever single day. If there was ever a piece of software you could trust,it's The KURE.
                        </p>
                        @include('mel4.smiles')
                    </div>

                    <h3>What happens if I don't like it?</h3>
                    <div>
                        <p>
                            Simply return it, "no questions asked", for a 100% full money back guarantee!
                        </p>
                        @include('mel4.smiles')
                    </div>
                    <h3>What do the reviewers say?</h3>
                    <div>
                        <p>
                            Kevin Harrington from Shark Tank gives it 5 stars!<br><br>
                            PC Magazine's Neil J. Rubenking says...<br>
                            "Picture this: Your computer is unusable because all your files have been encrypted, and the ransomware that did the dirty deed demands 300 euros, or else it will throw away the encryption key. What a nightmare! Wouldn't it be great if your antivirus would let you go back in time, before that nasty ransomware attack? Well, that's precisely what Centurion Technologies's Software does... you can undo any harm caused by malware with a simple reboot.
                            We are the number one providing this technology combined with Identity theft software on TV in America."
                        </p>
                        @include('mel4.smiles')
                    </div>

                    <h3>Is your Software "made in America"?</h3>
                    <div>
                        <p>
                            All our developers and designers are located either in our St. Louis facility or our Tampa facility. All of our tech support people are located in these facilities as well. These are the same techs that have performed live US based support for over a million computers for all the major TV Shopping Channels. We are a "Proud American Company"
                        </p>
                        @include('mel4.smiles')
                    </div>

                    <h3>Will you sell my Email address?</h3>
                    <div>
                        <p>
                            ABSOLUTELY NOT!!! Our business is built upon security for the U.S. Government and Military as well as hospitals and banking. We will NEVER sell or SHARE your email address. The only emails you will receive will be from us , your "partners" in protecting your personal information from viruses and identity theft.
                        </p>
                        @include('mel4.smiles')
                    </div>

                    <h3>How many computers is The Kure good for?</h3>
                    <div>
                        <p>
                            The Kure gives you your very own License. That License is good for one computer for one
                            full year.
                        </p>
                        @include('mel4.smiles')
                    </div>

                    <h3>Can I transfer the KURE if I buy a new computer?</h3>
                    <div>
                        <p>
                            Absolutely! Yes, you can transfer your very own personal license anytime during the year
                            to a new computer and use it for the remainder of the term. Hopefully you'll fall in
                            love with your computer again, because you are using the Kure. Maybe you won't have to
                            buy a new computer. :)
                        </p>
                        @include('mel4.smiles')
                    </div>

                    <h3>What version of Windows does The Kure work with?</h3>
                    <div>
                        <p>
                            Windows Vista, 7, 8, and the all new Windows 10
                        </p>
                        @include('mel4.smiles')
                    </div>
                    {{--<h3>How much are additional USB Flash Drives for The Kure?</h3>--}}

                    {{--<div>--}}
                        {{--<p>--}}
                            {{--Additional USB Flash Drives are ONLY $19.95 per year.--}}
                        {{--</p>--}}
                        {{--@include('mel4.smiles')--}}
                    {{--</div>--}}
                    <h3>Does it work with dial up Internet?</h3>

                    <div>
                        <p>
                            Because of the instability of Dial Up, we cannot guarantee the ease of installation and
                            updating of the Kure and of CyberSafePC. However, you do have a 30 day money back
                            guarantee, which includes the full refund of your shipping and handling. That being the
                            case you certainly are welcome to try it and see if your dial up connection will be
                            adequate. It is certainly worth the effort.
                        </p>
                        @include('mel4.smiles')
                    </div>
                    <h3>Can I purchase additional subscriptions to the Kure?</h3>

                    <div>
                        <p>
                            Yes, you can at the time of purchase, or after you have tried it and then know you love
                            it, you can then come back to <a href="http://thekure.com">www.thekure.com</a> to
                            purchase additional licenses of the Kure for friends, family and employees. They will
                            thank you for it!
                        </p>
                        @include('mel4.smiles')
                    </div>
                    <h3>How long does it take to activate my account?</h3>

                    <div>
                        <p>
                            Good News!! Once you receive The Kure and register it you are activated immediately.
                        </p>
                        @include('mel4.smiles')
                    </div>
                    <h3>Are there any other or hidden charges?</h3>

                    <div>
                        <p>
                            Absolutely not! There is NEVER a charge for update, upgrades or new versions. Even when
                            new versions of Windows come out. And yes, The Kure is already optimized to work with
                            the new Windows 10.
                        </p>
                        @include('mel4.smiles')
                    </div>
                    <h3>What do I do if I get a virus?</h3>

                    <div>
                        <p>
                            As long as you use the Kure as prescribed, you can never get a virus again with Kure!
                            Period. But we will go so far to say, that if you somehow ever did, (it's not going to
                            happen), we will remove it for free!.......But you're never going to get a virus. Can't
                            happen. Never! Get the picture? ?
                        </p>
                        @include('mel4.smiles')
                    </div>
                    <h3>Do you offer Customer Support?</h3>

                    <div>
                        <p>
                            We currently offer unlimited, FREE, customer support from 8AM-5PM, 7 days a week. That is for the as long as your license is active. Not a penny for our U.S. based customer service.
                        </p>
                        @include('mel4.smiles')
                    </div>
                    <h3>Do you take checks or money orders?</h3>

                    <div>
                        <p>
                            Yes, You can send a check or money order to:<br>
                            Centurion Technologies c/o Computer Centers<br>
                            5701 E Hillsborough Ave, Suite 2459<br>
                            Tampa, Fl 33610
                        </p>
                        @include('mel4.smiles')
                    </div>
                    <h3>Is the shipping and handling refundable?</h3>

                    <div>
                        <p>
                            Absolutely! We have a no questions asked, 30 day money back guarantee, all shipping and handling is refundable.
                        </p>
                        @include('mel4.smiles')
                    </div>

                    <h3>Are there additional charges to ship to Alaska, Hawaii or Canada?</h3>

                    <div>
                        <p>
                            Most companies charge additional shipping and handling fees to Alaska, Hawaii and Canada. Not so with The KURE. There are no additional charges to our friends in Alaska, Hawaii and Canada.  Period.
                            How lucky you are to live in those beautiful places. :)
                        </p>
                        @include('mel4.smiles')
                    </div>

                    <h3>Do I need to back up my data?</h3>
                    <div>
                        <p>
                            Yes! Even though The Kure guarantees that you will never, ever, ever get a virus again, there are many other ways your data can be corrupted or lost, besides from a virus. For example, you could have a hard drive failure. You could have an overly full hard drive that starts to lose data. You could have a lightning strike or a power surge. All of these situations can cause you to lose some or all of your data. So the answer is definitely yes! Everyone should back up there important data, preferably on a removable media or the cloud. </p>
                        @include('mel4.smiles')
                    </div>
                    <h3>Does this cover a MAC?</h3>

                    <div>
                        <p>
                            Sorry, not at this time.
                        </p>
                        @include('mel4.smiles')
                    </div>
                    <h3>Still Have a question?</h3>

                    <div>
                        <p>
                            If there is still something you need to know about the KURE, that we haven't covered here.  Just send us an email at, <a style="text-decoration:underline;" href="mailto:questions@thekure.com">Questions@TheKure.com</a>. And we'll get right back to you.
                        </p>
                        @include('mel4.smiles')
                    </div>

                    <h3>I NEED MORE INFO ABOUT THE KURE</h3>

                    <div>
                        <p>
                            No problem, simply fill out <a href="/contact" style="color:orange;text-decoration: underline;">this form</a> and we will get back to you ASAP!
                        </p>
                        @include('mel4.smiles')
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $("#accordion").accordion();
        });
    </script>
@stop