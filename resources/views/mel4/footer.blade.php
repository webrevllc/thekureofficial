<div class="footer">
    <div class="container-fluid myfooter" style="height:100%">
        <div class="container text-center">
            <div class="col-xs-12">
                <h4>&copy; {{date('Y')}} The Kure. All rights reserved.</h4>
                <a href="/terms">Terms & Conditions</a> <span style="color:#fff">|</span> <a href="/privacy">Privacy Policy</a><br><br>
                <script type="text/javascript" src="https://sealserver.trustwave.com/seal.js?style=invert&code=b6bf09fa3d084ffe8d6550d51af43672"></script>
            </div>
        </div>
    </div>
</div>