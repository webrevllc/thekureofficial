@extends('mel4.homelayout')

@section('content')
    <div class="navbar navbar-default navbar-fixed-top ">
        <a id="purchase_link" href="/purchase">
            <img id="purchase" src="img/headerbannerkure6.png?3" class="hidden-xs" style="width:100%;"/>
            <img src="/img/newkure.png" width="80%" class="center-block visible-xs"/>
            <h3 class="text-center visible-xs">Buy Now!</h3>
        </a>
    </div>

<div class="tech-wrapper">
    <div style="margin-top:170px;" class="hidden-xs"></div>
    <div class="container" >
        <div class="row">
            <div class="col-xs-12 " style="text-align: center;">
                <h1>NEVER EVER, EVER GET A VIRUS AGAIN! GUARANTEED!!</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-6" style="margin-top: 4em;">
                <iframe style="margin-top: 6px; " width="100%" height="225" class="fancy-border"
                        src="https://www.youtube.com/embed/D30jm6-ys5Y?autoplay=" frameborder="0"
                        allowfullscreen></iframe>
            </div>
            <div class="col-xs-12 col-md-6">
                <img width="100%" src="/img/thekureproduct.png"/>
            </div>
        </div>
        <div class="row" style="padding:10px;">
            <div class="col-xs-12 col-md-5">
                <img src="img/windows10.png" class="fancy-border" width="100%"/>
            </div>
            <div class="col-xs-12 col-md-7 fancy-well">
                <div class="text-center">
                    <h1>$59.95</h1>
                    <h6>annual value</h6>
                    <h4>INCLUDES FREE</h4>
                    <h1>CYBERSAFE PC</h1>
                    <h2>IDENTITY THEFT</h2>
                    <h4>SOFTWARE</h4>
                </div>
            </div>
        </div>

        <div class="row" style="margin-top: 2em; text-align: center;padding:10px;">
            <div class="col-xs-12 col-md-5 md-black-text fancy-border">
                <h2>Over Two Million PCs protected daily by the commercial version of The Kure.</h2>
                <br/>
                <h4 class="">JUST SOME OF OUR COMMERCIAL CUSTOMERS:</h4>

                <h4 class="">COLUMBIA UNIVERSITY</h4>

                <h4 class="">PENN STATE UNIVERSITY</h4>

                <h4 class="">FLORIDA STATE UNIVERSITY</h4>

                <h4 class="">U.S. OLYMPIC COMMITTEE</h4>

                <h4 class="">HURLBURT AIR FORCE BASE</h4>

                <h4 class="">CATHOLIC CHARITIES</h4>

                <h4 class="">AIR FORCE ACADEMY</h4>

                <h4 class="">U.S. NAVAL HOSPITAL</h4>
            </div>
            <div class="col-xs-12 col-md-7">
                <img src="img/vault.jpg" class="fancy-border" width="100%" style="margin-top: 3em;"/>
            </div>
        </div>

        <div class="row" style="margin-top: 2em;">
                <div class="col-xs-12 col-md-4" style="text-align: center;">
                    <img src="img/kevin.png" class="fancy-border" width="100%" style="margin-top: 3em; text-align: center"/>
                    <a href="/purchase" style="text-align: center;">
                        <button type="button" style="margin-left: 0.35em; margin-top: 0.8em; font-size: 3em;"
                                class="fancy-button" onclick="window.location.href = '/purchase'">ORDER NOW
                        </button>
                        <div class="text-center">
                            <small>100% SECURE 256-BIT SSL</small>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-md-8 fancy-border table-responsive" >
                    <table class="table  table-bordered table-jt" style="text-align: center; border-radius: 10px">
                        <tr>
                            <th>VIRUS TYPES</th>
                            <th style="color: red;text-align: center; text-shadow: 2px 2px 4px #000000;">Traditional
                                Antivirus EFFECTIVENESS
                            </th>
                            <th style="color: green;text-align: center; text-shadow: 2px 2px 4px #000000;">The Kure
                                EFFECTIVENESS
                            </th>
                        </tr>
                        <tr>
                            <td class="virus-type-cell">WORMS</td>
                            <td class="antivirus-table-cell">25%</td>
                            <td class="kure-table-cell">100%</td>
                        </tr>
                        <tr>
                            <td class="virus-type-cell">TROJAN HORSE</td>
                            <td class="antivirus-table-cell">25%</td>
                            <td class="kure-table-cell">100%</td>
                        </tr>
                        <tr>
                            <td class="virus-type-cell">SPYWARE</td>
                            <td class="antivirus-table-cell">25%</td>
                            <td class="kure-table-cell">100%</td>
                        </tr>
                        <tr>
                            <td class="virus-type-cell">ADWARE</td>
                            <td class="antivirus-table-cell">25%</td>
                            <td class="kure-table-cell">100%</td>
                        </tr>
                        <tr>
                            <td class="virus-type-cell">MALWARE</td>
                            <td class="antivirus-table-cell">25%</td>
                            <td class="kure-table-cell">100%</td>
                        </tr>
                        <tr>
                            <td class="virus-type-cell">RANSOMWARE</td>
                            <td class="antivirus-table-cell">25%</td>
                            <td class="kure-table-cell">100%</td>
                        </tr>
                        <tr>
                            <td class="virus-type-cell">AVERAGE ON ALL VIRUSES</td>
                            <td class="antivirus-table-cell" style="font-size: 2em;">25%</td>
                            <td class="kure-table-cell" style="font-size: 2em;">100%</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row fancy-well top-2" style="border: 10px solid #7d2e12; border-radius: 15px;">
                <div class="col-xs-12 text-center">
                    <h2>It's as easy to use as 1,2,3</h2>

                    <h2>1. Plug it in</h2>

                    <h2>2. Install the software</h2>

                    <h2>3. Never ever, ever get a Virus again!</h2>

                    <h2>LIMITED TIME OFFER with FREE SHIPPING AND HANDLING</h2>
                </div>
            </div>

            <div class="row top-2" style="padding: 10px;">
                <div class="col-xs-12 col-md-4 fancy-border">
                    <img src="img/person3.png" width="80%"/>
                    <p class="sm-black-text">When you install The Kure, the first thing it does is check
                        your system for viruses, spyware, etc. It will then clean your drives to ensure that The Kure is
                        installed on a perfectly clean system, so that every time you turn your system off and on again,
                        the system will return to a perfectly clean and pristine condition. No other software does that!</p>
                </div>
                <div class="col-xs-12 col-md-4  fancy-border">
                    <img src="img/person2.png" width="80%"/>
                    <p class="sm-black-text">The Kure protects your most important files on your PC, 24 hours a day, 7 days
                        a
                        week,
                        365 days a year. All your files...<br/>Pictures, Videos, Music, tax returns, stock portfolios, back
                        accounts,
                        credit card accounts, usernames and passwords, social security numbers, e-mails, receipts...<br/>
                        NO MORE VIRUSES, EVER!!!</p>
                </div>
                <div class="col-xs-12 col-md-4 fancy-border" style="text-align: center">
                    <img src="img/nicelady.PNG" width="80%"/>
                    <p class="sm-black-text">
                        5 Star Rated <img src="img/stars.PNG"/>
                    </p>

                    <p class="sm-black-text">
                        Patent Pending Technology
                    </p>

                    <p class="sm-black-text">
                        Used by U.S. Military
                    </p>

                    <p class="sm-black-text">
                        The ONLY safe way to surf the web... the ONLY way!!!
                    </p>

                    <p class="sm-black-text">
                        Cannot be defeated by new viruses!
                    </p>

                    <p class="sm-black-text">
                        Two million satisfied users
                    </p>

                    <p class="sm-black-text">
                        20 year old company serving Banking, Hospitals, and Government
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row" style="text-align: center; margin-top: 2em;">
            <div class="col-xs-12">
                <p class="md-red-text">SOMEONE GETS THEIR IDENTITY STOLEN
                    EVERY 2 SECONDS
                    <BR/>IDENTITY THEFT IS THE LARGEST PROPERTY CRIME IN THE U.S.</p>

                <p class="md-black-text">Hackers can steal your keystrokes as you type on your
                    PC. They can steal your:</p>
            </div>
        </div>

        <div class="row" style="text-align: center; margin-top: 2em;">
            <div class="col-xs-12 col-md-6">
                <ul style="text-align: left;" class="md-black-text">
                    <li>Usernames</li>
                    <li>Passwords</li>
                    <li>Account numbers</li>
                    <li>Credit Card information</li>
                    <li>Social Security numbers, and more!</li>
                </ul>
            </div>
            <div class="col-xs-12 col-md-6">
                <img src="img/identitytheft.PNG" width="100%"/>
            </div>
        </div>

        <div class="row" style="margin-top: 4em; text-align: center;">
            <div class="col-xs-12 col-md-6">
                <p class="md-black-text" style="text-shadow: 1px 1px 20px #000000;">They can spy on your husband, wife, and
                    children through the webcam
                    on your computer and post videos of them on the web.<br/><br/>They can listen to your most private
                    conversations
                    through the microphone on your PC.</p>
            </div>
            <div class="col-xs-12 col-md-6">
                <img src="img/dressing.PNG" width="70%"/>
            </div>
        </div>

        <div class="row" style="margin-top: 4em; text-align: center;">
            <div class="col-xs-12">
                <p class="md-black-text">Identity theft is the number one property crime in America</p>
                <img src="img/cyber2.png" width="60%" class="center-block"/>
                <p class="md-black-text" style="margin-top: 1em;">CyberSafePC protects your PC from
                    hackers attempting to invade
                    your privacy, steal your money, and ruin your credit. It will even alert you by text or email that
                    an
                    attempt on your system was blocked!</p>
            </div>
        </div>

        <div class="row text-center">
            <div class="col-xs-12 text-center">
                <p class="big-blue-text">A $19.95/yr VALUE ABSOLUTELY FREE WITH THE KURE</p>
            </div>
            <div class="col-xs-12 col-md-6 col-md-offset-3">
                <img src="img/cybersafe.jpg" width="100%"/>
            </div>
        </div>
        <img src="/img/crossman.png" width="100%" class="top-2"/>
        <div class="row fancy-well" style="margin-top: 4em; text-align: center;border: 10px solid #7d2e12; border-radius: 15px;">
            <div class="col-xs-12">
                <p class="big-black-text">STOP THROWING CASH IN THE TRASH</p>

                <p class="md-black-text">Why pay $50/yr for ANTIVIRUS that works only 25% of the time?</p>

                <p class="md-black-text">It's a Dinosaur, a Rotary Dial Phone. It's like having a security guard working
                    for
                    you only 25% of the time.</p>
            </div>
        </div>


        <div class="row" style="margin-top:4em; text-align: center">
            <div class="col-xs-12 col-md-3 " >
                <img src="img/dino.png" width="100%"/>
            </div>
            <div class="col-xs-12 col-md-4 " >
                <img src="img/commercial.PNG" width="100%" />
            </div>
            <div class="col-xs-12 col-md-5 " >
                <img src="img/snooze.png" width="100%" />
            </div>
        </div>
        <div class="row " style="margin-top: 4em; text-align: center;">
            <div class="col-xs-12 col-md-offset-1 col-md-2">
                <img src="img/clients/murphy.png" width="100%" class="fancy-border"/>
                <br/><Br/>

                <p>MURPHY NORMAN</p>

                <p>&quot;I had Antivirus software and it was really frustrating me. You need
                    to fire your Antivirus company and hire THE KURE!&quot;</p>
            </div>
            <div class="col-xs-12 col-md-2">
                <img src="img/clients/michael.jpg" width="100%" class="fancy-border"/>
                <br/><Br/>

                <p>MICHAEL VANDIVER</p>

                <p>&quot;I got hit by Ransomware and lost everything on my pc. Now I have both the KURE
                    and CYBERSAFEPC. They are both awesome. They really do what they say they do.
                    You have to protect yourself.&quot;</p>
            </div>
            <div class="col-xs-12 col-md-2">
                <img src="img/clients/porsche.jpg" width="100%" class="fancy-border"/>
                <br/><Br/>

                <p>PORSCHE PARCHER</p>

                <p>&quot;I was skeptical at first about The KURE. Then I heard that it was being used
                    in Universities and Government facilities, so I gave it a try. It's an awesome program!&quot;</p>
            </div>
            <div class="col-xs-12 col-md-2">
                <img src="img/clients/keith.jpg" width="100%" class="fancy-border"/>
                <br/><Br/>

                <p>KEITH MURPHY</p>

                <p>&quot;I work as an expert in the Internet security space. But we were getting viruses on our home
                    network.
                    I've gotten rid of all my Antivirus software and all we now use is THE KURE&quot;</p>
            </div>
            <div class="col-xs-12 col-md-2">
                <img src="img/clients/maria.jpg" width="100%" class="fancy-border"/>
                <br/><Br/>

                <p>MARIA SARDINAS</p>

                <p>&quot;It doesn't matter what Antivirus we used, we were always getting one thing or another.
                    But since I installed THE KURE, we've had nothing. Now we are protected.&quot;</p>
            </div>
        </div>



        <div class="row" style="margin-top: 4em; text-align: center;">
            <div class="col-xs-4">
                <img src="img/freesh2.png" width="100%"/>
            </div>
            <div class="col-xs-4">
                <img src="img/novirus2.png" width="100%"/>
            </div>
            <div class="col-xs-4">
                <img src="img/moneyback2.png" width="100%"/>
            </div>
        </div>

        <div class="row" style="margin-top: 4em; text-align: center;">
            <div class="col-xs-12">
                <p class="big-green-text">SAVE $40/yr NOW! ONLY $19.95/yr</p>

                <p class="md-green-text" style="text-shadow: 2px 2px 2px #ccc;">It takes seconds to order,
                    it could take 10 years to fix your credit!</p>
                <a href="purchase"><p class="md-green-text" style="text-shadow: 2px 2px 2px #ccc;">Order now for Free
                        Shipping!</p></a>
            </div>
        </div>
    </div>


    {{--<div class="navbar navbar-default visible-lg visible-md" style="background-color: #ffff00">--}}
        {{--<div class="container">--}}
            {{--<div class="row" style="text-align: center; padding-top: 4em; background-color: #ffff00">--}}
                {{--<div class="col-xs-2 col-xs-offset-1">--}}
                    {{--<a href="/about" style="font-size: 1.9em;">ABOUT US</a>--}}
                {{--</div>--}}
                {{--<div class="col-xs-2">--}}
                    {{--<a href="/faq" style="font-size: 1.9em;">FAQ</a>--}}
                {{--</div>--}}
                {{--<div class="col-xs-2">--}}
                    {{--<a href="/support" style="font-size: 1.9em;">SUPPORT</a>--}}
                {{--</div>--}}
                {{--<div class="col-xs-2">--}}
                    {{--<a href="/contact" style="font-size: 1.9em;">CONTACT</a>--}}
                {{--</div>--}}
                {{--<div class="col-xs-2" style="margin-top: -2em;">--}}
                    {{----}}
                    {{--<p class="sm-green-text" style="font-size: 0.8em;">A PROUD AMERICAN COMPANY</p>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="container-fluid" style="background:#ffff00;">
        <div class="container" style="padding:30px;background: transparent">
            <div class="col-xs-2 text-center" >
                <a href="/about" style="background:#000;padding:10px;font-size: 1.7em; color:#fff;">ABOUT</a>
            </div>
            <div class="col-xs-2 text-center" >
                <a href="/faq" style="background:#000;padding:10px;font-size: 1.7em;color:#fff;">FAQ</a>
            </div>
            <div class="col-xs-2 text-center">
                <a href="/support" style="background:#000;padding:10px;font-size: 1.7em;color:#fff;">SUPPORT</a>
            </div>
            <div class="col-xs-2 text-center" >
                <a href="/contact" style="background:#000;padding:10px;font-size: 1.7em;color:#fff;">CONTACT</a>
            </div>
            <div class="col-xs-4 text-center" >
                <img src="/img/flag1.png" style="width:30%" class="center-block"/>
                <p class="text-center">A Proud American Company</p>
            </div>

            {{--<div class="col-xs 12 col-md-4">--}}
                {{--<ul class="bottom-nav">--}}
                    {{--<li><a href="/about" style="font-size: 1.7em;">ABOUT</a></li>--}}
                    {{--<li><a href="/faq" style="font-size: 1.7em;">FAQ</a></li>--}}
                    {{--<li><a href="/support" style="font-size: 1.7em;">SUPPORT</a></li>--}}
                    {{--<li><a href="/contact" style="font-size: 1.7em;">CONTACT</a></li>--}}
                {{--</ul>--}}
            {{--</div>--}}
            {{--<div class="col-xs 12 col-md-4">--}}
                {{--<ul class="bottom-nav">--}}
                    {{--<li><a href="/terms" style="font-size: 1.7em;">Terms</a></li>--}}
                    {{--<li><a href="/privacy" style="font-size: 1.7em;">Privacy Policy</a></li>--}}
                {{--</ul>--}}
            {{--</div>--}}
            {{--<div class="col-xs 12 col-md-4">--}}
                {{--<ul class="bottom-nav">--}}
                    {{--<img src="/img/flag1.png" style="width:100%" class="center-block"/>--}}
                    {{--<h3 class="text-center">Proudly Made In America</h3>--}}
                {{--</ul>--}}
            {{--</div>--}}
        </div>
    </div>

@stop
