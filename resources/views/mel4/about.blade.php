@extends('mel4.homelayout')

@section('content')
    <div class="peoplebackground" style="margin-top: -20px;">
        <img src="img/aboutus3.jpg" width="100%"/>
    </div>
    <div >
        <div class="" style="padding-bottom: 4em; background:transparent;" >
            <div class="col-xs-12 " style="background:#f2f2f2;">
                <img src="/img/centurionlogo.png" class="center-block hidden-xs hidden-sm" style="width:30%;"/>
                <img src="/img/centurionlogo.png" class="center-block visible-sm" style="width:50%;"/>
                <img src="/img/centurionlogo.png" class="center-block visible-xs" style="width:75%;"/>

                <div class="panel panel-info">
                    <div class="panel-heading"><h3>The Next Generation of Computer Protection is Here</h3></div>
                    <div class="panel-body">
                        <h3>Centurion Technologies is the leading provider of Reboot/Restore technology over the past 20 years in the enterprise/commercial technology space. We are currently
                            protecting over 2 million computers in the most sensitive industries, where anti-virus and malware  protection are critical: hospitals, universities, medical facilities and
                            banking institutions. These are industries where cyber-security is critical. Our software solutions have been tested, approved and used in major government and
                            Military facilities as well, such as: Hurlburt Air Force Base, The Air Force Academy and the U.S. Naval Hospital. We believe we are the single most "trusted"
                            virus protection software company of it's kind. We have offices in both Missouri and Florida.</h3>
                        <h3>We are responsible for performing "technical support" for over one million computers, including those sold by all the major shopping channels. We are the company that you can "trust" with your most
                            important personal information and computer records. Our guarantee is simple, if you use the Kure as prescribed...you will never get a virus again! Our software
                            has been certified safe by McAfee<sup>&reg;</sup> and as mentioned before has been scrutinized and tested in every aspect by both U.S. Government and military agencies. TWO
                            MILLION users can't be wrong , our patent pending technology really works! All of our technicians and developers are based here in the United States. We are a product
                            that is "proudly" creating jobs in this country and is MADE IN AMERICA!</h3>
                    </div>
                </div>




                <div class="panel panel-info">
                    <div class="panel-heading"><h3 style="color:#fff;">Meet The CEO</h3></div>
                    <div class="panel-body">
                        <div class="pull-left">
                            <img src="/img/ceo.png" style="margin-right:10px;"/>
                        </div>
                        <h4><b>Peter Spezza is the Chief Executive Officer of Centurion Technologies LLC, A Missouri based company.</b></h4>
                        <h4>As CEO, Peter leads an Executive Leadership Team focused on Centurion's vision to change the way the world protects the household PC from the growing threats of viruses and cyber security threats. Under his leadership, Centurion is bringing innovative technology only available to big business up until now that provides "the next generation anti-virus".</h4>
                        <h4>He is responsible for the revolutionary business model that has become the new standard for all the shopping channels such as QVC and HSN by providing technical support bundles for millions of computers sold through their network with over 95% customer satisfaction.  Peter led the company's Sales and Partner Organizations, and helped drive and execute many of the company's strategy shifts into the home PC market with customized software making it available to the home PC user for the first time ever changing the entire Anti Virus industry as a whole.</h4>
                        <h4>Peter has more than 25 years of leadership experience as an entrepreneur and executive in several successful companies and ventures in the computer industry. He is committed to both ultimate customer satisfaction and also making sure all products and services are manufactured and developed right here in the U.S.A. Peter's goal is to keep jobs and technology innovations in America at all costs.</h4>
                        <h4>With experience, and tenacity as well as a vision to revolutionize the PC protection industry, Peter is dedicated to bringing the best of the best in technology to the home PC user in the most affordable manner to meet everyone's budget.</h4>
                        <h4>He stands behind his products and services and is driven by his vision.</h4>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading"><h3 style="color:#fff;">We are here to serve you across the United States</h3></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 col-md-7">
                                <h2 class="text-center" style="margin-top:100px;">Located in beautiful Tampa, Florida</h2>
                                <h4 class="text-center">
                                    Corporate Headquarters<br>
                                    324 N Dale Mabry Hwy Ste. 101<br>
                                    Tampa, FL 33609<br>
                                </h4>
                            </div>
                            <div class="col-xs-12 col-md-5">
                                <img src="/img/tampa.png" class="center-block" style="width:100%;"/>
                            </div>
                        </div>
                        <br>
                        {{--<div class="row">--}}

                            {{--<div class="col-xs-12 col-md-5">--}}
                                {{--<img src="/img/tampa.png" class="center-block" style="width:100%;"/>--}}
                            {{--</div>--}}
                            {{--<div class="col-xs-12 col-md-7">--}}
                                {{--<h2 class="text-center" style="margin-top:100px;">And our East Coast Corporate and Support Center--}}
                                    {{--located in Sunny Tampa, Florida--}}
                                {{--</h2>--}}
                                {{--<h4 class="text-center">--}}
                                    {{--5701 E Hillsborough Ave<br>--}}
                                    {{--Tampa, FL 33610<br>--}}
                                {{--</h4>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>

            {{--<div class="row" style="margin-top: 4em;">--}}
                {{--<div class="col-md-6">--}}
                    {{--<h1 class="center"--}}
                        {{--style="color:#fff;margin-top:0;background-color: rgba(0,0,0,0.8); padding: 1em; border: 1px solid white;">--}}
                        {{--We are a 20 year old company performing technical support and protecting over 2 million computers.--}}
                    {{--</h1>--}}
                {{--</div>--}}
                {{--<div class="col-md-6">--}}
                    {{--<img src="img/netpark.jpg" width="100%" style="padding-bottom: 4em;"/>--}}
                {{--</div>--}}
            {{--</div>--}}

        </div>
    </div>

@stop