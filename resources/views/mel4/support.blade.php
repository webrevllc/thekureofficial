@extends('mel4.homelayout')

@section('content')
    <style>
        .fixed{display:none;}
    </style>
    <div >
        <div  style="background:#f2f2f2; padding:15px;">
            <h1 class="text-center">TECHNICAL SUPPORT AND CUSTOMER SERVICE IS OUR #1 PRIORITY!</h1>
            <h4 class="text-center">We have over 17 years of experience in providing "Platinum Level" support to millions of customers and organizations like the US Olympic Committee, The US Air Force Academy, QVC, HSN and many, many more.
                Our "world class" Technical Support is available to you 7 days a week between the hours of
                8AM and 6PM EST or email us 24 hours a day at <a href="mailto:support@thekure.com">Support@TheKure.com</a> provided by our staff of live US based technicians.
            </h4>

            <h4 class="text-center">Serving your every need is our one and only mission. We have a history of doing it better, so we can be there for you whenever you need us.

                Ask any question. No problem is too big or too small. And best of all it's always FREE!
            </h4>

            @if(Session::has('success'))
                <div class="alert alert-success" role="alert">
                    <h4>{{ Session::get('success') }}</h4>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger" role="alert">
                    <h4>{{ Session::get('error') }}</h4>
                </div>
            @endif
            <form action="/inquiry" method="post" style="margin:30px;">
                {{csrf_field()}}
                <input type="hidden" name="department" value="support"/>
                {{--<div class="form-group">--}}
                    {{--<label for="department">Department:</label>--}}
                    {{--<select name="department" id="department" class="form-control">--}}
                        {{--<option value="questions">Pre-sales Questions</option>--}}
                        {{--<option value="support">Technical Support</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name" placeholder="name" required>
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" name="email" placeholder="email" required>
                </div>
                <div class="form-group">
                    <label for="phone">Phone Number:</label>
                    <input type="text" class="form-control" name="phone" placeholder="phone">
                </div>
                <div class="form-group">
                    <label for="message">Describe your issue:</label>
                    <textarea name="message" id="message" class="form-control" rows="10" required></textarea>
                </div>
                <button type="submit" class="btn btn-info pull-right">Connect</button>
                <div class="clearfix"></div>
            </form>
            <div class="alert alert-info">
                <a href="/pre-sales">
                    <h2 class="text-center" style="text-decoration: underline;">Pre-Order Question? Click HERE!</h2>
                </a>
            </div>
        </div>
    </div>
    <style>
        .helper{display:none;}
    </style>
    {{--<script type="text/javascript">--}}
        {{--window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=--}}
        {{--d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.--}}
        {{--_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");--}}
        {{--$.src="//v2.zopim.com/?2SxrEpGYJ9o3vUybODKB9MTOzMfM9g7I";z.t=+new Date;$.--}}
        {{--type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");--}}
    {{--</script>--}}
@stop