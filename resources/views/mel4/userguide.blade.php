<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>The Kure User Guide</title>

    <meta name="description" content="User Guide for The Kure">
    <meta name="author" content="The Kure">

    <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/userguidestyle.css" rel="stylesheet">

</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h1 class="red text-center">The Kure User Guide</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3" >
            <div class="row" style="position: fixed;">
                <div class="col-md-12">
                    <h3 class="red text-center">Table of Contents</h3>
                    <ol>
                        <li><a href="#install">Installing The Kure</a></li>
                        <li><a href="#whatis">What is The Kure?</a></li>
                        <li><a href="#restart">Why do I have to restart<br/> so many times?</a></li>
                        <li><a href="#setup">What's the difference<br/>between Express Setup<br/>and Advanced Setup?</a></li>
                        <li><a href="#software">How do I install new<br/>software on my computer<br/>with The Kure?</a></li>
                        <li><a href="#disable">How do I enable or disable<br/>The Kure?</a></li>
                        <li><a href="#detect">What happens when<br/>The Kure detects software<br/>being installed?</a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="col-sm-9">
            <div class="row">
                <div class="col-md-12" id="install">
                    <h2 class="red">Installing The Kure</h2>
                    <p>
                        To install The Kure on your computer we download the installation kit.  This is the software that will install and configure the software for you.
                        By the end of a successful installation, your computer will be protected by The Kure.
                        To download, press the Windows Key + R simultaneously <b>on your keyboard.</b> In the Run box type <b>www.thekure.com/download.</b>
                        Press Ok - This will take you to The Kure download page and the installation will start automatically.
                        Once finished, open the file by pressing run. (If you are using Firefox, Save the file) Follow Prompts.
                    </p>
                    <img style="display: block;margin: 0 auto;" src="img/install.png"/>
                    <p style="margin-top: 1em;">Once you run the installation kit, The Kure application will place files on your system and prompts will assist you through the registration process and configuration.
                        During the process it will be necessary to restart your computer 2 times.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" id="whatis">
                    <h2 class="red">What is The Kure?</h2>
                    <p>
                        The Kure is an application that protects your hard drive from unwanted changes such as viruses or ransomware.
                        In the event that something bad happens to your computer, simply restart and the bad is wiped away.
                        To better explain, let me provide you with an analogy. Picture your hard drive inside a safe or a vault.
                        The changes made to your hard drive revert to the state your system was in when you installed The Kure.
                        Well, you may say, this is great, but what about the changes I want to keep on my system, such as my documents, pictures and bank statements.
                        Great question and we have you covered! That is where Saved Folders come into play.
                    </p>
                    <img style="display: block;margin: 0 auto;" src="img/savedfolders.png"/>
                    <p>
                        Saved Folders are ordinary folders on your system that saves your changes through a restart.
                        When The Kure is installed we automatically add the common folders you might use to the list of Saved Folders but you are free to add and modify
                        this list as you feel necessary to suit your system.
                        Just remember- only files that are in Saved Folders will continue to be saved after you restart.
                        All others will disappear like they never happened. Ok, you say, but what about other changes such as installing software, updates and system wide things?
                        Do I have to make all of them Saved Folders?  How would I know where those folders are? There are times when you need to affect
                        your system and during those times you would simply disable the protection of The Kure, do your thing (such as install software,
                        updates or other activities that require changes to areas outside of your Saved Folders) and then turn it back on.
                        There is just one important step you need to remember... The state of The Kure protection only switches when you restart.
                        You would disable protection and then <b>restart your computer</b> to have your system available for updates, software installations and system wide
                        changes.  You can tell that The Kure is disabled by the red Kure logo in the system tray.
                    </p>
                    <img src="img/kurelogooff.png"  height="64" width="64"/>
                    <p>
                        When you are done, you Enable The Kure and <b>restart your computer</b>.
                        You will not be protected again until you restart your computer.
                        The Kure logo in the system tray will then be green to show you are protected.
                    </p>
                    <img src="img/kurelogoon.png" height="64" width="64"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" id="restart">
                    <h2 class="red">Why do I have to restart so many times?</h2>
                    <p>
                        During the installation process it will be necessary to restart your computer 2 times.
                        The first time is to install the specialized Kure driver which will ultimately protect your system.
                        The second restart is necessary to enable the protection on your system.
                    </p>
                    <img src="img/restart.png"/>
                    <p>
                        Restarting is always necessary when you enable or disable The Kure protection. Restarting is also
                        necessary to restore your computer from unwanted changes, such as viruses or malware.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" id="setup">
                    <h2 class="red">What is the difference between Express Setup and Advanced Setup?</h2>
                    <p>
                        During setup, you will be asked to create a password and provide a valid email address. By default, Express Setup
                        configures your Saved Folders using Windows defaults such as My Documents, My Pictures and My Music. In Advanced Setup,
                        Saved Folders may be added or removed. You will always have the ability to change these folders after setup has completed.
                    </p>
                    <img src="img/password.png"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" id="software">
                    <h2 class="red">How do I install new software on my computer with The Kure?</h2>
                    <p>
                        Simply disable protection, restart your computer, and install your software. When installation of your new software is complete,
                        re-enable protection and restart your computer so that your changes will be permanent and your system will remain protected.
                    </p>
                    <img src="img/disable.png"/>
                    <h3 class="red">Remember!</h3>
                    <p>
                        After <span style="color: green;">enabling</span> or <span style="color: red;">disabling</span>
                        The Kure, you must restart your computer for your changes to take effect.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" id="disable">
                    <h2 class="red">How do I enable or disable The Kure?</h2>
                    <ol>
                        <li>
                            Double click The Kure icon on your Desktop, and then click on the image that says The Kure
                            <br/>
                            <img src="img/desktop.png" width="500" style="margin: 1em 0 1em 0;"/>
                            <br/>
                        </li>
                        <li>
                            Press the green or red button in the center of the window to enable or disable The Kure.
                            Clicking the button will perform the action shown on the button.<br/>
                            <img src="img/enable.png"/>
                            <br/>
                        </li>
                        <li>
                            Enter your password that you created at the beginning of the installation process. Once
                            entered, you will be prompted to restart your computer.<br/><br/>
                            <img src="img/password2.png"/><br/>
                            <img src="img/restart2.png">
                        </li>
                        <li>
                            You can restart your computer by clicking the Yes button when prompted to restart your computer.
                            There is also a restart button on the main page of The Kure.
                        </li>
                        <li>
                            That's it!
                        </li>
                    </ol>
                </div>
            </div>
            <div class="row" style="margin-bottom: 20em;">
                <div class="col-md-12" id="detect">
                    <h2 class="red">What happens when The Kure detects software being installed?</h2>
                    <p>
                        When you see this message:<br/><br/>
                            <span style="font-style: italic;">"The Kure has detected that software is being installed or opened.
                                If you are opening software that is already installed, then no action is required.
                                If you are installing a new program, or updating an existing one, click Yes to restart
                                your computer in unprotected mode. Otherwise, click No to remain protected."</span>
                        <br/><br/>
                        The Kure recognizes when you launch or install new software.
                        Since your computer is protected, software installations are not permanent unless you
                        allow them to be by disabling protection. If you choose to install new software, you will
                        need to disable protection, restart your computer, and THEN install the software.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>