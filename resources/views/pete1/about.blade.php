@extends('pete1.layout')
@section('content')

<div class="container">
    <section class="logo-area container">
        <section class="limited">
            <img src="/p/img/header.png" />
        </section>
    </section>
</div>
<div class="row" style="background:#f2f2f2;margin:10px auto; border-top:1px solid #ccc;">
    <div class="container">
        <div class="page-header">
            <h2 style="color:#FE642E;">About Us</h2>
        </div>
    </div>
</div>
<div class="container">
    <div class="panel panel-orange">
        <div class="panel-heading"><h3>The Next Generation of Computer Protection is Here</h3></div>
        <div class="panel-body">
            <h4>Centurion Technologies is the leading provider of Reboot/Restore technology over the past 20 years in the enterprise/commercial technology space.</h4>
            <h4>We are currently protecting over 2 million computers in the most sensitive industries, where anti-virus and malware protection are critical. Hospitals,Universities,Medical facilities and Banking institutions. These are Industries where cyber-security is critical. Our software solutions have been tested, approved and used in major government and Military facilities as well,. such as Hurlburt Air Force Base, The Air Force Academy and the U.S. Naval Hospital.</h4>
            <h4>We believe we are the single most trusted virus protection software company of it's kind.</h4>
            <h4>We have offices in both Missouri and Florida. Our Florida facility has been responsible for performing technical support for over one million computers, including those sold by all the major shopping channels. </h4>
            <h4>We are the company that you can trust with your most important personal information and computer records. Our guarantee is simple, if you use the Kure as prescribed...you will never get a virus again! </h4>
            <h4>Our software has been certified safe by McAfee and as mentioned before has been scrutinized and tested in every aspect by both U.S. Government and military agencies. TWO MILLION users can't be wrong , our patent pending technology really works.! All our technician, and developers are based here in the United States. We are a product that is proudly creating jobs in this country and is MADE IN AMERICA!</h4>
        </div>
    </div>
    <div class="panel panel-orange">
        <div class="panel-heading"><h3>Meet The CEO</h3></div>
        <div class="panel-body">
            <div class="col-xs-2">
                <img src="/p/img/ceo.png"/>
            </div>
            <h4><b>Peter Spezza is the Chief Executive Officer of Centurion Technologies LLC, A Missouri based company with Locations in Fenton, MO and Tampa Fl.</b></h4>
            <h4>As CEO, Peter leads an Executive Leadership Team focused on Centurion's vision to change the way the world protects the household PC from the growing threats of Viruses and Cyber security threats. Under his leadership, Centurion is bringing innovative technology only available to big business up until now that provides "the next generation Anti-Virus".</h4>
            <h4>Peter has formed a strategic partnership with McAfee (an Intel security company) to bring 100% total PC protection from Viruses and all unwanted changes a typical windows PC.</h4>
            <h4>He is responsible for the revolutionary business model that has become the new standard for all the shopping channels such as QVC and HSN by providing technical support bundles for millions of computers sold through their network with over 95% customer satisfaction.  Peter led the company's Sales and Partner Organizations, and helped drive and execute many of the company's strategy shifts into the home PC market with customized software making it available to the home PC user for the first time ever changing the entire Anti Virus industry as a whole.</h4>
            <h4>Peter has more than 25 years of leadership experience as an entrepreneur and executive in several successful companies and ventures in the computer industry. He is committed to both ultimate customer satisfaction and also making sure all products and services are manufactured and developed right here in the U.S.A. Peter's goal is to keep jobs and technology innovations in America at all costs.</h4>
            <h4>With experience, and tenacity as well as a vision to revolutionize the PC protection industry, Peter is dedicated to bringing the best of the best in technology to the home PC user in the most affordable manner to meet everyone's budget.</h4>
            <h4>He stands behind his products and services and is driven by his vision.</h4>
        </div>
    </div>
</div>

@stop