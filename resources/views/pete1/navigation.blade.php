<footer>
    <nav class="navbar navbar-default hidden-xs">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="/v1">Home</a></li>
                    <li><a href="/v1-testimonials">Testimonials</a></li>
                    <li><a href="/v1-faq">FAQs</a></li>
                    <li><a href="/v1-about">About</a></li>
                    <li><a href="/v1-support">Support</a></li>
                    <li><a href="/v1-contact">Contact</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</footer>

<script>
    $(function() {
        $('.collapse.show-footer').on('show', function () {
            $("html, body").animate({ scrollTop: $(document).height() }, "slow");
        });
    });
</script>