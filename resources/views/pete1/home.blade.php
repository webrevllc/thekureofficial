@extends('pete1.layout')

@section('content')

    <div id="ninja-slider" class="slider">
        <div class="slider-inner">
            <ul>
                <li>
                    <div class="content">
                        <h3>Home Of</h3>
                        <img src="/img/newkure.png" style="width:50%" class="center-block" />
                    </div>
                </li>
                <li>
                    <div class="content">
                        <h3><span style="color:#fff">WE ARE</span> CENTURION TECHNOLOGIES</h3>
                    </div>
                </li>
                <li>
                    <div class="content">
                        <h3>NEVER EVER EVER <span style="color:#fff">GET A VIRUS AGAIN</span> </h3>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="container">
            <a href="/purchase"><img src="/p/img/header.png" width="100%"/></a>
        </div>
    </div>

    <section class="sub-slider">
        <div class="row">
            <div class="container">
                <div class="col-xs-12 col-md-6" >
                    <img src="/p/img/subslider-logo.png" class="center-block" width="80%"/>
                    <div class="col-xs-12 text-center">
                        <h4 class="whitee">It's like an artificial intelligence. It knows what documents you have created. It knows what pictures you've imported. It knows which
                            updates you've downloaded. It saves all of them, and only destroys the viruses.</h4>
                        <h4 class="whitee">How's that for INTELLIGENT?</h4>
                    </div>
                    <div class="col-xs-12">
                        <img src="/p/img/subslider-img-1.png" class="center-block"/>
                    </div>

                </div>
                <div class="col-xs-12 col-md-6">
                    <iframe style="margin-top: 6px; " width="100%" height="335px" class="fiverr"
                            src="https://www.youtube.com/embed/ohpFbHKUvic" frameborder="0"
                            allowfullscreen></iframe>
                    {{--<video class="fiverr" width="100%" height="auto" controls>--}}
                        {{--<source src="/p/video/kure.webm" type="video/webm">--}}
                        {{--<source src="/p/video/kure.mp4" type="video/mp4">--}}
                        {{--<source src="/p/video/kure.ogg" type="video/ogg">--}}
                        {{--Your browser does not support HTML5 video.--}}
                    {{--</video>--}}
                </div>
            </div>
        </div>
    </section>

    <div class="row" style="padding: 20px auto;">
        <div class="container">
            <div class="col-xs-12 col-md-3">
                <div class="row">
                    <img src="/p/img/productbox1.jpg?1"  width="90%"/>

                </div>
                <div class="row">
                    <div class="col-xs-6 col-md-6">
                        <img src="/p/img/main-img-2.png" class="center-block" width="50%"/>
                    </div>
                    <div class="col-xs-6 col-md-6">
                        <img src="/p/img/main-img-3.png" class="center-block" width="60%"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <img src="/p/img/main-img-4.png" class="center-block"/>
                        <a href="/purchase"><img src="/p/img/freetrial.png" class="center-block"/></a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <h3>What is The Kure?</h3>
                <p><strong>The Kure from Centurion Technologies powered by McAfee and Intel Security.</strong>
                    <br />A partnership second to none that brings the most innovative and revolutionary
                    technology to you "the home PC user" that was only available for big business up until now.</p>
                <p>If or when you get a virus, simply restart your computer and any and <strong>all viruses or malware along with any other unwanted changes are wiped away</strong> as if they never existed and all your files, pictures and
                    documents are all protected and saved where they should be.</p>
                <p>The Kure powered by McAfee and Intel Security is the only protection you'll ever need with a <span>100% guarantee and total peace of mind.</span></p>
                <p>We are so confident you will love it that we will give you a full 30 days to try it before you pay for it! <span>Start your free trial today!</span></p>
            </div>
            <div class="col-xs-12 col-md-5">
                <video class="fiverr" width="100%" height="auto" controls>
                    <source src="/p/video/KureRender2-HD.webm" type="video/webm">
                    <source src="/p/video/KureRender2-HD.mp4" type="video/mp4">
                    <source src="/p/video/KureRender2-HD.ogg" type="video/ogg">
                    Your browser does not support the video tag.
                </video>
                <h3 class="text-center">Turn it OFF, Turn it ON and the Virus is gone</h3>
                <a href="/purchase"><img src="/p/img/freetrial.png" class="center-block"/></a>
            </div>
        </div>
    </div>

    <section class="feautures">
        <div class="container">
            <div class="col-xs-12 col-md-4">
                <div class="feature text-center">
                    <img src="/p/img/feauture-icon-1.png" />
                    <h4>FUNCTIONALITY</h4>
                    <p>When you install The Kure, the first thing it does is check your system for viruses, spyware, etc. It will then clean your drives to ensure that The Kure is installed on a perfectly clean system, so that every time you turn your system off and on again, the system will return to a perfectly clean and pristine condition. No other software does that!</p>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="feature text-center">
                    <img src="/p/img/feauture-icon-2.png" />
                    <h4>TOTAL PROTECTION</h4>
                    <p>The Kure protects your most important files on your PC, 24 hours a day, 7 days a week, 365 days a year. All your files...
                        Pictures, Videos, Music, tax returns, stock portfolios, back accounts, credit cards, usernames and passwords,
                        security numbers, e-mails, receipts...</p>
                    <p><strong>NO MORE VIRUSES, EVER!!!</strong></p>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="feature text-center">
                    <img src="/p/img/feauture-icon-3.png" />
                    <h4>PATENT PENDING TECHNOLOGY</h4>
                    <p>MILLIONS of Satisfied Users</p>
                    <p>Serving Banks, Hospitals, Government, Education and more!</p>
                    <p>100% Effectiveness over traditional antiviruses</p>
                    <p><strong>Cannot be defeated by new viruses!</strong></p>
                </div>
            </div>
        </div>
    </section>


    <section class="additional">
        <div class="container">
            <div class="col-xs-12 col-md-6">
                <img src="/p/img/additional-img-1.png" class="center-block"/>
                <div class="col-xs-12 col-md-6 text-center">
                    <h4>COLUMBIA UNIVERSITY</h4>
                    <h4>PENN STATE UNIVERSITY</h4>
                    <h4>FLORIDA STATE UNIVERSITY</h4>
                    <h4>U.S. OLYMPIC COMMITTEE</h4>
                </div>
                <div class="col-xs-12 col-md-6 text-center">
                    <h4>HURLBURT AIR FORCE BASE</h4>
                    <h4>CATHOLIC CHARITIES</h4>
                    <h4>AIR FORCE ACADEMY</h4>
                    <h4>U.S. NAVAL HOSPITAL</h4>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <h4 class="valign">
                    The Kure made available for the home PC user, where up until now has only been available to the commercial market..<br><br>
                    <span style="color:#ff6700;">Protect your computer like the big companies do..</span>
                </h4>
            </div>


        </div>
    </section>

    <section class="graphics-chart">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2>Antivirus Comparison</h2>
                    <img src="/p/img/graphics-chart-img-1.png" />
                    <a href="/purchase" class="visible-xs" style="margin-top:80px;">
                        <img src="/p/img/freetrial.png" class="center-block"/>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="start-trial">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12 hidden-xs">
                    <a href="/purchase">
                        <img src="/p/img/freetrial.png" width="75%" class="center-block">
                    </a>
                </div>
                <div class="col-md-6 col-xs-12 text-center">
                    <p><span>Use The KURE for 30 full days.</span> At the end of the 30 days, if you are not absolutely in love with the savings, the security and the peace of mind you get with The KURE, just send it back, no questions asked and you owe nothing. And, we'll even cover the postage to return it!</p>
                </div>
            </div>
        </div>
    </section>

    <section class="recommendations">
        <div class="recommendation-1" id="edit">
            <div class="container">
                <div class="col-md-5 col-xs-12">
                    <img src="/p/img/recommendations-img-1.png" class="center-block"/>
                </div>
                <div class="col-md-7 col-xs-12">
                    <p>Craig Crossman is the Host of Computer America, the longest running computer show on radio. He is also a nationally syndicated newspaper technology expert, in over 200 newspapers. </p>
                    <p>Here's what he has to say about the KURE and CyberSafe PC:</p>
                    <div class="quote">
                        <p>"The Kure really, really works! In my 25 years of doing my computer radio talk show, I've never seen anything like it, especially at this price!</p>
                        <p>It's absolutely safe to use! If you have anything important on your computer, then you need The Kure!"</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="recommendation-2">
            <div class="container">
                <div class="col-md-5 col-xs-12">
                    <img class="center-block" src="/p/img/recommendations-img-2.png" style=""/>
                </div>
                <div class="col-md-7 col-xs-12">
                    <div class="quote">
                        <p>When I first heard about the Kure, I said this may be the most important computer product I have ever seen, the Kure really works! In fact, I give it 5 stars.</p>
                    </div>
                    <a href="/ptestimonials" >
                        <img src="/p/img/recommendations-img-3.png" width="60%" class="center-block" style="margin-top:15px;"/>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <div class="row">
        <div class="container">
            <img class="csbox" src="/p/img/free.png" width="100%"/>
            <img  src="/p/img/csafe.png" width="90%" class="center-block"/>
        </div>
    </div>


@stop