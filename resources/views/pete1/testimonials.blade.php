@extends('pete1.layout')
@section('content')

    <div class="container">
        <section class="logo-area container">
            <section class="limited">
                <img src="/p/img/header.png" />
            </section>
        </section>
    </div>
    <div class="row" style="background:#f2f2f2;margin:10px auto; border-top:1px solid #ccc;">
        <div class="container">
            <div class="page-header">
                <h2 style="color:#FE642E;">Testimonials</h2>
            </div>
        </div>
    </div>
<div class="container">
    <div class="col-xs-12 col-md-6">
        <div class="embed-responsive embed-responsive-16by9 col-xs-12 text-center">
            <video controls class="text-center">
                <source src="/p/video/crossman.webm" type="video/webm">
                <source src="/p/video/crossman.mp4" type="video/mp4">
                <source src="/p/video/crossman.ogg" type="video/ogg">
                Your browser does not support the video tag.
            </video>
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="embed-responsive embed-responsive-16by9 col-xs-12 text-center">
            <video controls class="text-center">
                <source src="/p/video/t2.webm" type="video/webm">
                <source src="/p/video/t2.mp4" type="video/mp4">
                <source src="/p/video/t2.ogg" type="video/mp4">
                Your browser does not support the video tag.
            </video>
        </div>
    </div>

    <div ><img class="quote" src="/p/img/q1.png" width="100%"/></div>
    <div ><img class="quote" src="/p/img/q2.png" width="100%"/></div>
    <div ><img class="quote" src="/p/img/testfix1.jpg" width="100%"/></div>
    <div ><img class="quote" src="/p/img/testfix2.jpg" width="100%"/></div>
</div>
@stop