@extends('pete1.layout')

@section('header')
        <!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
    if (matchMedia('only screen and (min-width: 641px)').matches) {
        var __lc = {};
        __lc.license = 6677561;
        (function () {
            var lc = document.createElement('script');
            lc.type = 'text/javascript';
            lc.async = true;
            lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(lc, s);
        })();
    }
</script>
<!-- End of LiveChat code -->
@stop
@section('content')

    <div class="container">
        <section class="logo-area container">
            <section class="limited">
                <img src="/p/img/header.png" />
            </section>
        </section>
    </div>
    <div class="row" style="background:#f2f2f2;margin:10px auto; border-top:1px solid #ccc;">
        <div class="container">
            <div class="page-header">
                <h2 style="color:#FE642E;">Support</h2>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-xs-12 col-md-6">
            <h2 class="text-center">TECHNICAL SUPPORT AND CUSTOMER SERVICE IS OUR #1 PRIORITY!</h2>
            <h3 class="text-center">
                We have over 17 years of experience in providing "Platinum Level" support to millions of customers and organizations like the US Olympic Committee, The US Air Force Academy, QVC, HSN and many, many more. Our "world class" Technical Support is available to you 7 days a week between the hours of 8AM and 6PM EST or email us 24 hours a day at Support@TheKure.com provided by our staff of live US based technicians.
                <br>
                <br>
                Serving your every need is our one and only mission. We have a history of doing it better, so we can be there for you whenever you need us. Ask any question. No problem is too big or too small. And best of all it's always FREE!
            </h3>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class="panel panel-orange">
                <div class="panel-heading"><h3>Send us an email!</h3></div>
                <div class="panel-body" style="padding:30px;">
                    <form action="/inquiry" method="post" >
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                @if(Session::has('success'))
                                    <div class="alert alert-success" role="alert">
                                        <h4>{{ Session::get('success') }}</h4>
                                    </div>
                                @endif
                                @if(Session::has('error'))
                                    <div class="alert alert-danger" role="alert">
                                        <h4>{{ Session::get('error') }}</h4>
                                    </div>
                                @endif
                                    <div class="clearfix"></div>
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label>Select Department</label>
                                    <select class="input-lg form-control" name="department" tabindex="1" required>
                                        <option value="">--Select--</option>
                                        <option value="support">Tech Support</option>
                                        <option value="questions">Pre-sales Questions</option>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" id="name" class="form-control input-lg" placeholder="Your Name" tabindex="2" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label>E-Mail</label>
                                    <input type="email" name="email" id="email" class="form-control input-lg" placeholder="Email" tabindex="3" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="phone">Phone Number</label>
                                    <input type="text" name="phone" id="phone" class="form-control input-lg" placeholder="Phone" tabindex="4" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="message">Describe Your Issue</label>
                                    <textarea name="message" id="message" class="form-control input-lg" placeholder="Describe" tabindex="5" rows="7" required></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <input type="submit" class="btn btn-lg btn-block btn-info" value="Submit"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>


@stop