@extends('pete1.layout')
@section('header')
        <!-- Start of LiveChat (www.livechatinc.com) code -->
        <script type="text/javascript">
            if (matchMedia('only screen and (min-width: 641px)').matches) {
                var __lc = {};
                __lc.license = 6677561;
                (function () {
                    var lc = document.createElement('script');
                    lc.type = 'text/javascript';
                    lc.async = true;
                    lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(lc, s);
                })();
            }
        </script>
        <!-- End of LiveChat code -->
@stop
@section('content')
    <div class="row">
        <img src="/p/img/contactusbanner2.jpg" width="100%"/>
    </div>

    <div class="container">
        {{--<div class="row">--}}
            {{--<img src="p/img/contact.png" style="margin: auto; padding-bottom: 35px">--}}

        {{--</div>--}}
        <div class="row">
            <div class="col-xs-12 col-sm-3">
                <img src="/img/centurionlogo.png"  class="center-block"/>
            </div>
            <div class="col-xs-12 col-sm-9" style="padding:30px;">
                <h2>Please feel free to let us know what you think of our products and services, as well as any recommendations you have for product improvement and future development.</h2>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <br>
                <h4>Corporate Headquarters</h4>
                <p>512 Rudder Rd.</p>
                <p>Fenton, MO 63026</p>
                <hr>
                <h4>Tampa Office</h4>
                <p>5701 E. Hillsborough Ave</p>
                <p>Tampa, FL 33610</p>
                <hr>
                <h4>Technical Support</h4>
                <p>Toll Free: 1-877-864-0190</p>
                <p>Email: <a href="mailto:support@thekure.com">Support@TheKure.com</a></p>
                <p>Phone Service and Online Support Hours:<br>
                    8:00a - 6:00p (EST) </p>
                <hr>
                <h4>Questions?</h4>
                <p>Toll Free: 1-877-864-0190</p>
                <p>Email: <a href="mailto:questions@thekure.com">Questions@TheKure.com</a></p>
                <p>Phone Service and Online Support Hours:<br>
                    8:00a - 6:00p (EST) </p>
            </div>
            <div class="col-xs-12 col-md-9" >
                <br>
                <br>
                <div class="panel panel-orange">
                    <div class="panel-heading"><h3>Send us an email!</h3></div>
                    <div class="panel-body" style="padding:30px;">
                        <form action="/inquiry" method="post" >
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    @if(Session::has('success'))
                                        <div class="alert alert-success" role="alert">
                                            <h4>{{ Session::get('success') }}</h4>
                                        </div>
                                    @endif
                                    @if(Session::has('error'))
                                        <div class="alert alert-danger" role="alert">
                                            <h4>{{ Session::get('error') }}</h4>
                                        </div>
                                    @endif
                                    <div class="clearfix"></div>
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label>Select Department</label>
                                        <select class="input-lg form-control" name="department" tabindex="1" required>
                                            <option value="">--Select--</option>
                                            <option value="support">Tech Support</option>
                                            <option value="questions">Pre-sales Questions</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" name="name" id="name" class="form-control input-lg" placeholder="Your Name" tabindex="2" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label>E-Mail</label>
                                        <input type="email" name="email" id="email" class="form-control input-lg" placeholder="Email" tabindex="3" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="phone">Phone Number</label>
                                        <input type="text" name="phone" id="phone" class="form-control input-lg" placeholder="Phone" tabindex="4" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="message">Add a message</label>
                                        <textarea name="message" id="message" class="form-control input-lg" tabindex="5" rows="7" required></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <input type="submit" class="btn btn-lg btn-block btn-info" value="Submit"/>
                            </div>
                        </form>
                    </div>
            </div>
        </div>
    </div>

@stop