<!DOCTYPE html>
<html>
<head>
    {{--<script src="//cdn.optimizely.com/js/3724870335.js"></script>--}}
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    {{--<script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>--}}
    <title>TheKure</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

    <link rel="stylesheet" href="/p/css/bootstrap.min.css?2" />
    <link rel="stylesheet" href="/p/css/app.min.css?2" />
    <link rel="stylesheet" href="/p/css/jquery-ui.min.css?2" />
    <link rel="stylesheet" href="/p/css/jquery-ui.theme.min.css?2" />
    <link rel="stylesheet" href="/p/css/styles.css?3" />
    <link rel="stylesheet" href="/p/css/text.css?2" />


    <script src="/p/js/jquery.min.js?2"></script>
    <script src="/p/js/bootstrap.min.js?2"></script>
    <script src="/p/js/jqueryui.min.js?2"></script>
    <script src="/p/js/ninja.js?2"></script>
    @yield('header')



</head>
<body>


<nav class="navbar navbar-default  navbar-fixed-top  visible-lg">
    <nav class="navbar navbar-default  navbar-fixed-top">
        <div class="container-fluid">
            <a href="/purchase"><img src="/p/img/headerbanner.jpg" width="100%" class="center-block"/></a>
        </div><!-- /.container-fluid -->
    </nav>
    {{--<div class="container">--}}
        {{--<ul class="nav navbar-nav" style="margin-top:10px;">--}}
            {{--<li><a href="http://www.pcmag.com/article2/0,2817,2469309,00.asp" target="_blank"><img src="/p/img/top-img-1.png"></a></li>--}}
            {{--<li><img src="/p/img/intel.png"></li>--}}
            {{--<li><a target="_blank" href="http://www.bbb.org/west-florida/business-reviews/computers-service-and-repair/invisatec-in-tampa-fl-90213698"><img src="/p/img/top-img-3.png"></a></li>--}}
            {{--<li><a target="_blank" href="http://www.pcmag.com/article2/0,2817,2469309,00.asp"><img src="/p/img/top-img-4.png"></a></li>--}}
        {{--</ul>--}}
        {{--<ul class="nav navbar-nav navbar-right">--}}
            {{--<li><a href="/purchase"><img src="/p/img/top-btn-1.png"></a></li>--}}
        {{--</ul>--}}
    {{--</div><!-- /.container-fluid -->--}}
</nav>
<nav class="navbar navbar-default  navbar-fixed-top  visible-md">
    <nav class="navbar navbar-default  navbar-fixed-top">
        <div class="container-fluid">
            <a href="/purchase"><img src="/p/img/headerbanner.jpg" width="100%" class="center-block"/></a>
        </div><!-- /.container-fluid -->
    </nav>
    {{--<div class="container">--}}
        {{--<ul class="nav navbar-nav" style="margin-top:10px;">--}}
            {{--<li><a target="_blank" href="http://www.pcmag.com/article2/0,2817,2469309,00.asp"><img src="/p/img/top-img-1.png"></a></li>--}}
            {{--<li><img src="/p/img/top-img-2.png"></li>--}}
            {{--<li><a target="_blank" href="http://www.bbb.org/west-florida/business-reviews/computers-service-and-repair/invisatec-in-tampa-fl-90213698"><img src="/p/img/top-img-3.png"></a></li>--}}
            {{--<li><a href="http://www.pcmag.com/article2/0,2817,2469309,00.asp"><img src="/p/img/top-img-4.png"></a></li>--}}
        {{--</ul>--}}
        {{--<ul class="nav navbar-nav navbar-right">--}}
            {{--<li><a href="/purchase"><img src="/p/img/top-btn-1.png"></a></li>--}}
        {{--</ul>--}}
    {{--</div><!-- /.container-fluid -->--}}
</nav>
<nav class="navbar navbar-default  navbar-fixed-top  visible-sm">
    <div class="container-fluid">
        <img src="/img/newkure.png" width="40%" class="center-block">
        <a href="/purchase"><img src="/p/img/freetrial.png" class="center-block"></a>
        {{--<ul class="nav navbar-nav small-nav" style="margin-top:10px;">--}}
            {{--<li><a target="_blank" href="http://www.pcmag.com/article2/0,2817,2469309,00.asp"><img src="/p/img/top-img-1.png"></a></li>--}}
            {{--<li><img src="/p/img/top-img-2.png"></li>--}}
            {{--<li><a href="http://www.bbb.org/west-florida/business-reviews/computers-service-and-repair/invisatec-in-tampa-fl-90213698"><img src="/p/img/top-img-3.png"></a></li>--}}
            {{--<li><a href="http://www.pcmag.com/article2/0,2817,2469309,00.asp"><img src="/p/img/top-img-4.png"></a></li>--}}
        {{--</ul>--}}
        {{--<ul class="nav navbar-nav navbar-right">--}}
            {{--<li><a href="/purchase"><img src="/p/img/top-btn-1.png"></a></li>--}}
        {{--</ul>--}}
    </div><!-- /.container-fluid -->
</nav>
<nav class="navbar navbar-default  navbar-fixed-top  visible-xs">
    <div class="container-fluid">
        <img src="/img/newkure.png" width="70%" class="center-block">
        <a href="/purchase"><img src="/p/img/freetrial.png" class="center-block"></a>
    </div><!-- /.container-fluid -->
</nav>
<nav class="navbar navbar-inverse visible-xs">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span style="color:#fff">MENU</span>
            </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="/v1">Home</a></li>
                <li><a href="/v1-testimonials">Testimonials</a></li>
                <li><a href="/v1-faq">FAQs</a></li>
                <li><a href="/v1-about">About</a></li>
                <li><a href="/v1-support">Support</a></li>
                <li><a href="/v1-contact">Contact</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

    @yield('content')
    @include('pete1.navigation')
    @include('pete1.tracking')
    @include('pete1.mouseflow')
</body>
</html>