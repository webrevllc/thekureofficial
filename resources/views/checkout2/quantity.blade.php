@extends('checkout2.layout')


@section('content')
    <style>

    </style>
    <div class="container" id="wrap">
        <div class="row">
            @if(Session::has('success'))
                <div class="alert alert-success" role="alert">
                    <h4>{{ Session::get('success') }}</h4>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger" role="alert">
                    <h4>{{ Session::get('error') }}</h4>
                </div>
            @endif
            <div class="col-xs-12 col-sm-8  col-sm-offset-2 ">
                <img src="/img/newkure.png" width="50%" class="center-block"/>
                <div class="panel-heading" style="width:100%;">
                    <h2 class="page-heading text-center">How Many Would You Like?</h2>
                </div>
                <div class="panel-body">
                    <img src="/img/kureproductlifetimepymts.png" style="width:100%;margin-top:10px;margin-left:-20px;" class="center-block">
                    {{--<h3 class="text-center">SPECIAL OFFER! Free Shipping on all orders!</h3>--}}
                    <form method="post" action="/purchase/2">
                        {{csrf_field()}}
                        <input type="hidden" value="{{$lead->lead_id}}" name="lead_id"/>
                        <select name="quantity" class="input-lg form-control text-center">
                            <option value="1">1 @ 2 payments of $24.95 </option>
                            <option value="2">2 @ 2 payments of $24.95 each</option>
                            <option value="3">3 @ 2 payments of $24.95 each</option>
                            <option value="4">4 @ 2 payments of $24.95 each</option>
                            <option value="5">5 @ 2 payments of $24.95 each</option>
                        </select>
                        <br>
                        {{--<input type="number" id="quantity" name="quantity" class="input-lg form-control text-center" min="1" value="1" /><br><br>--}}
                        <button type="submit" class="btn btn-success btn-block btn-lg text-center" >Next</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop