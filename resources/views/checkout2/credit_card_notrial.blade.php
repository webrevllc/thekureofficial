@extends('checkout2.layout')

@section('content')

    <div class="container">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <img src="/img/newkure.png" width="50%" class="center-block"/>
            <div class="well">
                <div class="row">
                    <div class="">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Product</th>
                                <th>Quantity</th>
                                {{--<th class="text-center">Price</th>--}}
                                <th class="text-center">Price Today</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="col-xs-8">
                                    <div class="media">
                                        <span class="thumbnail pull-left"> <img class="media-object" src="/img/kureproductlifetime.png" style="width: 72px; height: 72px;"> </span>
                                        <div class="media-body">
                                            <h4 class="media-heading">The Kure</h4>
                                            <span><b>NO ANNUAL FEES!</b></span>
                                        </div>
                                    </div>
                                </td>
                                <td class="col-xs-1 " style="text-align: center">
                                    <input type="number" class="form-control qty" id="KURE" value="{{$lead->quantity}}">
                                </td>
                                {{--<td class="col-xs-2 text-center"><strong>$19.95</strong></td>--}}
                                <td colspan="2" class="col-xs-2 text-center"><strong>${{number_format(49.95 * $lead->quantity, 2)}}</strong></td>
                                <td class="col-xs-1"> </td>
                            </tr>
                            {{--<tr>--}}
                                {{--<td class="col-xs-6">--}}
                                    {{--<div class="media">--}}
                                        {{--<span class="thumbnail pull-left"> <img class="media-object" src="/img/cybersafe.jpg" style="width: 72px; height: 72px;"> </span>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<h4 class="media-heading">CyberSafePC</h4>--}}
                                            {{--<span><b>Identity theft software</b></span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</td>--}}
                                {{--<td class="col-xs-1 " style="text-align: center">--}}
                                    {{--{{$lead->quantity}}--}}
                                {{--</td>--}}

                                {{--<td colspan="2" class="col-xs-2 text-center"><span class="text-success" style="font-size:1.4em;"><strong>FREE</strong></span></td>--}}
                                {{--<td class="col-xs-1"></td>--}}
                            {{--</tr>--}}
                            @if($lead->HWMON)
                                <tr>
                                    <td class="col-xs-6">
                                        <div class="media">
                                            <span class="thumbnail pull-left"> <img class="media-object" src="/img/upsells/hwmon.png" style="width: 72px; height: 72px;"> </span>
                                            <div class="media-body">
                                                <h4 class="media-heading">Hardware Monitoring</h4>
                                                <span><strong>Get the alert if anything goes wrong in real-time!</strong></span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="col-xs-1 " style="text-align: center">
                                        <input type="number" class="form-control qty"  id="HWMON" value="{{$lead->HWMON}}" max="{{$lead->quantity}}">
                                    </td>
                                    {{--<td class="col-xs-2 text-center"><strong>$4.95</strong></td>--}}
                                    <td colspan="2" class="col-xs-2 text-center"><strong>${{number_format(4.95 * $lead->HWMON, 2)}}</strong></td>
                                    <td class="col-xs-1">
                                        <a type="button" class="btn btn-danger btn-block" href="/purchase/remove/HWMON/?id={{$lead->lead_id}}">
                                            <span class="glyphicon glyphicon-remove"></span> Remove
                                        </a>
                                    </td>
                                </tr>
                            @endif
                            @if($lead->PCOS)
                                <tr>
                                    <td class="col-xs-6">
                                        <div class="media">
                                            <span class="thumbnail pull-left"> <img class="media-object" src="/img/upsells/pcos.png" style="width: 72px; height: 72px;"> </span>
                                            <div class="media-body">
                                                <h4 class="media-heading">PC Cleaner On Steroids</h4>
                                                <span><strong>Slow computer? Try this!</strong></span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="col-xs-1 " style="text-align: center">
                                        <input type="number" class="form-control qty" id="PCOS" value="{{$lead->PCOS}}" max="{{$lead->quantity}}">
                                    </td>
                                    {{--<td class="col-xs-2 text-center"><strong>$8.95</strong></td>--}}
                                    <td colspan="2" class="col-xs-2 text-center"><strong>${{number_format(8.95 * $lead->PCOS, 2)}}</strong></td>
                                    <td class="col-xs-1">
                                        <a type="button" class="btn btn-danger btn-block" href="/purchase/remove/PCOS/?id={{$lead->lead_id}}">
                                            <span class="glyphicon glyphicon-remove"></span> Remove
                                        </a>
                                    </td>
                                </tr>
                            @endif
                            @if($lead->PROC)
                                <tr>
                                    <td class="col-xs-6">
                                        <div class="media">
                                            <span class="thumbnail pull-left" > <img class="media-object" src="/img/shipping-guy.png" style="width: 72px; height: 72px;"> </span>
                                            <div class="media-body">
                                                <h4 class="media-heading">Expedited Processing</h4>
                                                <span><strong>For your whole order</strong></span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="col-xs-1 " style="text-align: center">-</td>
                                    {{--<td class="col-xs-2 text-center"><strong>$1.95</strong></td>--}}
                                    <td colspan="2" class="col-xs-2 text-center"><strong>$1.95</strong></td>
                                    <td class="col-xs-1">
                                        <a type="button" class="btn btn-danger btn-block" href="/purchase/remove/PROC/?id={{$lead->lead_id}}">
                                            <span class="glyphicon glyphicon-remove"></span> Remove
                                        </a>
                                    </td>
                                </tr>
                            @endif

                            <tr>
                                {{--<td class="col-xs-6" ><button class="btn  btn-info btn-block"><i class="fa fa-refresh"></i> Switch To Pay Now</button></td>--}}
                                <td></td>
                                <td></td>
                                <td colspan="3"><button class="btn  btn-info btn-block btn-lg" id="updateCart" onclick="updateCart()"><i class="fa fa-shopping-cart"></i> Update Cart</button></td>
                            </tr>
                            <tr>
                                <td colspan="2"><h4>Subtotal</h4></td>
                                <td></td>
                                <td></td>
                                <td class="text-right"><h4><strong>${{number_format($lead->subtotal, 2)}}</strong></h4></td>
                            </tr>

                            <tr>
                                <td colspan="2"><h4>Shipping</h4></td>
                                <td></td>
                                <td></td>
                                <td class="text-right"><h4><strong>$5.95</strong></h4></td>
                            </tr>
                            <tr>
                                <td colspan="2"><h4>Total</h4></td>
                                <td></td>
                                <td></td>
                                <td class="text-right"><h4><strong>${{number_format($lead->total, 2)}}</strong></h4></td>
                            </tr>
                            {{--<tr>--}}
                            {{--<td class="col-xs-6" ><button class="btn  btn-info btn-block"><i class="fa fa-refresh"></i> Switch To Pay Now</button></td>--}}
                            {{--<td></td>--}}
                            {{--<td></td>--}}
                            {{--<td colspan="3">--}}
                            {{--<button type="button" class="btn btn-success btn-block btn-lg" id="checkoutbtn" onclick="checkout('{{$lead->lead_id}}')">--}}
                            {{--Checkout <span class="glyphicon glyphicon-play"></span>--}}
                            {{--</button>--}}
                            {{--</td>--}}
                            {{--</tr>--}}

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
                <form role="form" method="post" action="/purchase/3">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$lead->lead_id}}" name="lead_id"/>
                    <div class="row">
                        <div class="col-xs-12">
                            @if(Session::has('error'))
                                <div class="alert alert-danger" role="alert">
                                    <h4>{{ Session::get('error') }}</h4>
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="cardNumber">Enter a credit or debit card number</label>
                                <div class="input-group">
                                    <input
                                            type="tel"
                                            class="form-control input-lg"
                                            name="card"
                                            placeholder="Valid Card Number"
                                            maxlength="16"
                                            required
                                    />
                                    <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group">
                                <label for="cardExpiry" ><span class="hidden-xs">Expiration</span><span class="visible-xs-inline">Exp</span> Month</label>
                                <select name="month" id="month" class="form-control input-lg " required>
                                    <option value="01">01 - January</option>
                                    <option value="02">02 - February</option>
                                    <option value="03">03 - March</option>
                                    <option value="04">04 - April</option>
                                    <option value="05">05 - May</option>
                                    <option value="06">06 - June</option>
                                    <option value="07">07 - July</option>
                                    <option value="08">08 - August</option>
                                    <option value="09">09 - September</option>
                                    <option value="10" >10 - October</option>
                                    <option value="11" >11 - November</option>
                                    <option value="12" >12 - December</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group">
                                <label for="cardExpiry" class="input_label"><span class="label_required">*</span><span class="hidden-xs">Expiration</span><span class="visible-xs-inline">Exp</span> Year</label>
                                <select name="year" id="year" class="form-control input-lg" required>
                                    <option value="16">2016</option>
                                    <option value="17">2017</option>
                                    <option value="18">2018</option>
                                    <option value="19">2019</option>
                                    <option value="20">2020</option>
                                    <option value="21">2021</option>
                                    <option value="22">2022</option>
                                    <option value="23">2023</option>
                                    <option value="24">2024</option>
                                    <option value="25">2025</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group">
                                <label for="cvc" class="input_label">CVC</label>
                                <div class="input-group">
                                    <input
                                            type="tel"
                                            class="form-control input-lg"
                                            name="cvc"
                                            placeholder="Card Verification Code"
                                            maxlength="4"
                                            required
                                    />
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <img src="/img/security.png" width="100%" class="center-block"/>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <img src="/img/bbb2.png" width="90%" class="center-block"/>
                        </div>
                        <div class="clearfix"></div>
                        <h3 class="text-center" style="font-size:16px;font-weight:bold;margin-top:10px;">Your Satisfaction is 100% Guaranteed</h3>

                        <!--closes satisfaction_guarantee_container div-->

                        <input type="submit" class="btn btn-block btn-success btn-lg" value="Continue"/>
                    </div>
                </form>
            </div>
        </div>
    <script>
        var checkout = function(lead_id){

            cb = $('#checkoutbtn');
            cb.addClass('disabled');
            cb.attr('disabled');
            cb.text('Please wait...');

            $.ajax({
                type: "GET",
                url: '/purchasing?id='+ lead_id,
                success:function(stuff){
                    window.location = '/thank-you?t=' + stuff;
//                    window.location = '/thank-you?t=' + stuff;
                },
                error:function(stuff){
                    console.log(stuff);
                }
            });
        };

        $(document).on("click touchstart", "#updateCart", function () {
            updateCart();
        });

        var updateCart = function(){
            hwmon = $('#HWMON').val();
            kure =  $('#KURE').val();
            pcos =  $('#PCOS').val();
            id = '{{$lead->lead_id}}';
            var products = {};
            if(typeof hwmon != 'undefined'){
                products.HWMON =hwmon;
            }
            if(typeof kure != 'undefined'){
                products.KURE =kure;
            }
            if(typeof pcos != 'undefined'){
                products.PCOS =pcos;
            }
            products.id = id;
            $.ajax({
                type: "POST",
                url: '/purchase/qty',
                data: products,
                success:function(stuff){
//                    console.log(stuff);
                    window.location = self.location;
                    location.reload(true);
                },
                error:function(stuff){
//                    console.log(stuff);
                    window.location = self.location;
                    location.reload(true);
                }
            });
        };
    </script>
    <script>
        var checkout = function(lead_id){

            cb = $('#checkoutbtn');
            cb.addClass('disabled');
            cb.attr('disabled');
            cb.text('Please wait...');

            $.ajax({
                type: "GET",
                url: '/purchasing?id='+ lead_id,
                success:function(stuff){
                    window.location = '/thank-you?t=' + stuff;
//                    window.location = '/thank-you?t=' + stuff;
                },
                error:function(stuff){
                    console.log(stuff);
                }
            });
        };

        $(document).on("click touchstart", "#updateCart", function () {
            updateCart();
        });

        var updateCart = function(){
            hwmon = $('#HWMON').val();
            kure =  $('#KURE').val();
            pcos =  $('#PCOS').val();
            id = '{{$lead->lead_id}}';
            var products = {};
            if(typeof hwmon != 'undefined'){
                products.HWMON =hwmon;
            }
            if(typeof kure != 'undefined'){
                products.KURE =kure;
            }
            if(typeof pcos != 'undefined'){
                products.PCOS =pcos;
            }
            products.id = id;
            $.ajax({
                type: "POST",
                url: '/purchase/qty',
                data: products,
                success:function(stuff){
//                    console.log(stuff);
                    window.location = self.location;
                    location.reload(true);
                },
                error:function(stuff){
//                    console.log(stuff);
                    window.location = self.location;
                    location.reload(true);
                }
            });
        };
    </script>
@stop