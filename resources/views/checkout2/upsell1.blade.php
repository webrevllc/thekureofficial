@extends('checkout2.layout')


@section('content')
    <style>

    </style>
    <div class="container" id="wrap">
        <div class="row">
            @if(Session::has('success'))
                <div class="alert alert-success" role="alert">
                    <h4>{{ Session::get('success') }}</h4>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger" role="alert">
                    <h4>{{ Session::get('error') }}</h4>
                </div>
            @endif
            <div class="col-xs-12 col-sm-8  col-sm-offset-2 ">

                <img src="/img/newkure.png" width="50%" class="center-block"/>
                <div class="panel-body text-center">
                    <h2 style="color:#31708f; font-size: 1.5em;">
                        Monitor your hardware from anywhere<br>
                        Receive a text message or email the moment a hardware problem arises
                    </h2>
                    <br>

                    <h3 style="color:#31708f; font-size: 2em;">$40 Value - only 2 payments of just $4.95 per pc</h3>
                    <h3 style="color:#31708f; font-size: 2em;">Alerts you to possible Hardware Problems like</h3>

                    <div class="col-md-6">
                        <img src="/img/upsells/hwmon.png" style="width:70%;display:block;margin:20px auto 0;">
                    </div>
                    <div class="col-md-6 top-2">
                        <ul class="fa-ul">
                            <li><i class="fa-li fa fa-2x fa-check-square"></i><h3 class="l2">Potential Hard Drive failure</h3></li>
                            <li><i class="fa-li fa fa-2x fa-check-square"></i><h3 class="l2">Potential System Overheat</h3></li>
                            <li><i class="fa-li fa fa-2x fa-check-square"></i><h3 class="l2">CPU Load</h3></li>
                            <li><i class="fa-li fa fa-2x fa-check-square"></i><h3 class="l2">Hard Drive Usage</h3></li>
                            <li><i class="fa-li fa fa-2x fa-check-square"></i><h3 class="l2">Memory Utilization</h3></li>
                            <li><i class="fa-li fa fa-2x fa-check-square"></i><h3 class="l2">Network Usage</h3></li>
                            <li><i class="fa-li fa fa-2x fa-check-square"></i><h3 class="l2">Completely Customizable!</h3></li>

                        </ul>
                    </div>
                </div>
                <div class="panel-footer">
                    <form method="post" action="/purchase/4">
                        {{csrf_field()}}
                        <input type="hidden" value="{{$lead->lead_id}}" name="lead_id"/>
                        <button type="submit" class="btn btn-success btn-block" style="font-size:1.4em;" >Yes! Add this to my {{env('offer_status')}}!</button>
                        {{--<button type="submit" class="btn btn-success btn-block" style="font-size:1.4em;" >Yes! Add this to my free trial!</button>--}}
                    </form>
                    <a href="/purchase/no/HWMON?id={{$lead->lead_id}}" class="btn btn-default btn-block">No, thanks</a>
                </div>
            </div>
        </div>
    </div>
@stop