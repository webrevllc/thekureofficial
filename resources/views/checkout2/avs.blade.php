@extends('checkout2.layout')

@section('content')

    <div class="container">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <img src="/img/newkure.png" width="50%" class="center-block"/>
            <div class="alert alert-success" role="alert" style="margin-top: 15px;">
                <b><span style="font-size:1.7em;text-decoration: underline" class="text-center;">Your card will not be charged today</span> but we do need your information only for when your free trial is over.
                    Please be assured that we will not bill you for anything until you have enjoyed your 30 day Free Trial.
                    Also, remember that we are so sure that you will love The KURE, we will even cover the postage to return it!</b>
            </div>
        <div class="panel-body">
                <form role="form" method="post" action="/purchase/avs">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$lead->lead_id}}" name="lead_id"/>
                    <div class="row">
                        <div class="col-xs-12">
                            @if(Session::has('error'))
                                <div class="alert alert-danger" role="alert">
                                    <h4>{{ Session::get('error') }}</h4>
                                </div>
                            @else
                                <div class="alert alert-danger" role="alert">
                                    <h4>There was an error, the address associated with your credit/debit card does not match the address you provided. Please correct your address and re-enter your credit/debit card.</h4>
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="cardNumber">Enter a credit or debit card number for <b style="text-decoration:underline;font-size: 1.4em;">future billing</b> after your free trial</label>
                                <div class="input-group">
                                    <input
                                            type="tel"
                                            class="form-control input-lg"
                                            name="card"
                                            placeholder="Valid Card Number"
                                            maxlength="16"
                                            required
                                            />
                                    <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="cardExpiry" ><span class="hidden-xs">Expiration</span><span class="visible-xs-inline">Exp</span> Month</label>
                                <select name="month" id="month" class="form-control input-lg " required>
                                    <option value="">month</option>
                                    <option value="01">01 - January</option>
                                    <option value="02">02 - February</option>
                                    <option value="03">03 - March</option>
                                    <option value="04">04 - April</option>
                                    <option value="05">05 - May</option>
                                    <option value="06">06 - June</option>
                                    <option value="07">07 - July</option>
                                    <option value="08">08 - August</option>
                                    <option value="09">09 - September</option>
                                    <option value="10" selected>10 - October</option>
                                    <option value="11">11 - November</option>
                                    <option value="12">12 - December</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="cardExpiry" class="input_label"><span class="label_required">*</span><span class="hidden-xs">Expiration</span><span class="visible-xs-inline">Exp</span> Year</label>
                                <select name="year" id="year" class="form-control input-lg" required>
                                    <option value="15">2015</option>
                                    <option value="16">2016</option>
                                    <option value="17">2017</option>
                                    <option value="18">2018</option>
                                    <option value="19">2019</option>
                                    <option value="20">2020</option>
                                    <option value="21">2021</option>
                                    <option value="22">2022</option>
                                    <option value="23">2023</option>
                                    <option value="24">2024</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label>Address Line 1</label>
                                <input type="text" name="add1" class="form-control input-lg" placeholder="Address" tabindex="3" required value="{{$lead->add1}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label>Address Line 2</label>
                                <input type="text" name="add2"  class="form-control input-lg" placeholder="Address" tabindex="4" value="{{$lead->add2}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <label>City</label>
                                <input type="text" name="city" class="form-control input-lg" placeholder="City" tabindex="5" required value="{{$lead->city}}">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <label>State</label>
                                <select name="state" id="state" class="form-control input-lg" required tabindex="6">
                                    @foreach(App\Http\States::all() as $abbr => $full)
                                        <option value="{{$abbr}}">{{$full}}</option>
                                    @endforeach
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <label>Zip</label>
                                <input type="text" name="zip" class="form-control input-lg" placeholder="Zip" tabindex="7" value="{{$lead->zip}}">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <img src="/img/security.png" width="100%" class="center-block"/>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <img src="/img/bbb2.png" width="90%" class="center-block"/>
                        </div>
                        <div class="clearfix"></div>
                        <h3 class="text-center" style="font-size:16px;font-weight:bold;margin-top:10px;">Your Satisfaction is 100% Guaranteed</h3>

                        <!--closes satisfaction_guarantee_container div-->

                        <input type="submit" class="btn btn-block btn-success btn-lg" value="Continue"/>
                    </div>
                </form>
            </div>
        </div>
    </div>

@stop