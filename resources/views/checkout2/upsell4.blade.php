@extends('checkout2.layout')


@section('content')
    <div class="container" id="wrap">
        <div class="row">
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $err)
                            <li>{{$err}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="col-xs-12 col-sm-8  col-sm-offset-2 ">
                <img src="/img/newkure.png" width="50%" class="center-block"/>
                <div class="panel-body text-center">
                    <div class="alert alert-default" role="alert">
                        <h3 style="font-size:1.3em;">Most of our customers find that it is much more convenient to pay for THE KURE today,
                            rather than tracking their payment 30 days from now.
                            <br><br>
                            And if for any reason you are not absolutely thrilled with the SECURITY,EASE OF USE AND POSSIBLE saving of hundreds
                            - even thousands - of dollars with THE KURE, you can return it no questions asked for a full product refund.
                            <br><br>
                            And as a special incentive to take advantage of our single pay, we will
                            give you <b style="text-decoration: underline;">FREE PRIORITY PROCESSING</b> for your entire order.
                        </h3>
                    </div>
                    <img src="/img/shipping-guyfree.png" style="width:40%;display:block;margin:10px auto 0;">
                </div>
                <form method="post" action="/purchase/7">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$lead->lead_id}}" name="lead_id"/>
                    <button type="submit" class="btn btn-success btn-block btn-lg" style="font-size:1.4em;" >Yes! I want FREE PRIORITY PROCESSING!</button>
                </form>
                <form method="post" action="/purchase/noswitch">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$lead->lead_id}}" name="lead_id"/>
                    <button type="submit" class="btn btn-default btn-block btn-lg" style="font-size:1.4em;" >No, continue with my Order</button>
                </form>
            </div>
            </div>
        </div>
    </div>
@stop