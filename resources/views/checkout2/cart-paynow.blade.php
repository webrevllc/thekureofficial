@extends('checkout2.layout')

@section('content')
    <style>
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{padding:0px;border:0;}
        /* Start by setting display:none to make this hidden.
   Then we position it in relation to the viewport window
   with position:fixed. Width, height, top and left speak
   speak for themselves. Background we set to 80% white with
   our animation centered, and no-repeating */
        .modaloverlay {
            display:    none;
            position:   fixed;
            z-index:    1000;
            top:        0;
            left:       0;
            height:     100%;
            width:      100%;
            background: rgba( 255,255,255,.7 )
            url('/img/processing.gif')
            50% 50%
            no-repeat;
        }

        /* When the body has the loading class, we turn
           the scrollbar off with overflow:hidden */
        body.loading {
            overflow: hidden;
        }

        /* Anytime the body has the loading class, our
           modal element will be visible */
        body.loading .modaloverlay {
            display: block;
        }
        .mybox{
            -webkit-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
            -moz-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
            box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
        }
    </style>

    <div class="container">
        <div class="col-xs-12 ">
            <div class="col-md-8 col-md-offset-2">
                <img src="/img/newkure.png" width="50%" class="center-block" style="margin-bottom:40px;"/>
                <div class="row">
                    <div class="well">
                        <button type="button" class="btn btn-success btn-lg pull-right mybox checkoutbtn"  id="checkoutbtn">
                            Click Here To Confirm Your Order
                        </button>
                        <div class="clearfix"></div>
                        <br>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Product</th>
                                <th>Quantity</th>
                                {{--<th class="text-center">Price</th>--}}
                                <th class="text-center">Price Today</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="col-xs-8">
                                    <div class="media">
                                        <span class="thumbnail pull-left"> <img class="media-object" src="/img/thekureproduct.png" style="width: 72px; height: 72px;"> </span>
                                        <div class="media-body">
                                            <h4 class="media-heading">The Kure</h4>
                                        </div>
                                    </div>
                                </td>
                                <td class="col-xs-1 " style="text-align: center">
                                    <input type="number" class="form-control qty" min="1" id="KURE" value="{{$lead->quantity}}">
                                </td>
                                {{--<td class="col-xs-2 text-center"><strong>$19.95</strong></td>--}}
                                <td colspan="2" class="col-xs-2 text-center">
                                    <strong>
                                        @if($lead->pay_now)
                                            ${{number_format(49.95 * $lead->quantity, 2)}}
                                        @else
                                            ${{number_format(24.95 * $lead->quantity, 2)}}
                                        @endif
                                    </strong></td>
                                <td class="col-xs-1"> </td>
                            </tr>
                            {{--<tr>--}}
                                {{--<td class="col-xs-6">--}}
                                    {{--<div class="media">--}}
                                        {{--<span class="thumbnail pull-left"> <img class="media-object" src="/img/cybersafe.jpg" style="width: 72px; height: 72px;"> </span>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<h4 class="media-heading">CyberSafePC</h4>--}}
                                            {{--<span><b>Identity theft software</b></span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</td>--}}
                                {{--<td class="col-xs-1 " style="text-align: center">--}}
                                    {{--{{$lead->quantity}}--}}
                                {{--</td>--}}

                                {{--<td colspan="2" class="col-xs-2 text-center"><span class="text-success" style="font-size:1.4em;"><strong>FREE</strong></span></td>--}}
                                {{--<td class="col-xs-1"></td>--}}
                            {{--</tr>--}}
                            @if($lead->lifetime)
                                <tr>
                                    <td class="col-xs-6">
                                        <div class="media">
                                            <span class="thumbnail pull-left"> <img class="media-object" src="/img/lifetime.png" style="width: 72px; height: 72px;"> </span>
                                            <div class="media-body">
                                                <h4 class="media-heading">Lifetime Subscription Add-On</h4>
                                                <span><strong>LIFETIME SUBSCRIPTION</strong></span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="col-xs-1 " style="text-align: center">
                                        {{--{{$lead->quantity}}--}}
                                        <input type="number" class="form-control qty" min="1" id="lifetime" value="{{$lead->lifetime}}" max="{{$lead->quantity}}">
                                    </td>
                                    {{--<td class="col-xs-2 text-center"><strong>$4.95</strong></td>--}}
                                    <td colspan="2" class="col-xs-2 text-center"><strong>${{number_format(39.95 * $lead->lifetime, 2)}}</strong></td>
                                    <td class="col-xs-1">
                                        <a type="button" class="btn btn-danger btn-block" href="/purchase/remove/lifetime/?id={{$lead->lead_id}}">
                                            <span class="glyphicon glyphicon-remove"></span> Remove
                                        </a>
                                    </td>
                                </tr>
                            @endif
                            @if($lead->five_year)
                                <tr>
                                    <td class="col-xs-6">
                                        <div class="media">
                                            <span class="thumbnail pull-left"> <img class="media-object" src="http://www.fit4females.com/wp-content/uploads/2015/03/SAVE-25.jpg" style="width: 72px; height: 72px;"> </span>
                                            <div class="media-body">
                                                <h4 class="media-heading">5 Years for The Price of 4!</h4>
                                                <span><strong>LIMITED TIME OFFER!</strong></span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="col-xs-1 " style="text-align: center">
                                        {{$lead->quantity}}
                                        {{--<input type="number" class="form-control qty" min="1" id="lifetime" value="{{$lead->lifetime}}" max="{{$lead->quantity}}">--}}
                                    </td>
                                    {{--<td class="col-xs-2 text-center"><strong>$4.95</strong></td>--}}
                                    <td colspan="2" class="col-xs-2 text-center"><strong>${{number_format(59.85 * $lead->quantity, 2)}}</strong></td>
                                    <td class="col-xs-1">
                                        <a type="button" class="btn btn-danger btn-block" href="/purchase/remove/five_year?id={{$lead->lead_id}}">
                                            <span class="glyphicon glyphicon-remove"></span> Remove
                                        </a>
                                    </td>
                                </tr>
                            @endif
                            @if($lead->HWMON)
                                <tr>
                                    <td class="col-xs-6">
                                        <div class="media">
                                            <span class="thumbnail pull-left"> <img class="media-object" src="/img/upsells/hwmon.png" style="width: 72px; height: 72px;"> </span>
                                            <div class="media-body">
                                                <h4 class="media-heading">Hardware Monitoring</h4>
                                                <span><strong>Get the alert if anything goes wrong in real-time!</strong></span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="col-xs-1 " style="text-align: center">
                                        <input type="number" class="form-control qty" min="1" id="HWMON" value="{{$lead->HWMON}}" max="{{$lead->quantity}}">
                                    </td>
                                    {{--<td class="col-xs-2 text-center"><strong>$4.95</strong></td>--}}
                                    <td colspan="2" class="col-xs-2 text-center">
                                        <strong>
                                            @if($lead->pay_now)
                                                ${{number_format(9.90 * $lead->HWMON, 2)}}
                                            @else
                                                ${{number_format(4.95 * $lead->HWMON, 2)}}
                                            @endif
                                        </strong>
                                    </td>
                                    <td class="col-xs-1">
                                        <a type="button" class="btn btn-danger btn-block" href="/purchase/remove/HWMON/?id={{$lead->lead_id}}">
                                            <span class="glyphicon glyphicon-remove"></span> Remove
                                        </a>
                                    </td>
                                </tr>
                            @endif
                            @if($lead->PCOS)
                                <tr>
                                    <td class="col-xs-6">
                                        <div class="media">
                                            <span class="thumbnail pull-left"> <img class="media-object" src="/img/upsells/pcos.png" style="width: 72px; height: 72px;"> </span>
                                            <div class="media-body">
                                                <h4 class="media-heading">PC Cleaner On Steroids</h4>
                                                <span><strong>Slow computer? Try this!</strong></span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="col-xs-1 " style="text-align: center">
                                        <input type="number" class="form-control qty" min="1" id="PCOS" value="{{$lead->PCOS}}" max="{{$lead->quantity}}">
                                    </td>
                                    {{--<td class="col-xs-2 text-center"><strong>$8.95</strong></td>--}}
                                    <td colspan="2" class="col-xs-2 text-center"><strong>${{number_format(8.95 * $lead->PCOS, 2)}}</strong></td>
                                    <td class="col-xs-1">
                                        <a type="button" class="btn btn-danger btn-block" href="/purchase/remove/PCOS/?id={{$lead->lead_id}}">
                                            <span class="glyphicon glyphicon-remove"></span> Remove
                                        </a>
                                    </td>
                                </tr>
                            @endif
                            @if($lead->PROC || $lead->pay_now)
                                <tr>
                                    <td class="col-xs-6">
                                        <div class="media">
                                            <span class="thumbnail pull-left" > <img class="media-object" src="/img/shipping-guy.png" style="width: 72px; height: 72px;"> </span>
                                            <div class="media-body">
                                                <h4 class="media-heading">Expedited Processing</h4>
                                                <span><strong>For your whole order</strong></span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="col-xs-1 " style="text-align: center">-</td>
                                    {{--<td class="col-xs-2 text-center"><strong>$1.95</strong></td>--}}
                                    <td colspan="2" class="col-xs-2 text-center"><strong>
                                            @if($lead->pay_now)
                                                FREE
                                            @else
                                                $1.99
                                            @endif
                                        </strong></td>
                                    <td class="col-xs-1">
                                        <a type="button" class="btn btn-danger btn-block" href="/purchase/remove/PROC/?id={{$lead->lead_id}}">
                                            <span class="glyphicon glyphicon-remove"></span> Remove
                                        </a>
                                    </td>
                                </tr>
                            @endif
                            @if($lead->EMER)
                                <tr>
                                    <td class="col-xs-6">
                                        <div class="media">
                                            <span class="thumbnail pull-left"> <img class="media-object" src="/img/upsells/911.png" style="width: 72px; height: 72px;"> </span>
                                            <div class="media-body">
                                                <h4 class="media-heading">Emergency 911 Service</h4>
                                                <span><strong>Get help NOW!</strong></span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="col-xs-1 " style="text-align: center">
                                        <input type="number" class="form-control qty" min="1" id="emer" value="{{$lead->EMER}}" max="{{$lead->quantity}}">
                                    </td>
                                    {{--<td class="col-xs-2 text-center"><strong>$8.95</strong></td>--}}
                                    <td colspan="2" class="col-xs-2 text-center"><strong>${{number_format(19.95 * $lead->EMER, 2)}}</strong></td>
                                    <td class="col-xs-1">
                                        <a type="button" class="btn btn-danger btn-block" href="/purchase/remove/EMER/?id={{$lead->lead_id}}">
                                            <span class="glyphicon glyphicon-remove"></span> Remove
                                        </a>
                                    </td>
                                </tr>
                            @endif

                            </tbody>
                        </table>
                        <div class="clearfix"></div>
                        <button class="btn  btn-lg btn-success" id="updateCart" onclick="updateCart()">Update Cart</button>
                        <div class="clearfix"></div>
                        <form role="form" data-toggle="validator" >
                            <div class="col-md-8">
                                <table>
                                    <tr>
                                        <td colspan="2"><h4>Subtotal</h4></td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-right">
                                            <h4>
                                                <strong>
                                                    @if($lead->pay_now)
                                                    ${{number_format($lead->subtotal, 2)}}
                                                    @else
                                                        ${{number_format($lead->today_total - 5.95, 2)}}
                                                    @endif
                                                </strong>
                                            </h4>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="2"><h4>Shipping</h4></td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-right"><h4><strong>$5.95</strong></h4></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><h4>Total</h4></td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-right">
                                            <h4>
                                                <strong>
                                                    @if($lead->pay_now)
                                                        ${{number_format($lead->total, 2)}}
                                                    @else
                                                        ${{number_format($lead->today_total, 2)}}
                                                    @endif
                                                </strong>
                                            </h4>
                                        </td>
                                    </tr>
                                    @if(! $lead->pay_now)
                                    <tr>
                                        <td colspan="4" class="text-center"><h4>The Remaining balance of <strong>${{number_format($lead->next_month, 2)}}</strong> will be charged 30 days from now</h4></td>
                                    </tr>
                                    @endif
                                    <tr>
                                        {{--<td class="col-xs-6" ><button class="btn  btn-info btn-block"><i class="fa fa-refresh"></i> Switch To Pay Now</button></td>--}}
                                        <td colspan="5">By clicking the "Confirm your order" button you agree to our <a target="_blank" href="/terms">Terms and Conditions</a></td>
                                    </tr>
                                </table>
                                <div class="checkbox">
                                    <label>
                                        {{--<input type="checkbox" required> I have read and agree to the <a target="_blank" href="/terms">Terms and Conditions</a>--}}
                                    </label>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-4" style="margin-top:30px;">
                                <img class="checkoutbtn" width="100%" style="margin-top: -70px; cursor:pointer" src="/img/order.png"/>
                                {{--<button type="button" class="btn btn-success btn-lg pull-right mybox checkoutbtn"  id="checkoutbtn">--}}
                                    {{--Click Here To Confirm Your Order--}}
                                {{--</button>--}}
                                {{--<a href="#"  id="checkoutbtn" >--}}
                                    {{--<img src="http://www.tandurust.co.in/images/order_now.png" width="100%"/>--}}
                                {{--</a>--}}
                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="oops" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h2 class="modal-title" id="myModalLabel">OOPS!</h2>
                </div>
                <div class="modal-body">
                    <h4>You are 'almost' finished with your order. Just one more CLICK and THE
                        KURE will be on its way to you. Just click the 'Checkout' button at the bottom of this page.</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Okay</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h2 class="modal-title" id="myModalLabel">Oops!</h2>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger">
                        <h4 id="theIssue"></h4>
                        <h5>If your card was declined, hit your browsers back button until you get to the credit card page and try a new card.</h5>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Okay</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $body = $("body");

        $(document).on("click", function(){

        });

        $(".checkoutbtn").click(function(){
            $body.addClass("modaloverlay");
            $.ajax({
                type: "GET",
                url: '/purchasing?id={{$lead->lead_id}}',
                success:function(stuff){
                    if(stuff.length > 8){
                        $body.removeClass("modaloverlay");
                        $('#theIssue').html(stuff);
                        $('#errorModal').modal('show');
                    }else{
                        window.location = '/thank-you?t=' + stuff;
                    }
                },
                error:function(stuff){
                    console.log(stuff);
                }
            });
        });


        $(document).on("click touchstart", "#updateCart", function () {
            updateCart();
        });

        var updateCart = function(){
            hwmon = $('#HWMON').val();
            kure =  $('#KURE').val();
            pcos =  $('#PCOS').val();
            lifetime =  $('#lifetime').val();
            EMER =  $('#emer').val();
            id = '{{$lead->lead_id}}';
            var products = {};
            if(typeof hwmon != 'undefined'){
                products.HWMON =hwmon;
            }
            if(typeof kure != 'undefined'){
                products.KURE =kure;
            }
            if(typeof pcos != 'undefined'){
                products.PCOS =pcos;
            }
            if(typeof lifetime != 'undefined'){
                products.lifetime = lifetime;
            }
            if(typeof EMER != 'undefined'){
                products.EMER = EMER;
            }
            products.id = id;
            $.ajax({
                type: "POST",
                url: '/purchase/qty',
                data: products,
                success:function(stuff){
//                    console.log(stuff);
                    window.location = self.location;
                    location.reload(true);
                },
                error:function(stuff){
//                    console.log(stuff);
                    window.location = self.location;
                    location.reload(true);
                }
            });
        };

//        ouibounce(false, {
//            callback: function() {
//                $('#oops').modal('show');
//            },
//            aggressive: true
//        });
    </script>

    {{--<script language="JavaScript">--}}
        {{--window.onbeforeunload = confirmExit;--}}
        {{--function confirmExit() {--}}
            {{--return "OOPS! You are 'almost' finished with your order. Just one more CLICK and THE  KURE will be on its way to you. Just click the 'Checkout' button at the bottom of this page.";--}}
        {{--}--}}
    {{--</script>--}}
@stop

