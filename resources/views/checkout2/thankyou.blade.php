@extends('checkout2.layout')

@section('header')
    <style>
        /*
    Step Bar which indicates progress when the user arrives at the step, and
    indicates the end of the progress with a different shape and color.
    */
        .bs-wizard {margin-top: 40px;}

        .bs-wizard {padding: 0 0 10px 0;}
        .bs-wizard > .bs-wizard-step {padding: 0; position: relative;}
        .bs-wizard > .bs-wizard-step + .bs-wizard-step {}
        .bs-wizard > .bs-wizard-step .bs-wizard-stepnum {color: #595959; font-size: 16px; margin-bottom: 5px;}
        .bs-wizard > .bs-wizard-step .bs-wizard-info {color: #999; font-size: 14px;}
        .bs-wizard > .bs-wizard-step > .bs-wizard-dot {position: absolute; width: 30px; height: 30px; display: block; background: orangered; top: 45px; margin-top: -15px; margin-left: -15px; border-radius: 50%;}
        .bs-wizard > .bs-wizard-step > .bs-wizard-dot:after {content: ' '; width: 14px; height: 14px; background: orange; border-radius: 50px; position: absolute; top: 8px; left: 8px; }
        .bs-wizard > .bs-wizard-step > .progress {position: relative; border-radius: 0px; height: 8px; box-shadow: none; margin: 20px 0;}
        .bs-wizard > .bs-wizard-step > .progress > .progress-bar {width:0px; box-shadow: none; background: orangered;}
        .bs-wizard > .bs-wizard-step:first-child.active > .progress > .progress-bar {width:0%;}
        .bs-wizard > .bs-wizard-step:first-child  > .progress {}
        .bs-wizard > .bs-wizard-step:last-child  > .bs-wizard-dot:last-child {background-color: #f5f5f5;left:100%;border-radius:0%}
        .bs-wizard > .bs-wizard-step:last-child  > .bs-wizard-dot:last-child:after {opacity: 0;}
        /*Definitions only for active status*/
        .bs-wizard > .bs-wizard-step.active .bs-wizard-stepnum {font-weight:bold}
        /*Definitions for disabled status*/
        .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot {background-color: #f5f5f5;}
        .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot:after {opacity: 0;}
        .bs-wizard > .bs-wizard-step.disabled a.bs-wizard-dot{ pointer-events: none; }
        .bs-wizard > .bs-wizard-step.disabled .bs-wizard-stepnum {color: #d5d5d5;}
        /*Definitions for complete status*/
        .bs-wizard > .bs-wizard-step.complete > .progress > .progress-bar {width:100%;}
        .bs-wizard > .bs-wizard-step.complete:last-child  > .bs-wizard-dot:last-child {background-color: #005A96;}
        .bs-wizard > .bs-wizard-step.complete:last-child  > .bs-wizard-dot:last-child:after {opacity: 1;}
        .bs-wizard > .bs-wizard-step.complete .bs-wizard-stepnum {color: #005A96;opacity: 0.5;}

    </style>
@stop
@section('content')
    {{--@if(! empty($data))--}}
        {{--<iframe width="1px" height="1px" src="//www.statisticspc.com/ActivateOrder/?ifr=1&vsid={{$data['vsid']}}&ItemSku0=TOTAL&ItemPrice0={{$data['amount']}}"></iframe>--}}
    {{--@endif--}}
        <div class="container">
            <div class="row">
                <h4 style="font-size:25px;" class="text-center">
                    Thank you for your purchase of
                </h4>
                <br>
                <img src="img/newkure.png" style="width:50%" class="center-block"/>
                <div class="col-md-10 col-md-offset-1 ">
                    <div class="alert alert-info" role="alert">
                        <h4 class="text-center">We are busy creating your unique membership cards for The KURE! Once we ship them an email will be sent to the address you provided. Typically we ship all orders within 24-48 hours.</h4>
                    </div>
                    <div class=" bs-wizard" style="border-bottom:0;">
                        <div class="col-xs-4 bs-wizard-step active ">
                            <div class="text-center bs-wizard-stepnum">Creating Membership Cards</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <span class="bs-wizard-dot"></span>
                        </div>
                        <div class="col-xs-4 bs-wizard-step disabled"><!-- complete -->
                            <div class="text-center bs-wizard-stepnum">Shipped</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <span class="bs-wizard-dot"></span>
                        </div>
                        <div class="col-xs-4 bs-wizard-step disabled"><!-- complete -->
                            <div class="text-center bs-wizard-stepnum">Protected By The KURE</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <span class="bs-wizard-dot"></span>
                            <span class="bs-wizard-dot"></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="panel panel-info">
                        <div class="panel-heading">Order Details</div>
                        <div class="panel-body">
                            <h4 style="font-weight:bold;text-decoration: underline;">Your Information</h4>
                            Name: {{ucwords(strtolower($order->user->name))}}<br>
                            Email: {{$order->user->email}}<br><br>
                            <h4 style="font-weight:bold;text-decoration: underline;">Order Information</h4>
                            Invoice Number: {{$order->invoice_number}}<br>
                            Total Paid:
                            @if($order->amount == 'Free Trial')
                                Free Trial<br>
                            @else
                                ${{number_format($order->amount, 2)}}<br>
                            @endif

                            @if($order->status == "2pay")
                                <h4 style="font-weight:bold;text-decoration: underline;">Two Easy Payments</h4>
                                You will be billed ${{number_format($sub->amount, 2)}}

                                @if($order->status == "trial")
                                    30 days from the date that you receive your software.
                                @else
                                    on {{date('F j, Y', strtotime($order->subscription->next_process_date))}}.
                                    {{--unless you cancel your subscription.--}}
                                @endif
                                <br>

                            @endif

                            <br>
                            @if($order->hasEMER)
                                <div class="alert alert-warning">
                                    <h4 style="font-weight:bold;text-decoration: underline; margin-top:0px;">Emergency 911 Information!</h4>
                                    <h5>You will be contacted by a live U.S. based technician within the next 12 to 24 hours. They will remove any and all viruses and malware. They will tune up and clean your PC while you wait for your edition of The KURE to arrive in the mail.</h5>
                                </div>
                            @endif

                            A receipt was emailed to the address above.<br>
                            You will also receive an email once your order has been shipped.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if($order->amount == 'Free Trial')
            <script type="text/javascript">
                var clicky_goal = { id: "304", revenue: "{{number_format($sub->amount, 2)}}" };
            </script>
        @else
            <script type="text/javascript">
                var clicky_goal = { id: "304", revenue: "{{number_format($order->amount, 2)}}" };
            </script>
        @endif

@stop