@extends('checkout2.layout')

@section('content')


    <div class="container" id="wrap">
        <div class="row">

            <div class="col-xs-12 col-sm-8  col-sm-offset-2 ">
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $err)
                                <li>{{$err}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <img src="/img/newkure.png" width="50%" class="center-block"/>
                <form role="form" data-toggle="validator" method="post" action="/purchase/1">
                    {{csrf_field()}}
                    <h3 class="text-center">Let's get you started!</h3>
                    {{--<hr class="colorgraph">--}}
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" name="first_name" id="first_name" class="form-control input-lg" placeholder="First Name" tabindex="1" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" name="last_name" id="last_name" class="form-control input-lg" placeholder="Last Name" tabindex="2" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="country">Country</label>
                                <select class="form-control input-lg" tabindex="3" required id="country" name="country" onChange="getState(this.value);">
                                    <option value="USA">USA</option>
                                    <option value="Canada">Canada</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label>Address Line 1</label>
                                <input type="text" name="add1" class="form-control input-lg" placeholder="Address" tabindex="3" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label>Address Line 2</label>
                                <input type="text" name="add2"  class="form-control input-lg" placeholder="Address" tabindex="4">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <label>City</label>
                                <input type="text" name="city" class="form-control input-lg" placeholder="City" tabindex="5" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <label>State</label>
                                <select name="state" id="state" class="form-control input-lg" required tabindex="6">

                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <label>Zip</label>
                                <input type="text" name="zip" class="form-control input-lg" placeholder="Zip" tabindex="7">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="tel" name="phone" id="phone" class="form-control input-lg" minlength="10" maxlength="10" placeholder="no dashes or spaces" required tabindex="8" />
                                <div class="help-block">(10 digits, do not include country code)</div>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label>Email Address</label>
                                <input type="email" name="email" id="email" class="form-control input-lg" placeholder="Email Address" tabindex="9" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label>Confirm Your Email Address</label>
                                <input type="email" name="email_confirmation" id="email" class="form-control input-lg" placeholder="Email Address" tabindex="10" data-match="#email" data-match-error="Whoops, these don't match" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label>Create a Password For Your New KURE Account</label>
                                <input type="password" name="password" id="password" class="form-control input-lg" placeholder="Password" tabindex="11" data-minlength="6" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label>Confirm Your Password</label>
                                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-lg" placeholder="Confirm Password" tabindex="12" data-match="#password" data-match-error="Whoops, these don't match" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    {{--<div class="row">--}}
                        {{--<div class="col-xs-4 col-sm-3 col-md-3">--}}
                {{--<span class="button-checkbox">--}}
                    {{--<button type="button" class="btn" data-color="info" tabindex="7">I Agree</button>--}}
                    {{--<input type="checkbox" name="t_and_c" id="t_and_c" class="hidden" value="1">--}}
                {{--</span>--}}
                        {{--</div>--}}
                        {{--<div class="col-xs-8 col-sm-9 col-md-9">--}}
                            {{--By clicking <strong class="label label-primary">Register</strong>, you agree to the <a href="#" data-toggle="modal" data-target="#t_and_c_m">Terms and Conditions</a> set out by this site, including our Cookie Use.--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<hr class="colorgraph">--}}

                    <div class="row">
                        <input type="hidden" name="src" value="{{$source}}"/>
                        <div class="col-xs-12"><input type="submit" value="Continue" class="btn btn-success btn-block btn-lg" tabindex="12"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<script>
    $(function() {
        getState("USA");
    });

    function getState(val) {
        $.ajax({
            type: "POST",
            url: "/get_state",
            data:'country='+val,
            success: function(data){
                $("#state").html(data);
            }
        });
    }
</script>

@stop