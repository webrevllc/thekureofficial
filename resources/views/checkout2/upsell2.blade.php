@extends('checkout2.layout')


@section('content')
    <div class="container" id="wrap">
        <div class="row">
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $err)
                            <li>{{$err}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="col-xs-12 col-sm-8  col-sm-offset-2 ">

                <img src="/img/newkure.png" width="50%" class="center-block"/>
                <div id="cybersafe">
                    <div class="panel-body text-center">
                        <h3 style="color:#31708f;">Cleans PC in just one click</h3>
                        <h3 style="color:#31708f;">Removes performance-hindering clutter</h3>
                        <h3 style="color:#31708f;">Repairs Windows registry problems</h3>
                        <h3 style="color:#31708f;">Speed up your computer</h3>
                        <h3 style="color:#31708f;">Fixes over 27,000 problems and errors with one click</h3>
                        <h3 style="color:#31708f!important; font-size: 1.8em;">Only $8.95/yr!</h3>
                        <img src="/img/upsells/pcos.png" style="width:50%;display:block;margin:10px auto 0; font-size: 1.4em;">
                    </div>

                    <form method="post" action="/purchase/5">
                        {{csrf_field()}}
                        <input type="hidden" value="{{$lead->lead_id}}" name="lead_id"/>
                        <button type="submit" class="btn btn-success btn-block" style="font-size:1.4em;" >Yes! Add this to my {{env('offer_status')}}!</button>
                        {{--<button type="submit" class="btn btn-success btn-block" style="font-size:1.4em;" >Yes! Add this to my free trial!</button>--}}
                    </form>
                    <a href="/purchase/no/PCOS?id={{$lead->lead_id}}" class="btn btn-default btn-block">No, thanks</a>

                </div>
            </div>
        </div>
    </div>
@stop