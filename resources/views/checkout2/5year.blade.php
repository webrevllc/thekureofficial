@extends('checkout2.layout')


@section('content')
    <style>

    </style>
    <div class="container" id="wrap">
        <div class="row">
            @if(Session::has('success'))
                <div class="alert alert-success" role="alert">
                    <h4>{{ Session::get('success') }}</h4>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger" role="alert">
                    <h4>{{ Session::get('error') }}</h4>
                </div>
            @endif
            <div class="col-xs-12 col-sm-8  col-sm-offset-2 ">
                <img src="/img/newkure.png" width="50%" class="center-block"/>
                <div class="panel-heading text-center" style="width:100%;">
                    <h1 class="page-heading ">BUY MORE<br>SAVE MORE!</h1>
                    <h4>FOR A LIMITED TIME ONLY!</h4>
                </div>
                <div class="panel-body">
                    <div class="alert alert-success" role="alert">
                        <h2 class="text-center" style="margin-top:0px;">
                            SAVE 25%<br>
                            GET 4 MORE YEARS OF THE KURE POWERED BY McAFEE<br>
                            FOR THE PRICE OF 3 YEARS<br>
                        </h2>
                    </div>
                        <h2 class="text-center">
                            <img src="/img/salesticker.jpg" style="width:38%;" class="center-block"><br>
                            <em>That’s an almost $80 value for only $59.85</em>
                        </h2>


                    {{--<h3 class="text-center">SPECIAL OFFER! Free Shipping on all orders!</h3>--}}
                    <form method="post" action="/purchase/5year">
                        {{csrf_field()}}
                        <input type="hidden" value="{{$lead->lead_id}}" name="lead_id"/>
                        <button type="submit" class="btn btn-success btn-block btn-lg text-center" >Yes I would love to save 25% on additional years for my order of The KURE</button>
                    </form>
                    <form method="post" action="/purchase/no5year">
                        {{csrf_field()}}
                        <input type="hidden" value="{{$lead->lead_id}}" name="lead_id"/>
                        <button type="submit" class="btn btn-default btn-block" >No, thanks</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop