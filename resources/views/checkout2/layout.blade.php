<!DOCTYPE html>
<html lang="en">

<head>
    {{--<script src="//cdn.optimizely.com/js/3724870335.js"></script>--}}
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="The Kure">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!--FAVICONS-->
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="https://bootswatch.com/cerulean/bootstrap.min.css?12" rel="stylesheet"/>
    <link rel="stylesheet" href="/css/c2/styles.css">


    <script src="https://code.jquery.com/jquery-latest.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="/js/jquery-ui.js"></script>
    {{--<script src="/js/sweetalert-dev.js"></script>--}}
    <script src="/js/validator.js"></script>
    {{--<script src="/js/ouibounce.js"></script>--}}
    <title>The Kure!</title>
    <link href='https://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
    <style>
        body{
            font-family: 'Ubuntu', sans-serif;
        }
        .media-body{
            padding-left:10px;}
    </style>
    @yield('header')
</head>

<body style="padding-top: 70px;">
@include('mel4.navbar')
@yield('content')
@include('flash')
@yield('footer')


@include('mel4.tracking')
@include('checkout2.mouseflow')
<div class="footer">
    <div class="container-fluid myfooter" style="height:100%">
        <div class="container text-center">
            <div class="col-xs-12">
                <h4>&copy; {{date('Y')}} The Kure. All rights reserved.</h4>
                <a href="/terms">Terms & Conditions</a> &middot; <a href="/privacy">Privacy Policy</a><br><br>
                <script type="text/javascript" src="https://sealserver.trustwave.com/seal.js?style=invert&code=b6bf09fa3d084ffe8d6550d51af43672"></script>
            </div>
        </div>
    </div>

</div>

</body>

</html>
