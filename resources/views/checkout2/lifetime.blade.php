@extends('checkout2.layout')


@section('content')
    <div class="container" id="wrap">
        <div class="row">
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $err)
                            <li>{{$err}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="col-xs-12 col-sm-8  col-sm-offset-2 ">
                <img src="/img/lifetime.png" style="width:100%;display:block;margin:10px auto 0;">
                <div class="panel-body text-center">
                    <div class="alert alert-default" role="alert">
                        <h2 style="font-size:1.5em;">As you know The KURE annual subscription is $19.95/year. As a Cyber Month,
                            one time only special, you can upgrade your annual subscription to a “Lifetime Subscription", for only $39.95, one time,
                            additional, per computer. This could save you as much as $100 or more in future renewals.
                            <br><br>
                            Your Lifetime Subscription includes, FREE Updates, FREE technical Support, FREE Expedited Shipping and you can transfer to a new computer unlimited times.
                        </h2>
                    </div>
                </div>
                <form method="post" action="/purchase/lifetime">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$lead->lead_id}}" name="lead_id"/>
                    <button type="submit" class="btn btn-success btn-block btn-lg" style="font-size:1.4em;" >Yes, I would like to upgrade to Lifetime for my main order!</button>
                </form>
                <form method="post" action="/purchase/noswitchagain">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$lead->lead_id}}" name="lead_id"/>
                    <button type="submit" class="btn btn-default btn-block btn-lg" style="font-size:1.4em;" >No, thanks</button>
                </form>
            </div>
        </div>
    </div>
@stop