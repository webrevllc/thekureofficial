@extends('checkout2.layout')


@section('content')
    <div class="container" id="wrap">
        <div class="row">
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $err)
                            <li>{{$err}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="col-xs-12 col-sm-8  col-sm-offset-2 ">

                <img src="/img/newkure.png" width="50%" class="center-block"/>
                <div class="panel-body text-center">
                    <div class="alert alert-success" role="alert">
                        <h3>Would you like your order to go to the top of the line? We will prioritize your entire order and rush it to you ASAP for <b>ONLY $1.99!</b></h3>
                    </div>
                    <img src="/img/shipping-guy.png" style="width:40%;display:block;margin:10px auto 0;">
                </div>
                <form method="post" action="/purchase/6">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$lead->lead_id}}" name="lead_id"/>
                    {{--<button type="submit" class="btn btn-success btn-block btn-lg" style="font-size:1.4em;" >Yes! Rush The Kure To Me, Add To My Free Trial!</button>--}}
                    <button type="submit" class="btn btn-success btn-block btn-lg" style="font-size:1.4em;" >Yes! Rush The Kure To Me!</button>
                </form>
                <form method="post" action="/purchase/freerushno">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$lead->lead_id}}" name="lead_id"/>
                    <button type="submit" class="btn btn-default btn-block " style="font-size:1.4em;" >No, thanks</button>
                </form>
            </div>
            </div>
        </div>
    </div>
@stop