@extends('checkout2.layout')


@section('content')
    <style>

    </style>
    <div class="container" id="wrap">
        <div class="row">
            @if(Session::has('success'))
                <div class="alert alert-success" role="alert">
                    <h4>{{ Session::get('success') }}</h4>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger" role="alert">
                    <h4>{{ Session::get('error') }}</h4>
                </div>
            @endif
            <div class="col-xs-12 col-sm-8  col-sm-offset-2 ">
                <img src="/img/newkure.png" width="50%" class="center-block"/>
                <div class="panel-heading" style="width:100%;">
                    <h2 class="page-heading text-center">How Many Would You Like To Add?</h2>
                </div>
                <div class="panel-body">

                    <h3 class="text-center">
                        Many of our customers would like to get additional Free Trials of The Kure for friends, family or employees.
                        <br><br>
                        <img src="/img/present.png" style="width:38%;" class="center-block">
                        So as a special offer, just today, you can order
                        <span style="text-decoration: underline;">additional Free Trials of the Kure with FREE Shipping and Handling, for each additional that you order!</span>
                        <br><br>
                        <em><b>That is a savings of about 13% on each FREE Trial.</b></em>
                    </h3>
                    {{--<h3 class="text-center">SPECIAL OFFER! Free Shipping on all orders!</h3>--}}
                    <form method="post" action="/purchase/qtyup">
                        {{csrf_field()}}
                        <input type="hidden" value="{{$lead->lead_id}}" name="lead_id"/>
                        <select name="quantity" class="input-lg form-control text-center">
                            <option value="2">1 @ $19.95 </option>
                            <option value="3">2 @ $19.95 each</option>
                            <option value="4">3 @ $19.95 each</option>
                            <option value="5">4 @ $19.95 each</option>
                        </select>
                        <br>
                        {{--<input type="number" id="quantity" name="quantity" class="input-lg form-control text-center" min="1" value="1" /><br><br>--}}
                        <button type="submit" class="btn btn-success btn-block btn-lg text-center" >Add To My Order</button>
                    </form>
                    <a href="/purchase/4?id={{$lead->lead_id}}" class="btn btn-default btn-block">No, thanks</a>
                </div>
            </div>
        </div>
    </div>
@stop