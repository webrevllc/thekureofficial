@extends('checkout2.layout')


@section('content')
    <div class="container" id="wrap">
        <div class="row">
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $err)
                            <li>{{$err}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="col-xs-12 col-sm-8  col-sm-offset-2 ">
                <img src="/img/newkure.png" width="50%" class="center-block"/>
                <div class="panel-body text-center">
                    <img src="/img/upsells/911.png" style="width:100%;display:block;margin:10px auto 0;">
                </div>
                <form method="post" action="/purchase/911yes">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$lead->lead_id}}" name="lead_id"/>
                    <button type="submit" class="btn btn-success btn-block btn-lg" style="font-size:1.4em;" >Yes, add this, I need help!*</button>
                </form>
                <form method="post" action="/purchase/911no">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$lead->lead_id}}" name="lead_id"/>
                    <button type="submit" class="btn btn-default btn-block btn-lg" style="font-size:1.4em;" >No, thanks</button>
                </form>
                <br>
                <p class="text-center">*Please note that this is obviously not part of the 2 easy payment offer and will be billed at this time.</p>
            </div>
        </div>
    </div>
@stop