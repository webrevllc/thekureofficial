@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart')

    <h4 class="secondary"><strong>NEW WEB TRIAL ORDER</strong></h4>
    <p>Trial order amount: ${{number_format($sub->amount, 2)}}</p>
    <p>{{$city}}, {{$state}}</p>
    <p>Source: {{$source}}</p>

    @include('beautymail::templates.widgets.articleEnd')


    @include('beautymail::templates.widgets.newfeatureStart')

    <h4 class="secondary"><strong>{{$user->name}}</strong></h4>
    @foreach(json_decode($order->products) as $product)
        <p>{{$product->name}} Qty: {{$product->quantity}}</p>
    @endforeach

    @include('beautymail::templates.widgets.newfeatureEnd')

@stop