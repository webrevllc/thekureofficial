@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart')

    <h4 class="secondary"><strong>The Kure - Customer Receipt</strong></h4>
    <h5>Thank you for your recent order. Here are you order details:</h5>
    <h5>Order Details:</h5>
    <p>Invoice #:{{$order->invoice_number}}</p>
    <p>Total Paid: ${{$order->amount}}</p>
    <p>Date of Purchase: {{ date('F j, Y',  strtotime($order->created_at))}}</p>

    @include('beautymail::templates.widgets.articleEnd')



@stop