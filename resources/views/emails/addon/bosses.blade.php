@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart')

    <h4 class="secondary"><strong>New AddOn Order</strong></h4>
    <p>${{$order->amount}}</p>

    @include('beautymail::templates.widgets.articleEnd')


@stop