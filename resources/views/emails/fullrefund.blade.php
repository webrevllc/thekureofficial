@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart')

    <h4 class="secondary"><strong>Hello {{$name}},</strong></h4>
    <h5>This is a courtesy email to let you know that we have recently processed a refund for your recent order.</h5>
    <h5>Amount: {{$refundInfo->amount}}</h5>
    <h5>Processed by: {{$refundInfo->by}}</h5>

    @include('beautymail::templates.widgets.articleEnd')

    @include('beautymail::templates.widgets.articleStart')
    <h5 class="secondary">You should receive the funds within 3-5 business days.</h5>

    @include('beautymail::templates.widgets.articleEnd')

@stop