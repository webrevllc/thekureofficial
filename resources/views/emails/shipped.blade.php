@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart')

    <h4 class="secondary"><strong>Great News {{$orderUser->user->name}}!</strong></h4>
    <h5>Your order for The Kure has shipped!</h5>
    <h5>Order Details:</h5>
    <p>Invoice #:{{$orderUser->invoice_number}}</p>
    <p>Total: ${{$orderUser->amount}}</p>
    <p>Date of Purchase: {{ date('F j, Y, g:i a',  strtotime($orderUser->created_at))}} (EST)</p>

    @include('beautymail::templates.widgets.articleEnd')

    @include('beautymail::templates.widgets.articleStart')
    <h5 class="secondary">Your Details:</h5>
    <p>Name: {{$orderUser->user->name}}</p>
    <p>Email: {{$orderUser->user->email}}</p>
    <br>
    <h5 class="secondary">Products Ordered:</h5>

    @foreach(json_decode($orderUser->products) as $product)
        <p>{{$product->name}} Qty: {{$product->quantity}}</p>
    @endforeach

    @include('beautymail::templates.widgets.articleEnd')

@stop