@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart')

    <h4 class="secondary"><strong>FAILED IVR ORDER</strong></h4>
    <p>${{$order->amount}}</p>
    <p>{{ date('m-d-Y H:i:s',  strtotime($order->created_at))}}</p>

    @include('beautymail::templates.widgets.articleEnd')


    @include('beautymail::templates.widgets.newfeatureStart')

    <h4 class="secondary"><strong>{{$user->name}}</strong></h4>
    <p>Email: {{$user->email}}</p>
    <p>Phone: {{$user->phone}}</p>
    <br>

    <h5>Order Summary</h5>
    @foreach(json_decode($order->products) as $product)
    <p>{{$product->name}} Qty: {{$product->quantity}}</p>
    @endforeach

    @include('beautymail::templates.widgets.newfeatureEnd')

@stop