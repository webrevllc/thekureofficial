@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart')

    <h4 class="secondary"><strong>The Kure - Customer Receipt</strong></h4>
    <h5>Welcome to The Kure Family! Here are you order details:</h5>
    <h5>Order Details:</h5>
    <p>Invoice #:{{$order->invoice_number}}</p>
    <p>Total Paid: ${{$order->amount}}</p>
    <p>Date of Purchase{{ date('m-d-Y H:i:s',  strtotime($order->created_at))}}</p>

    @include('beautymail::templates.widgets.articleEnd')

    @include('beautymail::templates.widgets.articleStart')
    <h5 class="secondary">Your Details:</h5>
    <p>Name: {{$user->name}}</p>
    <p>Email: {{$user->name}}</p>
    <br>
    <h5 class="secondary">Products Ordered:</h5>

    @foreach(json_decode($order->products) as $product)
        <p>{{$product->name}} Qty: {{$product->quantity}}</p>
    @endforeach

    @include('beautymail::templates.widgets.articleEnd')

@stop