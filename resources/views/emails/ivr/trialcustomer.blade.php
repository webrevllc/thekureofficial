@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart')

    <h4 class="secondary"><strong>The Kure - Customer Receipt</strong></h4>
    <h5>Welcome to The Kure Family! Here are you order details:</h5>
    <h5>Order Details:</h5>
    <p>Invoice #:{{$order->invoice_number}}</p>
    <p>Total Paid: FREE TRIAL</p>
    <p>Date of Purchase: {{ date('F j, Y, g:i a',  strtotime($order->created_at))}} (EST)</p>

    @include('beautymail::templates.widgets.articleEnd')

    @include('beautymail::templates.widgets.articleStart')
    <h5 class="secondary">Your Details:</h5>
    <p>Name: {{$user->name}}</p>
    <p>Email: {{$user->email}}</p>
    <br>
    <h5 class="secondary">Products Ordered:</h5>

    @foreach(json_decode($order->products) as $product)
        <p>{{$product->name}} Qty: {{$product->quantity}}</p>
    @endforeach

    <h5 style="font-weight:bold;text-decoration: underline;">Subscription Information</h5>
    You will be billed ${{number_format($sub->amount, 2)}} 30 days from the date that you receive your software.
    @include('beautymail::templates.widgets.articleEnd')

@stop