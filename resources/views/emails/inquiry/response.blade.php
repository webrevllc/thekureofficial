@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart')

    <h4 class="secondary"><strong>Hello {{$name}}!</strong></h4>

    @include('beautymail::templates.widgets.articleEnd')


    @include('beautymail::templates.widgets.newfeatureStart')

    <h4 class="secondary">
        <strong>

            {{$mymessage}}

        </strong>
    </h4>

    <br>
    <p><i>Please do not reply to this message.</i></p>

    @include('beautymail::templates.widgets.newfeatureEnd')

@stop