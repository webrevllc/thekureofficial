@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart')

    <h4 class="secondary"><strong>Hello {{$name}},</strong></h4>
    <h5>My name is Shane I am the Customer Service Manager from The Kure. You recently placed an order with us on {{date('F j, Y', strtotime($date))}} through our phone system. Your order however was not charged for shipping and handling and we will be processing your card for the remaining $5.95 shortly. Please, if you have any questions feel free to email me directly at <a href="mailto:shane@thekure.com">shane@thekure.com</a>.</a></h5>

    @include('beautymail::templates.widgets.articleEnd')

    @include('beautymail::templates.widgets.articleStart')
    <h5 class="secondary">Thank you for your understanding.</h5>
    <h5 class="secondary">Shane@TheKure</h5>

    @include('beautymail::templates.widgets.articleEnd')

@stop