@extends('auth.layout')
<style>
    h2{
        color:#000!important;
    }

    .theform, .lookup{
        margin-top:130px;
    }

    .lookup{display: none;}


    .form-signin-heading {
        text-align:center;
        margin-bottom: 30px;
    }



</style>
@section('content')
    <div class = "container theform">
        <h1 class="text-center">Sorry, there seems to have been a problem with that request. Please call us at:
            <br>
            <br>

            {{env('phone')}}

            <br>
            <br>
            or email us at <a href="mailto:questions@thekure.com">Questions@thekure.com</a></h1>
    </div>
    <div class="clearfix"></div>

@stop