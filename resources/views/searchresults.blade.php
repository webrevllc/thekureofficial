@extends('admin.layout')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Search Results <small>{{$name}}</small></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">

            @if(count($users)>0)
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-users fa-fw"></i> All Users Matching Search
                    <div class="pull-right">
                        {{count($users)}}
                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Member Since</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $customer)
                                        <tr>
                                            <td><a href='customers/{{$customer->id}}'>{{$customer->name}}</a></td>
                                            <td>{{$customer->email}}</td>
                                            <td>{{date("F j, Y", strtotime($customer->created_at))}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.col-lg-4 (nested) -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.panel-body -->
            </div>
            @else
                <div class="alert alert-danger">
                    <h1 style="margin-top:0px;">Sorry!</h1>
                    <p>No results match "{{$name}}"</p>
                </div>
            @endif
        </div>
    </div>
    <!-- /.row -->
@stop