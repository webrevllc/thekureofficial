<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>The Kure - Admin</title>

    <!-- Bootstrap Core CSS -->
    <link href="/css/app.css" rel="stylesheet">
    <link rel="stylesheet" href="https://bootswatch.com/cerulean/bootstrap.min.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/useraccount.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery -->
    <script src="/js/libs.js"></script>

</head>

<body>
    <div class="wrapper">
        @include('auth.navbar')
        @yield('content')
        <div class="push"></div>
    </div>
    <div class="footer">
        <div class="container-fluid myfooter" style="height:100%">
            <div class="container">
                <div class="col-xs-12 col-sm-6 col-md-4"></div>
                <div class="col-xs-12 col-sm-6 col-md-4 text-center"><h4>&copy; {{date('Y')}} The Kure. All rights reserved.</h4></div>
                <div class="col-xs-12 col-sm-6 col-md-4"></div>
            </div>
        </div>
    </div>




<!-- Custom Theme JavaScript -->
<script src="/js/admin/admin.js"></script>

</body>

</html>
