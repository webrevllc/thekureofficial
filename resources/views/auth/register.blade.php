@extends('auth.layout')
<style>
    h2{
        color:#000!important;
    }

    .lookup{
        margin-top:130px;
    }

    .form-signin-heading {
        text-align:center;
        margin-bottom: 30px;
    }

</style>
@section('content')
<div class = "container lookup">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
        @if(Session::has('success'))
            <div class="alert alert-success" role="alert">
                <h4>{{ Session::get('success') }}</h4>
            </div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-danger" role="alert">
                <h4>{{ Session::get('error') }}</h4>
            </div>
        @endif
        <form class="form-signin" method="POST" action="/auth/createAccount">
            {{--<img src="/img/newkure.png" class="center-block" style="width:80%"/>--}}
            {!! csrf_field() !!}
            <h2 class="form-signin-heading">Add your login information to your account.</h2>
            <input type="hidden" name="uid" value="{{$order->user->id}}"/>
            <div class="form-group">
                <label for="email">Enter your email address</label>
                <input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control input-lg" placeholder="jsmith@example.com" required="" autofocus="" />
            </div>

            <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" id="password" class="form-control input-lg" placeholder="Password" tabindex="11" data-minlength="6" required>
                <div class="help-block with-errors"></div>
            </div>

            <div class="form-group">
                <label>Confirm Your Password</label>
                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-lg" placeholder="Confirm Password" tabindex="12" data-match="#password" data-match-error="Whoops, these don't match" required>
                <div class="help-block with-errors"></div>
            </div>
            <button class="btn btn-lg btn-info btn-block"  name="Submit" value="Login" type="Submit">Create Your Account</button>
        </form>
    </div>
</div>
<div class="clearfix"></div>
@stop