@extends('auth.layout')
<style>
    h2{
        color:#000!important;
    }

    .theform, .lookup{
        margin-top:130px;
    }

    .lookup{display: none;}


    .form-signin-heading {
        text-align:center;
        margin-bottom: 30px;
    }



</style>
@section('content')
    <div class = "container theform">
        <div class="col-xs-12 col-sm-5 col-sm-offset-1" style="border-right: 1px solid #afafaf;padding:13px 39px 10px 42px;">
            <form class="form-signin" method="POST" action="/auth/login">
                {{--<img src="/img/newkure.png" class="center-block" style="width:80%"/>--}}
                {!! csrf_field() !!}
                <h2 class="form-signin-heading">Sign in to manage my account.</h2>
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control input-lg" placeholder="Username" required="" autofocus="" />
                </div>
                <div class="form-group">
                    <label for="email">Password</label>
                    <input type="password" class="form-control input-lg" name="password" placeholder="Password" required=""/>
                </div>

                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember"> Remember Me
                    </label>
                </div>
                <button class="btn btn-lg btn-info btn-block"  name="Submit" value="Login" type="Submit">Login</button>
                <a href="/password/email" class="text-center" style="font-weight:bold;text-decoration:underline;">Forgot password?</a>

                @if(count($errors) > 0)
                    <div class="alert alert-danger" style="margin-top:40px;">
                        <ul>
                            @foreach($errors->all() as $err)
                                <li>{{$err}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </form>
        </div>
        <div class="col-xs-12 col-sm-6">
            <h2 class="text-center" style="margin-top:180px;">If you purchased The Kure via the phone system and have not yet registered, <a href="/auth/lookup" id="r1">click here.</a></h2>
        </div>
    </div>
    <div class="clearfix"></div>



    <
@stop