@extends('auth.layout')
<style>
    h2{
        color:#000!important;
    }

    .lookup{
        margin-top:130px;
    }

    .form-signin-heading {
        text-align:center;
        margin-bottom: 30px;
    }

</style>
@section('content')
<div class = "container lookup">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
        @if(Session::has('success'))
            <div class="alert alert-success" role="alert">
                <h4>{{ Session::get('success') }}</h4>
            </div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-danger" role="alert">
                <h4>{{ Session::get('error') }}</h4>
            </div>
        @endif
        <form class="form-signin" method="POST" action="/auth/lookupAccount">
            {{--<img src="/img/newkure.png" class="center-block" style="width:80%"/>--}}
            {!! csrf_field() !!}
            <h2 class="form-signin-heading">Before we can log you in, we must find your account.</h2>
            <div class="form-group">
                <label for="serial">Any serial number on your account</label>
                <input type="text" id="serial" name="serial" value="{{ old('serial') }}" class="form-control input-lg" placeholder="1234-WXYZ-ABCD" required="" autofocus="" />
            </div>
            <div class="form-group">
                <label for="transaction">Transaction number from your order</label>
                <input type="text" class="form-control input-lg" name="transaction" value="{{ old('transaction') }}" placeholder="ABC123" required=""/>
            </div>


            <button class="btn btn-lg btn-info btn-block"  name="Submit" value="Login" type="Submit">Lookup Account</button>

            @if(count($errors) > 0)
                <div class="alert alert-danger" style="margin-top:40px;">
                    <ul>
                        @foreach($errors->all() as $err)
                            <li>{{$err}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </form>
    </div>
</div>
<div class="clearfix"></div>
@stop