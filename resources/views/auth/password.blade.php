@extends('auth.layout')
<style>


    .form-signin-heading {
        text-align:center;
        margin-bottom: 30px;
    }

</style>
@section('content')
    <div class = "container">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <form class="form-signin" method="POST" action="/password/email">
                <img src="/img/newkure.png" class="center-block" style="width:80%"/>
                {!! csrf_field() !!}
                <h2 class="form-signin-heading">Forgot your password? Just want to reset it?</h2>
                <input type="email" name="email" value="{{ old('email') }}" class="form-control input-lg" placeholder="Email Address" required="" autofocus="" />
                <br>
                <button class="btn btn-lg btn-info btn-block"  name="Submit" value="Reset my password" type="Submit">Reset My Password</button>
                @if(count($errors) > 0)
                    <div class="alert alert-danger" style="margin-top:40px;">
                        <ul>
                            @foreach($errors->all() as $err)
                                <li>{{$err}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </form>
        </div>

    </div>

@stop