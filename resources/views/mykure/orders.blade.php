@foreach($user->orders as $order)
    <div class="row">
        <div class="panel {{$order->status == 'paid' ? 'panel-success' : 'panel-info'}}" style="margin: 1em;">
            <div class="panel-heading">
                <h3>Invoice #{{$order->invoice_number}} -- {{ucfirst($order->status)}}</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    @if($order->status == 'paid')
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Upcoming Payment</h3>
                            </div>
                            <div class="panel-body">
                                <table class="table table-responsive">
                                    <tr>
                                        <th>Payment Amount</th>
                                        <th>${{number_format($order->amount, 2)}}</th>
                                    </tr>
                                    <tr>
                                        <th>Payment Date</th>
                                        {{--<th>{{$order->next_process_date != "0000-00-00" ? date("M j, Y", strtotime($order->next_process_date)) : "Not yet shipped"}}</th>--}}
                                    </tr>
                                </table>
                            </div>
                        </div>
                    @elseif($order->status == 'trial')
                        <table class="table table-bordered">
                            @foreach(json_decode($order->products) as $product)
                                <tr>
                                    <td>
                                        {{$product->name}}
                                    </td>
                                    <td class="text-right">
                                        ${{number_format(($product->price / 100), 2)}}
                                    </td>
                                    <td class="text-right">
                                        {{$product->quantity}}
                                    </td>
                                    <td class="text-right">
                                        ${{number_format(($product->price / 100) * $product->quantity, 2)}}
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="3">Total:</td>
                                <td class="text-right">{{$order->amount}}</td>
                            </tr>
                        </table>

                    @endif

                </div>

            </div>
        </div>
    </div>
@endforeach
