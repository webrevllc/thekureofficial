@extends("mykure.layout")
@section("content")

    @if(count($errors) > 0)
        <div class="alert alert-danger" style="margin-top: 1em;">
            <ul>
                @foreach($errors->all() as $err)
                    <li>{{$err}}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {{--Update Password Modal--}}
    <div class="modal fade" id="pwModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modify Subscription</h4>
                </div>
                <form action="/mykure/account/updatePassword" method="POST" data-toggle="validator">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" id="password" class="form-control input-lg"
                                   placeholder="Password" tabindex="11" required>

                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <label>Confirm Password</label>
                            <input type="password" name="password_confirmation" id="password_confirmation"
                                   data-match-error="Whoops, these don't match"
                                   data-match="#password" class="form-control input-lg" placeholder="Confirm Password"
                                   tabindex="12" required>

                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    {{--Update Billing Modal--}}
    <div class="modal fade" id="billingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modify Billing</h4>
                </div>
                <form action="/mykure/account/updateBilling" method="POST" data-toggle="validator">
                    {{csrf_field()}}
                    <div class="modal-body">

                        <div class="form-group col-md-12">
                            <label>Name on Card</label>
                            <input type="text" name="name" id="name" class="form-control"
                                   placeholder="Name on Card"
                                   value="{{ucwords(strtolower(auth()->user()->name))}}" required>

                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Card Number</label>
                            <input type="number" maxlength="16" name="card_number" id="card_number"
                                   class="form-control"
                                   placeholder="Card Number" required>

                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <label>Expiration Month</label>
                                <select name="month" id="month" class="form-control " required="">
                                    <option value="">month</option>
                                    <option value="01">01 - January</option>
                                    <option value="02">02 - February</option>
                                    <option value="03">03 - March</option>
                                    <option value="04">04 - April</option>
                                    <option value="05">05 - May</option>
                                    <option value="06">06 - June</option>
                                    <option value="07">07 - July</option>
                                    <option value="08">08 - August</option>
                                    <option value="09">09 - September</option>
                                    <option value="10" selected="">10 - October</option>
                                    <option value="11">11 - November</option>
                                    <option value="12">12 - December</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Expiration Year</label>
                                <select name="year" id="year" class="form-control" required="">
                                    <option value="15">2015</option>
                                    <option value="16">2016</option>
                                    <option value="17">2017</option>
                                    <option value="18">2018</option>
                                    <option value="19">2019</option>
                                    <option value="20">2020</option>
                                    <option value="21">2021</option>
                                    <option value="22">2022</option>
                                    <option value="23">2023</option>
                                    <option value="24">2024</option>
                                </select>
                            </div>

                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-md-12" style="margin-top: 1em;">
                            <label>Billing Address</label>
                            <input type="text" name="address" id="address" class="form-control"
                                   placeholder="Billing Address" required>

                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12 col-sm-4">
                                <div class="form-group">
                                    <label>City</label>
                                    <input type="text" name="city" class="form-control" placeholder="City" tabindex="5"
                                           required="">

                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="form-group">
                                    <label>State</label>
                                    <select name="state" id="state" class="form-control" required="" tabindex="6">
                                        <option value="AL">ALABAMA</option>
                                        <option value="AZ">ARIZONA</option>
                                        <option value="AR">ARKANSAS</option>
                                        <option value="CA">CALIFORNIA</option>
                                        <option value="CO">COLORADO</option>
                                        <option value="CT">CONNECTICUT</option>
                                        <option value="DE">DELAWARE</option>
                                        <option value="DC">DISTRICT OF COLUMBIA</option>
                                        <option value="FL">FLORIDA</option>
                                        <option value="GA">GEORGIA</option>
                                        <option value="ID">IDAHO</option>
                                        <option value="IL">ILLINOIS</option>
                                        <option value="IN">INDIANA</option>
                                        <option value="IA">IOWA</option>
                                        <option value="KS">KANSAS</option>
                                        <option value="KY">KENTUCKY</option>
                                        <option value="LA">LOUISIANA</option>
                                        <option value="ME">MAINE</option>
                                        <option value="MD">MARYLAND</option>
                                        <option value="MA">MASSACHUSETTS</option>
                                        <option value="MI">MICHIGAN</option>
                                        <option value="MN">MINNESOTA</option>
                                        <option value="MS">MISSISSIPPI</option>
                                        <option value="MO">MISSOURI</option>
                                        <option value="MT">MONTANA</option>
                                        <option value="NE">NEBRASKA</option>
                                        <option value="NV">NEVADA</option>
                                        <option value="NH">NEW HAMPSHIRE</option>
                                        <option value="NJ">NEW JERSEY</option>
                                        <option value="NM">NEW MEXICO</option>
                                        <option value="NY">NEW YORK</option>
                                        <option value="NC">NORTH CAROLINA</option>
                                        <option value="ND">NORTH DAKOTA</option>
                                        <option value="OH">OHIO</option>
                                        <option value="OK">OKLAHOMA</option>
                                        <option value="OR">OREGON</option>
                                        <option value="PA">PENNSYLVANIA</option>
                                        <option value="RI">RHODE ISLAND</option>
                                        <option value="SC">SOUTH CAROLINA</option>
                                        <option value="SD">SOUTH DAKOTA</option>
                                        <option value="TN">TENNESSEE</option>
                                        <option value="TX">TEXAS</option>
                                        <option value="UT">UTAH</option>
                                        <option value="VT">VERMONT</option>
                                        <option value="VI">VIRGIN ISLANDS</option>
                                        <option value="VA">VIRGINIA</option>
                                        <option value="WA">WASHINGTON</option>
                                        <option value="WV">WEST VIRGINIA</option>
                                        <option value="WI">WISCONSIN</option>
                                        <option value="WY">WYOMING</option>
                                    </select>

                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="form-group">
                                    <label>Zip</label>
                                    <input type="text" name="zip" class="form-control" placeholder="Zip" tabindex="7">

                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{--Update Shipping/Contact Modal--}}
    <div class="modal fade" id="shippingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modify Shipping Info</h4>
                </div>
                <form action="/mykure/account/updateBilling" method="POST" data-toggle="validator">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group col-md-12">
                            <label>Name</label>
                            <input type="text" name="name" id="name" class="form-control"
                                   placeholder="Name"
                                   value="{{ucwords(strtolower(auth()->user()->name))}}" required>

                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-md-12" style="margin-top: 1em;">
                            <label>Shipping Address</label>
                            <input type="text" name="address" id="address" class="form-control"
                                   placeholder="Shipping Address" required>

                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12 col-sm-4">
                                <div class="form-group">
                                    <label>City</label>
                                    <input type="text" name="city" class="form-control" placeholder="City" tabindex="5"
                                           required="">

                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="form-group">
                                    <label>State</label>
                                    <select name="state" id="state" class="form-control" required="" tabindex="6">
                                        <option value="AL">ALABAMA</option>
                                        <option value="AZ">ARIZONA</option>
                                        <option value="AR">ARKANSAS</option>
                                        <option value="CA">CALIFORNIA</option>
                                        <option value="CO">COLORADO</option>
                                        <option value="CT">CONNECTICUT</option>
                                        <option value="DE">DELAWARE</option>
                                        <option value="DC">DISTRICT OF COLUMBIA</option>
                                        <option value="FL">FLORIDA</option>
                                        <option value="GA">GEORGIA</option>
                                        <option value="ID">IDAHO</option>
                                        <option value="IL">ILLINOIS</option>
                                        <option value="IN">INDIANA</option>
                                        <option value="IA">IOWA</option>
                                        <option value="KS">KANSAS</option>
                                        <option value="KY">KENTUCKY</option>
                                        <option value="LA">LOUISIANA</option>
                                        <option value="ME">MAINE</option>
                                        <option value="MD">MARYLAND</option>
                                        <option value="MA">MASSACHUSETTS</option>
                                        <option value="MI">MICHIGAN</option>
                                        <option value="MN">MINNESOTA</option>
                                        <option value="MS">MISSISSIPPI</option>
                                        <option value="MO">MISSOURI</option>
                                        <option value="MT">MONTANA</option>
                                        <option value="NE">NEBRASKA</option>
                                        <option value="NV">NEVADA</option>
                                        <option value="NH">NEW HAMPSHIRE</option>
                                        <option value="NJ">NEW JERSEY</option>
                                        <option value="NM">NEW MEXICO</option>
                                        <option value="NY">NEW YORK</option>
                                        <option value="NC">NORTH CAROLINA</option>
                                        <option value="ND">NORTH DAKOTA</option>
                                        <option value="OH">OHIO</option>
                                        <option value="OK">OKLAHOMA</option>
                                        <option value="OR">OREGON</option>
                                        <option value="PA">PENNSYLVANIA</option>
                                        <option value="RI">RHODE ISLAND</option>
                                        <option value="SC">SOUTH CAROLINA</option>
                                        <option value="SD">SOUTH DAKOTA</option>
                                        <option value="TN">TENNESSEE</option>
                                        <option value="TX">TEXAS</option>
                                        <option value="UT">UTAH</option>
                                        <option value="VT">VERMONT</option>
                                        <option value="VI">VIRGIN ISLANDS</option>
                                        <option value="VA">VIRGINIA</option>
                                        <option value="WA">WASHINGTON</option>
                                        <option value="WV">WEST VIRGINIA</option>
                                        <option value="WI">WISCONSIN</option>
                                        <option value="WY">WYOMING</option>
                                    </select>

                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="form-group">
                                    <label>Zip</label>
                                    <input type="text" name="zip" class="form-control" placeholder="Zip" tabindex="7">

                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container well" style="margin-top:4em;">
        <div class="row">
            <div class="col-sm-12"><h1>{{ucwords(strtolower(auth()->user()->name))}}</h1></div>
            {{--<div class="col-sm-2"><a href="/users" class="pull-right"><img title="profile image" class="img-circle img-responsive" src="http://www.gravatar.com/avatar/28fd20ccec6865e2d5f0e1f4446eb7bf?s=100"></a></div>--}}
        </div>
        <div class="row">
            <div class="col-sm-4"><!--left col-->

                {{--<div class="panel panel-info">--}}
                    {{--<div class="panel-heading">Profile</div>--}}
                    {{--<div class="panel-body">--}}
                        {{--<ul class="list-group">--}}
                            {{--<li class="list-group-item text-right"><span class="pull-left"><strong>Joined</strong></span> {{date("F j, Y", strtotime(auth()->user()->created_at))}}</li>--}}
                            {{--<li class="list-group-item text-right"><span class="pull-left"><strong>Email</strong></span> {{strtolower(auth()->user()->email)}}</li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}

                <div class="panel panel-info">
                    <div class="panel-heading">Recent Orders </div>
                    <div class="panel-body">
                        @foreach($user->orders as $order)
                            <ul class="list-group">
                                <li class="list-group-item text-right"><span class="pull-left"><strong>Date</strong></span> {{date("F j, Y", strtotime($order->created_at))}}</li>
                                <li class="list-group-item text-right"><span class="pull-left"><strong>Amount</strong></span> ${{number_format($order->amount, 2)}}</li>
                            </ul>
                        @endforeach
                    </div>
                </div>

                <div class="panel panel-info">
                    <div class="panel-heading">Payment Information <i class="fa fa-dashboard fa-1x"></i><span class="pull-right"></span></div>
                    <div class="panel-body">
                        <ul class="list-group">
                            <li class="list-group-item text-right"><span class="pull-left"><strong>Next Payment</strong></span> {{date("F j, Y", strtotime($user->subscriptions[0]->next_process_date))}}</li>
                            <li class="list-group-item text-right"><span class="pull-left"><strong>Amount</strong></span> ${{$user->subscriptions[0]->amount}}</li>
                            <li class="list-group-item text-right"><span class="pull-left"><strong>Card Number</strong></span> {{$billing->xml->paymentProfile->payment->creditCard->cardNumber}}</li>
                        </ul>
                    </div>
                </div>

            </div><!--/col-3-->
            <div class="col-sm-8 ">

            </div><!--/tab-content-->
        </div><!--/col-9-->
    </div><!--/row-->
    @foreach($user->subscriptions as $index => $sub)
        <div class="modal fade" id="modifyModal-{{$index}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Modify Subscription</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-responsive">
                            <tr>
                                <th>Product</th>
                                <th>Quantity</th>
                                <th>Price</th>
                            </tr>
                            @foreach(json_decode($sub->products) as $product)
                                @if($product->name != "Shipping")
                                    <tr>
                                        <td>{{$product->name}}</td>

                                        <td><input type="number" value="{{$product->quantity}}"
                                                   max="{{$product->quantity}}" min="0"/></td>

                                        <td>${{number_format($product->price / 100, 2)}}</td>
                                    </tr>
                                @endif
                            @endforeach
                            <tr>
                                <td><a href="#" style="font-size: 1.4em;">Add Product</a></td>
                            </tr>
                            <tr>
                                <td>Total</td>
                                <td></td>
                                <td>${{number_format($sub->amount, 2)}}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach


    <div class="modal fade" id="cancelSubscription" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Cancel Subscription</h4>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <h3>Are you sure you want to cancel this subscription?</h3>
                        <h4>By cancelling this subscription you will not be charged on your next billing date. Any and all licenses associated to this subscription will terminate on that date.</h4>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="/mykure/cancel?sub={{$sub->id}}" type="button" class="btn btn-default" >Yes, Cancel</a>
                    <button type="button" class="btn btn-success" data-dismiss="modal">No, Keep My Subscription</button>
                </div>
            </div>
        </div>
    </div>
@stop