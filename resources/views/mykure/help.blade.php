@extends("mykure.layout")
@section("content")
    <div class="row">
        <h2>Help & Support</h2>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4>My Serial Numbers</h4>
            </div>
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <th>Serial Number</th>
                        <th>Invoice Number</th>
                        <th>Computer Name</th>
                        <th>Status</th>
                    </tr>
                    @foreach($serials as $serial)
                        @foreach($serial as $serialNum)
                            <tr>
                                <td>{{$serialNum->serial}}</td>
                                <td>{{$serialNum->transaction}}</td>
                                <td>{{$serialNum->friendlyName}}</td>
                                <td style="color: {{$serialNum->used == true ? 'darkgreen' : 'red'}};">{{$serialNum->used == true ? "Registered" : 'Not Registered'}}</td>
                            </tr>
                        @endforeach
                    @endforeach
                </table>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4>My Kure</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-9">

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <p style="font-size: 1.2em;">Forgot your password for The Kure? No problem! Enter your serial
                            number and we'll get your password for you.</p>
                        <label>Serial Number</label>

                        <div class="input-group">
                            <input class="form-control" id="serial" type="text" name="serial"
                                   placeholder="Serial Number"
                                   style="margin-right: 1em;"/>
                            <span class="input-group-btn">
                                <button id="btnGetKurePassword" class="btn btn-success" type="button"
                                        style="margin-left: 1em;">Retrieve
                                    Password
                                </button>
                            </span>
                        </div>
                        <div id="kurePassword" style="display: none; font-size: 1.4em; margin-top: 1em;">

                        </div>
                    </div>

                </div>
                <div class="row" style="margin-top: 4em;">
                    <div class="col-md-12">
                        <p style="font-size: 1.2em;">Need to download The Kure?</p>
                        <button class="btn btn-success" style="margin-top: 1em;"
                                onclick="window.location.href = 'https://www.thekure.com/download'">
                            Click here to download The Kure
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4>Need Help?</h4>
            </div>
            <div class="panel-body">
                <div class="col-md-3">
                    <img src="/img/expedite.jpg" width="100%"/>
                </div>
                <div class="col-md-9" style="padding-top: 1.5em;">
                    <p style="font-size: 1.2em;">Our support staff is available every day, from 8AM to 6PM EST.</p>

                    <p style="font-size: 1.2em;">Give us a call at <span
                                style="font-weight: bold;">1-855-837-2617</span></p>

                    <p style="font-size: 1.2em;">Or feel free to submit a ticket if you need help or have a question for
                        us.</p>
                    <button class="btn btn-success" style="margin-top: 2em;"
                            onclick="window.location.href = 'https://centuriontechnologies.desk.com/customer/portal/emails/new'">
                        Click here to submit a ticket
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $("#btnGetKurePassword").click(function () {

                var serial = $("#serial").val();
                var button = $(this);

                $(this).addClass('disabled');
                $(this).html("Loading...");

                $.ajax({
                    url: "/mykure/support/getKurePassword",
                    type: "POST",
                    data: "serial=" + serial,
                    success: function (result) {
                        if (result == "" || result == null) {
                            $("#kurePassword").text("No Kure password associated with this serial.").fadeIn();
                        } else {
                            $("#kurePassword").text("Your Kure password: " + result).fadeIn();
                        }
                        button.removeClass('disabled');
                        button.html("Retrieve Password");
                    },
                    error: function () {
                        $("#kurePassword").text("Could not find password").fadeIn();
                        button.removeClass('disabled');
                        button.html("Retrieve Password");
                    }
                });
            });
        });
    </script>
@stop