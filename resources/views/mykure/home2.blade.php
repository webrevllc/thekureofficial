@extends("mykure.layout")
@section("content")
<style>
    /* Tab Navigation */
    .nav-tabs {
        margin: 0;
        padding: 0;
        border: 0;
    }
    .nav-tabs > li > a {
        background: #DADADA;
        border-radius: 0;
        box-shadow: inset 0 -8px 7px -9px rgba(0,0,0,.4),-2px -2px 5px -2px rgba(0,0,0,.4);
    }
    .nav-tabs > li.active > a,
    .nav-tabs > li.active > a:hover {
        background: #F5F5F5;
        box-shadow: inset 0 0 0 0 rgba(0,0,0,.4),-2px -3px 5px -2px rgba(0,0,0,.4);
    }

    /* Tab Content */
    .tab-pane {
        background: #F5F5F5;
        box-shadow: 0 0 4px rgba(0,0,0,.4);
        border-radius: 0;
        padding: 10px;
    }

</style>
    @if(count($errors) > 0)
        <div class="alert alert-danger" style="margin-top: 1em;">
            <ul>
                @foreach($errors->all() as $err)
                    <li>{{$err}}</li>
                @endforeach
            </ul>
        </div>
    @endif

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li class="active">
                <a href="#profile" role="tab" data-toggle="tab">
                    <icon class="fa fa-user"></icon> Profile
                </a>
            </li>
            <li><a href="#subscriptions" role="tab" data-toggle="tab">
                    <i class="fa fa-refresh"></i> Subscriptions
                </a>
            </li>
            <li>
                <a href="#messages" role="tab" data-toggle="tab">
                    <i class="fa fa-shopping-cart"></i> Orders
                </a>
            </li>
            <li>
                <a href="#settings" role="tab" data-toggle="tab">
                    <i class="fa fa-dollar"></i> Update Billing
                </a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane fade active in" id="profile">
                @include('mykure.profile')
                <div class="clearfix"></div>
            </div>
            <div class="tab-pane fade" id="subscriptions">
                @include('mykure.subscriptions')
                <div class="clearfix"></div>
            </div>
            <div class="tab-pane fade" id="messages">
                @include('mykure.orders')
                <div class="clearfix"></div>
            </div>
            <div class="tab-pane fade" id="settings">
                @include('mykure.billing')
                <div class="clearfix"></div>
            </div>
        </div>


@stop