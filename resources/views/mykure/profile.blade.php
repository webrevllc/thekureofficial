<h3>Update Profile</h3>
<form class="form" action="" method="post" id="registrationForm">
    <div class="form-group">
        <div class="col-xs-12">
            <label for="first_name"><h4>Name</h4></label>
            <input type="text" class="form-control" name="first_name" id="first_name" title="enter your first name if any." value="{{auth()->user()->name}}">
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <label for="phone"><h4>Phone</h4></label>
            <input type="text" class="form-control" name="phone" id="phone" value="{{auth()->user()->phone}}" >
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <label for="email"><h4>Email</h4></label>
            <input type="email" class="form-control disabled" name="email" id="email" disabled="disabled" value="{{strtolower(auth()->user()->email)}}">
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-6">
            <label for="password"><h4>Password</h4></label>
            <input type="password" class="form-control" name="password" id="password" placeholder="password" title="enter your password.">
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-6">
            <label for="password2"><h4>Verify</h4></label>
            <input type="password" class="form-control" name="password2" id="password2" placeholder="confirm password" title="enter your password2.">
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <br>
            <button class="btn btn-info pull-right" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
            <div class="clearfix"></div>
        </div>
    </div>
</form>