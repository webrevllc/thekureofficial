<style>
    .spark-settings-tabs-stacked {
        border-radius: 0; }
    .spark-settings-tabs-stacked a {
        border-bottom: 1px solid rgba(0, 0, 0, 0.1);
        border-left: 3px solid transparent;
        font-size: 1.3em;
        color: #555; }
    .spark-settings-tabs-stacked a i {
        color: #aaa;
        position: relative; }
    .spark-settings-tabs-stacked li:last-child a {
        border-bottom: 0; }
    .spark-settings-tabs-stacked li.active a {
        border-left: 3px solid #2fa4e7; }
    .spark-settings-tabs-stacked li a:active, .spark-settings-tabs-stacked li a:hover, .spark-settings-tabs-stacked li a:link, .spark-settings-tabs-stacked li a:visited {
        background-color: white; }
</style>

<div class=" text-left" style="margin-top: 2em; border-bottom: solid 2px #ffffff;">
    <div class="">
        <h4>Account Owner:</h4>
        <h3 style="margin-top: 0;">{{ucwords(strtolower(auth()->user()->name))}}</h3>
    </div>
<br>
    <div class="">
        <h4>Member Since:</h4>
        <h3 style="margin-top: 0">{{date("F j, Y", strtotime(auth()->user()->created_at))}}</h3>
    </div>
</div>
<ul class="nav spark-settings-tabs-stacked" role="tablist" style="margin-top:40px;">
    <li role="presentation" class="{{Request::is('mykure') ? 'active' : ''}}"><a href="/mykure">Account/Billing Info</a></li>
    <li role="presentation" class="{{Request::is('mykure/support') ? 'active' : ''}}"><a href="/mykure/support">Help & Support</a></li>
</ul>