<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>My Kure Dashboard</title>

    <!-- Bootstrap Core CSS -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/css/admin/metis.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/css/libs.css" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://bootswatch.com/cerulean/bootstrap.min.css">
    <link href="/css/useraccount.css" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-latest.js?1123"></script>
    <script src="/js/bootstrap.min.js?1"></script>
    <script src="/js/sweetalert-dev.js?1"></script>

</head>

<body>
<div class="wrapper">
    @include('mykure.navbar')
    <div class="container">


        <div class="col-md-3">
            @include('mykure.sidebar')
        </div>
        <div class="col-md-9">
            @yield('content')
        </div>
    </div>
    <div class="push"></div>
</div>
<div class="footer">
    <div class="container-fluid myfooter" style="height:100%">
        <div class="container">
            <div class="col-xs-12 col-sm-6 col-md-4"></div>
            <div class="col-xs-12 col-sm-6 col-md-4 text-center"><h4>&copy; {{date('Y')}} The Kure. All rights reserved.</h4></div>
            <div class="col-xs-12 col-sm-6 col-md-4"></div>
        </div>
    </div>
</div>
@include('flash')
</body>

</html>
