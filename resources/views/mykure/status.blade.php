@extends('auth.layout')



@section('content')
    <style>
        /*
    Step Bar which indicates progress when the user arrives at the step, and
    indicates the end of the progress with a different shape and color.
    */
        .bs-wizard {margin-top: 40px;}

        .bs-wizard {padding: 0 0 10px 0;}
        .bs-wizard > .bs-wizard-step {padding: 0; position: relative;}
        .bs-wizard > .bs-wizard-step + .bs-wizard-step {}
        .bs-wizard > .bs-wizard-step .bs-wizard-stepnum {color: #595959; font-size: 16px; margin-bottom: 5px;}
        .bs-wizard > .bs-wizard-step .bs-wizard-info {color: #999; font-size: 14px;}
        .bs-wizard > .bs-wizard-step > .bs-wizard-dot {position: absolute; width: 30px; height: 30px; display: block; background: orangered; top: 45px; margin-top: -15px; margin-left: -15px; border-radius: 50%;}
        .bs-wizard > .bs-wizard-step > .bs-wizard-dot:after {content: ' '; width: 14px; height: 14px; background: orange; border-radius: 50px; position: absolute; top: 8px; left: 8px; }
        .bs-wizard > .bs-wizard-step > .progress {position: relative; border-radius: 0px; height: 8px; box-shadow: none; margin: 20px 0;}
        .bs-wizard > .bs-wizard-step > .progress > .progress-bar {width:0px; box-shadow: none; background: orangered;}
        .bs-wizard > .bs-wizard-step:first-child.active > .progress > .progress-bar {width:0%;}
        .bs-wizard > .bs-wizard-step:first-child  > .progress {}
        .bs-wizard > .bs-wizard-step:last-child  > .bs-wizard-dot:last-child {background-color: #f5f5f5;left:100%;border-radius:0%}
        .bs-wizard > .bs-wizard-step:last-child  > .bs-wizard-dot:last-child:after {opacity: 0;}
        /*Definitions only for active status*/
        .bs-wizard > .bs-wizard-step.active .bs-wizard-stepnum {font-weight:bold}
        /*Definitions for disabled status*/
        .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot {background-color: #f5f5f5;}
        .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot:after {opacity: 0;}
        .bs-wizard > .bs-wizard-step.disabled a.bs-wizard-dot{ pointer-events: none; }
        .bs-wizard > .bs-wizard-step.disabled .bs-wizard-stepnum {color: #d5d5d5;}
        /*Definitions for complete status*/
        .bs-wizard > .bs-wizard-step.complete > .progress > .progress-bar {width:100%;}
        .bs-wizard > .bs-wizard-step.complete:last-child  > .bs-wizard-dot:last-child {background-color: #005A96;}
        .bs-wizard > .bs-wizard-step.complete:last-child  > .bs-wizard-dot:last-child:after {opacity: 1;}
        .bs-wizard > .bs-wizard-step.complete .bs-wizard-stepnum {color: #005A96;opacity: 0.5;}

    </style>
    <div class="container">
        <div class="row">
            <h4 style="font-size:25px;" class="text-center">
                Thank you for your purchase of
            </h4>
            <br>
            <img src="img/newkure.png" style="width:50%" class="center-block"/>
            <div class="col-md-10 col-md-offset-1 ">
                <div class="alert alert-success" role="alert">
                    @if($order->printed && !$order->shipped)
                        <h4 class="text-center">We are busy creating your unique membership cards for The KURE! Once we ship them an email will be sent to the address you provided. Typically we ship all orders within 24-48 hours.</h4>
                    @elseif($order->shipped && !$order->activated)
                        <h4 class="text-center">We are busy creating your unique membership cards for The KURE! Once we ship them an email will be sent to the address you provided. Typically we ship all orders within 24-48 hours.</h4>
                    @elseif($order->activated)
                        <h4 class="text-center">We are busy creating your unique membership cards for The KURE! Once we ship them an email will be sent to the address you provided. Typically we ship all orders within 24-48 hours.</h4>
                    @endif
                </div>
                <div class=" bs-wizard" style="border-bottom:0;">
                    <div class="col-xs-4 bs-wizard-step {{$order->printed  ? 'complete' : 'active'}} ">
                        <div class="text-center bs-wizard-stepnum">Creating Membership Cards</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <span class="bs-wizard-dot"></span>
                    </div>
                    <div class="col-xs-4 bs-wizard-step {{$order->shipped && !$order->activated ? 'complete' : 'active'}}"><!-- complete -->
                        <div class="text-center bs-wizard-stepnum">Shipped</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <span class="bs-wizard-dot"></span>
                    </div>
                    <div class="col-xs-4 bs-wizard-step {{$order->activated ? 'complete' : 'active'}} {{!$order->shipped ? 'disabled' : 'active'}}"><!-- complete -->
                        <div class="text-center bs-wizard-stepnum">Protected By The KURE</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <span class="bs-wizard-dot"></span>
                        <span class="bs-wizard-dot"></span>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br>
            </div>
        </div>
    </div>


@stop