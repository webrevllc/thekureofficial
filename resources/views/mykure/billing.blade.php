<style type="text/css">
    ol{
        padding-left: 0;
        margin-left:0;
    }

    ol>li {
        list-style-type: none;
        margin-bottom: .5em;
    }

    ol>li input[type=radio] {
        display: -moz-inline-box;
        display: inline-block;
        vertical-align: middle;
    }

    ol>li label {
        display: -moz-inline-box;
        display: inline-block;
        vertical-align: middle;
    }
</style>
<div class="col-md-6">
    <h3>Default Billing Profile</h3>
    <h4>{{$defaultBilling->billTo->firstName}} {{$defaultBilling->billTo->lastName}}</h4>
    <h4>{{$defaultBilling->billTo->address}}</h4>
    <h4>{{$defaultBilling->billTo->city}}, {{$defaultBilling->billTo->state}} {{$defaultBilling->billTo->zip}}</h4>
    <h4>Card: {{$defaultBilling->payment->creditCard->cardNumber}}</h4>

</div>
<div class="col-md-6">
    <form method="post" target="_blank" action="https://secure.authorize.net/profile/manage">
        <input type="hidden" name="Token" value="{{$token}}" />
        <h4>Clicking the button below will open up a separate tab, once you have added/edited your information refresh this page and select your default billing information, then click "Save Changes"</h4>
        <button type="submit" class="btn btn-primary btn-block" >Manage my payment and shipping information</button>
    </form>
</div>
<div class="col-md-12">
    <h3>Other Billing Profiles</h3>
    <form method="post">
        {{csrf_field()}}
        <ol>
        @foreach($profile->paymentProfiles as $paymentProfile)
            <li>
                <input type="radio" name="billing" id="optionsRadios1" value="{{$paymentProfile->customerPaymentProfileId}}" />
                <label for="optionsRadios1">
                    <h5>{{$paymentProfile->billTo->firstName}} {{$paymentProfile->billTo->lastName}}</h5>
                    <h5>{{$paymentProfile->billTo->address}}</h5>
                    <h5>{{$paymentProfile->billTo->city}}, {{$paymentProfile->billTo->state}} {{$paymentProfile->billTo->zip}}</h5>
                    <h5>Card: {{$paymentProfile->payment->creditCard->cardNumber}}</h5>
                </label>
            </li>
        @endforeach
        </ol>
        <button type="submit" class="btn btn-success"><i class="fa fa-refresh"></i> Save Changes</button>
    </form>

</div>
<div class="clearfix"></div>




