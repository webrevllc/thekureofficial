@extends("mykure.layout")
@section("content")
@if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        <h4>{{ Session::get('success') }}</h4>
    </div>
@endif
@if(Session::has('error'))
    <div class="alert alert-danger" role="alert">
        <h4>{{ Session::get('error') }}</h4>
    </div>
@endif
    @foreach($x->subscriptions as $index => $sub)
        <div class="modal fade" id="modifyModal-{{$index}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Modify Subscription</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-responsive">
                            <tr>
                                <th>Product</th>
                                <th>Quantity</th>
                                <th>Price</th>
                            </tr>
                            @foreach(json_decode($sub->products) as $product)
                                @if($product->name != "Shipping")
                                    <tr>
                                        <td>{{$product->name}}</td>

                                        <td><input type="number" value="{{$product->quantity}}"
                                                   max="{{$product->quantity}}" min="0"/></td>

                                        <td>${{number_format($product->price / 100, 2)}}</td>
                                    </tr>
                                @endif
                            @endforeach
                            <tr>
                                <td><a href="#" style="font-size: 1.4em;">Add Product</a></td>
                            </tr>
                            <tr>
                                <td>Total</td>
                                <td></td>
                                <td>${{number_format($sub->amount, 2)}}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    <div class="row">
        <h2>My Subscriptions</h2>
        @foreach($x->subscriptions as $index => $sub)
            <div class="row">
                <div class="panel {{$sub->status == 'active' ? 'panel-primary' : 'panel-warning'}}"
                     style="margin: 1em;">
                    <div class="panel-heading">
                        <h3>Invoice #{{$sub->unique_id}} -- {{ucfirst($sub->status)}}
                            <span style="float: right;">Start Date: {{date("F j, Y", strtotime($sub->created_at))}}</span>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            @if($sub->status == 'active')
                                <div class="col-md-4">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Upcoming Payment</h3>
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-responsive">
                                                <tr>
                                                    <th>Payment Amount</th>
                                                    <th>${{number_format($sub->amount, 2)}}</th>
                                                </tr>
                                                <tr>
                                                    <th>Payment Date</th>
                                                    <th>{{$sub->next_process_date != "0000-00-00" ? date("M j, Y", strtotime($sub->next_process_date)) : "Not yet shipped"}}</th>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="col-md-4">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Products</h3>
                                    </div>
                                    <div class="panel-body">
                                        <table class="table table-responsive">
                                            <tr>
                                                <th>Product</th>
                                                <th>Quantity</th>
                                                <th>Price</th>
                                            </tr>
                                            @foreach(json_decode($sub->products) as $product)
                                                @if($product->name != "Shipping")
                                                    <tr>
                                                        <td>{{$product->name}}</td>

                                                        <td class="text-center">{{$product->quantity}}</td>

                                                        <td class="text-right">${{number_format($product->price / 100, 2)}}</td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                            <tr>
                                                <td style="font-weight:bold;">Total</td>
                                                <td></td>
                                                <td class="text-right" style="font-weight:bold;">${{number_format($sub->amount, 2)}}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Previous Payments</h3>
                                    </div>
                                    <div class="panel-body">
                                        <table class="table">
                                            <tr>
                                                <th>Payment Date</th>
                                                <th class="text-right">Payment Amount</th>
                                            </tr>
                                            @foreach($orders->orders as $order)
                                                @if($order->invoice_number == $sub->unique_id)
                                                    <tr>
                                                        <td>{{date("M j, Y", strtotime($order->created_at))}}</td>
                                                        <td class="text-right">${{number_format(floatval($order->amount), 2)}}</td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if($sub->status == 'active')
                        <div class="row">
                            <div class="col-md-12">
                                {{--<button class="btn btn-primary" id="btn-{{$index}}" data-toggle="modal"--}}
                                        {{--data-target="#modifyModal-{{$index}}">Modify--}}
                                    {{--Subscription--}}
                                {{--</button>--}}
                                <a href="/mykure/pdf?order={{$sub->order_id}}" target="_blank" class="btn btn-info"><i class="fa fa-print"></i> View Receipt</a>
                                <button class="btn btn-danger" data-toggle="modal" data-target="#cancelSubscription"><i class="fa fa-trash-o" ></i> Cancel Subscription</button>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>



    <div class="modal fade" id="cancelSubscription" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Cancel Subscription</h4>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <h3>Are you sure you want to cancel this subscription?</h3>
                        <h4>By cancelling this subscription you will not be charged on your next billing date. Any and all licenses associated to this subscription will terminate on that date.</h4>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="/mykure/cancel?sub={{$sub->id}}" type="button" class="btn btn-default" >Yes, Cancel</a>
                    <button type="button" class="btn btn-success" data-dismiss="modal">No, Keep My Subscription</button>
                </div>
            </div>
        </div>
    </div>
@stop