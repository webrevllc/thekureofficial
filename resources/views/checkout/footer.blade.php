<footer id="footer" class="nice-orange-gradient ie_clearfix_block  clearfix_block">
    <nav>
        <ul class="footer_links_list" role="navigation">

            <li class="footer_link">
                <a data-toggle="modal" data-target="#privacy">
                    <span class="footer_link_icon" id="footer_privacy"></span>
                    <span class="footer_text_link">Privacy Policy</span>
                </a>
            </li>

            <li class="footer_link">
                <a data-toggle="modal" data-target="#security">
                    <span class="footer_link_icon" id="footer_security"></span>
                    <span class="footer_text_link">Security Policy</span>
                </a>
            </li>

            <li class="footer_link">
                <a data-toggle="modal" data-target="#purchasing">
                    <span class="footer_link_icon" id="footer_purchasing"></span>
                    <span class="footer_text_link">Purchasing Policy</span>
                </a>
            </li>

            <li class="footer_link">
                <a data-toggle="modal" data-target="#returns">
                    <span class="footer_link_icon" id="footer_return"></span>
                    <span class="footer_text_link">Return Policy</span>
                </a>
            </li>

            <li class="footer_link">
                <a data-toggle="modal" data-target="#contact">
                    <span class="footer_link_icon" id="footer_contact"></span>
                    <span class="footer_text_link">Contact Options</span>
                </a>
            </li>

        </ul>

    </nav>

</footer>

<p class="text-center">&copy; {{date('Y')}} All Rights Reserved Centurion Technologies</p>

<div class="modal fade" id="privacy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Privacy Policy</h4>
            </div>
            <div >
                <div class="policy_content_container">
                    <p class="popup_content_heading">Information About You</p>
                    <p class="popup_content_text">The goal at Centurion Technologies is the 100% protection of your privacy and your data. Therefore we make very effort to  collect the "minimum" amount of personal information about you as possible. The information that is required only to complete your order and to provide you with updates to you software and any other future billings or to provide you with "excellent" customer service. We do not share any of your personal information with any third party!</p>
                    <hr class="popup_divider">
                    {{--<p class="popup_content_heading">Customer Service Correspondence</p>--}}
                    {{--<p class="popup_content_text"> We may collect information that identifies you personally when you submit comments, questions or suggestions to our Customer Service department. </p>--}}
                    {{--<hr class="popup_divider">--}}
                    <p class="popup_content_heading">Use of Your Personal and Non-Personal Information</p>
                    <p class="popup_content_text">We may use, sell, disclose, or otherwise dispose of Personal Information we collect in the following circumstances: </p>
                    <hr class="popup_divider">
                    <p class="popup_content_heading">Internal Analysis and Promotions</p>
                    <p class="popup_content_text">We may use information about you for our own internal statistical, design, operational purposes such as to estimate our audience size; measure aggregate traffic patterns; and understand demographic, customer interest, purchasing and other trends among our customers. We may also use your Personal Information for the marketing and/or promotion of our products or services. We may outsource all of these tasks to third parties on a confidential basis. If you have received an email from any members of the Centurion Technologies, and you wish to no longer receive emails, please click the unsubscribe link found at the bottom of the email. </p>
                    <hr class="popup_divider">
                    {{--<p class="popup_content_heading">Co-Branded Service Offerings</p>--}}
                    {{--<p class="popup_content_text"> We may share this information with certain businesses with whom Centurion Technologies has formed a strategic alliance. You may be able to identify these businesses because Centurion Technologies has authorized them to display co-branded Web pages or links to Centurion Technologies. These strategic partners are requested to clearly state their own privacy policies on their own web sites or promotional materials. </p>--}}
                    {{--<hr class="popup_divider">--}}
                    <p class="popup_content_heading">Transaction Processing</p>
                    <p class="popup_content_text">Obviously your Personal information will be used "only" to complete your order included in the online "shopping cart" which allows you to track your selected products and proceed through purchase. Also we are obviously  required to share transactional information with the entities necessary to process credit card payments  with your banking institution .</p>
                    <hr class="popup_divider">
                    <p class="popup_content_heading">We NEVER share you information with Third-Party marketers. NEVER!</p>
                    {{--<p class="popup_content_text"> We may arrange to have marketing and promotional information of other companies sent to you that we think might be of interest to you. </p>--}}
                    <hr class="popup_divider">
                    <p class="popup_content_heading">Legal Requirement</p>
                    <p class="popup_content_text">We may disclose personal information if required to do so by law or in the good-faith belief that such action is necessary to: (a) conform with the law, respond to claims or comply with legal process served on our company or the Site; (b) protect and defend our rights or property, the Site, our employees, customers or the public. </p>
                    {{--<p class="popup_content_heading">Company Sale</p>--}}
                    {{--<p class="popup_content_text"> Information collected through our Site is considered a trade secret of Centurion Technologies. As the owner of such information, we may disclose or sell such information as an asset of the company in conjunction with the sale to a third party of our company or a portion of our assets. </p>--}}
                    {{--<hr class="popup_divider">--}}
                    {{--<p class="popup_content_heading">Links To Other Sites</p>--}}
                    {{--<p class="popup_content_text"> Please be aware that we provide links to third-party Web sites as a service to our customers, and that we are not responsible for the content or information collection practices of those pages. Please note that these Web sites' privacy policies may differ from those of Centurion Technologies. We encourage your to review and understand their privacy practices before providing them with information. </p>--}}
                    {{--<hr class="popup_divider">--}}
                    <p class="popup_content_heading">Children</p>
                    <p class="popup_content_text">Our Site is a general audience site that is not designed nor intended to collect Personal Information from children under the age of 13. To respect the privacy of children and to comply with the Children's Online Privacy Protection Act, children under the age of 13 should not provide any Personal Information on this Site. We ask that parents supervise their children while online. </p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="security" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Security Policy</h4>
            </div>
            <div >
                <div class="policy_content_container">
                    <p class="popup_content_heading">Order Security</p>
                    <p class="popup_content_text">
                        Centurion Technologies realizes the importance of our customers having the same confidence using credit cards online as they do when ordering by phone. That is why we use industry-standard encryption (256 Bit SSL) to protect the confidentiality of the information you submit during checkout and ensure that your online purchase is safe with us.
                        <br>
                        <br>
                        Encryption is a process by which we use software to make your credit card number and personal information unreadable to others during the order process. Centurion Technologies currently uses the industry-standard SSL (Secure Sockets Layer) encryption to protect your information in transit on the majority of our sites. We believe in keeping up-to-date with new technologies in order to provide the best security to our customers. That means when you shop online with Centurion Technologies, you're buying with confidence.
                        <br>
                        <br>
                        If at any time you feel uncomfortable providing personal information online, you may place an order toll-free by phone at 1-855-837-2617.
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="purchasing" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Purchasing Policy</h4>
            </div>
            <div >
                <div class="policy_content_container">
                    <p class="popup_content_heading">Product Sales and Delivery</p>
                    <p class="popup_content_text">
                        Most orders are shipped within  less then a week of receipt of your order. However, depending on demand for the product, some items may require up to 2 weeks for shipping.
                    </p>
                    <hr class="popup_divider">
                    <p class="popup_content_heading">Cancelled Orders</p>
                    <p class="popup_content_text"> Orders can be cancelled only prior to payment processing. Orders that are being packaged for shipping, can not be cancelled. For all products please refer to your invoice or billing statement for the dedicated toll free number related to your purchase. If you do not have the information available you may call 1-855-837-2617 (Toll Free). </p>
                    <hr class="popup_divider">
                    <p class="popup_content_heading">Back-Orders</p>
                    <p class="popup_content_text">If your order cannot be shipped within the stated delivery date, we will notify you of the expected ship date. Your credit card will not be charged until 30 days after your order is shipped.</p>
                    <hr class="popup_divider">
                    {{--<p class="popup_content_heading">30-Day Guarantee on Returns & Exchanges</p>--}}
                    {{--<p class="popup_content_text"> We will gladly accept any return within 30 days, provided the item is in new or like-new condition (see Return Policy). We will only issue a refund or send a replacement as needed once the item has been received in our warehouse. </p>--}}
                    {{--<hr class="popup_divider">--}}
                    <p class="popup_content_heading">Non-Continental U.S. Shipping Surcharge</p>
                    <p class="popup_content_text">There is no additional shipping surcharge  for Hawaii, Alaska or our friends in Canada.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="returns" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">No Hassle Return Policy</h4>
            </div>
            <div >
                <div class="policy_content_container">
                    {{--<p class="popup_content_heading">Our Guarantee</p>--}}
                    {{--<p class="popup_content_text">We can't believe that anyone wouldn't fall in love with The KURE but just in case that happens��</p>--}}
                    {{--<hr class="popup_divider">--}}
                    {{--<p class="popup_content_heading">Return Information</p>--}}
                    <p class="popup_content_text">
                        We can't believe that anyone wouldn't fall in love with The KURE but just in case that happens......
                        <br>
                        <br>
                        With our Absolutely Free Trial there is no need for a refund. Because we never charged you a penny in the first place. Just notify us of your intent within 30 days of receipt of the KURE and you owe nothing. No questions asked!
                        <br>
                        <br>
                        If you selected the "Pay Now" option, again notify us within 30 days from receipt of the KURE, "no questions asked" and we will immediately process your refund. It can usually take up to 7 days for the credit to appear on your account.
                        <br>
                        {{--1. Your order number and/or your complete name, address, and billing phone or email address--}}
                        {{--<br>--}}
                        {{--2. A detailed explanation regarding your reason for returning the item(s)--}}
                        {{--<br>--}}
                        {{--3. Information about whether you would like a refund or a replacement--}}
                        {{--<br>--}}
                        {{--<br>--}}
                        {{--- No other paperwork, invoice, or preauthorization is needed for your return.--}}
                    </p>
                    {{--<hr class="popup_divider">--}}
                    {{--<p class="popup_content_heading">Exceptions to our return policy</p>--}}
                    {{--<p class="popup_content_text">--}}
                        {{--* Consumable items (such as products that can be diminished or used up) are not returnable once they have been opened. However, we will accept a return for any unused and unopened consumable item.--}}
                        {{--<br>--}}
                        {{--* All items must be returned in their original packaging along with all accessories, parts, and instructions manuals that were shipped with your original order. Our 30-day return policy starts from the day your product is received. We need to have the return package postmarked within 30 days of the date that your item was delivered (regardless of what date the product was first used).--}}
                        {{--<br>--}}
                        {{--* Shipping and handling costs are non-refundable--}}
                        {{--<br>--}}
                        {{--* DVD'S and software products may only be exchanged in the event of a manufacture defect--}}
                        {{--<br>--}}
                        {{--<br>--}}
                        {{--<span class="bold">Note:</span>--}}
                        {{--If we receive your item back in our warehouse and it does not meet our return policy terms, then we may not issue you a refund or send you a replacement. Such a return also will not be sent back to you.--}}
                    {{--</p>--}}
                    {{--<hr class="popup_divider">--}}
                    {{--<p class="popup_content_heading">Refund Policy</p>--}}
                    {{--<p class="popup_content_text">--}}
                        {{--Once your return is received, it will be processed within 10 business days. Centurion Technologies will not be responsible for loss or damage of return shipments. A credit will be issued to your original method of payment for the full purchase price of your order excluding shipping and handling and any separate fees. Credit will be issued within 5-7 business days of receiving your returned item.--}}
                        {{--<br>--}}
                        {{--<br>--}}
                        {{--<span class="bold">Note:</span>--}}
                        {{--If we receive your item back in our warehouse and it does not meet our return policy terms, then we may not issue you a refund or send you a replacement.--}}
                    {{--</p>--}}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="contact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Contact Information</h4>

            </div>
            <div >
                <div class="policy_content_container">
                    <p class="popup_content_heading">Centurion Technologies Customer Service</p>
                    <p class="popup_content_text">
                        {{--If you ordered from a toll free number that was provided in a commercial that you saw on TV, or if you ordered directly from one of our product-devoted websites, please refer to your invoice or billing statement for the dedicated toll free number related to your purchase.--}}
                        {{--<br>--}}
                        {{--<br>--}}
                        If you have any questions about anything, non technical please feel free to contact us toll free:
                        <br>
                        <br>
                        1-855-837-2617
                        <br>
                        (M-F 8:00 AM to 5:00 PM Central Standard Time)
                        <br>
                        <br>
                        Centurion Technologies LLC
                        <br>
                        5701 E Hillsborough Ave
                        <br>
                        Tampa, FL 33610
                        <br>
                        <br>
                        <br>
                        Your Satisfaction is 100% Guaranteed.
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>