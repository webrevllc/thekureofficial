<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="author" content="The Kure">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!--FAVICONS-->
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link href="/css/app.css" rel="stylesheet">
    {{--<link href="/css/libs.css" rel="stylesheet">--}}
    <link href="/css/global_cart.css?4" rel="stylesheet">
    <link href="/css/responsive_cart.css?23" rel="stylesheet">
    {{--<link href="/css/flaticon.css" rel="stylesheet">--}}
    <link href="/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">


    <script src="https://code.jquery.com/jquery-latest.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="/js/jquery-ui.js"></script>
    {{--<script src="/js/sweetalert-dev.js"></script>--}}
    <script src="/js/validator.js"></script>
    <script src="/js/scripts.js?3"></script>
    <title>The Kure!</title>
</head>

<body>
@yield('content')
@include('flash')
@yield('footer')

        {{--<!--Start of Zopim Live Chat Script-->--}}
{{--<script type="text/javascript">--}}
    {{--window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=--}}
            {{--d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.--}}
            {{--_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");--}}
        {{--$.src="//v2.zopim.com/?2SxrEpGYJ9o3vUybODKB9MTOzMfM9g7I";z.t=+new Date;$.--}}
                {{--type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");--}}
{{--</script>--}}
@include('mel4.tracking')
{{--@include('checkout.mouseflow')--}}
</body>

</html>
