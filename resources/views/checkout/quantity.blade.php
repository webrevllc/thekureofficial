@extends('checkout.layout')


@section('content')
    <div class="container white-bg box-shadow" style="padding: 2em;">
        <div class="row">
            <div class="col-md-12 ">
                <div class="col-md-3">
                    <img src="img/newkure.png" width="100%"/>
                </div>
                <div class="col-md-6" style="margin-top: -1em;">
                    <h2 class="header_headline">
                        Please Enter Your Information
                    </h2>
                </div>
                <div class="col-md-3" style="margin-top: 0.7em;">
                    <img src="img/verified.gif"/>
                </div>
            </div>
        </div>
        <div class="row hidden-xs" style="margin-top: 2em;" >
            <section class="breadcrumbs breadcrumbs_bg black-silver-gradient">
                <ul class="order_progress">
                    <li class="breadcrumbs_headline"><span class="btn_textshadow">Checkout</span> <span
                                class="breadcrumbs_headline_arrows"></span></li>
                    <li class="progress_text current_location"><span class="breadcrumbs_checkmark">1.</span> Select
                        Products
                    </li>
                    <li class="progress_text"><span class="btn_textshadow">2. Billing &amp; Shipping</span>
                    </li>
                    <li class="progress_text">3. Order Confirmation</li>
                </ul>
            </section>
        </div>
        <div class="row top-2">
            <div class="panel col-md-6 col-md-offset-3  col-xs-12panel-danger">
                <div class="panel-heading" style="width:100%;">
                    <h2 class="panel-title">How many computers do you want to protect?</h2>
                </div>
                <div class="panel-body">
                    <img src="./img/thekureproduct.png" style="width:100%;margin-top:10px;margin-left:-20px;" class="center-block">
                    {{--<h3 class="text-center">SPECIAL OFFER! Free Shipping on all orders!</h3>--}}
                </div>
                <div class="panel-footer">
                    <form method="post" action="/setquantity">
                        {{csrf_field()}}
                        <hr>
                        <input type="number" id="quantity" name="quantity" class="mytext" min="1" value="1"><br><br>
                        <button type="submit" class="btn btn-danger btn-block">Next</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop