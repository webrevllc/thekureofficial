
@extends('checkout.layout')


        @section('content')
            {{--@inject('states', 'App\Http\States::all()')--}}
            <style>
             .credit-card-box .panel-title {
                 display: inline;
                 font-weight: bold;
             }
        .credit-card-box .form-control.error {
            border-color: red;
            outline: 0;
            box-shadow: inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(255,0,0,0.6);
        }
        .credit-card-box label.error {
            font-weight: bold;
            color: red;
            padding: 2px 8px;
            margin-top: 2px;
        }
        .credit-card-box .payment-errors {
            font-weight: bold;
            color: red;
            padding: 2px 8px;
            margin-top: 2px;
        }
        .credit-card-box label {
            display: block;
        }
        /* The old "center div vertically" hack */
        .credit-card-box .display-table {
            display: table;
        }
        .credit-card-box .display-tr {
            display: table-row;
        }
        .credit-card-box .display-td {
            display: table-cell;
            vertical-align: middle;
            width: 100%;
        }
        /* Just looks nicer */
        .credit-card-box .panel-heading img {
            min-width: 180px;
        }
        .fa-ul{color:#31708f;}
        li h3 {font-size:1.5em;margin:15px;text-align: left;}
        .l2{
            line-height: 1.8em;}
        legend {
            font-family: 'Open Sans', Verdana, Arial, Helvetica, sans-serif;
            display: block;
            padding: 0;
            margin-bottom: 20px;
            font-size: 21px;
            line-height: 40px;
            color: #333333;
            border: 0;
            text-decoration: underline;
        }
    </style>
    <div class="container white-bg box-shadow" style="padding: 2em;">
        <div class="row">
            <div class="col-md-12 ">
                <div class="col-md-3">
                    <img src="/img/newkure.png" width="100%"/>
                </div>
                <div class="col-md-6" style="margin-top: -1em;">
                    <h2 class="header_headline">
                        Please Enter Your Information
                    </h2>
                </div>
                <div class="col-md-3 hidden-xs" style="margin-top: 0.7em;">
                    <img src="/img/verified.gif"/>
                </div>
            </div>
        </div>

        <div class="row hidden-xs" style="margin-top: 2em;">
            <section class="breadcrumbs breadcrumbs_bg black-silver-gradient">
                <ul class="order_progress">
                    <li class="breadcrumbs_headline"><span class="btn_textshadow">Checkout</span> <span
                                class="breadcrumbs_headline_arrows"></span></li>
                    <li class="progress_text completed_location"><span class="breadcrumbs_checkmark">1.</span> Select
                        Products
                    </li>
                    <li class="progress_text current_location"><span class="btn_textshadow">2. Billing &amp; Shipping</span>
                    </li>
                    <li class="progress_text">3. Order Confirmation</li>
                </ul>
            </section>
        </div>
        <div class="row" style="margin-top: 2em;">
            <div class="col-md-8 col-xs-12" >

                <!--Section 1 Start-->

                <div class="form_section_header nice-orange-gradient" id="form_contact">
                    <div>
                        <input type="hidden" class="accordian_index" value="0">

                        <h2 class="section_header_headline">
                            <span class="section_header_checkmark hide_mark" id="contact_check"></span>
                            <span class="section_header_icon" id="section_header_contact_icon"></span>
                            <span class="section_header_heading">1. Contact Information</span>
                            <a href="#" id="edit-contact">
                                <span class="section_header_heading"
                                      style="float: right; margin-right: 1em; color: white">Edit</span></a>
                        </h2>
                    </div>
                </div>

                <div class="">
                    <div class="row">
                        <div class="col-md-12  col-xs-12" id="accordion">

                            <form data-toggle="validator" role="form" id="firstForm">
                                <div class="col-md-6 col-xs-12">
                                    {{csrf_field()}}
                                    <div class=" form-group">
                                        <label for="first_name" class="input_label"><span class="label_required">*</span>First Name</label>
                                        <input type="text" name="first_name" id="first_name" class="form-control" placeholder="first name"  required/>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class=" form-group">
                                        <label for="last_name" class="input_label"><span class="label_required">*</span>Last Name</label>
                                        <input type="text" name="last_name" id="last_name" class="form-control text" placeholder="last name" required/>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class=" form-group">
                                        <label for="email" class="input_label"><span class="label_required">*</span>Email</label>
                                        <input type="email" name="email" id="email"  class="form-control text" placeholder="valid email address" required/>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class=" form-group">
                                        <label for="email2" class="input_label"><span class="label_required">*</span>Confirm Email</label>
                                        <input type="email"  id="email2" data-match="#email" data-match-error="Whoops, these don't match" class="form-control text" placeholder="valid email address" required/>
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    <div class=" form-group">
                                        <label for="phone" class="input_label"><span class="label_required">*</span>Phone</label>
                                        <input type="tel" name="phone" id="phone" class="form-control text" minlength="10" maxlength="10" placeholder="no dashes or spaces" required />
                                        <div class="help-block">(10 digits, do not include country code)</div>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class=" form-group">
                                        <label for="password" class="input_label"><span class="label_required">*</span>Password</label>
                                        <input type="password" name="password" id="password" class="form-control text" placeholder="create password" required minlength="6"/>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class=" form-group">
                                        <label for="cpassword" class="input_label"><span class="label_required">*</span>Password</label>
                                        <input type="password"  id="cpassword" class="form-control text" placeholder="confirm password" required data-match="#password" data-match-error="Whoops, these don't match"/>
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    <button id="continue-billing" class="orange" type="submit" style="margin-left: 1.5em;pointer-events: all;cursor: pointer;">
                                        <img src="/img/orange.gif"/>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!--Section 1 End-->


                <!--Section 2 Start-->
                <div class="form_section_header nice-orange-gradient top-2" id="billing_form">
                    <input type="hidden" class="accordian_index" value="0">

                    <h2 class="section_header_headline">
                        <span class="section_header_checkmark hide_mark" id="billing_info"></span>
                        <span class="section_header_icon" id="section_header_billing_icon"></span>
                        <span class="section_header_heading">2. Billing/Shipping Info</span>
                        <a href="#" id="edit-billing"><span class="section_header_heading"
                                                            style="float: right; margin-right: 1em; color: white">Edit</span></a>
                    </h2>

                </div>

                <div class="">
                    <div class="row">
                        <div class="col-md-12 col-xs-12" id="accordion-billing">
                            <form id="secondForm" data-toggle="validator" role="form">
                                <div class="col-md-6 col-xs-12">
                                    <fieldset>
                                        <div class=" form-group">
                                            <label for="add1" class="input_label"><span class="label_required">*</span>Address 1</label>
                                            <input type="text" name="add1" id="add1" class="form-control" placeholder="address" required/>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class=" form-group">
                                            <label for="add2" class="input_label"><span class="label_required"></span>Address 2</label>
                                            <input type="text" name="add2" id="add2" class="form-control" placeholder="address 2"/>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class=" form-group">
                                            <label for="city" class="input_label"><span class="label_required">*</span>City</label>
                                            <input type="text" name="city" id="city" class="form-control" placeholder="city" required/>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class=" form-group">
                                            <label for="state" class="input_label"><span class="label_required">*</span>State</label>
                                            <select name="state" id="state" class="form-control" required>
                                                @foreach(App\Http\States::all() as $abbr => $full)
                                                    <option value="{{$abbr}}">{{$abbr}}</option>
                                                @endforeach
                                            </select>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class=" form-group">
                                            <label for="zip" class="input_label label_zip"><span class="label_required">*</span>Zip Code</label>
                                            <input type="text" name="zip" id="zip" class="form-control" maxlength="6" placeholder="zip code" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </fieldset>
                                    {{--<div class=" form-group">--}}
                                        {{--<div class="checkbox">--}}
                                            {{--<label>--}}
                                                {{--<input type="checkbox" id="ship_check" name="shipping"> Ship to Different Address?--}}
                                            {{--</label>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    <button id="back-to-contact" class="btn top-2" type="button" style="margin-left: 1em;">Back</button>
                                    <button id="continue-billing" class="orange" type="submit" style="margin-left: 1.5em;pointer-events: all;cursor: pointer;">
                                        <img src="/img/orange.gif"/>
                                    </button>
                                </div>
                                <div class="col-md-6">
                                    {{--<fieldset>--}}
                                        {{--<div id="shipping_address">--}}
                                            {{--<legend>Shipping Infomation</legend>--}}
                                            {{--<div class=" form-group">--}}
                                                {{--<label for="sadd1" class="input_label">Address 1</label>--}}
                                                {{--<input type="text" name="sadd1" id="sadd1" class="text" placeholder="address"/>--}}
                                                {{--<div class="help-block with-errors"></div>--}}
                                            {{--</div>--}}
                                            {{--<div class=" form-group">--}}
                                                {{--<label for="sadd2" class="input_label">Address 2</label>--}}
                                                {{--<input type="text" name="sadd2" id="sadd2" class="text" placeholder="address 2"/>--}}
                                                {{--<div class="help-block with-errors"></div>--}}
                                            {{--</div>--}}
                                            {{--<div class=" form-group">--}}
                                                {{--<label for="scity" class="input_label">City</label>--}}
                                                {{--<input type="text" name="scity" id="scity" class="text" placeholder="city"/>--}}
                                                {{--<div class="help-block with-errors"></div>--}}
                                            {{--</div>--}}
                                            {{--<div class=" form-group">--}}
                                                {{--<label for="sstate" class="input_label">State</label>--}}
                                                {{--<select name="sstate" id="sstate" class="form-control">--}}
                                                    {{--@foreach(App\Http\States::all() as $abbr => $full)--}}
                                                        {{--<option value="{{$abbr}}">{{$abbr}}</option>--}}
                                                    {{--@endforeach--}}
                                                {{--</select>--}}
                                            {{--</div>--}}
                                            {{--<div class=" form-group">--}}
                                                {{--<label for="szip" class="input_label label_zip">Zip Code</label>--}}
                                                {{--<input type="text" name="szip" id="szip" class="form-control" maxlength="6" placeholder="zip code">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</fieldset>--}}
                                </div>


                            </form>
                        </div>
                    </div>
                </div>

                <!--Section 2 End-->

                <!--Section 3 Start-->

                <div class="form_section_header nice-orange-gradient top-2" id="payment_form">
                    <input type="hidden" class="accordian_index" value="0">

                    <h2 class="section_header_headline">
                        <span class="section_header_checkmark hide_mark" id="payment_checkmark"></span>
                        <span class="section_header_icon" id="section_header_payment_icon"></span>
                        <span class="section_header_heading">3. Payment Information</span>
                        <a href="#" id="edit-payment"><span class="section_header_heading"
                                                            style="float: right; margin-right: 1em; color: white">Edit</span></a>
                    </h2>

                </div>

                <form id="paymentForm" data-toggle="validator" role="form">
                    <div id="payment_info" style="display: block;">
                            <br>
                            <div class="alert alert-success" role="alert">
                                <b>Your card will not be charged today, but we do need your information only for when your free trial is over. Please be assured that we will not bill you for anything until you have enjoyed your 30 day Free Trial. Also, remember that we are so sure that you will love The KURE, we will even cover the postage to return it!</b>
                            </div>
                            {{--<div class="row  form-group">--}}
                            {{--<div class="input_row  form-group">--}}
                                {{--<p class="form_section_content_input">--}}
                                    {{--<label for="cardType" class="input_label"><span class="label_required">*</span>Credit/Debit Card</label>--}}
                                    {{--<select name="cardType" id="cardType" class="select_card form-control">--}}
                                        {{--<option value="" id="cc_default" class="paypal_hidden">select</option>--}}
                                        {{--<option value="V" class="paypal_hidden">VISA</option>--}}
                                        {{--<option value="M" class="paypal_hidden">Mastercard</option>--}}
                                        {{--<option value="DS" class="paypal_hidden">Discover</option>--}}
                                        {{--<option value="AX" class="paypal_hidden">American Express</option>--}}
                                    {{--</select>--}}
                                {{--</p>--}}
                                {{--<span class="image_cc"></span>--}}
                            {{--</div>--}}
                            <!--closes credit_card  div-->

                            <div class="panel panel-default credit-card-box">
                                <div class="panel-heading display-table" >
                                    <div class="row display-tr" >
                                        <h3 class="panel-title display-td" >Payment Details</h3>
                                        <div class="display-td" >
                                            <img class="img-responsive pull-right" src="/img/cards.png">
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <form role="form" id="payment-form">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <label for="cardNumber" class="input_label"><span class="label_required">*</span>Enter credit or debit card number for future billing after your free trial</label>
                                                    <div class="input-group">
                                                        <input
                                                                type="tel"
                                                                class="form-control"
                                                                name="card"
                                                                placeholder="Valid Card Number"
                                                                maxlength="16"
                                                                required
                                                                />
                                                        <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <br>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-6">
                                                <div class="form-group">
                                                    <label for="cardExpiry" class="input_label"><span class="label_required">*</span><span class="hidden-xs">Expiration</span><span class="visible-xs-inline">Exp</span> Month</label>
                                                    <select name="month" id="month" class="form-control " required>
                                                        <option value="">month</option>
                                                        <option value="01">01 - January</option>
                                                        <option value="02">02 - February</option>
                                                        <option value="03">03 - March</option>
                                                        <option value="04">04 - April</option>
                                                        <option value="05">05 - May</option>
                                                        <option value="06">06 - June</option>
                                                        <option value="07">07 - July</option>
                                                        <option value="08">08 - August</option>
                                                        <option value="09">09 - September</option>
                                                        <option value="10" selected>10 - October</option>
                                                        <option value="11">11 - November</option>
                                                        <option value="12">12 - December</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-6">
                                                <div class="form-group">
                                                    <label for="cardExpiry" class="input_label"><span class="label_required">*</span><span class="hidden-xs">Expiration</span><span class="visible-xs-inline">Exp</span> Year</label>
                                                    <select name="year" id="year" class="form-control" required>
                                                        <option value="15">2015</option>
                                                        <option value="16">2016</option>
                                                        <option value="17">2017</option>
                                                        <option value="18">2018</option>
                                                        <option value="19">2019</option>
                                                        <option value="20">2020</option>
                                                        <option value="21">2021</option>
                                                        <option value="22">2022</option>
                                                        <option value="23">2023</option>
                                                        <option value="24">2024</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <br>
                                            <br>
                                            <br>
                                            <h3 class="text-center" style="font-size:16px;font-weight:bold;">Your Satisfaction is 100% Guaranteed</h3>
                                            <img src="/img/verified.gif" class="center-block"/>
                                            <!--closes satisfaction_guarantee_container div-->

                                            <div class="pull-right">
                                                <button id="back-to-billing" class="btn top-2" type="button" style="margin-left: 1em;">Back</button>
                                                <button id="continue-upsells" class="orange" type="submit" style="margin-left: 1.5em;pointer-events: all;cursor: pointer;">
                                                    <img src="/img/orange.gif"/>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                            {{--<div class="form-group">--}}
                                {{--<label for="cardNumber" class="input_label"><span class="label_required">*</span>Credit Card Number</label>--}}
                                {{--<input type="text" name="card" id="cardNumber" class="text form-control" maxlength="16" placeholder="no dashes or spaces" required>--}}
                                {{--<div class="help-block with-errors"></div>--}}
                            {{--</div>--}}
                            {{--<!--closes cardNumber  div-->--}}


                            {{--<div class="row form-group" id="expirationRow">--}}
                                {{--<p class="form_section_content_input">--}}
                                    {{--<label for="month" class="input_label"><span class="label_required">*</span>Expiration Date</label>--}}
                                    {{--<select name="month" id="month" class="select_month " required>--}}
                                        {{--<option value="">month</option>--}}
                                        {{--<option value="01">01 - January</option>--}}
                                        {{--<option value="02">02 - February</option>--}}
                                        {{--<option value="03">03 - March</option>--}}
                                        {{--<option value="04">04 - April</option>--}}
                                        {{--<option value="05">05 - May</option>--}}
                                        {{--<option value="06">06 - June</option>--}}
                                        {{--<option value="07">07 - July</option>--}}
                                        {{--<option value="08">08 - August</option>--}}
                                        {{--<option value="09">09 - September</option>--}}
                                        {{--<option value="10" selected>10 - October</option>--}}
                                        {{--<option value="11">11 - November</option>--}}
                                        {{--<option value="12">12 - December</option>--}}
                                    {{--</select>--}}
                                    {{--<span class="expiration_divider">/</span>--}}
                                    {{--<select name="year" id="year" class="select_year " required>--}}
                                        {{--<option value="15">2015</option>--}}
                                        {{--<option value="16">2016</option>--}}
                                        {{--<option value="17">2017</option>--}}
                                        {{--<option value="18">2018</option>--}}
                                        {{--<option value="19">2019</option>--}}
                                        {{--<option value="20">2020</option>--}}
                                        {{--<option value="21">2021</option>--}}
                                        {{--<option value="22">2022</option>--}}
                                        {{--<option value="23">2023</option>--}}
                                        {{--<option value="24">2024</option>--}}
                                    {{--</select>--}}
                                {{--</p>--}}
                            {{--</div>--}}

                            {{--<!--closes cardExpirationDate  div-->--}}
                            {{--<div class="row">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label for="cardCVV" class="input_label"><span class="label_required">*</span>CCV</label>--}}
                                    {{--<input type="text" id="cardCVV" name="cardCVV" class="" maxlength="4">--}}
                                    {{--<span class="image_ccv"></span>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <!--closes cardCVV  div-->





                        <!--closes payment_info fieldset-->

                    </div>
                </form>
                <!--Section 3 End-->

                <!--Section 4 Start-->
                <div class="form_section_header nice-orange-gradient top-2" id="upsells">
                    <input type="hidden" class="accordian_index" value="0">

                    <h2 class="section_header_headline">
                        <span class="section_header_checkmark hide_mark"></span>
                        <span class="section_header_icon" id="section_header_contact_icon"></span>

                        <span class="section_header_heading">4. Additional Offers</span>
                        <a href="#" id="edit-offers">
                            <span class="section_header_heading" style="float: right; margin-right: 1em; color: white">Edit</span>
                        </a>
                    </h2>

                </div>
                <div id="addl-offers">
                    <div id="hwmon">
                        <div class="panel-body text-center">
                            <h2 style="color:#31708f; font-size: 1.5em;">
                                Monitor your hardware from anywhere<br>
                                Receive a text message or email the moment a hardware problem arises
                            </h2>
                            <br>

                            <h3 style="color:#31708f; font-size: 2em;">$19.99 Annual Value - only $4.95/yr!</h3>

                            <div class="col-md-6">
                                <img src="/img/upsells/hwmon.png" style="width:70%;display:block;margin:20px auto 0;">
                            </div>
                            <div class="col-md-6 top-2">
                                <ul class="fa-ul">
                                    <li><i class="fa-li fa fa-2x fa-check-square"></i><h3 class="l2">CPU Load</h3></li>
                                    <li><i class="fa-li fa fa-2x fa-check-square"></i><h3 class="l2">Hard Drive Usage</h3></li>
                                    <li><i class="fa-li fa fa-2x fa-check-square"></i><h3 class="l2">Memory Utilization</h3></li>
                                    <li><i class="fa-li fa fa-2x fa-check-square"></i><h3 class="l2">Network Usage</h3></li>
                                    <li><i class="fa-li fa fa-2x fa-check-square"></i><h3 class="l2">Completely Customizable!</h3></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button type="button" class="btn btn-info btn-block" style="font-size:1.4em;" id="hwmon-yes">Yes! Add this to my order!</button>
                            <button id="hwmon-no" type="button" class="btn btn-default btn-block">No,thanks</button>
                        </div>
                    </div>

                    <div id="rush">
                        <div class="panel-body text-center">
                            <div class="alert alert-info" role="alert">
                                <h3 style="font-size:1.3em;">Would you like your order to go to the top of the line? We will prioritize your order and rush it to you ASAP for <b>ONLY $1.99!</b></h3>
                            </div>
                            <img src="/img/shipping-guy.png" style="width:40%;display:block;margin:10px auto 0;">
                        </div>
                        <button type="button" class="btn btn-info btn-block" style="font-size:1.4em;" id="rush-yes">Yes! Rush The Kure To Me!</button>
                        <button id="rush-no" type="button" class="btn btn-default btn-block">No, thanks</button>
                    </div>

                    <div id="cybersafe">
                        <div class="panel-body text-center">
                            <h4 style="color:#31708f;">Cleans PC in just one click</h4>
                            <h4 style="color:#31708f;">Removes performance-hindering clutter</h4>
                            <h4 style="color:#31708f;">Repairs Windows registry problems</h4>
                            <h4 style="color:#31708f;">Speed up your computer</h4>
                            <h4 style="color:#31708f;">Fixes over 27,000 problems and errors with one click</h4>
                            <h3 style="color:#31708f!important; font-size: 1.8em;">Only $8.95/yr!</h3>
                            <img src="/img/upsells/pcos.png" style="width:50%;display:block;margin:10px auto 0; font-size: 1.4em;">
                        </div>
                        <button type="button" class="btn btn-info btn-block" style="font-size:1.4em;" id="cybersafe-yes">Yes! Clean My PC!</button>
                        <button id="cybersafe-no" type="button" class="btn btn-default btn-block">No, thanks</button>
                    </div>

                    <div id="emergency">
                        <div class="panel-body text-center">
                            <img src="/img/emer.png" style="width:100%;display:block;margin:10px auto 0px;">
                        </div>
                        <button type="button" class="btn btn-primary btn-block" style="font-size:1.4em;" id="emergency-yes">I NEED HELP NOW! </button>
                        <button id="emergency-no"  type="button" class="btn btn-danger btn-block">No, thanks</button>
                    </div>

                    <div id="paynow">
                        <div class="panel-body text-center">
                            <div class="row">
                                <div class="alert alert-info" role="alert">
                                    <h3 style="font-size:1.3em;"><b>LIMITED OFFER!</b></h3>
                                </div>
                            </div>
                            <div class="row">
                                <h2 style="font-size: 1.2em; line-height:1.2em; color:#31708f">
                                    Most of our customers find that it is much more convenient to pay for THE KURE today.
                                    rather than tracking their payment 30 days from now. And, as a special incentive to
                                    pay for order today, we are offering a subscription to <b>CyberSafePC Identity Theft Software</b>, a $19.95/yr
                                    value <b>ABSOLUTELY FREE</b> for as long as you own the KURE. This is a <b>FREE</b> gift, just for processing your credit card today.
                                </h2>
                            </div>
                            <div class="row">
                                <div class="col-md-6 ">
                                    <ul class="fa-ul">
                                        <li><i class="fa-li fa fa-2x fa-check-square"></i><h3>Prevents Webcam and Microphone Hijacking!</h3></li>
                                        <li><i class="fa-li fa fa-2x fa-check-square"></i><h3>Provides Keystroke Logging Protection!</h3></li>
                                        <li><i class="fa-li fa fa-2x fa-check-square"></i><h3>Secures your data during online shopping and banking!</h3></li>
                                        <li><i class="fa-li fa fa-2x fa-check-square"></i><h3>Real-time email or text alerts!</h3></li>
                                    </ul>
                                </div>
                                <div class="col-md-6 top-2">
                                    <img class="center-block" width="100%" src="/img/cyber.png"/>
                                    <h3 style="padding:20px;text-align:center;font-weight:bold;color:#31708f;font-size:1.4em;">Pay today, and get it for free!</h3>
                                </div>
                            </div>
                            <div class="alert alert-info" role="alert">
                                <h3 style="font-size:1.3em;"><b>Would you like to pay the full amount now and receive CyberSafePC Identity Theft for FREE?</b></h3>
                            </div>
                            <button type="button" class="btn btn-info btn-lg btn-block" id="paynow-yes">Pay the full amount now</button>
                            <button id="paynow-no"  type="button" class="btn btn-default  btn-block">No thanks, just start my free trial.</button>
                        </div>
                    </div>
                    <div id="cspc">
                        <div class="panel-body text-center">
                            <div class="row">
                                <div class="alert alert-info" role="alert">
                                    <h3 style="font-size:1.3em;"><b>Would you like to add CyberSafePC Identity Theft to your package?</b></h3>
                                </div>
                            </div>
                            <div class="row">
                                <h2 style="font-size: 1.2em; line-height:1.2em; color:#31708f">
                                    CyberSafePC Identity Theft Software constantly monitors your
                                    system for intrusion from hackers and will alert you via text or email
                                    that an attempt on your system was blocked. Regularly CyberSafePC Identity Theft sells for $19.95, but
                                    you can try it now <b>ABSOLUTELY FREE</b> for 30 days then <b>ONLY $4.95/yr!</b>
                                </h2>
                            </div>
                            <div class="row">
                                <div class="col-md-6 top-2">
                                    <img class="center-block" width="100%" src="/img/cyber.png"/>
                                    <h3 style="padding:20px;text-align:center;font-weight:bold;color:#31708f;font-size:1.4em;">Try it out, then only $4.95/yr!</h3>
                                </div>
                                <div class="col-md-6 ">
                                    <ul class="fa-ul">
                                        <li><i class="fa-li fa fa-2x fa-magic"></i><h3>Prevents Webcam and Microphone Hijacking!</h3></li>
                                        <li><i class="fa-li fa fa-2x fa-magic"></i><h3>Provides Keystroke Logging Protection!</h3></li>
                                        <li><i class="fa-li fa fa-2x fa-magic"></i><h3>Secures your data during online shopping and banking!</h3></li>
                                        <li><i class="fa-li fa fa-2x fa-magic"></i><h3>Real-time email or text alerts!</h3></li>
                                    </ul>
                                </div>
                            </div>

                            <button type="button" class="btn btn-info btn-lg btn-block" id="cspc-yes">Yes, Add CyberSafePC Identity Theft Protection</button>
                            <button id="cspc-no"  type="button" class="btn btn-default btn-block">No thanks</button>
                        </div>
                    </div>
                    <div id="submit" class="grey">
                        <div class="panel-body text-center">
                            <div id="errors" style="margin-bottom:40px;font-weight:bold;font-size:1.1em;text-align:center;display: none;"></div>
                            {{--<h2 class="text-center">Click "Submit Order!" to Join:</h2>--}}
                            <img src="/img/newkure.png" style="width:100%;" class="center-block">
                        </div>
                        <button type="button" class="sbutton center-block" id="submitOrder">
                            <img src="/img/submit_order.gif" class="center-block"/>
                        </button>
                        <img src="/img/pw.gif" class="center-block" id="pw" style="display:none;"/>
                    </div>
                </div>


            </div>
            <div class="col-md-4">
                <aside id="sidebar" class="ie_clearfix_block">
                    <div class="sidebar_cart nice-lt-blue-gradient ie_clearfix">

                        <div class="sidebar_cart_header nice-orange-gradient ie_clearfix_block">
                            <h3 class="sidebar_header_headline">
                                <span class="sidebar_header_icon" id="sidebar_header_summary"></span>
                                <span class="sidebar_header_heading">Order Summary</span>
                            </h3>

                            <a class="edit_link btn_small bg_light_grey btn_general ala_carte"
                               href="order_form.jsp?action=Clear%20Cart" style="display: none;">Edit Cart</a>
                        </div>
                        <!--closes sidebar_cart_header div-->


                        <div class="sidebar_cart_content ie_clearfix_block">

                            <ul id="first_product" class="sidebar_cart_product_row">
                                <li class="sidebar_cart_product_description">The Kure</li>
                                <li class="sidebar_cart_product_quantity">1</li>
                                <li class="sidebar_cart_product_times">x</li>
                                <li class="sidebar_cart_product_price">$19.95</li>
                                {{--<li class="sidebar_cart_product_description">CyberSafePC</li>--}}
                                {{--<li class="sidebar_cart_product_quantity"></li>--}}
                                {{--<li class="sidebar_cart_product_times">x</li>--}}
                                {{--<li class="sidebar_cart_product_price">FREE</li>--}}
                                <div id="addl-items"></div>
                            </ul>


                            <ul class="sidebar_cart_summary_row">
                                <li class="sidebar_cart_label">Subtotal</li>
                                <li class="sidebar_cart_price" id="subtotal">$19.95</li>
                            </ul>


                            <ul class="sidebar_cart_summary_row">
                                <li class="sidebar_cart_label">Shipping</li>
                                <li class="sidebar_cart_price">$2.95</li>
                            </ul>


                            <ul class="sidebar_cart_summary_row">
                                <li class="sidebar_cart_label"></li>
                                <li class="sidebar_cart_price"></li>
                            </ul>


                            <ul class="sidebar_cart_final_row">
                                <div id="today-free" style="color:red;">
                                    <li class="sidebar_cart_final_label">Today's Total</li>
                                    <li class="sidebar_cart_final_price"><span id="">FREE</span></li>
                                </div>

                                <li class="sidebar_cart_final_label" id="today-after">Your Total After Trial</li>
                                <li class="sidebar_cart_final_price"><span id="orderTotal">$19.95</span></li>
                                <li id="freeness" style="margin-top:20px;margin-bottom:20px;"><p>We will <b style="color:red;">not</b> bill you for anything until you have enjoyed your 30 day Free Trial. Price shown above reflects your price after your Free Trial is over.</p></li>
                                <li class="" style="display:block;">
                                    <a href="#" id="reset_cart" class="btn btn-xs btn-info" style="color:#000;">Edit Cart</a>
                                </li>
                            </ul>

                        </div>
                        <!--closes sidebar_content div-->

                    </div>
                    <!--closes sidebar_cart div-->


                    {{--<div class="offer_details ie_clearfix nice-lt-blue-gradient">--}}

                        {{--<div class="details_header nice-orange-gradient ie_clearfix_block">--}}
                            {{--<h3 class="sidebar_header_headline">--}}
                                {{--<span class="sidebar_header_icon" id="sidebar_header_offer"></span>--}}
                                {{--<span class="sidebar_header_heading">Order Details</span>--}}
                            {{--</h3>--}}
                        {{--</div>--}}
                        <!--closes details_header div-->


                        {{--<div class="details_message ie_clearfix_block">--}}
                            {{--<p class="order_notes"><span class="bolder">Please Note:</span></p>--}}

                            {{--<p class="order_notes">An Additional $5.99 shipping surcharge is added for items sent outside of--}}
                                {{--the continental United States, Alaska, Hawaii or Canada</p>--}}

                            {{--<p class="order_notes">*MO residents, sales tax applies.</p>--}}

                        {{--</div>--}}
                        <!--closes details_message div-->

                    {{--</div>--}}
                    <!--closes offer_details div-->


                    <div class="ssl_badge using_ssl hidden-xs" style="display: block;">

                        <span class="ir">Secured by 256 bit SSL encryption</span>

                    </div>


                </aside>
            </div>
        <div class="modal fade" id="quantity-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Would you like to change the quantity?</h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <img src="/img/thekureproduct.png" style="width:100%;margin-top:10px;margin-left:-20px;" class="center-block">
                            {{--<h3 class="text-center">SPECIAL OFFER! Free Shipping on all orders!</h3>--}}
                        </div>
                        <div class="panel-footer">
                            <input type="number" id="new-quantity" name="quantity" class="mytext" min="1" value="1"><br><br>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">No thanks!</button>
                        <button type="button" id="qty-changed" class="btn btn-success">Change Quantity</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('checkout.footer')



@stop