@extends('checkout.layout')


@section('content')
    <div class="container white-bg box-shadow" style="padding: 2em;">
        <div class="row">
            <div class="col-md-12 ">
                <div class="col-md-3">
                    <img src="img/newkure.png" width="100%"/>
                </div>
                <div class="col-md-6" style="margin-top: -1em;">
                    <h2 class="header_headline">
                        Please Enter Your Information
                    </h2>
                </div>
                <div class="col-md-3" style="margin-top: 0.7em;">
                    <img src="img/verified.gif"/>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 2em;">
            <section class="breadcrumbs breadcrumbs_bg black-silver-gradient">
                <ul class="order_progress">
                    <li class="breadcrumbs_headline"><span class="btn_textshadow">Checkout</span> <span class="breadcrumbs_headline_arrows"></span></li>
                    <li class="progress_text "><span class="breadcrumbs_checkmark">1.</span> Select Products</li>
                    <li class="progress_text"><span class="btn_textshadow">2. Billing &amp; Shipping</span></li>
                    <li class="progress_text current_location">3. Order Confirmation</li>
                </ul>
            </section>
        </div>
        <div class="row top-2">
            <h4 style="font-size:25px;" class="text-center">
                Thank you for your purchase of
            </h4>
            <br>
            <img src="img/newkure.png" style="width:50%" class="center-block"/>
            <div class="col-md-10 col-md-offset-1 " style="font-size:1.40em; margin-top:30px;">
                <div class="alert alert-info" role="alert">
                    <h3 style="font-weight:bold;text-decoration: underline;">Your Details</h3>
                    Name: {{$order->user->name}}<br>
                    Email: {{$order->user->email}}<br><br>
                    <h2 style="font-weight:bold;text-decoration: underline;">Order Details</h2>
                    Invoice Number: {{$order->invoice_number}}<br>
                    Total Paid:
                    @if($order->amount == 'Free Trial')
                        Free Trial<br>
                    @else
                        ${{number_format($order->amount, 2)}}<br>
                    @endif
                    <br>
                    <h2 style="font-weight:bold;text-decoration: underline;">Products Purchased</h2>
                    @foreach(json_decode($order->products)  as $product)
                        {{$product->name}} x {{$product->quantity}}<br>
                    @endforeach
                    <br>
                    <h2 style="font-weight:bold;text-decoration: underline;">Subscription Information</h2>
                    You will be billed ${{number_format($sub->amount, 2)}}

                    @if($order->status == "trial")
                        30 days from the date that you receive your software.
                    @else
                        on {{date('F j, Y', strtotime($order->subscription->next_process_date))}} unless you cancel your subscription.
                    @endif
                </div>



                <br>
                <br>
                <br>

                <p>A receipt was emailed to the address above.</p>
                <p>You will receive an email once your order has been shipped.</p>
            </div>
        </div>
    </div>
@stop