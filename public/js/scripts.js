$(function () {
    var products = {};
    var qty = getUrlParameter("qty");
    var defaultKurePrice = 19.95;
    var pcosPrice = 8.95;
    var cspcPrice = 4.95;
    var expeditedPrice = 1.99;
    var emergencyPrice = 19.95;
    var hwmonPrice = 4.95;
    var shipping = 5.95;

    var cardNumber = $("#cardNumber");
    var sale = false;
    var saleable = false;

    var total = (qty * defaultKurePrice + shipping).toFixed(2);

    var orderTotal = $("#orderTotal");
    var subTotal = $("#subtotal");

    if (qty != null) {
        $(".sidebar_cart_product_quantity").text(qty);
        orderTotal.text("$" + (qty * defaultKurePrice + shipping).toFixed(2));
        subTotal.text("$" + (qty * defaultKurePrice).toFixed(2));
    }

    $("#accordion-billing").slideToggle(0);
    $("#payment_info").slideToggle(0);

    //$("#shipping_address").hide();
    //$("#ship_check").change(function() {
    //    if(this.checked) {
    //        $("#shipping_address").slideDown();
    //    }else{
    //        $("#shipping_address").slideUp();
    //    }
    //});

    $("#edit-contact").hide();
    $("#edit-billing").hide();
    $("#edit-payment").hide();
    $("#edit-offers").hide();

    $("#hwmon").hide();
    $("#rush").hide();
    $("#cybersafe").hide();
    $("#emergency").hide();
    $("#paynow").hide();
    $("#cspc").hide();
    $("#submit").hide();

    $("#accordion").accordion({
        header: "h2"
    });

    $("#firstForm").validator().on('submit', function(e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault();
            $("#accordion").slideToggle(500);
            $("#accordion-billing").slideToggle(500);
            $("#edit-contact").show();
        }
    });

    $("#secondForm").validator().on('submit', function(e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault();
            $("#accordion-billing").slideToggle(500);
            $("#payment_info").slideToggle(500);
            $("#edit-billing").show();
        }
    });

    $("#paymentForm").validator().on('submit', function(e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault();
            $("#payment_info").slideUp();
            $("#edit-payment").show();
            if(sale == false){
                $("#hwmon").fadeIn(200);
            }
        }
    });

    $("#edit-contact, #back-to-contact").click(function () {
        $("#accordion").slideDown();
        $("#accordion-billing").slideUp();
        $("#payment_info").slideUp();
    });

    $("#edit-billing, #back-to-billing").click(function () {
        $("#accordion").slideUp();
        $("#accordion-billing").slideDown();
        $("#payment_info").slideUp();
    });

    $("#edit-payment").click(function () {
        $("#payment_info").slideDown();
        $("#accordion").slideUp();
        $("#accordion-billing").slideUp();
    });

    $("#hwmon-yes").click(function () {
        products["HWMONK"] = qty;
        var hwmon = parseFloat((qty * hwmonPrice).toFixed(2));
        total = (parseFloat(total) + parseFloat(hwmon)).toFixed(2);
        subTotal = (parseFloat(total - shipping)).toFixed(2);
        $("#addl-items").append("<div class='top-15 addl-items'><ul>" +
        "<li class='sidebar_cart_product_description_image'>" +
        "</li>" +
        "<li class='sidebar_cart_product_description'>Hardware Monitor</li>" +
        "<li class='sidebar_cart_product_quantity'>" + qty + "</li>" +
        "<li class='sidebar_cart_product_times'>×</li>" +
        "<li class='sidebar_cart_product_price'>$" + hwmonPrice + "</li>" +
        "</ul></div>");
        $("#orderTotal").text("$" + total);
        $("#subtotal").text("$" + subTotal);
        $("#hwmon").fadeOut("slow", function(){
            $("#cybersafe").fadeIn();
        });
    });

    $("#hwmon-no").click(function () {
        $("#hwmon").fadeOut("slow", function(){
            $("#cybersafe").fadeIn();
        });
    });

    $("#rush-yes").click(function () {
        products["PROC"] = 1;
        var expedite = parseFloat((expeditedPrice).toFixed(2));
        total = (parseFloat(total) + parseFloat(expedite)).toFixed(2);
        subTotal = (parseFloat(total - shipping)).toFixed(2);
        $("#addl-items").append("<div class='top-15 addl-items'><ul>" +
        "<li class='sidebar_cart_product_description_image'>" +
        "</li>" +
        "<li class='sidebar_cart_product_description'>Rush Processing</li>" +
        "<li class='sidebar_cart_product_quantity'>" + 1 + "</li>" +
        "<li class='sidebar_cart_product_times'>×</li>" +
        "<li class='sidebar_cart_product_price'>$" + expeditedPrice + "</li>" +
        "</ul></div>");
        $("#orderTotal").text("$" + total);
        $("#subtotal").text("$" + subTotal);
        $("#rush").fadeOut("slow", function(){
            $("#submit").fadeIn();
        });
    });

    $("#rush-no").click(function () {
        $("#rush").fadeOut("slow", function(){
            $("#submit").fadeIn();
        });
    });

    $("#cybersafe-yes").click(function () {
        products["PCOS"] = qty;
        var pcos = parseFloat((qty * pcosPrice).toFixed(2));
        total = (parseFloat(total) + parseFloat(pcos)).toFixed(2);
        subTotal = (parseFloat(total - shipping)).toFixed(2);

        $("#addl-items").append("<div class='top-15 addl-items'><ul>" +
        "<li class='sidebar_cart_product_description_image'>" +
        "</li>" +
        "<li class='sidebar_cart_product_description'>PC Cleaner on Steroids</li>" +
        "<li class='sidebar_cart_product_quantity'>" + qty + "</li>" +
        "<li class='sidebar_cart_product_times'>×</li>" +
        "<li class='sidebar_cart_product_price'>$" + pcosPrice + "</li>" +
        "</ul></div>");
        $("#orderTotal").text("$" + total);
        $("#subtotal").text("$" + subTotal);
        $("#cybersafe").fadeOut("slow", function(){
            $("#paynow").fadeIn();
        });
    });

    $("#cybersafe-no").click(function () {
        $("#cybersafe").fadeOut("slow", function(){
            $("#paynow").fadeIn();
        });
    });

    $("#cspc-yes").click(function () {
        products["CSPC30"] = qty;
        console.log(products);
        var cspc = parseFloat((qty * cspcPrice).toFixed(2));
        total = (parseFloat(total) + parseFloat(cspc)).toFixed(2);
        subTotal = (parseFloat(total - shipping)).toFixed(2);

        $("#addl-items").append("<div class='top-15 addl-items'><ul>" +
            "<li class='sidebar_cart_product_description_image'>" +
            "</li>" +
            "<li class='sidebar_cart_product_description'>CybserSafePC</li>" +
            "<li class='sidebar_cart_product_quantity'>" + qty + "</li>" +
            "<li class='sidebar_cart_product_times'>×</li>" +
            "<li class='sidebar_cart_product_price'>$" + cspcPrice + "</li>" +
            "</ul></div>");
        $("#orderTotal").text("$" + total);
        $("#subtotal").text("$" + subTotal);
        $("#cspc").fadeOut("slow", function(){
            $("#rush").fadeIn();
        });
    });

    $("#cspc-no").click(function () {
        $("#cspc").fadeOut("slow", function(){
            $("#rush").fadeIn();
        });
    });

    $("#paynow-yes").click(function () {
        products["PAYNOW"] = "true";
        $('#freeness').hide();
        $('#today-free').hide();
        $('#today-after').hide();
        $("#addl-items").append("<div class='top-15 addl-items'><ul>" +
            "<li class='sidebar_cart_product_description_image'>" +
            "</li>" +
            "<li class='sidebar_cart_product_description'>CybserSafePC</li>" +
            "<li class='sidebar_cart_product_quantity'>" + qty + "</li>" +
            "<li class='sidebar_cart_product_times'>×</li>" +
            "<li class='sidebar_cart_product_price'>FREE</li>" +
            "</ul></div>");
        $("#paynow").fadeOut("slow", function(){
            $("#rush").fadeIn();
        });
    });

    $("#paynow-no").click(function () {
        products["PAYNOW"] = "false";
        $("#paynow").fadeOut("slow", function(){
            $("#cspc").fadeIn();
        });
    });

    $("#emergency-yes").click(function() {
        products["EMER"] = qty;
        var emer = parseFloat((qty * emergencyPrice).toFixed(2));
        total = (parseFloat(total) + parseFloat(emer)).toFixed(2);
        subTotal = (parseFloat(total - shipping)).toFixed(2);
        $("#addl-items").append("<div class='addl-items top-15'><ul>" +
        "<li class='sidebar_cart_product_description_image'>" +
        "</li>" +
        "<li class='sidebar_cart_product_description'>Emergency Service</li>" +
        "<li class='sidebar_cart_product_quantity'>" + qty + "</li>" +
        "<li class='sidebar_cart_product_times'>×</li>" +
        "<li class='sidebar_cart_product_price'>$" + emergencyPrice + "</li>" +
        "</ul></div>");
        $("#orderTotal").text("$" + total);
        $("#subtotal").text("$" + subTotal);
        $("#emergency").fadeOut("slow", function(){
            $("#submit").fadeIn();
        });
    });

    $("#continue-upsells").click(function(){
        saleable = true;
    });
    $("#qty-changed").click(function(){
        qty = $("#new-quantity").val();
        $("#quantity-modal").modal("hide");
        $(".sidebar_cart_product_quantity").text(qty);
        total = (qty * defaultKurePrice + shipping).toFixed(2);
        subTotal = (qty * defaultKurePrice).toFixed(2);
        $("#orderTotal").text("$" + total);
        $("#subtotal").text("$" + subTotal);

    });
    $("#reset_cart").click(function() {
        $(".addl-items").remove();
        products.length = 0;
        total = (qty * defaultKurePrice + shipping).toFixed(2);
        subTotal = (qty * defaultKurePrice).toFixed(2);
        $("#orderTotal").text("$" + total);
        $("#subtotal").text("$" + subTotal);

        if(saleable){

            $('#freeness').show();
            $("#hwmon").hide();
            $("#rush").hide();
            $("#cybersafe").hide();
            $("#emergency").hide();
            $("#paynow").hide();
            $("#submit").hide();
            $('#errors').hide();
            $("#hwmon").fadeIn();
            $("#quantity-modal").modal("show");
        }else{
            $("#quantity-modal").modal("show");
        }
    });

    $("#emergency-no").click(function() {
        $("#emergency").fadeOut("slow", function(){
            $("#submit").fadeIn();
        });

    });

    $("#submitOrder").click(function(){
        sale = true;
        $("#submitOrder").hide();
        $("#pw").show();
        var a = $('#firstForm').serialize();
        var b = $('#secondForm').serialize();
        var c = $('#paymentForm').serialize();
        products["KURE"] = qty;
        products["SHIP"] = 1;
        //products["AV"] = qty;
        var p = $.param(products);
        var d = a + '&' + b+ '&' +c + '&' +p;
        //console.log(d);
        $.ajax({
            type: "POST",
            url: '/checkout',
            data: d,
            success:function(data){
                $("#pw").hide();
                $("#submitOrder").show();
                if(data.indexOf('success') >= 0){
                    var trans = data.substr(data.indexOf("=") + 1);
                    window.location = "/thankyou?t=" + trans;
                }else{
                    console.log(data);
                    $("#pw").hide();
                    $("#submitOrder").show();
                    $('#errors').show();
                    $('#errors').addClass('alert alert-danger');
                    $('#errors').html(data);
                }
            },
            error:function(data){
                $("#pw").hide();
                $("#submitOrder").show();
                console.log(data);
                $('#errors').show();
                $('#errors').addClass('alert alert-info');
                $('#errors').html('An error has occurred, The Kure Staff has been notified, you will be contacted once we have resolved the issue.');
            }
        });
    });
    //window.onbeforeunload = function(){ if(prompt('Are you sure you want to leave?')) {
    //    //$.get( "myphp.php", { val: 1 } );
    //    alert('ok');
    //} else return false; };
});

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;
    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
