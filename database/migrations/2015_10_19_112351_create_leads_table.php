\<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lead_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('add1');
            $table->string('add2');
            $table->string('city');
            $table->string('state');
            $table->string('zip');
            $table->string('email');
            $table->string('phone');
            $table->string('password');
            $table->string('step');
            $table->string('auth_profile');
            $table->string('default_billing');
            $table->string('default_shipping');
            $table->boolean('has_converted');
            $table->string('quantity');
            $table->boolean('HWMON');
            $table->boolean('CSPC');
            $table->boolean('PROC');
            $table->boolean('PCOS');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('leads');
    }
}
