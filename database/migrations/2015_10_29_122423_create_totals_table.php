<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTotalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('totals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date');
            $table->integer('total');
            $table->integer('ivr_total');
            $table->integer('ivr_paid');
            $table->integer('ivr_trial');
            $table->integer('web_total');
            $table->integer('web_paid');
            $table->integer('web_trial');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
