<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendingSupportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pending_support', function(Blueprint $table){
            $table->increments('id');
            $table->integer('order_id');
            $table->string('name');
            $table->string('phone');
            $table->string('quantity');
            $table->string('assigned_to');
            $table->integer('resolved');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
