<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name' => 'The Kure',
            'price' => 1995,
            'description' => 'Antivirus is the treatment, we are The Kure!',
            'recurring' => true,
            'ivr_code' => 'KURE',
            'api_code' => 'KURE'
        ]);

        Product::create([
            'name' => 'Hardware Monitoring',
            'price' => 495,
            'description' => 'Get the alert if anything goes wrong in real-time!',
            'recurring' => true,
            'ivr_code' => 'HWMONK',
            'api_code' => 'HWMON'
        ]);

        Product::create([
            'name' => 'CyberSafePC',
            'price' => 0,
            'description' => 'Award winning, powerful software!',
            'recurring' => true,
            'ivr_code' => 'FCSPC',
            'api_code' => 'CSPC'
        ]);

        Product::create([
            'name' => 'PC Cleaner On Steroids',
            'price' => 895,
            'description' => 'Slow computer? Try this!',
            'recurring' => true,
            'ivr_code' => 'PCOS',
            'api_code' => 'PCOS'
        ]);

        Product::create([
            'name' => 'Magic Flash Drive',
            'price' => 3995,
            'description' => 'US Based Tech Support 24/7/365',
            'recurring' => true,
            'ivr_code' => 'MFD',
            'api_code' => 'FIXMYPC'
        ]);

        Product::create([
            'name' => 'Expedited Processing',
            'price' => 199,
            'description' => 'We rush your order to the front of the line!',
            'recurring' => false,
            'ivr_code' => 'PROC',
            'api_code' => ''
        ]);

        Product::create([
            'name' => 'Emergency Service',
            'price' => 1995,
            'description' => 'Get help now!',
            'recurring' => false,
            'ivr_code' => 'EMER',
            'api_code' => ''
        ]);
    }
}
