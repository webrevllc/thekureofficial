<?php

namespace App\Providers;

use App\Contracts\SubscriptionContract;
use App\Contracts\UserPaymentContract;
use App\Order;
use App\Services\SubscriptionService;
use App\Services\UserPaymentService;
use AuthorizeNetAddress;
use AuthorizeNetCustomer;
use AuthorizeNetPaymentProfile;
use AuthorizeNetTD;
use AuthorizeNetCIM;
use AuthorizeNetAIM;
use AuthorizeNetLineItem;
use App\Contracts\Payment;
use AuthorizeNetTransaction;
use App\Services\AuthPayment;
use App\Services\IVRPayments;
use App\Services\WebPayments;
use App\Contracts\WebPaymentContract;
use App\Contracts\IVRPaymentContract;
use Illuminate\Support\ServiceProvider;

class AuthNetServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $login = env('AUTHORIZENET_API_LOGIN_ID');
        $transaction = env('AUTHORIZENET_TRANSACTION_KEY');
        define("AUTHORIZENET_SANDBOX", env('AUTHORIZENET_SANDBOX'));

        $this->app->bind(Payment::class, function() use($login, $transaction){
            return new AuthPayment(
                new AuthorizeNetAIM($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetCIM($api_login_id = $login, $transaction_key = $transaction),
                new Order,
                new AuthorizeNetTD($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetTransaction($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetLineItem($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetCustomer($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetPaymentProfile($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetAddress($api_login_id = $login, $transaction_key = $transaction)
            );
        });

        $this->app->bind(WebPaymentContract::class, function() use($login, $transaction){
            return new WebPayments(
                new AuthorizeNetAIM($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetCIM($api_login_id = $login, $transaction_key = $transaction),
                new Order,
                new AuthorizeNetTD($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetTransaction($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetLineItem($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetCustomer($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetPaymentProfile($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetAddress($api_login_id = $login, $transaction_key = $transaction)
            );
        });

        $this->app->bind(IVRPaymentContract::class, function() use($login, $transaction){
            return new IVRPayments(
                new AuthorizeNetAIM($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetCIM($api_login_id = $login, $transaction_key = $transaction),
                new Order,
                new AuthorizeNetTD($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetTransaction($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetLineItem($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetCustomer($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetPaymentProfile($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetAddress($api_login_id = $login, $transaction_key = $transaction)
            );
        });

        $this->app->bind(UserPaymentContract::class, function() use($login, $transaction){
            return new UserPaymentService(
                new AuthorizeNetAIM($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetCIM($api_login_id = $login, $transaction_key = $transaction),
                new Order,
                new AuthorizeNetTD($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetTransaction($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetLineItem($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetCustomer($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetPaymentProfile($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetAddress($api_login_id = $login, $transaction_key = $transaction)
            );
        });

        $this->app->bind(SubscriptionContract::class, function() use($login, $transaction){
            return new SubscriptionService(
                new AuthorizeNetAIM($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetCIM($api_login_id = $login, $transaction_key = $transaction),
                new Order,
                new AuthorizeNetTD($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetTransaction($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetLineItem($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetCustomer($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetPaymentProfile($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetAddress($api_login_id = $login, $transaction_key = $transaction)
            );
        });
    }
}
