<?php

namespace App\Providers;

use App\Contracts\LegacyPayment;
use App\Services\LegacyAuthPayment;
use App\Order;
use AuthorizeNetAIM;
use AuthorizeNetCIM;
use AuthorizeNetLineItem;
use AuthorizeNetTD;
use AuthorizeNetTransaction;
use Illuminate\Support\ServiceProvider;

class LegacyAuthPaymentServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(LegacyPayment::class, function(){
            $login = '9kRX6dV2Ta';
            $transaction = '9C99t9fE62pEVs3m';
            return new LegacyAuthPayment(
                new AuthorizeNetAIM($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetCIM($api_login_id = $login, $transaction_key = $transaction),
                new Order,
                new AuthorizeNetTD($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetTransaction($api_login_id = $login, $transaction_key = $transaction),
                new AuthorizeNetLineItem($api_login_id = $login, $transaction_key = $transaction)
            );
        });
    }
}
