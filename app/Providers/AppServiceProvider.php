<?php

namespace App\Providers;





use App\Contracts\Invisacloud;
use App\Services\InvisacloudConnector;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Invisacloud::class, function(){
            return new InvisacloudConnector(new Client(['base_uri' => 'https://www.invisacloud.com']));
        });
    }
}
