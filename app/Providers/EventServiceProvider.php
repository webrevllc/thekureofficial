<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\UserSignedUp' => [
            'App\Listeners\WelcomeTheUser',
        ],
        'App\Events\OrderPlacedOnWebsite' => [
            'App\Listeners\Web\CreateTheOrder',
            'App\Listeners\Web\SetAuthNetProfileIDsOnUser',
            'App\Listeners\Web\CreateSubscription',
            'App\Listeners\Web\SettAddressAssignSerial',
            'App\Listeners\Web\MakeInvisacloudUser',
            'App\Listeners\Web\UploadOrderToInvisacloud',
            'App\Listeners\Web\mailNotificationToBosses',
            'App\Listeners\Web\EmailCustomerReceipt',
        ],
        'App\Events\IVR\OrderPlacedByIVR' => [
            'App\Listeners\IVR\Success\CreateIVROrder',
            'App\Listeners\IVR\Success\SetIVRAuthNetProfileIDsOnUser',
            'App\Listeners\IVR\Success\CreateIVRSubscription',
            'App\Listeners\IVR\Success\SettAddressAssignSerial',
            'App\Listeners\IVR\Success\MakeInvisacloudUser',
            'App\Listeners\IVR\Success\MakeInvisacloudProduct',
            'App\Listeners\IVR\Success\EmailTheCustomer',
            'App\Listeners\IVR\Success\EmailNotificationToBosses',
        ],
        'App\Events\IVR\FailedIVROrder' => [
            'App\Listeners\IVR\Failed\CreateFailedIVROrder',
            'App\Listeners\IVR\Failed\SendEmails',
        ],
        'App\Events\Subscription\SuccessfulSubscriptionRenewal' => [
            'App\Listeners\Subscription\Success\CreateSubscriptionOrder',
            'App\Listeners\Subscription\Success\UpdateSubscription',
            'App\Listeners\Subscription\Success\SendSuccessEmails',
            'App\Listeners\Subscription\Success\UpdateInvisacloudSubscription',
        ],
        'App\Events\Subscription\FailedSubscriptionRenewal' => [
//            'App\Listeners\Subscription\Failed\CreateNewFailedSubscriptionOrder',
//            'App\Listeners\Subscription\Failed\UpdateFailedSubscription',
//            'App\Listeners\Subscription\Failed\SendFailedEmails',
        ],
        'App\Events\Subscription\SubscriptionModifiedEvent' => [
            'App\Listeners\Subscription\Modified\MakeChangeOrder',
            'App\Listeners\Subscription\Modified\EmailCustomer',
        ],
        'App\Events\Question' => [
            'App\Listeners\Support\NewQuestion',
        ],

        'App\Events\SupportIssue' => [
            'App\Listeners\Support\NewSupportIssue',
        ],

        'App\Events\OrderHasShipped' => [
            'App\Listeners\Shipped\UpdateSubscription',
            'App\Listeners\Shipped\EmailCustomer',
            'App\Listeners\Shipped\UpdateInvisacloudProducts',
        ],
        'App\Events\Refunds\PartialRefund' => [
            'App\Listeners\Refund\Partial\UpdateSubscription',
            'App\Listeners\Refund\Partial\CreateRefund',
            'App\Listeners\Refund\Partial\EmailCustomer',
            'App\Listeners\Refund\Partial\MakeChangeOrder',
        ],
        'App\Events\Refunds\FullRefund' => [
            'App\Listeners\Refund\Full\UpdateSubscription',
            'App\Listeners\Refund\Full\CreateRefund',
            'App\Listeners\Refund\Full\EmailCustomer',
        ],
        'App\Events\Sale\AddOnSale' => [
            'App\Listeners\AddOnSale\CreateOrder',
            'App\Listeners\AddOnSale\UpdateSubscription',
            'App\Listeners\AddOnSale\EmailCustomer',
            'App\Listeners\AddOnSale\EmailBosses',
        ],
        'App\Events\IVR\TrialOrderPlacedByIVR' => [
            'App\Listeners\IVR\TrialSuccess\CreateIVROrder',
            'App\Listeners\IVR\TrialSuccess\CreateTrialIVRSubscription',
            'App\Listeners\IVR\TrialSuccess\SettAddressAssignSerial',
            'App\Listeners\IVR\TrialSuccess\MakeInvisacloudUser',
            'App\Listeners\IVR\TrialSuccess\MakeInvisacloudProduct',
            'App\Listeners\IVR\TrialSuccess\EmailTheCustomer',
            'App\Listeners\IVR\TrialSuccess\EmailNotificationToBosses',
        ],
        'App\Events\TrialOrderPlacedOnWebsite' => [
            'App\Listeners\Web\TrialSuccess\CreateTrialWebOrder',
            'App\Listeners\Web\TrialSuccess\CreateTrialWebSubscription',
            'App\Listeners\Web\TrialSuccess\SettAddressAssignSerial',
            'App\Listeners\Web\TrialSuccess\MakeInvisacloudUser',
            'App\Listeners\Web\TrialSuccess\MakeInvisacloudProduct',
            'App\Listeners\Web\TrialSuccess\EmailTheCustomer',
            'App\Listeners\Web\TrialSuccess\EmailNotificationToBosses',
        ],
        'App\Events\Crons\CSVComplete' => [
            'App\Listeners\Crons\FirePDF',
        ],
        'App\Events\Crons\PDFComplete' => [
            'App\Listeners\Crons\FireZippy',
        ],
        'App\Events\Crons\ZippyComplete' => [
            'App\Listeners\Crons\NotifyComplete',
        ],
        'App\Events\WebNew\TrialOrderComplete' => [
            'App\Listeners\WebNew\TrialSuccess\CreateTrialWebOrder',
            'App\Listeners\WebNew\TrialSuccess\CreateTrialWebSubscription',
            'App\Listeners\WebNew\TrialSuccess\SettAddressAssignSerial',
            'App\Listeners\WebNew\TrialSuccess\UpdateReferral',
            'App\Listeners\WebNew\TrialSuccess\MakeInvisacloudUser',
            'App\Listeners\WebNew\TrialSuccess\MakeInvisacloudProduct',
            'App\Listeners\WebNew\TrialSuccess\EmailTheCustomer',
            'App\Listeners\WebNew\TrialSuccess\EmailNotificationToBosses',
        ],
        'App\Events\WebNew\EasyPayOrderComplete' => [
            'App\Listeners\WebNew\EasyPayOrderComplete\CreateEasyPayWebOrder',
            'App\Listeners\WebNew\EasyPayOrderComplete\CreateEasyPayWebSubscription',
            'App\Listeners\WebNew\EasyPayOrderComplete\SetAddressAssignSerial',
            'App\Listeners\WebNew\EasyPayOrderComplete\UpdateReferral',
            'App\Listeners\WebNew\EasyPayOrderComplete\MakeInvisacloudUser',
            'App\Listeners\WebNew\EasyPayOrderComplete\MakeInvisacloudProduct',
            'App\Listeners\WebNew\EasyPayOrderComplete\EmailTheCustomer',
            'App\Listeners\WebNew\EasyPayOrderComplete\EmailNotificationToBosses',
        ],
        'App\Events\WebNew\NewPaidOrderComplete' => [
            \App\Listeners\WebNew\PayNowSuccess\CreateTrialWebOrder::class,
            \App\Listeners\WebNew\PayNowSuccess\CreateTrialWebSubscription::class,
            \App\Listeners\WebNew\PayNowSuccess\SettAddressAssignSerial::class,
            \App\Listeners\WebNew\PayNowSuccess\UpdateReferral::class,
            \App\Listeners\WebNew\PayNowSuccess\MakeInvisacloudUser::class,
            \App\Listeners\WebNew\PayNowSuccess\MakeInvisacloudProduct::class,
            \App\Listeners\WebNew\PayNowSuccess\EmailTheCustomer::class,
            \App\Listeners\WebNew\PayNowSuccess\EmailNotificationToBosses::class,
            \App\Listeners\WebNew\PayNowSuccess\EmailSupport911::class,
            \App\Listeners\WebNew\PayNowSuccess\AddOrderToPendingSupportTable::class,
        ],
        'App\Events\WebNew\NewLifetimePaidOrderComplete' => [
            'App\Listeners\WebNew\PayNowLifetimeSuccess\CreateOrder',
            \App\Listeners\WebNew\PayNowLifetimeSuccess\CreateSubscription::class,
            'App\Listeners\WebNew\PayNowLifetimeSuccess\SetAddressAssignSerial',
            'App\Listeners\WebNew\PayNowLifetimeSuccess\MakeInvisacloudUser',
            'App\Listeners\WebNew\PayNowLifetimeSuccess\MakeInvisacloudProduct',
            'App\Listeners\WebNew\PayNowLifetimeSuccess\EmailTheCustomer',
            'App\Listeners\WebNew\PayNowLifetimeSuccess\EmailNotificationToBosses',
        ],
        'App\Events\WebNew\EmergencySaleMade' => [
            'App\Listeners\WebNew\EmergencySaleMade\CreateOrder',
            'App\Listeners\WebNew\EmergencySaleMade\CreateSubscription',
            'App\Listeners\WebNew\EmergencySaleMade\SetAddressAssignSerial',
            'App\Listeners\WebNew\EmergencySaleMade\MakeInvisacloudUser',
            'App\Listeners\WebNew\EmergencySaleMade\MakeInvisacloudProduct',
            'App\Listeners\WebNew\EmergencySaleMade\EmailTheCustomer',
            'App\Listeners\WebNew\EmergencySaleMade\EmailNotificationToBosses',
            'App\Listeners\WebNew\EmergencySaleMade\EmailChad',
            'App\Listeners\WebNew\EmergencySaleMade\AddOrderToPendingSupportTable',
        ],
        'App\Events\IVRNew\TrialIVROrder' => [
            'App\Listeners\IVRNew\TrialSuccess\CreateIVROrder',
            'App\Listeners\IVRNew\TrialSuccess\CreateTrialIVRSubscription',
            'App\Listeners\IVRNew\TrialSuccess\SettAddressAssignSerial',
            'App\Listeners\IVRNew\TrialSuccess\MakeInvisacloudUser',
            'App\Listeners\IVRNew\TrialSuccess\MakeInvisacloudProduct',
            'App\Listeners\IVRNew\TrialSuccess\EmailTheCustomer',
            'App\Listeners\IVRNew\TrialSuccess\EmailNotificationToBosses',
        ],
        \App\Events\IVRNew\PaidIVROrder::class => [
            \App\Listeners\IVRNew\PaidSuccess\CreateIVROrder::class,
            \App\Listeners\IVRNew\PaidSuccess\CreateTrialIVRSubscription::class,
            \App\Listeners\IVRNew\PaidSuccess\SettAddressAssignSerial::class,
            \App\Listeners\IVRNew\PaidSuccess\MakeInvisacloudUser::class,
            \App\Listeners\IVRNew\PaidSuccess\MakeInvisacloudProduct::class,
            \App\Listeners\IVRNew\PaidSuccess\EmailTheCustomer::class,
            \App\Listeners\IVRNew\PaidSuccess\EmailNotificationToBosses::class,
        ],
        'App\Events\Refunds\TrialCancelled' => [
            'App\Listeners\Refund\Trial\CreateRefund',
            'App\Listeners\Refund\Trial\EmailUser',
        ],
        'App\Events\AdminOrder\UserFreeTrialOrder' => [
            'App\Listeners\AdminOrder\TrialSuccess\CreateTrialWebOrder',
            'App\Listeners\AdminOrder\TrialSuccess\CreateTrialWebSubscription',
            'App\Listeners\AdminOrder\TrialSuccess\SettAddressAssignSerial',
            'App\Listeners\AdminOrder\TrialSuccess\MakeInvisacloudUser',
            'App\Listeners\AdminOrder\TrialSuccess\MakeInvisacloudProduct',
            'App\Listeners\AdminOrder\TrialSuccess\EmailTheCustomer',
        ],
        'App\Events\Subscription\FailedCancelled' => [
            'App\Listeners\Subscription\CancelFailed\UpdateSubscription',
            'App\Listeners\Subscription\CancelFailed\UpdateFailedSub',
            'App\Listeners\Subscription\CancelFailed\EmailUser',
        ],
        'App\Events\IVRNew\DeclinedIVROrder' => [
            'App\Listeners\IVRNew\DeclinedIVROrder\CreateIVRLead',
            'App\Listeners\IVRNew\DeclinedIVROrder\EmailCustomer',
        ],
        'App\Events\WebNew\CreateEmerSale' => [
            'App\Listeners\WebNew\CreateEmerSale\CreateOrder',
            'App\Listeners\WebNew\CreateEmerSale\EmailTheCustomer',
            'App\Listeners\WebNew\CreateEmerSale\EmailNotificationToBosses',
            'App\Listeners\WebNew\CreateEmerSale\AddOrderToPendingSupportTable',
        ],
        'App\Events\IVRNew\CreateEmerSale' => [
            'App\Listeners\IVRNew\CreateEmerSale\CreateOrder',
            'App\Listeners\IVRNew\CreateEmerSale\EmailTheCustomer',
            'App\Listeners\IVRNew\CreateEmerSale\EmailNotificationToBosses',
            'App\Listeners\IVRNew\CreateEmerSale\AddOrderToPendingSupportTable',
        ],

    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
