<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PendingSupport extends Model
{
    protected $table = 'pending_support';

    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}
