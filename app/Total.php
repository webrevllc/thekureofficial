<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Total extends Model
{
    protected $table = "totals";
    protected $fillable = ['date', 'total', 'ivr_total', 'ivr_paid', 'ivr_trial', 'web_total', 'web_paid', 'web_trial'];

    public function updateTotals($type, $amount)
    {
        $date = Carbon::today();
        if($this->where('date', $date)->get()->isEmpty()){
            $this->date = $date;
            $this->save();
        }
        $today = $this->where('date', $date)->first();

        $old_type = $today->$type;
        $new_type = $old_type + $amount;
        $today->$type =  number_format($new_type, 2);

        if (strpos($type,'ivr') !== false) {
            $old_ivr_total = $today->ivr_total;
            $today->ivr_total = $old_ivr_total + $amount;
        }else{
            $old_web_total = $today->web_total;
            $today->web_total = number_format($old_web_total + $amount, 2);
        }

        $old_total = $today->total;
        $today->total =  number_format($old_total + $amount, 2);

        $today->save();
    }
}
