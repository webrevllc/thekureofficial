<?php

namespace App;

use App;
use App\Contracts\Payment;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Serial extends Model
{
    protected $table = 'serials';
    protected $fillable = ['serial', 'order_id', 'is_used', 'name', 'transaction', 'member_since', 'assigned_date'];


    public function count()
    {
        return Serial::where('is_used', 0)->count();
    }

    public function assignSerial($orderID)
    {
        $order = Order::with('user','subscription')->find($orderID);

        if($order->status == 'failed'){
        }else{
            if(strtolower($order->user->name) == "shane rosenthal" ||
                strtolower($order->user->name) == "peter spezza" ||
                strtolower($order->user->name) == "john miles" ||
                strtolower($order->user->name) == "mel arthur" ||
                strtolower($order->user->name) == "eddie taylor" ||
                strtolower($order->user->name) == "jeffrey rosenthal"
            ){
                //DO NOT ASSIGN SERIALS FOR ADMINS
            }else{

                $quantity = $this->getKureQtyInOrder($order);

                for($q=0;$q<$quantity;$q++){
                    $address = Address::where('order_id', $order->id)->first();
                    $s = Serial::where('is_used', 0)->first();
                    $s->name = $order->user->name;
                    $s->subscription_id = $order->subscription->id;
                    $s->is_used = true;
                    $s->user_id = $order->user->id;
                    $s->transaction = $order->invoice_number;
                    $s->member_since = date('F j, Y', strtotime($order->user->created_at));
                    $s->assigned_date = Carbon::now();
                    $s->address = ucwords(strtolower($address->address));
                    $s->city = ucwords(strtolower($address->city));
                    $s->state = $address->state;
                    $s->zip = ucwords(strtolower($address->zip));
                    $order->serials()->save($s);
                }
            }
        }
    }

    public function getKureQtyInOrder($order)
    {
        $prods = json_decode($order->products);
        $quantity = 0;
        foreach($prods as $p){
            if($p->web_code == "KURE"){
                $quantity = $p->quantity;
                break;
            }
        }
        return $quantity;
    }

    public function order(){
        return $this->belongsTo('App\Order');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }


}
