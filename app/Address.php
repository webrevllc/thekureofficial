<?php

namespace App;

use App;
use App\Contracts\Payment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class Address extends Model
{
    protected $table = "addresses";
    protected $fillable = ['name', 'address', 'city', 'state', 'zip', 'member_since', 'transaction', 'serials'];

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function makeAddress($orderID)
    {
        $order = Order::with('user')->find($orderID);
        if ($order != null) {
            if(strtolower($order->user->name) == "shane rosenthal" || $order->user->name == "Peter Spezza"){
            }else{
                if($order->status != 'failed'){
                    if ($order->address == null) {
                        if($this->addAddress($order))
                            $array[] = $order;
                    }
                }
            }
        }
    }

    public function addAddress($order)
    {
        $payment = App::make(Payment::class);
        $cimAdd = $payment->getShippingAddress($order->user->auth_profile, $order->user->default_shipping);
        if(!$cimAdd){
            Mail::raw($order->user->id. ' Address not created', function ($message) {
                $message->to('srosenthal82@gmail.com');
                $message->cc('jrosenthal@computercentersusa.com');
                $message->subject('Address not created');
            });
        }else{
            $address = new Address([
                'order_id' => $order->id,
                'name' => $cimAdd->firstName . ' ' . $cimAdd->lastName,
                'address' => $cimAdd->address,
                'city' => $cimAdd->city,
                'state' => $cimAdd->state,
                'zip' => $cimAdd->zip,
            ]);
            $order->address()->save($address);
        }
    }

//    public function createAddress($orderInfo, $type)
//    {
//        if($type ==)
//
//    }
}
