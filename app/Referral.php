<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referral extends Model
{
    protected $fillable = ['referring_transaction_number', 'completed_referral_transaction_number'];
}
