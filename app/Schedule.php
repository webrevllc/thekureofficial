<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table = 'schedules';

    public function airdates()
    {
        return $this->hasMany('App\AirDate');
    }
}
