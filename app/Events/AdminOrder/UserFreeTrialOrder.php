<?php

namespace App\Events\AdminOrder;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserFreeTrialOrder extends Event
{
    use SerializesModels;

    /**
     * @var
     */
    public $products;
    /**
     * @var
     */
    public $user;
    /**
     * @var
     */
    public $invoice_number;
    /**
     * @var
     */
    public $total;

    /**
     * Create a new event instance.
     *
     * @param $products
     * @param $user
     * @param $invoice_number
     * @param $total
     */
    public function __construct($products, $user, $invoice_number, $total)
    {
        $this->products = $products;
        $this->user = $user;
        $this->invoice_number = $invoice_number;
        $this->total = $total;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
