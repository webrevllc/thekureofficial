<?php

namespace App\Events;

use App\Events\Event;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OrderPlacedOnWebsite extends Event implements ShouldQueue
{
    use SerializesModels;
    /**
     * @var Request
     */
    public $request;
    /**
     * @var
     */
    public $transactionID;
    /**
     * @var
     */
    public $invoiceNumber;
    /**
     * @var
     */
    public $total;
    /**
     * @var
     */
    public $last4;
    /**
     * @var
     */
    public $products;
    /**
     * @var User
     */
    public $user;
    /**
     * @var
     */
    public $city;
    /**
     * @var
     */
    public $state;
    /**
     * @var
     */
    public $quantity;

    /**
     * Create a new event instance.
     *
     * @param Request $request
     * @param $transactionID
     * @param $invoiceNumber
     * @param $total
     * @param $last4
     * @param $products
     * @param User $user
     * @param $city
     * @param $state
     * @param $quantity
     */
    public function __construct($request, $transactionID, $invoiceNumber, $total, $last4, $products, User $user, $city, $state, $quantity) //remove lots replace with order id ;)
    {
        //
        $this->request = $request;
        $this->transactionID = $transactionID;
        $this->invoiceNumber = $invoiceNumber;
        $this->total = $total;
        $this->last4 = $last4;
        $this->products = $products;
        $this->user = $user;
        $this->city = $city;
        $this->state = $state;
        $this->quantity = $quantity;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
