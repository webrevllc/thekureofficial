<?php

namespace App\Events\WebNew;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class FreeTrialWithEMER extends Event
{
    use SerializesModels;
    /**
     * @var
     */
    public $lead;
    /**
     * @var
     */
    public $products;
    /**
     * @var
     */
    public $user;
    /**
     * @var
     */
    public $invoice_number;


    /**
     * FreeTrialWithEMER constructor.
     * @param $lead
     * @param $products
     * @param $user
     * @param $invoice_number
     */
    public function __construct($lead, $products, $user, $invoice_number)
    {
        $this->lead = $lead;
        $this->products = $products;
        $this->user = $user;
        $this->invoice_number = $invoice_number;
    }
}
