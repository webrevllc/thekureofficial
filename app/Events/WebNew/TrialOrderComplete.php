<?php

namespace App\Events\WebNew;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TrialOrderComplete extends Event
{
    use SerializesModels;
    /**
     * @var
     */
    public $lead;
    /**
     * @var
     */
    public $products;
    /**
     * @var
     */
    public $user;
    /**
     * @var
     */
    public $invoice_number;

    /**
     * Create a new event instance.
     *
     * @param $lead
     * @param $products
     * @param $user
     * @param $invoice_number
     */
    public function __construct($lead, $products, $user, $invoice_number)
    {
        $this->lead = $lead;
        $this->products = $products;
        $this->user = $user;
        $this->invoice_number = $invoice_number;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
