<?php

namespace App\Events\WebNew;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CreateEmerSale extends Event
{
    use SerializesModels;
    /**
     * @var
     */
    public $lead;
    /**
     * @var
     */
    public $emer;
    /**
     * @var
     */
    public $user;
    /**
     * @var
     */
    public $invoice_number;
    /**
     * @var
     */
    public $transaction_id;

    /**
     * Create a new event instance.
     *
     * @param $lead
     * @param $emer
     * @param $user
     * @param $invoice_number
     * @param $transaction_id
     */
    public function __construct($lead, $emer, $user, $invoice_number, $transaction_id)
    {
        $this->lead = $lead;
        $this->emer = $emer;
        $this->user = $user;
        $this->invoice_number = $invoice_number;
        $this->transaction_id = $transaction_id;
    }

}
