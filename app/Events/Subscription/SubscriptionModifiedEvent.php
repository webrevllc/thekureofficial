<?php

namespace App\Events\Subscription;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SubscriptionModifiedEvent extends Event
{
    use SerializesModels;
    /**
     * @var
     */
    public $subscription;
    /**
     * @var
     */
    public $requestData;

    /**
     * Create a new event instance.
     *
     * @param $subscription
     * @param $requestData
     */
    public function __construct($subscription, $requestData)
    {
        $this->subscription = $subscription;
        $this->requestData = $requestData;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
