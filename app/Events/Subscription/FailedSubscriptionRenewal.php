<?php

namespace App\Events\Subscription;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class FailedSubscriptionRenewal extends Event
{
    use SerializesModels;

    /**
     * @var
     */
    public $subscription;
    /**
     * @var
     */
    public $transactionId;

    /**
     * Create a new event instance.
     *
     * @param $subscription
     * @param $transactionId
     */
    public function __construct($subscription, $transactionId)
    {
        $this->subscription = $subscription;
        $this->transactionId = $transactionId;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
