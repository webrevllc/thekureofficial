<?php

namespace App\Events\Subscription;

use App\Events\Event;
use App\Subscription;
use Illuminate\Queue\SerializesModels;

class FailedCancelled extends Event
{
    use SerializesModels;
    /**
     * @var
     */
    public $subscription;
    /**
     * @var
     */
    public $failed;

    /**
     * Create a new event instance.
     *
     * @param $subscription
     * @param $failed
     */
    public function __construct($subscription, $failed)
    {
        $this->subscription = $subscription;
        $this->failed = $failed;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
