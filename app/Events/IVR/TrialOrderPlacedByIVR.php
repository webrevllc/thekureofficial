<?php

namespace App\Events\IVR;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TrialOrderPlacedByIVR extends Event
{
    use SerializesModels;

    public $orderData;
    public $transactionID;
    public $invoiceNumber;
    public $total;
    public $products;
    public $user;
    public $city;
    public $state;
    public $trialornot;

    /**
     * Create a new event instance.
     *
     * @param $orderData
     * @param $transactionID
     * @param $invoiceNumber
     * @param $total
     * @param $last4
     * @param $products
     * @param $user
     * @param $city
     * @param $state
     */
    public function __construct($orderData, $transactionID, $invoiceNumber, $total, $products, $user, $city, $state, $trialornot)
    {
        //
        $this->orderData = $orderData;
        $this->transactionID = $transactionID;
        $this->invoiceNumber = $invoiceNumber;
        $this->total = $total;
        $this->products = $products;
        $this->user = $user;
        $this->city = $city;
        $this->state = $state;
        $this->trialornot = $trialornot;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
