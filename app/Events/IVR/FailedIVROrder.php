<?php

namespace App\Events\IVR;

use App\Events\Event;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class FailedIVROrder extends Event
{
    use SerializesModels;
    /**
     * @var
     */
    public $orderInfo;
    /**
     * @var
     */
    public $total;
    /**
     * @var
     */
    public $reason;
    /**
     * @var
     */
    public $productsOrdered;
    /**
     * @var
     */
    public $invoice_number;
    /**
     * @var
     */
    public $user;
    /**
     * @var
     */
    private $trialornot;

    /**
     * Create a new event instance.
     *
     * @param $orderInfo
     * @param $invoice_number
     * @param $total
     * @param $reason
     * @param $productsOrdered
     * @param $user
     * @param $trialornot
     */
    public function __construct($orderInfo, $invoice_number, $total, $reason, $productsOrdered, $user, $trialornot)
    {
        //
        $this->orderInfo = $orderInfo;
        $this->total = $total;
        $this->reason = $reason;
        $this->productsOrdered = $productsOrdered;
        $this->invoice_number = $invoice_number;
        $this->user = $user;
        $this->trialornot = $trialornot;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
