<?php

namespace App\Events\IVR;

use App\Events\Event;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OrderPlacedByIVR extends Event implements ShouldQueue
{
    use SerializesModels;
    /**
     * @var
     */
    public $orderData;
    /**
     * @var
     */
    public $transactionID;
    /**
     * @var
     */
    public $invoiceNumber;
    /**
     * @var
     */
    public $total;
    /**
     * @var
     */
    public $last4;
    /**
     * @var
     */
    public $products;
    /**
     * @var
     */
    public $user;
    /**
     * @var
     */
    public $city;
    /**
     * @var
     */
    public $state;


    /**
     * Create a new event instance.
     *
     * @param $orderData
     * @param $transactionID
     * @param $invoiceNumber
     * @param $total
     * @param $last4
     * @param $products
     * @param $user
     * @param $city
     * @param $state
     */
    public function __construct($orderData, $transactionID, $invoiceNumber, $total, $last4, $products, $user, $city, $state)
    {
        //
        $this->orderData = $orderData;
        $this->transactionID = $transactionID;
        $this->invoiceNumber = $invoiceNumber;
        $this->total = $total;
        $this->last4 = $last4;
        $this->products = $products;
        $this->user = $user;
        $this->city = $city;
        $this->state = $state;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
