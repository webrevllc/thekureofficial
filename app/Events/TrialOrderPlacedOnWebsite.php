<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TrialOrderPlacedOnWebsite extends Event
{
    use SerializesModels;
    /**
     * @var
     */
    public $request;
    /**
     * @var
     */
    public $transaction_id;
    /**
     * @var
     */
    public $invoice_number;
    /**
     * @var
     */
    public $total;
    /**
     * @var
     */
    public $products;
    /**
     * @var
     */
    public $user;
    /**
     * @var
     */
    public $quantity;
    /**
     * @var
     */
    public $city;
    /**
     * @var
     */
    public $state;

    /**
     * Create a new event instance.
     *
     * @param $request
     * @param $transaction_id
     * @param $invoice_number
     * @param $total
     * @param $products
     * @param $user
     * @param $quantity
     * @param $city
     * @param $state
     */
    public function __construct($request, $transaction_id, $invoice_number, $total, $products, $user, $quantity, $city, $state)
    {
        //
        $this->request = $request;
        $this->transaction_id = $transaction_id;
        $this->invoice_number = $invoice_number;
        $this->total = $total;
        $this->products = $products;
        $this->user = $user;
        $this->quantity = $quantity;
        $this->city = $city;
        $this->state = $state;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
