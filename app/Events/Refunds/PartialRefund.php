<?php

namespace App\Events\Refunds;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PartialRefund extends Event
{
    use SerializesModels;
    /**
     * @var
     */
    public $order;
    /**
     * @var
     */
    public $refundAmount;
    /**
     * @var
     */
    public $reason;
    /**
     * @var
     */
    public $products;
    /**
     * @var
     */
    public $newOrderTotal;
    /**
     * @var
     */
    public $subscriptionId;
    /**
     * @var
     */
    public $user;
    /**
     * @var
     */
    public $approvedRefundTransactionID;


    /**
     * Create a new event instance.
     *
     * @param $order
     * @param $refundAmount
     * @param $reason
     * @param $products
     * @param $newOrderTotal
     * @param $subscriptionId
     * @param $user
     * @param $approvedRefundTransactionID
     */
    public function __construct($order, $refundAmount, $reason, $products, $newOrderTotal, $subscriptionId, $user, $approvedRefundTransactionID)
    {
        $this->order = $order;
        $this->refundAmount = $refundAmount;
        $this->reason = $reason;
        $this->products = $products;
        $this->newOrderTotal = $newOrderTotal;
        $this->subscriptionId = $subscriptionId;
        $this->user = $user;
        $this->approvedRefundTransactionID = $approvedRefundTransactionID;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
