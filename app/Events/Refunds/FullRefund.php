<?php

namespace App\Events\Refunds;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class FullRefund extends Event
{
    use SerializesModels;
    /**
     * @var
     */
    public $order;
    /**
     * @var
     */
    public $refundAmount;
    /**
     * @var
     */
    public $subscriptionID;
    /**
     * @var
     */
    public $approvedRefundTransactionID;

    /**
     * Create a new event instance.
     *
     * @param $order
     * @param $refundAmount
     * @param $subscriptionID
     * @param $approvedRefundTransactionID
     */
    public function __construct($order, $refundAmount, $subscriptionID, $approvedRefundTransactionID)
    {
        $this->order = $order;
        $this->refundAmount = $refundAmount;
        $this->subscriptionID = $subscriptionID;
        $this->approvedRefundTransactionID = $approvedRefundTransactionID;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
