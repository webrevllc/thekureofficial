<?php

namespace App\Events\Refunds;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TrialCancelled extends Event
{
    use SerializesModels;
    /**
     * @var
     */
    public $subscription;

    /**
     * Create a new event instance.
     *
     * @param $subscription
     */
    public function __construct($subscription)
    {
        //
        $this->subscription = $subscription;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
