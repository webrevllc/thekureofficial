<?php

namespace App\Events\Sale;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AddOnSale extends Event
{
    use SerializesModels;
    /**
     * @var
     */
    public $user;
    /**
     * @var
     */
    public $products;
    /**
     * @var
     */
    public $transactionId;
    /**
     * @var
     */
    public $invoiceNumber;
    /**
     * @var
     */
    public $amount;

    /**
     * Create a new event instance.
     *
     * @param $user
     * @param $products
     * @param $transactionId
     * @param $invoiceNumber
     * @param $amount
     */
    public function __construct($user, $products, $transactionId, $invoiceNumber,$amount)
    {
        $this->user = $user;
        $this->products = $products;
        $this->transactionId = $transactionId;
        $this->invoiceNumber = $invoiceNumber;
        $this->amount = $amount;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
