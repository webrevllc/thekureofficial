<?php

namespace App\Events\IVRNew;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class CreateEmerSale extends Event
{
    use SerializesModels;
    /**
     * @var
     */
    public $orderInfo;
    /**
     * @var
     */
    public $user;
    /**
     * @var
     */
    public $invoice_number;
    /**
     * @var
     */
    public $transaction_id;
    /**
     * @var
     */
    public $emer;
    /**
     * @var
     */
    public $emerTotal;

    /**
     * Create a new event instance.
     *
     * @param $orderInfo
     * @param $user
     * @param $invoice_number
     * @param $transaction_id
     */
    public function __construct($orderInfo, $user, $invoice_number, $transaction_id, $emer, $emerTotal)
    {
        $this->orderInfo = $orderInfo;
        $this->user = $user;
        $this->invoice_number = $invoice_number;
        $this->transaction_id = $transaction_id;
        $this->emer = $emer;
        $this->emerTotal = $emerTotal;
    }
}
