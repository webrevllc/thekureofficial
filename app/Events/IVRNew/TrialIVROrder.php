<?php

namespace App\Events\IVRNew;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TrialIVROrder extends Event
{
    use SerializesModels;
    /**
     * @var
     */
    public $orderInfo;
    /**
     * @var
     */
    public $user;
    /**
     * @var
     */
    public $invoice_number;
    /**
     * @var
     */
    public $productsOrdered;

    /**
     * Create a new event instance.
     *
     * @param $orderInfo
     * @param $user
     * @param $invoice_number
     * @param $productsOrdered
     */
    public function __construct($orderInfo, $user, $invoice_number, $productsOrdered)
    {
        $this->orderInfo = $orderInfo;
        $this->user = $user;
        $this->invoice_number = $invoice_number;
        $this->productsOrdered = $productsOrdered;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
