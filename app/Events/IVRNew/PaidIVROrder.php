<?php

namespace App\Events\IVRNew;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PaidIVROrder extends Event
{
    use SerializesModels;

    /**
     * @var
     */
    public $orderInfo;
    /**
     * @var
     */
    public $user;
    /**
     * @var
     */
    public $invoice_number;
    /**
     * @var
     */
    public $productsOrdered;
    /**
     * @var
     */
    public $total;
    /**
     * @var
     */
    public $transactionId;
    /**
     * @var
     */
    public $twoPay;

    /**
     * Create a new event instance.
     *
     * @param $orderInfo
     * @param $user
     * @param $invoice_number
     * @param $productsOrdered
     * @param $total
     * @param $transactionId
     * @param $twoPay
     */
    public function __construct($orderInfo, $user, $invoice_number, $productsOrdered, $total, $transactionId, $twoPay)
    {
        $this->orderInfo = $orderInfo;
        $this->user = $user;
        $this->invoice_number = $invoice_number;
        $this->productsOrdered = $productsOrdered;
        $this->total = $total;
        $this->transactionId = $transactionId;
        $this->twoPay = $twoPay;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
