<?php

namespace App\Events\IVRNew;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class DeclinedIVROrder extends Event
{
    use SerializesModels;
    /**
     * @var
     */
    public $orderInfo;

    /**
     * Create a new event instance.
     *
     * @param $orderInfo
     */
    public function __construct($orderInfo)
    {
        $this->orderInfo = $orderInfo;
    }

}
