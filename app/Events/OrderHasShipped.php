<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OrderHasShipped extends Event
{
    use SerializesModels;
    /**
     * @var
     */
    public $orderUser;

    /**
     * Create a new event instance.
     *
     * @param $orderUser
     */
    public function __construct($orderUser)
    {
        //
        $this->orderUser = $orderUser;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
