<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\CreateCSV::class,
        \App\Console\Commands\PDFs::class,
        \App\Console\Commands\ZipnSend::class,
        \App\Console\Commands\SerialManager::class,
        \App\Console\Commands\SendAbandonedEmail::class,
        \App\Console\Commands\ProcessSubscriptions::class,
        \App\Console\Commands\FormatNames::class,
        \App\Console\Commands\CountdownDecrementer::class,
        \App\Console\Commands\FailSubProcessing::class,
        \App\Console\Commands\CapitalizeCSV::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('make:capitalcsv')->dailyAt("07:25");
        $schedule->command('make:shipping')->dailyAt("07:30");
        $schedule->command('make:serials')->everyThirtyMinutes();
        $schedule->command('email:abandoned')->hourly();
        $schedule->command('make:failedsub')->dailyAt("10:00");
        $schedule->command('make:subscription')->dailyAt("10:30");
        $schedule->command('format:names')->dailyAt("07:00");
        $schedule->command('countdown:decrease')->hourly();
    }
}
