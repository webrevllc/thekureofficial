<?php

namespace App\Console\Commands;

use App\Emails\AbandonedCart;
use App\Lead;
use Illuminate\Console\Command;

class SendAbandonedEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:abandoned';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends an email to users that abandoned their shopping cart';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $leads = Lead::where('has_converted', false)->where('email_sent', false)->get();

        foreach($leads as $lead){
            (new AbandonedCart)->sendTo($lead);
            $lead->email_sent = true;
            $lead->save();
        }
    }
}
