<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CountdownDecrementer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'countdown:decrease';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $countdown = DB::table('countdown')->where('id', 1)->first();

        $amount = $countdown->amount;

        $decrease_amount = rand(1,3);

        $new_amount = $amount - $decrease_amount;

        DB::statement("UPDATE countdown SET amount = $new_amount where id = 1");

    }
}
