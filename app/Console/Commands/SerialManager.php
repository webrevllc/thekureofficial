<?php

namespace App\Console\Commands;

use App\Contracts\Invisacloud;
use App\Serial;
use Illuminate\Console\Command;

class SerialManager extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:serials';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Serial $serial, Invisacloud $invisacloud)
    {
        if($serial->count() < 100){
            $serials = $invisacloud->createSerials(200);
            foreach($serials as $s){
                $newSerial = new Serial;
                $newSerial->serial = $s;
                $newSerial->save();
            }
        }
    }
}
