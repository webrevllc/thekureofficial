<?php

namespace App\Console\Commands;

use App\Serial;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class FormatNames extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'format:names';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';



    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::whereBetween('created_at',[Carbon::yesterday()->startOfDay(), Carbon::now()])->get();;;
        $serials = Serial::whereBetween('created_at',[Carbon::yesterday()->startOfDay(), Carbon::now()])->get();;

        $this->formatName($users);
        $this->formatName($serials);

    }

    public function formatName($collection)
    {
        foreach($collection as $col){
            $col->name = ucwords(strtolower($col->name));
            $col->save();
        }
    }

}
