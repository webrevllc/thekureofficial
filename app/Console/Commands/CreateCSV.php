<?php

namespace App\Console\Commands;

use App\Events\Crons\CSVComplete;
use App\Serial;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Excel;

class CreateCSV extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:shipping';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';
    /**
     * @var Excel
     */
    private $excel;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Excel $excel)
    {
        parent::__construct();
        $this->excel = $excel;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = date('Y-m-d', strtotime(Carbon::today()));
        $oldFile = $name.'.csv';
        $this->excel->create($name, function ($e) use ($name){
            $e->sheet($name, function ($sheet) {
                $serials = Serial::where('name', '!=', 'Jeffrey Rosenthal')
                    ->where('name', '!=', 'EDDIE TAYLOR')
                    ->where('name', '!=', 'Ugo Spezza')
                    ->where('name', '!=', 'peter spezza')
                    ->whereBetween('assigned_date',[Carbon::yesterday()->startOfDay(), Carbon::today()->startOfDay()])->get();
                $add = $serials;
                $sheet->fromArray($add);
            });
        })->store('csv');
        Storage::makeDirectory($name);
        Storage::move($oldFile, $name.'/'.$name.'.csv');
        event(new CSVComplete);

    }
}
