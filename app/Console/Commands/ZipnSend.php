<?php

namespace App\Console\Commands;

use App\Events\Crons\ZippyComplete;
use Carbon\Carbon;
use File;
use Illuminate\Console\Command;
use Storage;

class ZipnSend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:zippy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = date('Y-m-d', strtotime(Carbon::today()));
        $path ='storage/app/'.date('Y-m-d');
        Zip($path, $path.'/'.$name.'.zip');
        $file = File::get($path.'/'.$name.'.zip');
        if(Storage::disk('s3')->put($name.'.zip', $file)){
            File::deleteDirectory($path);
        }
        event(new ZippyComplete);
    }
}
