<?php

namespace App\Console\Commands;

use App\Contracts\SubscriptionContract;
use App\Failedsub;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class FailSubProcessing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:failedsub';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * @var
     */
    private $subscriptionContract;

    /**
     * Create a new command instance.
     *
     * @param SubscriptionContract $subscriptionContract
     */
    public function __construct(SubscriptionContract $subscriptionContract)
    {
        parent::__construct();

        $this->subscriptionContract = $subscriptionContract;
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fs = Failedsub::where('processed', false)->whereBetween('next_process_date', [Carbon::today(), Carbon::now()])->get();

        foreach($fs as $failedSub){
            $user = User::find($failedSub->user_id);
            $this->info($failedSub->id);
            $response = $this->subscriptionContract->chargeFailedSubscription($fs, $user);
            if($response == 'success'){
                $failedSub->processed = true;
                $failedSub->processed_by = 'AUTO PROCESSED';
                $failedSub->save();
            }else{
                $failedSub->attempts++;
                $failedSub->next_process_date = Carbon::today()->addDays(4);
                $failedSub->save();
            }

        }
    }
}
