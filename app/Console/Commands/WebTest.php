<?php

namespace App\Console\Commands;

use App\Contracts\Payment;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class WebTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:web';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';
    /**
     * @var Carbon
     */
    private $carbon;
    /**
     * @var Payment
     */
    private $payment;

    /**
     * Create a new command instance.
     *
     * @param Carbon $carbon
     * @param Payment $payment
     */
    public function __construct(Carbon $carbon, Payment $payment)
    {
        parent::__construct();
        $this->carbon = $carbon;
        $this->payment = $payment;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        DB::transaction(function(){
//        $url = 'https://www.thekure.com/checkout';
            $url = 'http://localhost:8000/checkout';
            $data = json_decode(Storage::disk('local')->get('web-test.json'));
            $billing = $data->Billing;
            $email = rawurlencode('srosenthal82+'.$this->carbon->now()->timestamp.'@gmail.com');
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded; charset=utf-8","Accept:application/json, text/javascript, */*; q=0.01"));
            curl_setopt( $ch, CURLOPT_URL, $url );
            curl_setopt( $ch, CURLOPT_POST, 1 );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt( $ch, CURLOPT_POSTFIELDS,
                "first_name=Peter&last_name=Spezza&email=$email&password=password&phone=4073129455&add1=5701+E.+HILLSBOROUGH+AVE&add2=&city=TAMPA&state=FL&zip=33601&cardType=0&card=$billing->Card&month=$billing->Month&year=$billing->Year&cardCVV=111&HWMONK=1&PCOS=1&KURE=1&FCSPC=1");
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
            curl_exec( $ch );
            curl_close($ch);
            $this->voidTransaction(rawurldecode($email));
            $this->deleteAll(rawurldecode($email));
        });
        DB::rollback();
//        $this->sendSuccessMail();
    }
    private function sendSuccessMail(){
        Mail::raw('IVR TEST SUCCESS AT '. $this->carbon->now() , function($message)
        {
            $message->from('websuccess@theukre.com', 'WEB TEST');
            $message->to('srosenthal82@gmail.com')->cc('jrosenthal@computercentersusa.com');
            $message->subject('SUCCESSFUL WEB TEST');
        });
    }

    private function voidTransaction($email)
    {
        $user = User::with('orders')->where('email', $email)->first();
        $auth_transaction = $user->orders()->first()->auth_transaction;
        $this->payment->voidTransaction($auth_transaction);
    }

    public function deleteAll($email)
    {
        $user = User::with('orders')->with('subscriptions')->where('email', $email)->first();
        $subscription = $user->subscriptions()->first();
        $order = $user->orders()->first();

        $subscription->status = 'TEST';
        $subscription->save();

        $order->source = 'TEST';
        $order->save();

        $user->password = 'TEST';
        $user->save();

        $user->delete();
        $order->delete();
        $subscription->delete();
    }
}
