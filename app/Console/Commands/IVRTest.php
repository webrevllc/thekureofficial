<?php

namespace App\Console\Commands;

use App\Contracts\Payment;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class IVRTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:ivr';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';
    /**
     * @var Client
     */
    private $client;
    /**
     * @var Carbon
     */
    private $carbon;
    /**
     * @var Payment
     */
    private $payment;

    /**
     * Create a new command instance.
     *
     * @param Carbon $carbon
     * @param Payment $payment
     * @internal param Client $client
     */
    public function __construct(Carbon $carbon, Payment $payment)
    {
        parent::__construct();
        $this->carbon = $carbon;
        $this->payment = $payment;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $url = 'https://www.thekure.com/kurejson';
        $url = 'http://localhost:8000/kurejson';
        $email = 'srosenthal82+'.$this->carbon->now()->timestamp.'@gmail.com';
        $data = $this->setData($email);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=utf-8","Accept:application/json, text/javascript, */*; q=0.01"));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if($httpcode == '500'){
            $this->sendFailMail();
//            $this->voidTransaction($email);
        }else{
            $this->voidTransaction($email);
            $this->sendSuccessMail();
            $this->deleteAll($email);
        }
    }
    private function setData($email){
        $data = json_decode(Storage::disk('local')->get('ivr-test.json'));
        $data[0]->Order->Customer->Email = $email;
        return json_encode($data);
    }

    private function sendFailMail(){
        Mail::raw('IVR TEST FAILED AT '. $this->carbon->now() , function($message)
        {
            $message->from('ivrfailure@theukre.com', 'IVR TEST');
            $message->to('srosenthal82@gmail.com')->cc('jrosenthal@computercentersusa.com');
            $message->subject('FAILED IVR TEST');
        });
    }
    private function sendSuccessMail(){
        Mail::raw('IVR TEST SUCCESS AT '. $this->carbon->now() , function($message)
        {
            $message->from('ivrsuccess@theukre.com', 'IVR TEST');
            $message->to('srosenthal82@gmail.com')->cc('jrosenthal@computercentersusa.com');
            $message->subject('SUCCESSFUL IVR TEST');
        });
    }

    private function voidTransaction($email){
        $user = User::with('orders')->where('email', $email)->first();
        $auth_transaction = ($user->orders()->first()->auth_transaction);
        $this->payment->voidTransaction($auth_transaction);
    }

    public function deleteAll($email)
    {
        $user = User::where('email', $email)->first();
        $subscription = $user->subscriptions()->first();
        $order = $user->orders()->first();

        $subscription->status = 'TEST';
        $subscription->save();

        $order->source = 'TEST';
        $order->save();

        $user->password = 'TEST';
        $user->save();


        $user->delete();
        $order->delete();
        $subscription->delete();
    }
}
