<?php

namespace App\Console\Commands;

use App;
use App\Events\Crons\PDFComplete;
use App\Order;
use Carbon\Carbon;
use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;
use DB;
use DOMPDF_Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class PDFs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:pdf';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $x = DB::table('orders')->where('id', DB::raw("(select max(`id`) from orders)"))->first();
        $name = date('Y-m-d', strtotime(Carbon::today()));
        $path ='storage/app/'.$name;
        Storage::makeDirectory($name);
            $orders = Order::with('user', 'address', 'subscription')->whereBetween('created_at',[Carbon::yesterday()->startOfDay(), Carbon::yesterday()->endOfDay()])->get();
//            $orders = Order::with('user', 'address', 'subscription')->whereId(521)->get();
        foreach($orders as $order){
            $barcode = new BarcodeGenerator();
            $barcode->setText($order->invoice_number);
            $barcode->setType(BarcodeGenerator::Code128);
            $barcode->setScale(1);
            $barcode->setThickness(15);
            $code = $barcode->generate();
            $pdf = App::make('dompdf.wrapper');
            if ($order->status == 'failed' || $order->subscription_id == 0) {

            } else {
                if ( ! is_null($order->user)) {
                    if(strtolower($order->user->name) == "shane rosenthal"
                        || strtolower($order->user->name) == "peter spezza"
                        || strtolower($order->user->name) == "ugo spezza"
                        || strtolower($order->user->name) == "eddie taylor"
                        || strtolower($order->user->name) == "jeff rosenthal"
                        || strtolower($order->user->name) == "mel arthur"){

                    }else{
                        if ($order->amount == "Free Trial") {

                            $pdf->loadView('pdf.lettertrial', compact('order', 'code'))->save($path . '/' . $order->id . '.pdf');

                        } else {
                            try {
                                if (count($order->address) > 0) {
                                    $pdf->loadView('pdf.letter', compact('order', 'code'))->save($path . '/' . $order->id . '.pdf');
                                    $this->info('paid '.$order->id);
                                } else {
                                    $this->info('null '.$order->id);
                                }
                            } catch (DOMPDF_Exception $m) {

                            }
                        }
                    }
                }
            }
        }

        event(new PDFComplete);
    }
}
