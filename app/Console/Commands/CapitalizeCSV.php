<?php

namespace App\Console\Commands;

use App\Serial;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CapitalizeCSV extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:capitalcsv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $serials = Serial::whereBetween("assigned_date", [Carbon::yesterday()->startOfDay(), Carbon::now()])->get();
//        $serials = Serial::get();

            foreach($serials as $serial){
                $serial->name = ucwords(strtolower($serial->name));
                $serial->address = ucwords(strtolower($serial->address));
                $serial->city = ucwords(strtolower($serial->city));
                $serial->save();
            }
    }
}
