<?php

namespace App\Console\Commands;

use App\Contracts\SubscriptionContract;
use App\Failedsub;
use App\Subscription;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Mail;

class ProcessSubscriptions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:subscription';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process all subscriptions for payment';
    /**
     * @var SubscriptionContract
     */
    private $subscriptionContract;

    /**
     * ProcessSubscriptions constructor.
     * @param SubscriptionContract $subscriptionContract
     */
    public function __construct(SubscriptionContract $subscriptionContract)
    {
        parent::__construct();

        $this->subscriptionContract = $subscriptionContract;
    }


    public function handle()
    {
        $subscriptions = Subscription::with('user')
            ->where('status', 'active')
            ->whereBetween('next_process_date', [Carbon::today(), Carbon::now()])
            ->get();
        foreach($subscriptions as $subscription){
            if($this->subscriptionContract->userHasValidCIM($subscription)){
                $this->info($subscription->id);
                $this->subscriptionContract->chargeSubscription($subscription);
            }else{
                $this->info('FAILED '.$subscription->id);
                $failedSub = new Failedsub;
                $failedSub->reason = 'INVALID USER PROFILE';
                $failedSub->amount = $subscription->amount;
                $failedSub->user_id = $subscription->user->id;
                $failedSub->subscription_id = $subscription->id;
                $failedSub->next_process_date = Carbon::today()->addDays(4);
                $failedSub->attempts = 1;
                $failedSub->save();
            }
        }

        Mail::raw(count($subscriptions), function ($message) {
            $message->subject('Subscriptions Processed');
            $message->to('shane@computercentersusa.com');
        });
    }
}
