<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LegacyOrder extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'Transactions';
    protected $fillable = ['products', 'status'];
}
