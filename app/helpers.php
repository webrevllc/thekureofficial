<?php
use App\Address;
use App\Contracts\Invisacloud;
use App\Contracts\Payment;
use App\Inquiry;
use App\Order;
use App\Product;
use App\Refund;
use App\Serial;
use App\Subscription;
use App\User;
use Carbon\Carbon;

//define("AUTHORIZENET_SANDBOX", env('AUTHORIZENET_SANDBOX'));
function flash($title = null, $message = null)
{
    $flash = app('App\Http\Flash');

    if(func_num_args() == 0){
        return $flash;
    }
    return $flash->info($title, $message);
}

function set_active($path, $active = 'active') {
    return call_user_func_array('Request::is', (array)$path) ? $active : '';
}



function makeWebProductsArray($data, $paynow){
    $products = array();
    if($data->KURE > 0){
        $p = Product::where('ivr_code', '=', "KURE")->first();
        $p->quantity = $data->KURE;
        $products[] = $p;
    }
    if($data->PCOS > 0){
        $p = Product::where('ivr_code', '=', "PCOS")->first();
        $p->quantity = $data->PCOS;
        $products[] = $p;
    }
    if($data->CSPC30 > 0){
        $p = Product::where('ivr_code', '=', "CSPC30")->first();
        $p->quantity = $data->CSPC30;
        $products[] = $p;
    }
    if($data->PROC > 0){
        $p = Product::where('ivr_code', '=', "PROC")->first();
        $p->quantity = $data->PROC;
        $products[] = $p;
    }
    if($data->EMER > 0){
        $p = Product::where('ivr_code', '=', "EMER")->first();
        $p->quantity = $data->EMER;
        $products[] = $p;
    }
    if($data->HWMONK > 0){
        $p = Product::where('ivr_code', '=', "HWMONK")->first();
        $p->quantity = $data->HWMONK;
        $products[] = $p;
    }
    if($paynow == 'payNow'){
        $p = Product::where('ivr_code', '=', "CSPC")->first();
        $p->quantity = $data->KURE;
        $products[] = $p;
    }
//    if($data->AV > 0){
//        $p = Product::where('ivr_code', '=', "AV")->first();
//        $p->quantity = $data->KURE;
//        $products[] = $p;
//    }
    $p = Product::where('ivr_code', '=', "SHIP")->first();
    $p->quantity = '1';
    $products[] = $p;

    return $products;
}

function failedOrders()
{
    return Order::with('user')->whereStatus('failed')->get();
}

function subscriptionsRenewingToday(){
    return DB::table('subscriptions')
        ->where('status', 'active')
        ->with('subscription')->whereBetween('next_process_date',[Carbon::today(), \Carbon\Carbon::now()])->get();
}

function todaysWebOrders(){
    return Order::with('user')
        ->where('source', 'web')
        ->where('status', 'paid')
        ->with('subscription')->whereBetween('created_at',[Carbon::today(), \Carbon\Carbon::now()])->get();
}

function todaysTrialWebOrders(){
    return Order::with('user')->with('subscription')
        ->where('source', 'web')
        ->where('status', 'trial')
        ->with('subscription')->whereBetween('created_at',[Carbon::today(), \Carbon\Carbon::now()])->get();
}

function todaysIVROrders(){
    $todayIVR = Order::with('user')
        ->where('source', 'ivr')
        ->where('status', 'paid')
        ->with('subscription')->whereBetween('created_at',[Carbon::today(), \Carbon\Carbon::now()])->get();
    return $todayIVR;
}

function todaysTrialIVROrders(){
    $todayIVR = Order::with('user')->with('subscription')
        ->where('source', 'ivr')
        ->where('status', 'trial')
        ->with('subscription')->whereBetween('created_at',[Carbon::today(), \Carbon\Carbon::now()])->get();
    return $todayIVR;
}

function calculateOrdersTotal($orders){
    $myArray = array();
    foreach($orders as $order){
        $myArray[] = $order->amount;
    }
    return array_sum($myArray);
}

function calculateTrialSubscriptionsTotal($orders){
    $myArray = array();
    foreach($orders as $order){
        $myArray[] = $order->subscription->amount;
    }
    return array_sum($myArray);
}

function ordersThatNeedShipping(){
    $orders = Order::where('shipped', '!=', 1)->where('source', '!=', 'add-on')->where('status', '=', 'paid')->get();
    return $orders;
}
function trialOrdersThatNeedShipping(){
    $orders = Order::where('shipped', '!=', 1)->where('source', '!=', 'add-on')->where('status', '=', 'trial')->get();
    return $orders;
}


function outstandingInquiries(){
    $inquiries = Inquiry::where('status', '=', 'new')->get();
    return $inquiries;
}


function customerNoOrder(){
    $users = User::has('orders', '<', 1)->where('admin', '!=', true)->get();
    return $users;
}



function shipping($order){
    if($order->source == 'ivr'){
        return 5.95;
    }else{
        return 0;
    }
}

function calculateProration($order, $products)
{
    $productProration = array();
    $created_at = $order->created_at;
    foreach ($products as $p)
    {
        $yearlyCost = $p->price / 100 * $p->quantity;
        $now = Carbon::now();
        $difference = ($created_at->diff($now)->days < 1)
            ? 'today'
            : $created_at->diffInDays($now);
        $prorationdays = 365 - $difference;
        $dailyPrice = $yearlyCost / 365;
        $price = number_format($dailyPrice * $prorationdays, 2);
        $productProration[] = $price;
    }
    return array_sum($productProration);
}

//TOTAL
function totalSum(){
    $orders = Order::all();
    return calculateSum($orders);
}

function totalRefunds(){
    $orders = Refund::all();
    return calculateSum($orders);
}
//TODAY
function todayDollars(){
    $orders = Order::with('subscription')->whereBetween('created_at', [Carbon::today(), \Carbon\Carbon::now()])->get();
    return calculateSum($orders);
}

function todayRefunds(){
    $orders = Refund::whereBetween('created_at', [Carbon::today(), \Carbon\Carbon::now()])->get();
    return calculateSum($orders);
}

//THIS WEEK
function weekDollars(){
    $orders = Order::with('subscription')->whereBetween('created_at', [Carbon::now()->startOfWeek(), \Carbon\Carbon::now()])->get();
    return calculateSum($orders);
}
function weekRefunds(){
    $orders = Refund::whereBetween('created_at', [Carbon::now()->startOfWeek(), \Carbon\Carbon::now()])->get();
    return calculateSum($orders);
}

//THIS MONTH
function monthDollars(){
    $orders = Order::with('subscription')->whereBetween('created_at', [Carbon::now()->firstOfQuarter(), \Carbon\Carbon::now()])->count();
    return calculateSum($orders);
}
function monthRefunds(){
    $orders = Refund::whereBetween('created_at', [Carbon::now()->firstOfMonth(), \Carbon\Carbon::now()])->get();
    return calculateSum($orders);
}

//THIS QUARTER
function quarterDollars(){
    $orders = Order::with('subscription')->whereBetween('created_at', [Carbon::now()->firstOfQuarter(), \Carbon\Carbon::now()])->get();
    return calculateSum($orders);
}

function quarterRefunds(){
    $orders = Refund::whereBetween('created_at', [Carbon::now()->firstOfQuarter(), \Carbon\Carbon::now()])->get();
    return calculateSum($orders);
}

//THIS YEAR
function yearDollars(){
    $orders = Order::with('subscription')->whereBetween('created_at', [Carbon::now()->firstOfYear(), \Carbon\Carbon::now()])->get();
    return calculateSum($orders);
}

function yearRefunds(){
    $orders = Refund::whereBetween('created_at', [Carbon::now()->firstOfYear(), \Carbon\Carbon::now()])->get();
    return calculateSum($orders);
}

//CUSTOM
function getCustomDateDollars($from, $to){
    $orders = Order::with('subscription')->whereBetween('created_at', [$from, $to])->get();
    return calculateSum($orders);
}
function getCustomDateRefunds($from, $to){
    $orders = Refund::whereBetween('created_at', [$from, $to])->get();
    return calculateSum($orders);
}



function calculateSum($orders){
    $t = array();
    foreach($orders as $order){
        if($order->amount == "Free Trial" || $order->amount == ".01"){
            $t[] = $order->subscription->amount;
        }
        $t[] = $order->amount;
    }
    return number_format(array_sum($t), 2);
}


function Zip($source, $destination)
{
    $the_folder = $source;
    $zip_file_name = $destination;


    $download_file = false;


    class FlxZipArchive extends ZipArchive
    {
        /** Add a Dir with Files and Subdirs to the archive;;;;; @param string $location Real Location;;;;  @param string $name Name in Archive;;; @author Nicolas Heimann;;;; @access private  * */

        public function addDir($location, $name)
        {
            $this->addEmptyDir($name);

            $this->addDirDo($location, $name);
        } // EO addDir;

        /**  Add Files & Dirs to archive;;;; @param string $location Real Location;  @param string $name Name in Archive;;;;;; @author Nicolas Heimann
         * @access private   *
         */
        private function addDirDo($location, $name)
        {
            $name .= '/';
            $location .= '/';

            // Read all Files in Dir
            $dir = opendir($location);
            while ($file = readdir($dir)) {
                if ($file == '.' || $file == '..') continue;
                // Rekursiv, If dir: FlxZipArchive::addDir(), else ::File();
                $do = (filetype($location . $file) == 'dir') ? 'addDir' : 'addFile';
                $this->$do($location . $file, $name . $file);
            }
        } // EO addDirDo();
    }

    $za = new FlxZipArchive;
    $res = $za->open($zip_file_name, ZipArchive::CREATE);
    if ($res === TRUE) {
        $za->addDir($the_folder, basename($the_folder));
        $za->close();
    } else {
        echo 'Could not create a zip archive';
    }

    if ($download_file) {
        ob_get_clean();
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
        header("Content-Type: application/zip");
        header("Content-Disposition: attachment; filename=" . basename($zip_file_name) . ";");
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: " . filesize($zip_file_name));
        readfile($zip_file_name);
    }
}


function removeNonRecurring($subscription)
{
    $oldProducts = json_decode($subscription->products);
    $newProducts = array();
    foreach($oldProducts as $p){
        if ($p->recurring){
            $newProducts[] = $p;
        }
    }
    return json_encode($newProducts);
}
function calculateNewTotal($products)
{
    $price = array();
    foreach($products as $p){
        $x = $p->price * $p->quantity;
        $price[] = $x;
    }
    $productTotal = array_sum($price)/100;

    return number_format($productTotal, 2);
}
