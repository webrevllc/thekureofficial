<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'description', 'price', 'recurring'];

    protected $table = 'products';

    public function is_recurring()
    {
        return $this->recurring;
    }

    public function allProducts()
    {
        return Product::all();
    }

}
