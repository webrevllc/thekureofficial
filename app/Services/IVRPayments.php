<?php
/**
 * Created by PhpStorm.
 * User: srose
 * Date: 9/28/2015
 * Time: 12:47 PM
 */

namespace App\Services;


use App\Contracts\IVRPaymentContract;
use App\Events\IVR\FailedIVROrder;
use App\Events\IVR\OrderPlacedByIVR;
use App\Events\IVR\TrialOrderPlacedByIVR;
use App\Events\IVRNew\CreateEmerSale;
use App\Events\IVRNew\PaidIVROrder;
use App\Events\IVRNew\TrialIVROrder;
use App\IVRLead;
use App\Product;
use Carbon\Carbon;

class IVRPayments extends WebPayments implements IVRPaymentContract
{
    public function createIVRAuthOnly($orderInfo, $user)
    {
        $city = $orderInfo->Customer->City;
        $state = $orderInfo->Customer->State;

        //set Billing Info for Authorize
        $billingInfo = $this->makeIVRBillingInfo($orderInfo); //ADAPT INFO FOR METHOD
        $this->setAIMBillingFields($billingInfo); //SET DATA

        //set Shipping info for Authorize
        $shippingInfo = $this->makeIVRShippingInfo($orderInfo);
        $this->setAIMShippingFields($shippingInfo);

        //grab an invoice number - as an instance so we can pass it to the event
        $invoice_number = $this->order->newInvoiceNumber();
        $this->setAIMInvoiceNumber($invoice_number);
        $this->setAIMCustomer($user->id);

        //create line items
        $productsOrdered = $this->makeFullProducts($orderInfo->Products); //ADAPT PRODUCTS FOR AIM
        $this->createAIMLineItems($productsOrdered); //SET DATA

        //calculate the total - as an instance so we can pass it to the event
        $total = '.01';
        $this->setAIMTotal($total);
        //charge the card
        $response = $this->makeAIMPayment( 'authorizeOnly', $total, $orderInfo->Billing->Card, $orderInfo->Billing->Expiration );
        if($response->approved){
            $transaction_id = $response->transaction_id;
            $keys = $this->createCIMProfile($transaction_id);
            $this->addNewProfileKeysToUser($keys, $user);
            $this->voidTransaction($transaction_id);
            event(new TrialOrderPlacedByIVR($orderInfo, $response->transaction_id, $invoice_number, $total, $productsOrdered, $user, $city, $state, 'trial'));
            return $response->response_code;
        }else{
            event(new FailedIVROrder($orderInfo, $invoice_number, $total, $response->response_reason_text, $productsOrdered, $user, 'trial'));
            return $response->response_reason_text;
        }
    }
    public function makeIVRPayment($orderInfo, $user, $type)
    {

        //VOID THE TRANSACTION BEFORE ANYTHING ELSE
//        if(! $orderInfo->Billing->Status == 'NOT AUTHED'){
//            $this->voidTransaction($orderInfo['Billing']['Transaction_ID']);
//        }
        if($type == 'authorizeOnly'){
            $this->createIVRAuthOnly($orderInfo, $user);
            exit;
        }else{
            $city = $orderInfo->Customer->City;
            $state = $orderInfo->Customer->State;

            //set Billing Info for Authorize
            $billingInfo = $this->makeIVRBillingInfo($orderInfo); //ADAPT INFO FOR METHOD
            $this->setAIMBillingFields($billingInfo); //SET DATA

            //set Shipping info for Authorize
            $shippingInfo = $this->makeIVRShippingInfo($orderInfo);
            $this->setAIMShippingFields($shippingInfo);

            //grab an invoice number - as an instance so we can pass it to the event
            $invoice_number = $this->order->newInvoiceNumber();
            $this->setAIMInvoiceNumber($invoice_number);
            $this->setAIMCustomer($user->id);

            //create line items
            $productsOrdered = $this->makeFullProducts($orderInfo->Products); //ADAPT PRODUCTS FOR AIM
            $this->createAIMLineItems($productsOrdered); //SET DATA

            //calculate the total - as an instance so we can pass it to the event
            $total = $this->order->total($productsOrdered, $user);
            $this->setAIMTotal($total);

            //charge the card
            $response = $this->makeAIMPayment( 'authCapture', $total, $orderInfo->Billing->Card, $orderInfo->Billing->Expiration );

            //HANDLE THE RESPONSE
            if($response->approved){
                $transaction_id = $response->transaction_id;
                $last4 = $this->getLast4($transaction_id);
                event(new OrderPlacedByIVR($orderInfo, $transaction_id, $invoice_number, $total, $last4, $productsOrdered, $user, $city, $state, 'non-trial'));
                return $response->response_code;
            }else{
                event(new FailedIVROrder($orderInfo, $invoice_number, $total, $response->response_reason_text, $productsOrdered, $user, 'non-trial'));
                return $response->response_reason_text;
            }
        }
    }

    public function newCIMSale($orderInfo, $user, $type)
    {
        if($orderInfo->Billing->Transaction_ID !== '0' || $orderInfo->Billing->Transaction_ID !== '0'){
            $this->voidTransaction($orderInfo->Billing->Transaction_ID);
        }
        $customerProfile = $this->authorizeNetCustomer;
        $customerProfile->description = $user->id;
        $customerProfile->merchantCustomerId = $orderInfo->Customer->IVR_ID;
        $customerProfile->email = 'thekure+'.mt_rand().'@gmail.com';

        $paymentProfile = $this->authorizeNetPaymentProfile;
        $paymentProfile->customerType = "individual";
        $paymentProfile->billTo->firstName = ucwords(strtolower($orderInfo->Customer->First_Name));
        $paymentProfile->billTo->lastName = ucwords(strtolower($orderInfo->Customer->Last_Name));
        $paymentProfile->billTo->address = $orderInfo->Customer->Address_1 . ' ' . $orderInfo->Customer->Address_2;
        $paymentProfile->billTo->city = ucwords(strtolower($orderInfo->Customer->City));
        $paymentProfile->billTo->state = $orderInfo->Customer->State;
        $paymentProfile->billTo->country = $orderInfo->Customer->Country;
        $paymentProfile->billTo->zip = $orderInfo->Customer->Zip;
        $paymentProfile->billTo->phoneNumber = $orderInfo->Customer->Phone;
        $paymentProfile->payment->creditCard->cardNumber = $orderInfo->Billing->Card;
        $paymentProfile->payment->creditCard->expirationDate = $orderInfo->Billing->Expiration_Year . '-' . $orderInfo->Billing->Expiration_Month;//  "2015-10";
        $customerProfile->paymentProfiles[] = $paymentProfile;

        $CIMaddress = $this->authorizeNetAddress;
        $CIMaddress->firstName = $orderInfo->Shipping->First_Name;
        $CIMaddress->lastName = $orderInfo->Shipping->Last_Name;
        $CIMaddress->address = $orderInfo->Shipping->Address_1 . ' ' . $orderInfo->Shipping->Address_2;
        $CIMaddress->city = $orderInfo->Shipping->City;
        $CIMaddress->state = $orderInfo->Shipping->State;
        $CIMaddress->country = $orderInfo->Shipping->Country;
        $CIMaddress->zip = $orderInfo->Shipping->Zip;
//        $CIMaddress->phoneNumber = $orderInfo->Shipping->Phone;
        $customerProfile->shipToList[] = $CIMaddress;

        $CIMrequest = $this->authorizeNetCIM;
        $response = $CIMrequest->createCustomerProfile($customerProfile, "liveMode");
        $productsOrdered = $this->makeFullProducts($orderInfo->Products);
        $total = $this->order->total($productsOrdered, $user);
        if($response->isError()){
            return \Response::json($response->getMessageText(), 200);
        }else{
            $user->auth_profile = (string) $response->xml->customerProfileId;
            $user->default_billing = (string) $response->xml->customerPaymentProfileIdList->numericString;
            $user->default_shipping = (string) $response->xml->customerShippingAddressIdList->numericString;
            $user->save();
            $invoice_number = $this->order->newInvoiceNumber();
            if($type == 'authorizeOnly'){
                event(new TrialIVROrder( $orderInfo, $user, $invoice_number, $productsOrdered ));
                return \Response::json($invoice_number, 200);
            }else{
                $transaction = $this->authorizeNetTransaction;
                $transaction->amount = $total;
                $transaction->customerProfileId = $user->auth_profile;
                $transaction->customerPaymentProfileId = $user->default_billing;
                $transaction->customerShippingAddressId = $user->default_shipping;
                $response = $this->authorizeNetCIM->createCustomerProfileTransaction("AuthCapture", $transaction);
                $transactionResponse = $response->getTransactionResponse();
                $transactionId = $transactionResponse->transaction_id;
                if($transactionResponse->approved){
                    event(new PaidIVROrder( $orderInfo, $user, $invoice_number, $productsOrdered, $total, $transactionId ));
                    return \Response::json($transactionId, 200);
                }else{

                    $l = new IVRLead;
                    $l->name = $orderInfo->Customer->First_Name.' '. $orderInfo->Customer->Last_Name;
                    $l->email =  $user->email;
                    $l->address =  $orderInfo->Customer->Address_1 . ' ' . $orderInfo->Customer->Address_2;
                    $l->city =  $orderInfo->Customer->City;
                    $l->state =  $orderInfo->Customer->State;
                    $l->zip =  $orderInfo->Customer->Zip;
                    $l->country =  $orderInfo->Customer->Country;
                    $l->phone =  $orderInfo->Customer->Phone;
                    $l->products = json_encode($productsOrdered);
                    $l->amount = $this->order->totalNoUser($l->products);
                    $l->save();
                    return \Response::json($transactionResponse->response_reason_text, 200);
                }
            }
        }
    }

    public function updateCustomerWithJSON($user, $orderData, $type)
    {
        //SET UP THE DATA WE NEED FOR THE FUNCTION TO PERFORM

        $dt = Carbon::now();
        $rand = random_int(1,5000);
        $request = $this->authorizeNetCIM;
        $transaction_id = $orderData->Billing->Transaction_ID;
        $productsOrdered = $this->makeFullProducts($orderData->Products);
        $twoPay = $orderData->Billing->TWOPAY == "true" ? true : false;
        $total = $this->order->twoPayTotal($productsOrdered, $user, $twoPay);
        $invoice_number = $this->order->newInvoiceNumber();

        //GET CUSTOMER PROFILE IDS FOR UPDATING
        $response = $this->authorizeNetCIM->createCustomerProfileFromTransaction($transaction_id);
        $customerProfileID = (string) $response->xml->customerProfileId;
        $customerBillingID = (string) $response->xml->customerPaymentProfileIdList->numericString;

        //UPDATE THE CUSTOMER/USER DATA
        $customerProfile = $this->authorizeNetCustomer;
        $customerProfile->description = 'THEKURE';
        $customerProfile->merchantCustomerId = $orderData->Customer->IVR_ID;
        if($orderData->Customer->Email == ''){
            $customerProfile->email = 'thekure+'.$dt->timestamp.$rand.'@gmail.com';
        }else{
            $customerProfile->email = $orderData->Customer->Email;
        }
        //ADD A SHIPPING PROFILE
        $address = $this->authorizeNetAddress;
        $address->firstName = $orderData->Shipping->First_Name;
        $address->lastName = $orderData->Shipping->Last_Name;
        $address->address = $orderData->Shipping->Address_1.' '.$orderData->Shipping->Address_2;
        $address->city = $orderData->Shipping->City;
        $address->state = $orderData->Shipping->State;
        $address->zip = $orderData->Shipping->Zip;
        $address->country = $orderData->Shipping->Country;
        $address->phoneNumber = $orderData->Shipping->Phone;

        //MAKE ALL AUTHORIZENET CALLS DOWN HERE
        $response = $request->createCustomerShippingAddress($customerProfileID, $address);
        $customerAddressId = $response->getCustomerAddressId();
        $request->updateCustomerProfile($customerProfileID, $customerProfile);
        $this->updateUserAuthProfiles($customerProfileID, $customerBillingID, $customerAddressId, $user);
        if($orderData->Billing->HASEMER != ''){
            $this->createTrialEMEROrder($orderData, $user, $transaction_id);
        }
        if($type == 'authorizeOnly') {
            event(new TrialIVROrder($orderData, $user, $invoice_number, $productsOrdered));
            return \Response::json($invoice_number, 200);
        }else{
            event(new PaidIVROrder( $orderData, $user, $invoice_number, $productsOrdered, $total, $transaction_id, $twoPay ));
            return \Response::json($invoice_number, 200);
        }

    }

    public function updateUserAuthProfiles($customerProfileID, $customerBillingID, $customerAddressId, $user)
    {
        $user->auth_profile = $customerProfileID;
        $user->default_billing = $customerBillingID;
        $user->default_shipping = $customerAddressId;
        $user->save();
    }

    public function makeProfileOnANet($trans)
    {
        $response = $this->authorizeNetCIM->createCustomerProfileFromTransaction($trans);
//        dd($response);
    }

    public function createTrialEMEROrder($orderInfo, $user, $transaction_id)
    {
        $invoice_number = $this->order->newInvoiceNumber();
        $emer = $this->makeEmer($orderInfo);
        $emerTotal = $this->calculateEmerTotal($emer);
        event(new CreateEmerSale($orderInfo, $user, $invoice_number, $transaction_id, $emer, $emerTotal));
    }

    public function makeEmer($orderInfo)
    {
        $emer = Product::find(15);
        $emer->quantity = $orderInfo->Products->EMER;
        return $emer;
    }

    public function calculateEmerTotal($emer)
    {
        return (number_format(($emer->price/100) * $emer->quantity, 2));
    }
}