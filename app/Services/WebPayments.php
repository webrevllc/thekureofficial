<?php
/**
 * Created by PhpStorm.
 * User: srose
 * Date: 9/25/2015
 * Time: 10:56 AM
 */

namespace App\Services;


use App\Contracts\WebPaymentContract;
use App\Events\OrderPlacedOnWebsite;
use App\Events\TrialOrderPlacedOnWebsite;
use App\Lead;
use Carbon\Carbon;
use Cookie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class WebPayments extends AuthPayment implements WebPaymentContract{


    public function createAuthOnly($request, $products, $user, $quantity)
    {
        //SET CITY/STATE FOR EMAILING TO BOSSES
        $city = $request->city;
        $state = $request->state;

        //set Billing Info for Authorize
        $billingInfo = $this->makeBillingInfo($request); //ADAPT INFO FOR METHOD
        $this->setAIMBillingFields($billingInfo); //SET DATA

        //set Shipping info for Authorize
        $shippingInfo = $this->makeShippingInfo($request); //ADAPT INFO FOR METHOD
        $this->setAIMShippingFields($shippingInfo); //SET DATA

        //grab an invoice number - as an instance so we can pass it to the event
        $invoice_number = $this->order->newInvoiceNumber();
        //set the invoice number on AuthNetAIM order
        $this->setAIMInvoiceNumber($invoice_number);
//        $this->authorizeNetAIM->invoice_num = $invoice_number;

        //create customer ID
        $this->setAIMCustomer($user->id);
//        $this->authorizeNetAIM->setFields(['cust_id' => $user->id, 'description' => 'The Kure']);

        //create line items
        $this->createAIMLineItems($products);

        //calculate the total - as an instance so we can pass it to the event
        $total = '.01';
        //set the total on AuthNetAIM order
        $this->setAIMTotal($total);
//        $this->authorizeNetAIM->amount = $total;

        //ATTEMPT PURCHASE
        $response = $this->makeAIMPayment('authorizeOnly', $total, $this->makePaymentInfo($request)->card,$this->makePaymentInfo($request)->expiration);

        if($response->approved){
            $transaction_id = $response->transaction_id;
            $keys = $this->createCIMProfile($transaction_id);
            $this->addNewProfileKeysToUser($keys, $user);
            $this->voidTransaction($transaction_id);
            event(new TrialOrderPlacedOnWebsite($request, $transaction_id, $invoice_number, $total, $products, $user, $quantity, $city, $state));
            exit('success='.$transaction_id);
        }else{
            exit($response->response_reason_text);
        }
    }

    /**
     *
     * @param $request
     * @param $products
     * @param $user
     * @param $type
     */
    public function makeWebPayment($request, $products, $user, $type, $quantity)
    {
        if($type == 'authorizeOnly'){
            $this->createAuthOnly($request, $products, $user, $quantity);
            exit;
        }else{
            //SET CITY/STATE FOR EMAILING TO BOSSES
            $city = $request->city;
            $state = $request->state;

            //set Billing Info for Authorize
            $billingInfo = $this->makeBillingInfo($request); //ADAPT INFO FOR METHOD
            $this->setAIMBillingFields($billingInfo); //SET DATA

            //set Shipping info for Authorize
            $shippingInfo = $this->makeShippingInfo($request); //ADAPT INFO FOR METHOD
            $this->setAIMShippingFields($shippingInfo); //SET DATA

            //grab an invoice number - as an instance so we can pass it to the event
            $invoice_number = $this->order->newInvoiceNumber();
            //set the invoice number on AuthNetAIM order
            $this->setAIMInvoiceNumber($invoice_number);
            $this->authorizeNetAIM->invoice_num = $invoice_number;

            //create customer ID
            $this->setAIMCustomer($user->id);
            $this->authorizeNetAIM->setFields(['cust_id' => $user->id, 'description' => 'The Kure']);

            //create line items
            $this->createAIMLineItems($products);

            //calculate the total - as an instance so we can pass it to the event
            $total = $this->order->total($products, $user);
            //set the total on AuthNetAIM order
            $this->setAIMTotal($total);
            $this->authorizeNetAIM->amount = $total;

            //ATTEMPT PURCHASE
            $response = $this->makeAIMPayment($type, $total, $this->makePaymentInfo($request)->card,$this->makePaymentInfo($request)->expiration);
            if($response->approved){
                $transaction_id = $response->transaction_id;
                $last4 = $this->getLast4($transaction_id);
                event(new OrderPlacedOnWebsite($request, $transaction_id, $invoice_number, $total, $last4, $products, $user, $city, $state, $quantity));
                echo "success=" . $transaction_id;
            }else{
                echo $response->response_reason_text;
            }
        }
    }


    public function leadAuth($request, $lead)
    {

        //$request contains card data, $lead contains user data
        //SET CITY/STATE FOR EMAILING TO BOSSES
        $city = $lead->city;
        $state = $lead->state;

        //set Billing Info for Authorize
        $billingInfo = $this->makeBillingInfo($lead); //ADAPT INFO FOR METHOD
        $this->setAIMBillingFields($billingInfo); //SET DATA

        //set Shipping info for Authorize
        $shippingInfo = $this->makeShippingInfo($lead); //ADAPT INFO FOR METHOD
        $this->setAIMShippingFields($shippingInfo); //SET DATA

        //grab an invoice number - as an instance so we can pass it to the event
//        $invoice_number = $this->order->newInvoiceNumber();
        //set the invoice number on AuthNetAIM order
//        $this->setAIMInvoiceNumber($invoice_number);
//        $this->authorizeNetAIM->invoice_num = $invoice_number;

        //create customer ID
        $this->setAIMCustomer($lead->id);
//        $this->authorizeNetAIM->setFields(['cust_id' => $user->id, 'description' => 'The Kure']);

        //create line items
//        $this->createAIMLineItems($products);

        //calculate the total - as an instance so we can pass it to the event
        $total = '.01';
        //set the total on AuthNetAIM order
        $this->setAIMTotal($total);
//        $this->authorizeNetAIM->amount = $total;

        //ATTEMPT PURCHASE
        $response = $this->makeAIMPayment('authorizeOnly', $total, $this->makePaymentInfo($request)->card,$this->makePaymentInfo($request)->expiration);
        if($response->approved){
            $transaction_id = $response->transaction_id;
            $keys = $this->createCIMProfile($transaction_id);
            $this->voidTransaction($transaction_id);
            $this->setLeadKeys($keys, $lead, $transaction_id);

            return 'success';
        }else{
            return $response;
        }
    }

    public function setLeadKeys($keys, $lead, $transaction_id)
    {
        $lead->auth_profile = $keys->customerProfileID;
        $lead->default_billing = $keys->customerBillingID;
        $lead->default_shipping = $keys->customerShippingID;
        $lead->auth_transaction = $transaction_id;
        $lead->invoice_number = $this->order->newInvoiceNumber();;
        $lead->step = '4';
        $lead->save();
    }

    public function pay_now_sale($lead, $products)
    {

        return $this->createCIMTransaction($lead, $products);

    }

    public function createCIMTransaction($lead, $products)
    {
        $transaction = $this->authorizeNetTransaction;
        $transaction->amount = $lead->total;
        $transaction->customerProfileId = $lead->auth_profile;
        $transaction->customerPaymentProfileId = $lead->default_billing;
        $transaction->customerShippingAddressId = $lead->default_shipping;
        $response = $this->authorizeNetCIM->createCustomerProfileTransaction("AuthCapture", $transaction);
        $transactionResponse = $response->getTransactionResponse();
        if($transactionResponse->approved){
            $transactionId = $transactionResponse->transaction_id;
            return ['transactionID' => $transactionId];
        }else{
            return $response->getMessageText();
        }
    }

    public function leadCIM($request, $lead)
    {
        $customerProfile = $this->authorizeNetCustomer;
        $customerProfile->description = Carbon::now()->timestamp;
        $customerProfile->merchantCustomerId = $lead->last_name;
        $customerProfile->email = $lead->email;

        $paymentProfile = $this->authorizeNetPaymentProfile;
        $paymentProfile->customerType = "individual";
        $paymentProfile->billTo->firstName = $lead->first_name;
        $paymentProfile->billTo->lastName = $lead->last_name;
        $paymentProfile->billTo->address = $lead->add1 . ' ' . $lead->add2;
        $paymentProfile->billTo->city = $lead->city;
        $paymentProfile->billTo->state = $lead->state;
        $paymentProfile->billTo->country = $lead->country;
        $paymentProfile->billTo->zip = $lead->zip;
        $paymentProfile->billTo->phoneNumber = $lead->phone;
        $paymentProfile->payment->creditCard->cardNumber = $request->card;
        $paymentProfile->payment->creditCard->expirationDate = '20' . $request->year . '-' . $request->month;//  "2015-10";
        $customerProfile->paymentProfiles[] = $paymentProfile;

        $CIMaddress = $this->authorizeNetAddress;
        $CIMaddress->firstName = $lead->first_name;
        $CIMaddress->lastName = $lead->last_name;
//        $address->company = "John Doe Company";
        $CIMaddress->address = $lead->add1 . ' ' . $lead->add2;
        $CIMaddress->city = $lead->city;
        $CIMaddress->state = $lead->state;
        $CIMaddress->country = $lead->country;
        $CIMaddress->zip = $lead->zip;
        $CIMaddress->phoneNumber = $lead->phone;
        $customerProfile->shipToList[] = $CIMaddress;

//        $CIMrequest = $this->authorizeNetCIM;
        $response = $this->authorizeNetCIM->createCustomerProfile($customerProfile, "liveMode");
//        dd($response);
        if($response->getMessageCode() == 'E00039'){
//            $str = str_replace("A duplicate record with ID ", "", $response->getMessageText());
//            $id = str_replace(substr($str, -16), "", $str);
//            $newlead = Lead::where('auth_profile', $id)->first();

            $oldLeads = Lead::where('email', $lead->email)->get();
            foreach($oldLeads as $oldLead){
                if($oldLead->auth_profile !== ''){
                    $newlead = $oldLead;
                    $lead = $this->convertLeads($lead, $newlead);
                }
            }
            return ['lead' => $lead];
        }elseif($response->isError()){
            return $response->getMessageText();
        }else{
            if((string) $response->xml->customerProfileId == ''){
                Mail::raw($lead->id, function ($message) {
                    $message->subject('LEAD ERROR!');
                    $message->to('shane@computercentersusa.com');
                    $message->cc('jrosenthal@computercentersusa.com');
                });
                return 'Sorry, there seems to be a problem. The Kure Team has been notified and will contact you shortly to complete your order.';
            }else{
                $lead->auth_profile = (string) $response->xml->customerProfileId;
                $lead->default_billing = (string) $response->xml->customerPaymentProfileIdList->numericString;
                $lead->default_shipping = (string) $response->xml->customerShippingAddressIdList->numericString;

                $lead->step = '4';
                $lead->save();
                return 'success';
            }

        }
    }

    public function singleTimePayment($user, $total)
    {

//        $transaction = new AuthorizeNetTransaction;
//        $transaction->amount = "9.79";
//        $transaction->customerProfileId = $customerProfileId;
//        $transaction->customerPaymentProfileId = $paymentProfileId;
//        $transaction->customerShippingAddressId = $customerAddressId;


        $transaction = $this->authorizeNetTransaction;
        $transaction->amount = $total;
        $transaction->customerProfileId =  $user->auth_profile;
        $transaction->customerPaymentProfileId = $user->default_billing;
        $transaction->customerShippingAddressId = $user->default_shipping;
        dd($transaction);
        $response = $this->authorizeNetCIM->createCustomerProfileTransaction("AuthCapture", $transaction);
        $transactionResponse = $response->getTransactionResponse();
        if($transactionResponse->approved){
            $transactionId = $transactionResponse->transaction_id;
            return ['transactionID' => $transactionId];
        }else{
            return $response->getMessageText();
        }
    }

    public function convertLeads($lead, $newlead)
    {
        $lead->auth_profile = $newlead->auth_profile;
        $lead->default_billing = $newlead->default_billing;
        $lead->default_shipping = $newlead->default_shipping;
        $lead->save();
        return $lead;
    }

}