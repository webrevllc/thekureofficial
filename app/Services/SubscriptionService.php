<?php
/**
 * Created by PhpStorm.
 * User: srose
 * Date: 9/25/2015
 * Time: 10:56 AM
 */

namespace App\Services;


use App\Contracts\SubscriptionContract;
use App\Events\Subscription\Success;
use App\Events\Subscription\SuccessfulSubscriptionRenewal;
use App\Failedsub;
use App\Subscription;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class SubscriptionService extends AuthPayment implements SubscriptionContract{


    public function userHasValidCIM($subscription)
    {
        if($subscription->user->auth_profile == ''){ // or others
            return false;
        }
        return true;
    }

    public function theUserHasValidCIM($user)
    {
        if($user->auth_profile == ''){ // or others
            return false;
        }
        return true;
    }

    public function chargeSubscription($subscription)
    {
        $user = $subscription->user;
        $transaction = $this->authorizeNetTransaction;
        $transaction->amount = (string) $subscription->amount;
        $transaction->customerProfileId = (string) $user->auth_profile;
        $transaction->customerPaymentProfileId = (string) $user->default_billing;
        $transaction->customerShippingAddressId = (string) $user->default_shipping;
        $transaction->order->invoiceNumber = (string) $user->default_shipping;
        $response = $this->authorizeNetCIM->createCustomerProfileTransaction("AuthCapture", $transaction);
        $transactionResponse = $response->getTransactionResponse();
        $transactionId = $transactionResponse->transaction_id;
        if($transactionResponse->approved){
            event(new SuccessfulSubscriptionRenewal($subscription, $transactionId));
        }else{
            $reason = $transactionResponse->response_reason_text;
            $failedSub = new Failedsub;
            $failedSub->reason = $reason;
            $failedSub->amount = $subscription->amount;
            $failedSub->user_id = $subscription->user->id;
            $failedSub->subscription_id = $subscription->id;
            $failedSub->next_process_date = Carbon::today()->addDays(4);
            $failedSub->attempts = 1;
            $failedSub->save();
        }
    }

    public function chargeFailedSubscription($failedSub, $user)
    {
        $transaction = $this->authorizeNetTransaction;
        $transaction->amount = (string) $failedSub->amount;
        $transaction->customerProfileId = (string) $user->auth_profile;
        $transaction->customerPaymentProfileId = (string) $user->default_billing;
        $transaction->customerShippingAddressId = (string) $user->default_shipping;
        $transaction->order->invoiceNumber = (string) $user->default_shipping;
        $response = $this->authorizeNetCIM->createCustomerProfileTransaction("AuthCapture", $transaction);
        $transactionResponse = $response->getTransactionResponse();
        $transactionId = $transactionResponse->transaction_id;
        if($transactionResponse->approved){
            $s = Subscription::find($failedSub->subscription_id);
            event(new SuccessfulSubscriptionRenewal($s, $transactionId));
            return 'success';
        }else{
           return $response->getErrorMessage();
        }
    }

    public function updateUserPayment( $user, $requestData ) {
        // TODO: Implement updateUserPayment() method.
    }

    public function createExistingUserOrder($user, $total)
    {
        // TODO: Implement createExistingUserOrder() method.
    }
}