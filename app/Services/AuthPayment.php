<?php
namespace App\Services;

use App\User;
use App\Order;
use AuthorizeNetAddress;
use AuthorizeNetCustomer;
use AuthorizeNetPaymentProfile;
use AuthorizeNetTD;
use AuthorizeNetAIM;
use AuthorizeNetCIM;
use AuthorizeNetLineItem;
use App\Contracts\Payment;
use AuthorizeNetTransaction;
use App\Events\Sale\AddOnSale;
use App\Events\IVR\FailedIVROrder;
use App\Events\IVR\OrderPlacedByIVR;
use App\Http\Controllers\Traits\MakesOrderObjects;



class AuthPayment implements Payment
{

    use MakesOrderObjects;
    /**
     * @var AuthorizeNetAIM
     */
    public $authorizeNetAIM;
    /**
     * @var AuthorizeNetCIM
     */
    public $authorizeNetCIM;
    /**
     * @var Order
     */
    public $order;
    /**
     * @var AuthorizeNetTD
     */
    public $authorizeNetTid;
    /**
     * @var AuthorizeNetTransaction
     */
    public $authorizeNetTransaction;
    /**
     * @var AuthorizeNetLineItem
     */
    public $authorizeNetLineItem;
    /**
     * @var AuthorizeNetCustomer
     */
    public $authorizeNetCustomer;
    /**
     * @var AuthorizeNetPaymentProfile
     */
    public $authorizeNetPaymentProfile;
    /**
     * @var AuthorizeNetAddress
     */
    public $authorizeNetAddress;

    /**
     * @param AuthorizeNetAIM $authorizeNetAIM
     * @param AuthorizeNetCIM $authorizeNetCIM
     * @param Order $order
     * @param AuthorizeNetTD $authorizeNetTid
     * @param AuthorizeNetTransaction $authorizeNetTransaction
     * @param AuthorizeNetLineItem $authorizeNetLineItem
     * @param AuthorizeNetCustomer $authorizeNetCustomer
     * @param AuthorizeNetPaymentProfile $authorizeNetPaymentProfile
     * @param AuthorizeNetAddress $authorizeNetAddress
     */
    public function __construct(AuthorizeNetAIM $authorizeNetAIM, AuthorizeNetCIM $authorizeNetCIM, Order $order, AuthorizeNetTD $authorizeNetTid, AuthorizeNetTransaction $authorizeNetTransaction, AuthorizeNetLineItem $authorizeNetLineItem, AuthorizeNetCustomer $authorizeNetCustomer, AuthorizeNetPaymentProfile $authorizeNetPaymentProfile, AuthorizeNetAddress $authorizeNetAddress)
    {
        $this->authorizeNetAIM = $authorizeNetAIM;
        $this->authorizeNetCIM = $authorizeNetCIM;
        $this->order = $order;
        $this->authorizeNetTid = $authorizeNetTid;
        $this->authorizeNetTransaction = $authorizeNetTransaction;
        $this->authorizeNetLineItem = $authorizeNetLineItem;
        $this->authorizeNetCustomer = $authorizeNetCustomer;
        $this->authorizeNetPaymentProfile = $authorizeNetPaymentProfile;
        $this->authorizeNetAddress = $authorizeNetAddress;
    }

    public function getLast4($order)
    {
        return substr($this->authorizeNetTid->getTransactionDetails($order)->xml->transaction->payment->creditCard->cardNumber, -4);
    }



    //AIM FUNCTIONS
    public function setAIMCustomer($id)
{
    $this->authorizeNetAIM->setFields(['cust_id' => $id, 'description' => 'The Kure']);
}

    public function setAIMTotal($total)
    {
        $this->authorizeNetAIM->amount = $total;
    }

    public function createAIMLineItems($products)
    {
        foreach($products as $p){
            $id = $p->id;
            $name = $p->name;
            $description = $p->description;
            $cost = $p->price/100;
            $lineQty = $p->quantity;
            $this->authorizeNetAIM->addLineItem($id, $name, $description, $lineQty, $cost, 'N');
        }
    }

    public function refund($amount, $order)
    {
        $last4 = $order->last_4;
//        $this->authorizeNetAIM->setFields((array)$this->addBillingInfoToRefunds($order->user->id));
        $response = $this->authorizeNetAIM->credit($order->auth_transaction, $amount, $last4);
//        dd($response);
        if($response->approved){
            $transactionId = $response->transaction_id;
            return ['id' => $transactionId];
        }else{
            return $response->error_message;
        }
    }

    public function voidTransaction($trans_id)
    {
        $this->authorizeNetAIM->void($trans_id);
    }

    public function setAIMBillingFields($orderInfo)
    {
        $this->authorizeNetAIM->setFields((array) $orderInfo);
    }

    public function setAIMShippingFields($orderInfo)
    {
        $this->authorizeNetAIM->setFields((array) $orderInfo);
    }

    private function makeAIMCustomer($request, $email)
    {
        $customer = new \stdClass();
        $customer->first_name = (string) $request->firstName;
        $customer->last_name = (string) $request->lastName;
        $customer->address = (string) $request->address;
        $customer->city = (string) $request->city;
        $customer->state = (string) $request->state;
        $customer->zip = (string) $request->zip;
        $customer->email = $email;
        return $customer;
    }

    public function capturePriorAuth($trans_id, $amount)
    {
        $this->authorizeNetAIM->priorAuthCapture($trans_id, $amount);
    }

    public function setAIMInvoiceNumber($invoice_number)
    {
        $this->authorizeNetAIM->invoice_num = $invoice_number;
    }

    public function makeAIMPayment($type, $amount, $card, $exp)
    {
        $this->authorizeNetAIM->amount = $amount;
        $this->authorizeNetAIM->card_num = $card;
        $this->authorizeNetAIM->exp_date = $exp;

        if($type == 'authorizeOnly'){
            $response = $this->authorizeNetAIM->authorizeOnly();
        }else{
            $response = $this->authorizeNetAIM->authorizeAndCapture();
        }

        return $response;
    }


    //CIM FUNCTIONS
    public function createCIMProfile($transaction)
    {
            $response = $this->createAuthIDs($transaction);
            if($response->isError()){
                return false;
            }else{
                $customerProfileID = $response->xml->customerProfileId;
                $customerBillingID = $response->xml->customerPaymentProfileIdList->numericString;
                $customerShippingID = $response->xml->customerShippingAddressIdList->numericString;
                $keys = new \stdClass();
                $keys->customerProfileID = (string)$customerProfileID;
                $keys->customerBillingID = (string)$customerBillingID;
                $keys->customerShippingID = (string)$customerShippingID;
                return $keys;
            }

    }

    public function createAuthIDs($transaction_id)
    {
        return $this->authorizeNetCIM->createCustomerProfileFromTransaction($transaction_id);
    }

    public function getAuthIDs($customerProfileID)
    {
        return $this->authorizeNetCIM->getCustomerProfile($customerProfileID);
    }

    public function addBillingInfoToRefunds($userID)
    {
        $user = User::find($userID);
        $billing = $this->getBillingInfo($user->auth_profile, $user->default_billing);
        $customer = $this->makeAIMCustomer($billing, $user->email);
        $customer = $this->makeAIMCustomer($billing, $user->email);
        return $customer;

    }

    public function addNewProfileKeysToUser($keys, $user)
    {
        $user->auth_profile = $keys->customerProfileID;
        $user->default_billing = $keys->customerBillingID;
        $user->default_shipping = $keys->customerShippingID;
        $user->save();
    }

    public function makeSubscriptionPayment($subscription)
    {
        $user = User::where('id', '=', $subscription->user_id)->first();
//        $taxable = $user->is_taxable();
        $transaction = $this->authorizeNetTransaction;
        $total = $this->order->recurringProductsTotal(json_decode($subscription->products) );
        $transaction->amount = $total;
//        $transaction->lineItems[] = (array)$this->createCIMLineItems(json_decode($subscription->products), $taxable);
        $transaction->customerProfileId = $user->auth_profile;
        $transaction->customerPaymentProfileId = $user->default_billing;
        $transaction->customerShippingAddressId = $user->default_shipping;
        $response = $this->authorizeNetCIM->createCustomerProfileTransaction("AuthCapture", $transaction, "The Kure Subscription Renewal");

        if($response->isOk()){
            //event(new SubscriptionProcessedSuccesfully());
        }else{
            //event(new SubscriptionProcessingFailed());
        }
    }

    public function createCIMLineItems($products, $taxable, $transaction)
    {

        foreach($products as $product){
            $lineItem = new AuthorizeNetLineItem;
            $lineItem->itemId = $product->id;
            $lineItem->name = $product->name;
            $lineItem->description = $product->description;
            $lineItem->quantity = $product->quantity;
            $lineItem->unitPrice = $product->price/100;
            $lineItem->taxable = $taxable;
            $transaction->lineItems[] = $lineItem;
        }
    }

    public function addOnSale($order, $amount, $quantity, $products)
    {
        $invoiceNumber = $this->order->newInvoiceNumber();
        $customerProfileId = $order->user->auth_profile;
        $paymentProfileId = $order->user->default_billing;
        $customerAddressId = $order->user->default_shipping;
        $request = $this->authorizeNetCIM;
        $transaction = $this->authorizeNetTransaction;
        $transaction->order->invoiceNumber = $invoiceNumber;
        $transaction->amount = $amount;
        $transaction->customerProfileId = $customerProfileId;
        $transaction->customerPaymentProfileId = $paymentProfileId;
        $transaction->customerShippingAddressId = $customerAddressId;

        foreach($products as $product){

            $lineItem              = $this->authorizeNetLineItem;
            $lineItem->itemId      = $product->id;
            $lineItem->name        = $product->name;
            $lineItem->description = $product->description;
            $lineItem->quantity    = $quantity;
            $lineItem->unitPrice   = $product->price/100;
            $lineItem->taxable     = $order->user->taxable;
            $transaction->lineItems[] = $lineItem;
        }
        $response = $request->createCustomerProfileTransaction("AuthCapture", $transaction);

        $transactionResponse = $response->getTransactionResponse();
        if($transactionResponse->approved){
            $transactionId = $transactionResponse->transaction_id;

            event(new AddOnSale($order->user, $products, $transactionId, $invoiceNumber,$amount));
            return $transactionId;
        }
        return  'declined?';
    }

    public function getBillingInfo($auth_profile, $default_billing) //maybe just accept user id later?
    {
        $billing = $this->authorizeNetCIM->getCustomerPaymentProfile($auth_profile, $default_billing);
        return $billing->xml->paymentProfile->billTo;
    }

    public function getShippingAddress($auth_profile, $default_shipping) //maybe just accept user id later?
    {
        $ids = $this->authorizeNetCIM->getCustomerShippingAddress($auth_profile, $default_shipping);
        return $ids->xml->address ;
    }

    public function instantiateCIMTransaction()
    {
        return $this->authorizeNetTransaction;
    }

    public function makeCIMPayment($user, $products)
    {
        $total = $this->order->total($products, $user);
        $taxable = $user->taxable;
        $transaction = $this->authorizeNetTransaction;
        $transaction->amount = $total;
        $this->createCIMLineItems($products, $taxable, $transaction);
        $transaction->customerProfileId = $user->auth_profile;
        $transaction->customerPaymentProfileId = $user->default_billing;
        $transaction->customerShippingAddressId = $user->default_shipping;
        if ($taxable) {
            $taxAmount = $this->order->calculateTax($total);
            $transaction->tax->amount = $taxAmount;
            $transaction->tax->description = 'TAX';
        }

        dd($transaction);
    }

    public function CIMRefund($user, $amount, $transId)
    {
        $transaction = $this->authorizeNetTransaction;
        $transaction->amount = $amount;
        $transaction->customerProfileId = $user->auth_profile;
        $transaction->customerPaymentProfileId = $user->default_billing;
        $transaction->transId = $transId; // original transaction ID
        $response = $this->authorizeNetCIM->createCustomerProfileTransaction("Refund", $transaction);

        $transactionResponse = $response->getTransactionResponse();

        if($transactionResponse->approved){
            $transactionId = $transactionResponse->transaction_id;
            return ['id' => $transactionId];
        }else{
            return $transactionResponse->error_message;
        }
    }

    public function getCustomerPaymentProfile($userId)
    {
        $user = User::find($userId);
        return $this->authorizeNetCIM->getCustomerPaymentProfile($user->auth_profile, $user->default_billing);
    }

    public function getCustomerShippingProfile($userId)
    {
        $user = User::find($userId);
        return $this->authorizeNetCIM->getCustomerShippingAddress($user->auth_profile, $user->default_shipping);
    }

    public function getHostedPage($profile)
    {
        $response = $this->authorizeNetCIM->getHostedProfilePageRequest($profile);
        return $response->xml->token;
    }

    public function getProfile($profile)
    {
        $response = $this->authorizeNetCIM->getCustomerProfile($profile);

        return $response->xml->profile;
    }

}

