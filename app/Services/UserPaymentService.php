<?php
/**
 * Created by PhpStorm.
 * User: srose
 * Date: 9/28/2015
 * Time: 12:47 PM
 */

namespace App\Services;




use App\Contracts\UserPaymentContract;

class UserPaymentService extends IVRPayments implements UserPaymentContract
{


	public function updateUserPayment( $user, $requestData ) {
		$card = $requestData->card;
		$month = $requestData->month;
		$year = $requestData->year;
		$paymentProfile = new $this->authorizeNetPaymentProfile;
		$paymentProfile->customerType = "individual";
		$paymentProfile->payment->creditCard->cardNumber = $card;
		$paymentProfile->payment->creditCard->expirationDate = $year.'-'.$month;
		$paymentProfile->billTo->firstName = $requestData->fname;
		$paymentProfile->billTo->lastName = $requestData->lname;
		$paymentProfile->billTo->address = $requestData->add1.' ' .$requestData->add2;
		$paymentProfile->billTo->city = $requestData->city;
		$paymentProfile->billTo->state = $requestData->state;
		$paymentProfile->billTo->zip = $requestData->zip;
		$paymentProfile->billTo->country = $requestData->country;
		$response = $this->authorizeNetCIM->createCustomerPaymentProfile($user->auth_profile, $paymentProfile, "liveMode");
		if($response->isOk())
		{
			$user->default_billing = (string)$response->xml->customerPaymentProfileId;  //188665251
			$user->save();
//			dd((string)$response->xml->customerPaymentProfileId);
			return 'success';
		}else{
			return $response->getErrorMessage();

		}
	}

	public function createExistingUserOrder($user, $total)
	{
		$transaction = $this->authorizeNetTransaction;
		$transaction->amount = $total;
		$transaction->customerProfileId = $user->auth_profile;
		$transaction->customerPaymentProfileId = $user->default_billing;
		$transaction->customerShippingAddressId = $user->default_shipping;
		dd($transaction);
		$response = $this->authorizeNetCIM->createCustomerProfileTransaction("AuthCapture", $transaction);
		$transactionResponse = $response->getTransactionResponse();
		if($transactionResponse->approved){
			$transactionId = $transactionResponse->transaction_id;
			return ['transactionID' => $transactionId];
		}else{
			return $response->getMessageText();
		}
	}
}