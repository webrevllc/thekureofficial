<?php

namespace App\Services;

use App\Contracts\Invisacloud;
use App\Order;
use App\User;


class InvisacloudConnector implements Invisacloud
{

public function getToken() {
    $tokenUrl = 'https://www.invisacloud.com/token';
    $myPost = 'userName=invisatecwoo@test.com&Password=Test11..&grant_type=password';
    $ch     = curl_init();
    curl_setopt( $ch, CURLOPT_URL, $tokenUrl );
    curl_setopt( $ch, CURLOPT_POST, 1 );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch, CURLOPT_POSTFIELDS, $myPost );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    $results = curl_exec( $ch );
    curl_close( $ch );
    $jsonResult = json_decode( $results, true);
    $token = ( $jsonResult['access_token'] );
    return $token;
}


    public function makeUser($user, $transaction_id, $campaign)
    {
        $order = Order::where('auth_transaction', $transaction_id)->first();
        $transactionID = $order->invoice_number;
        $fname = $user->name;
        $email = $user->email;
        $profileID = $user->auth_profile;
        $shippingProfileID = $user->default_shipping;
        $billingProfileID = $user->default_billing;
        $id = $user->id;
        $token = $this->getToken();
        $url        = "https://www.invisacloud.com/api/customers";
//        $url        = "http://ccuinvisatec.thecodingpit.com/api/customers";
        $ch         = curl_init();
        $headers = array( "Authorization: Bearer $token" );
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_POST, 1 );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt( $ch, CURLOPT_POSTFIELDS,
            "CustomerID=$id&Fname=$fname&Transaction=$transactionID&Email=$email&ProfileID=$profileID&ShippingProfileID=$shippingProfileID&BillingProfileID=$billingProfileID&Campaign=$campaign" );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $server_output = curl_exec( $ch );
        $object        = json_decode( $server_output );
        curl_close( $ch );
        return $object;
    }

    public function makeNewUser($user, $invoice_number, $campaign)
    {
        $order = Order::where('invoice_number', $invoice_number)->first();
        $transactionID = $order->invoice_number;
        $fname = $user->name;
        $email = $user->email;
        $profileID = $user->auth_profile;
        $shippingProfileID = $user->default_shipping;
        $billingProfileID = $user->default_billing;
        $id = $user->id;
        $token = $this->getToken();
        $url        = "https://www.invisacloud.com/api/customers";
//        $url        = "http://ccuinvisatec.thecodingpit.com/api/customers";
        $ch         = curl_init();
        $headers = array( "Authorization: Bearer $token" );
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_POST, 1 );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt( $ch, CURLOPT_POSTFIELDS,
            "CustomerID=$id&Fname=$fname&Transaction=$transactionID&Email=$email&ProfileID=$profileID&ShippingProfileID=$shippingProfileID&BillingProfileID=$billingProfileID&Campaign=$campaign" );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $server_output = curl_exec( $ch );
        $object        = json_decode( $server_output );
        curl_close( $ch );
        return $object;
    }

    public function uploadOrder($order){
        $myBigArray = array();
        $products = json_decode($order->products);
//        if($order->status == "trial"){
        if($order->status == "2pay"){
            $lol = 33;
        }else{
            $lol = env('ONEPAY_LOL');
        }
        foreach($products as $eachProduct){
            if($eachProduct->ivr_code != "PROC" && $eachProduct->ivr_code != "EMER" && $eachProduct->ivr_code != "SHIP"){
                if($eachProduct->ivr_code == "KLIFE"){
                    $lol = 5000;
                }
                $myArray = array(
                    "ID" => $eachProduct->quantity,
                    "sold" => 1,
                    "appletname" => $eachProduct->api_code,
                    "transaction" => $order->invoice_number,
                    "lengthOfLicense" => $lol
                );
                array_push($myBigArray, $myArray);
            }
        }
        $jsonArray = json_encode($myBigArray);
        $token = $this->getToken();
//        $url = "http://ccuinvisatec.thecodingpit.com/api/AppletLicensing";
        $url = "https://www.invisacloud.com/api/appletlicensing";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=utf-8","Accept:application/json, text/javascript, */*; q=0.01", "Authorization: Bearer $token"));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonArray);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);
        return $response;
    }


    public function updateOrder($order){
        $myBigArray = array();
        $products = json_decode($order->products);
        if($order->status == "trial"){
            $lol = 30;
        }else{
            $lol = env('ONEPAY_LOL');
        }
        foreach($products as $eachProduct){
            if($eachProduct->ivr_code != "PROC" && $eachProduct->ivr_code != "EMER"){
                if($eachProduct->ivr_code == "SHIP"){
                    $myArray = array(
                        "ID" => $eachProduct->quantity,
                        "sold" => 1,
                        "appletname" => "KURE",
                        "transaction" => $order->invoice_number,
                        "lengthOfLicense" => $lol
                    );
                    array_push($myBigArray, $myArray);
                }else{
                    $myArray = array(
                        "ID" => $eachProduct->quantity,
                        "sold" => 1,
                        "appletname" => $eachProduct->api_code,
                        "transaction" => $order->invoice_number,
                        "lengthOfLicense" => $lol
                    );
                    array_push($myBigArray, $myArray);
                }
            }
        }
        $jsonArray = json_encode($myBigArray);
        $token = $this->getToken();
        $url = "https://www.invisacloud.com/api/appletlicensing/update";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=utf-8","Accept:application/json, text/javascript, */*; q=0.01", "Authorization: Bearer $token"));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonArray);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    public function createSerials($quantity)
    {
        $sarray = array();

        $token = $this->getToken();
        $url = "https://www.invisacloud.com/api/serialnumbers/create/$quantity";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=utf-8","Accept:application/json, text/javascript, */*; q=0.01", "Authorization: Bearer $token"));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec( $ch );
        $object        = json_decode( $server_output );
        curl_close( $ch );


        return $object;
    }

    public function isSerialUsed($serial) {
        $token = $this->getToken();
        $url = "https://www.invisacloud.com/api/serialnumbers/isserialused/$serial";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer $token"));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec( $ch );
        $object        = json_decode( $server_output );
        curl_close( $ch );


        return $object;
    }

    public function getKurePassword($serial) {
        $token = $this->getToken();
        $url = "https://www.invisacloud.com/api/thekure/getpassword/$serial";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer $token"));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec( $ch );
        $object        = json_decode( $server_output );
        curl_close( $ch );


        return $object;
    }

    public function getFriendlyName($serial) {
        $token = $this->getToken();
        $url = "https://www.invisacloud.com/api/invisatecaccts/getfriendlyname/$serial";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer $token"));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec( $ch );
        $object        = json_decode( $server_output );
        curl_close( $ch );

        return $object;
    }

    public function subscriptionRenewal($subscription, $numberOfDays){
        $myBigArray = array();
        $products = json_decode($subscription->products);
        foreach($products as $eachProduct){
            $myArray = array(
                "ID" => $eachProduct->quantity,
                "sold" => 1,
                "appletname" => $eachProduct->api_code,
                "transaction" => $subscription->unique_id,
                "lengthOfLicense" => $numberOfDays
            );
            array_push($myBigArray, $myArray);
        }
        $jsonArray = json_encode($myBigArray);
        $token = $this->getToken();
        $url = "https://www.invisacloud.com/api/appletlicensing/update";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=utf-8","Accept:application/json, text/javascript, */*; q=0.01", "Authorization: Bearer $token"));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonArray);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    public function updateSubscription($subscription){
        $myBigArray = array();
        $products = json_decode($subscription->products);
        $lol = 455;

        foreach($products as $eachProduct){
            if($eachProduct->ivr_code != "PROC" && $eachProduct->ivr_code != "EMER"){
                if($eachProduct->ivr_code == "SHIP"){
                    $myArray = array(
                        "ID" => $eachProduct->quantity,
                        "sold" => 1,
                        "appletname" => "KURE",
                        "transaction" => $subscription->invoice_number,
                        "lengthOfLicense" => $lol
                    );
                    array_push($myBigArray, $myArray);
                }else{
                    $myArray = array(
                        "ID" => $eachProduct->quantity,
                        "sold" => 1,
                        "appletname" => $eachProduct->api_code,
                        "transaction" => $subscription->invoice_number,
                        "lengthOfLicense" => $lol
                    );
                    array_push($myBigArray, $myArray);
                }
            }
        }
        $jsonArray = json_encode($myBigArray);
        $token = $this->getToken();
        $url = "https://www.invisacloud.com/api/appletlicensing/update";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=utf-8","Accept:application/json, text/javascript, */*; q=0.01", "Authorization: Bearer $token"));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonArray);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
}


