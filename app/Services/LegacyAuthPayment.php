<?php
namespace App\Services;

use App\Contracts\LegacyPayment;
use App\Events\IVR\FailedIVROrder;
use App\Events\IVR\OrderPlacedByIVR;
use App\Order;
use App\User;
use AuthorizeNetAIM;
use AuthorizeNetCIM;
use App\Contracts\Payment;
use App\Events\OrderPlacedOnWebsite;
use App\Http\Controllers\Traits\MakesOrderObjects;
use AuthorizeNetLineItem;
use AuthorizeNetTD;
use AuthorizeNetTransaction;
use Illuminate\Http\Request;

class LegacyAuthPayment implements LegacyPayment
{

    use MakesOrderObjects;
    /**
     * @var AuthorizeNetAIM
     */
    protected $authorizeNetAIM;
    /**
     * @var AuthorizeNetCIM
     */
    protected $authorizeNetCIM;
    /**
     * @var Order
     */
    private $order;
    /**
     * @var AuthorizeNetTD
     */
    private $authorizeNetTid;
    /**
     * @var AuthorizeNetTransaction
     */
    private $authorizeNetTransaction;
    /**
     * @var AuthorizeNetLineItem
     */
    private $authorizeNetLineItem;

    /**
     * @param AuthorizeNetAIM $authorizeNetAIM
     * @param AuthorizeNetCIM $authorizeNetCIM
     * @param Order $order
     * @param AuthorizeNetTD $authorizeNetTid
     * @param AuthorizeNetTransaction $authorizeNetTransaction
     * @param AuthorizeNetLineItem $authorizeNetLineItem
     */
    public function __construct(AuthorizeNetAIM $authorizeNetAIM, AuthorizeNetCIM $authorizeNetCIM, Order $order, AuthorizeNetTD $authorizeNetTid, AuthorizeNetTransaction $authorizeNetTransaction, AuthorizeNetLineItem $authorizeNetLineItem)
    {
        $this->authorizeNetAIM = $authorizeNetAIM;
        $this->authorizeNetCIM = $authorizeNetCIM;
        $this->order = $order;
        $this->authorizeNetTid = $authorizeNetTid;
        $this->authorizeNetTransaction = $authorizeNetTransaction;
        $this->authorizeNetLineItem = $authorizeNetLineItem;
    }


    public function createCIMLineItems($products, $taxable)
    {
        $lineItems = array();
        foreach($products as $product){
            if($product->recurring) {
                $lineItem = new AuthorizeNetLineItem;
                $lineItem->itemId = $product->id;
                $lineItem->name = $product->name;
                $lineItem->description = $product->description;
                $lineItem->quantity = $product->quantity;
                $lineItem->unitPrice = "2.00";
                $lineItem->taxable = 'false';
                $lineItems[] = $lineItem;
            }
        }
        return $lineItems;
    }


    /**
     * @param $customerProfileID
     * @return \AuthorizeNetCIM_Response
     */
    public function getAuthIDs($customerProfileID)
    {
        return $this->authorizeNetCIM->getCustomerProfile($customerProfileID);
    }


    /**
     * Issue a refund for an order
     * @param $amount
     * @param $order
     * @return mixed
     */
    public function refund($amount, $order)
    {
        $last4 = $this->getLast4($order);
        if($order->type == 'aim'){
            $this->authorizeNetAIM->setFields((array)$this->addBillingInfoToRefunds($order->user->id));
            $response = $this->authorizeNetAIM->credit($order->auth_transaction, $amount, $last4);
            if($response->approved){
                return true;
            }
            else{
                return false;
            }
        }
        return false;
    }

    /**
     * Returns the last four of the billing card number
     * @param $order
     * @return mixed
     */
    public function getLast4($order)
    {

        if($order->last_4 == ''){
            $user = $order->user;
            $profileID = $user->auth_profile;
            $paymentProfile = $user->default_billing;
            return $this->authorizeNetCIM->getCustomerPaymentProfile($profileID, $paymentProfile)->xml->paymentProfile->payment->creditCard->cardNumber;
        }else{
            return $order->last_4;
        }

    }

    /**
     * Add billing info to refunds
     * @param $userID
     * @return \stdClass
     */
    public function addBillingInfoToRefunds($userID)
    {
        $user = User::find($userID);
        $billing = $this->getBillingInfo($user->auth_profile, $user->default_billing);
        $customer = $this->makeAIMCustomer($billing, $user->email);
        return $customer;

    }

    public function getShippingAddress($auth_profile, $default_shipping) //maybe just accept user id later?
    {
        $ids = $this->authorizeNetCIM->getCustomerShippingAddress($auth_profile, $default_shipping);
        return $ids->xml->address ;
    }

    public function getBillingInfo($auth_profile, $default_billing) //maybe just accept user id later?
    {
        $billing = $this->authorizeNetCIM->getCustomerPaymentProfile($auth_profile, $default_billing);
        return $billing->xml->paymentProfile->billTo;
    }

    private function makeAIMCustomer($request, $email)
    {
        $customer = new \stdClass();
        $customer->first_name = (string) $request->firstName;
        $customer->last_name = (string) $request->lastName;
        $customer->address = (string) $request->address;
        $customer->city = (string) $request->city;
        $customer->state = (string) $request->state;
        $customer->zip = (string) $request->zip;
        $customer->email = $email;
        return $customer;
    }




    ///////////////////SUBSCRIPTIONS///////////////////////

    public function makeSubscriptionPayment($subscription)
    {
        $user = User::where('id', '=', $subscription->user_id)->first();
//        $taxable = $user->is_taxable();
        $transaction = $this->authorizeNetTransaction;
        $total = $this->order->recurringProductsTotal(json_decode($subscription->products) );
        $transaction->amount = $total;
//        $transaction->lineItems[] = (array)$this->createCIMLineItems(json_decode($subscription->products), $taxable);
        $transaction->customerProfileId = $user->auth_profile;
        $transaction->customerPaymentProfileId = $user->default_billing;
        $transaction->customerShippingAddressId = $user->default_shipping;
        $response = $this->authorizeNetCIM->createCustomerProfileTransaction("AuthCapture", $transaction, "The Kure Subscription Renewal");

        if($response->isOk()){
            //event(new SubscriptionProcessedSuccesfully());
        }else{
            //event(new SubscriptionProcessingFailed());
        }
    }
}