<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Failedsub extends Model
{
    protected $table ='failedsubs';

    public function subscription(  ) {
        return $this->belongsTo('App\Subscription');
    }

    public function user(  ) {
        return $this->belongsTo('App\User');
    }
}
