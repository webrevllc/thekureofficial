<?php

namespace App;

use App\Events\OrderHasShipped;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscription extends Model
{
    protected $fillable = ['unique_id', 'status', 'products', 'next_process_date', 'user_id', 'amount'];

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function change_orders()
    {
        return $this->hasMany('App\ChangeOrder');
    }
    public function failedSubscription()
    {
        return $this->hasOne('App\FailedSub');
    }

    public function markAsShipped($invoice_number)
    {
        $order = Order::with('user', 'subscription')->where('invoice_number', $invoice_number)->first();
        $order->shipped = true;
        $order->shipped_date = Carbon::now();
        $order->shipped_by = \Auth::user()->name;
        $order->save();

        if($order->status == 'trial'){
            $order->subscription->next_process_date = Carbon::now()->addDays(35);
            $order->subscription->save();
        }
//        return redirect()->back();
    }
}
