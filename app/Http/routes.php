<?php


use App\Address;
use App\Emails\EmailBosses;
use App\Emails\SupportNotification;
use App\Events\OrderHasShipped;
use App\Events\WebNew\NewPaidOrderComplete;
use App\Failedsub;
use App\Lead;
use App\Order;
use App\Product;
use App\Serial;
use App\Subscription;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

Route::get('/secretsignin', function(){
   \Auth::loginUsingId(1029);
    return redirect('/mykure');
});

Route::get('/pete', function(){
   return view('pete1.home');
});

Route::get('t', function(){
    $order = Order::where('invoice_number', 'GPPYJUVJ')->first();
    dd(number_format($order->amount, 2));
});

Route::get('/invoice', function(){
    return redirect('/download');
});

Route::get('/error', function(){
    return view('default');
});

//IVR AND UTILITIES
Route::post('kurejson', 'IVRController@newOrder');
Route::post('kurejsonV8', 'IVRController@newOrderV8');
Route::post('testjson', 'IVRController@testJson');

Route::post('jeff', 'JeffController@hello');

Route::get('/mark_shipped', 'ShippingController@newShipped');
Route::get('/mark_printed', 'ShippingController@newPrinted');
Route::get('/referral', 'WebController@referral');
Route::post('/referral', 'WebController@referralPost');



if(env('offer_status') == 'order')
    {
        //    PURCHASE/LEAD GENERATION FREE 911
        Route::get('/purchase', 'LeadControllerFree911@purchase2');
        Route::get('/purchase/{step}', 'LeadControllerFree911@leadStepLookup');
        Route::get('/purchase/no/{product}', 'LeadControllerFree911@noToUpsell');
        Route::post('/purchase/noswitch', 'LeadControllerFree911@keepFree');
        Route::post('/purchase/noswitchagain', 'LeadControllerFree911@paidNoLifetime');
        Route::get('/purchase/remove/{product}', 'LeadControllerFree911@removeProduct');
        Route::post('/purchase/1', 'LeadControllerFree911@create');
        Route::post('/purchase/2', 'LeadControllerFree911@setQuantity');
        Route::post('/purchase/3', 'LeadControllerFree911@validateCard');
        Route::post('/purchase/4', 'LeadControllerFree911@addHWMON');
        Route::post('/purchase/5', 'LeadControllerFree911@addPCOS');
        Route::post('/purchase/6', 'LeadControllerFree911@addPROC');
        Route::post('/purchase/7', 'LeadControllerFree911@switchFree');
        Route::post('/purchase/lifetime', 'LeadControllerFree911@lifetime');
        Route::post('/purchase/avs', 'LeadControllerFree911@avs');
        Route::post('/purchase/qty/', 'LeadControllerFree911@changeQty');
        Route::get('/purchasing', 'LeadControllerFree911@saleMade');
        Route::post('/purchase/paidrushyes', 'LeadControllerFree911@paidrushyes');
        Route::post('/purchase/paidrushno/', 'LeadControllerFree911@paidrushno');
        Route::post('/purchase/freerushno/', 'LeadControllerFree911@freerushno');
        Route::post('/purchase/911yes', 'LeadControllerFree911@yes911');
        Route::post('/purchase/911no/', 'LeadControllerFree911@no911');
        Route::post('/purchase/qtyup/', 'LeadControllerFree911@qtyUpsell');
        Route::post('/purchase/5year/', 'LeadControllerFree911@yes5year');
        Route::post('/purchase/no5year/', 'LeadControllerFree911@no5year');
    }else{
        //    PURCHASE/LEAD GENERATION PAID 911
        Route::get('/purchase', 'LeadControllerPaid911@purchase2');
        Route::get('/purchase/{step}', 'LeadControllerPaid911@leadStepLookup');
        Route::get('/purchase/no/{product}', 'LeadControllerPaid911@noToUpsell');
        Route::post('/purchase/noswitch', 'LeadControllerPaid911@keepFree');
        Route::post('/purchase/noswitchagain', 'LeadControllerPaid911@paidNoLifetime');
        Route::get('/purchase/remove/{product}', 'LeadControllerPaid911@removeProduct');
        Route::post('/purchase/1', 'LeadControllerPaid911@create');
        Route::post('/purchase/2', 'LeadControllerPaid911@setQuantity');
        Route::post('/purchase/3', 'LeadControllerPaid911@validateCard');
        Route::post('/purchase/4', 'LeadControllerPaid911@addHWMON');
        Route::post('/purchase/5', 'LeadControllerPaid911@addPCOS');
        Route::post('/purchase/6', 'LeadControllerPaid911@addPROC');
        Route::post('/purchase/7', 'LeadControllerPaid911@switchFree');
        Route::post('/purchase/lifetime', 'LeadControllerPaid911@lifetime');
        Route::post('/purchase/avs', 'LeadControllerPaid911@avs');
        Route::post('/purchase/qty/', 'LeadControllerPaid911@changeQty');
        Route::get('/purchasing', 'LeadControllerPaid911@saleMade');
        Route::post('/purchase/paidrushyes', 'LeadControllerPaid911@paidrushyes');
        Route::post('/purchase/paidrushno/', 'LeadControllerPaid911@paidrushno');
        Route::post('/purchase/freerushno/', 'LeadControllerPaid911@freerushno');
        Route::post('/purchase/911yes', 'LeadControllerPaid911@yes911');
        Route::post('/purchase/911no/', 'LeadControllerPaid911@no911');
        Route::post('/purchase/5year/', 'LeadControllerPaid911@yes5year');
    }


//referral



//MAIN SITE
if(env('offer_status') == 'free trial'){
    Route::get('/', 'LeadControllerFree911@getHome');
    Route::get('/faq', function(){
        return view('mel4.freefaq');
    });
}else{
    Route::get('/', function () {
        return view('mel6.home');
    });
    Route::get('/faq', function(){
        return view('mel4.faq');
    });
}

Route::get('/userguide', function() {
    return view('mel4.userguide');
});

Route::get('/pre-sales', function () {
    return view('mel4.presales');
});

Route::get('/about', function (Request $request) {
    return view('mel4.about');
});

Route::get('/contact', function(){
    return view('mel4.contact');
});
Route::get('/watch', function(){
    return view('mel4.watch');
});
Route::get('/cancel', function(){
    return view('mel4.cancel');
});
Route::get('/support', function(){
    return view('mel4.support');
});
Route::post('/get_state', 'WebController@getState');
Route::get('/download', function () {
    return view('mel5.download');
});
Route::get('/terms', function () {
    return view('checkout.terms');
});
Route::get('/privacy', function () {
    return view('mel4.privacy');
});
Route::get('/thankyou', 'WebController@showThanks');
Route::get('/thank-you', 'WebController@showThanks2');
Route::get('/order-status', 'WebController@showThanks2');
Route::post('/cancel', 'SubscriptionsController@cancel');
Route::post('/setquantity', function (Request $request) {
    return redirect('checkout/?qty=' . $request->quantity);
});
Route::get('/orderstatus', function(){
    return view('mykure.orderstatus');
});
Route::post('/orderstatus', 'WebController@orderstatus');



Route::post('/webpayment', 'WebController@makePayment');

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');


// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');
Route::post('auth/createAccount', 'UserController@createAccount');
Route::get('auth/lookup', function(){
    return view('auth.lookup');
});
Route::post('auth/lookupAccount', 'UserController@lookupAccount');
Route::post('/inquiry', 'InquiryController@store');

//ADMIN
Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {

    Route::get('/pdf', 'PDFController@makePDF');
    Route::get('/failed_subs', 'SubscriptionsController@allFailed');
    Route::get('/cancelFailed/{subscriptionID}', 'SubscriptionsController@cancelFailed');
    Route::get('/removeFailed/{subscriptionID}', 'SubscriptionsController@removeFromFailedList');
    Route::get('/', 'AdminController@dashboard');
    Route::get('/pvm', 'LeadController@showPVM');
    Route::post('/pvm', 'LeadController@getCustom');
    Route::post('/lookup', 'AdminController@lookup');
    Route::post('/lookup-order', 'AdminController@lookupOrder');
    Route::resource('/inquiries', 'InquiryController');
    Route::resource('/shipments', 'ShippingController');
    Route::resource('/customers', 'CustomersManagementController');
    Route::resource('/order', 'OrderController');
    Route::post('/subscription/{subscriptionId}/partial', 'SubscriptionsController@partialRefund');
    Route::post('/subscription/{id}/modify-trial', 'SubscriptionsController@modifyTrial');
    Route::post('/subscription/{id}/full', 'SubscriptionsController@fullRefund');
    Route::post('/subscription/{id}/add-on', 'SubscriptionsController@addOn');
    Route::post('/inquiries/{id}/message', 'InquiryController@sendMessage');
    Route::post('/ordernotfound/{id}', 'OrderController@uploadOrder');
    Route::post('/customernotfound/{id}', 'OrderController@uploadCustomer');
    Route::get('/trialpdf/{orderID}', 'ShippingController@getTrialReceipt');
    Route::get('/shipments/marked/{orderID}', 'ShippingController@markAsShipped');
    Route::get('/failed', 'AdminController@failed');
    Route::get('/financial', 'AdminController@financial');
    Route::post('/financial', 'AdminController@makeCustomDate');
    Route::get('/missing-address', 'AdminController@missingAddress');
    Route::post('/schedule', 'AdminController@uploadSchedule');
    Route::get('/leads', 'AdminController@showLeads');
    Route::get('/user/edit/{id}', 'UserController@edit');
    Route::post('/user/edit/{id}', 'UserController@update');
    Route::post('/send-receipt', 'UserController@sendReceipt');
    Route::post('/resendCards', 'UserController@resendCards');
    Route::post('/subscription/{id}/cancel-trial', 'SubscriptionsController@cancelTrial');
    Route::post('/edit_cc/{userid}', 'UserController@updateCC');
    Route::get('/paysubscription/{sub_id}', 'SubscriptionsController@payNow');
    Route::get('/chargefailedsubscription/{sub_id}', 'SubscriptionsController@chargeFailed');
    Route::get('/user/order/{id}', 'AdminController@showOrderPage');
    Route::get('/ivr_leads', 'AdminController@showIVRLeads');
    Route::get('/ivr_lead/{id}', 'AdminController@deleteIVRLead');
    Route::post('/user/order/{id}', 'UserController@makeNewOrder');

    Route::get('/csv', 'PDFController@createCSV');

//    Route::get('/weborders',function(\Maatwebsite\Excel\Excel $excel){
//        $excel->create('Web Report', function ($e){
//            $e->sheet('Web Report', function ($sheet) {
//                $orders = Order::with('subscription')->where('source', 'web')->whereBetween('created_at', [Carbon::createFromDate(2015,12,28)->startOfDay(), Carbon::yesterday()->endOfDay()])->get();
//                foreach($orders as $order){
//                    if($order->amount == "Free Trial"){
//                        $order->amount = $order->subscription->amount;
//                    }
//                }
//                $sheet->fromArray($orders);
//            });
//        })->download('xls');
//    });
});

Route::group(['prefix' => 'mykure', 'middleware' => 'auth'], function () {
    Route::get('/', "MyKureAccountController@index");
    Route::get('/account', "MyKureAccountController@account");
    Route::get('/support', 'MyKureAccountController@support');
    Route::post('/account/updatePassword', 'MyKureAccountController@updatePassword');
    Route::post('/support/getKurePassword', 'MyKureAccountController@getKurePassword');
    Route::get('/pdf', 'MyKureAccountController@generatePDFReceipt');
    Route::get('/cancel', 'MyKureAccountController@cancelSubscription');
    Route::post('/','MyKureAccountController@updateBilling');

});
