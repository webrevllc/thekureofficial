<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class MustBeAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (auth()->check()){
            $user = User::find(auth()->user()->id);
            if($user->admin){
                return $next($request);
            }
            return redirect('/mykure');
        }
        return redirect('/auth/login');
    }
}
