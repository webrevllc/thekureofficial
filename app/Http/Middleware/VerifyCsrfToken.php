<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'kurejson',
        'kurejsonV8',
        'kurejson.php',
        'checkout',
        '/checkout',
        '/setquantity',
        'setquantity',
        'purchase/qty',
        'purchase/*',
        'purchasing',
        'get_state',
        'mykure/support/getKurePassword',
        'testjson',
        ''
    ];
}
