<?php

namespace App\Http\Controllers;

use App\Emails\DeclinedCard;
use App\Events\IVRNew\DeclinedIVROrder;
use App\IVRLead;
use App\User;
use App\Order;
use App\Product;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Contracts\IVRPaymentContract;
use App\Http\Controllers\Traits\MakesOrderObjects;


class IVRController extends Controller
{

    use MakesOrderObjects;

    /**
     * @var IVRPaymentContract
     */
    private $payment;
    /**
     * @var User
     */
    private $user;
    /**
     * @var Product
     */
    private $product;
    /**
     * @var Order
     */
    private $order;

    public function __construct(IVRPaymentContract $payment, User $user, Product $product, Order $order)
    {
        $this->payment = $payment;
        $this->user = $user;
        $this->product = $product;
        $this->order = $order;
    }

   public function newOrderV8(Request $request)
    {
        $data = $request->all();
        $orderInfo = $data[0]['Order'];
        //CONVERT ARRAY TO OBJECT
//        $orderInfo = json_decode(json_encode($orderInfo));
        $response = $this->makeIVRPaymentV8($orderInfo);
        return $response;
    }

    public function makeIVRPaymentV8($orderInfo)
    {
        if($this->isNotADuplicate($orderInfo)){
            if($this->checkIfTrial($orderInfo)){
                $type = 'authorizeOnly';
            }else{
                $type = 'authorizeAndCapture';
            }
            if($this->checkIfLead($orderInfo)){
                $this->makeIVRLead($orderInfo);
                return \Response::json('Lead Made', 201);
            }else{

                $orderInfo = json_decode(json_encode($orderInfo));
                $user = $this->doesUserExist($orderInfo);
                return $this->payment->updateCustomerWithJSON($user, $orderInfo, $type);
            }
        }else{
            return \Response::json('DUPLICATE', 405);
        }
    }

    private function createIVRUser($customer)
    {
        $dt = Carbon::now();
        $rand = random_int(1,5000);
        try{
            $user = User::where('email', $customer->Email)->firstOrFail();
            return $user;
        }catch(ModelNotFoundException $m){
            $user = new User;
            $user->name = ucwords(strtolower($customer->First_Name)). ' '.  ucwords(strtolower($customer->Last_Name));
            $user->phone = $customer->Phone;
            $user->offer = env('offer');
            if($customer->Email == ''){
                $customer->Email = 'thekure+'.$dt->timestamp.$rand.'@gmail.com';
            }
            $user->email = strtolower($customer->Email);
            $user->password = bcrypt('password');
            if($customer->State == "MO"){
                $user->taxable = true;
                $user->tax_rate = $customer->TAX;
            }
            $user->save();

            return $user;
        }


    }

    /**
     * @param $orderInfo
     * @return User
     */
    public function doesUserExist($orderInfo)
    {
        try{
            $user = User::whereEmail(strtolower($orderInfo->Customer->Email))->firstorfail();
            return $user;
        }catch(ModelNotFoundException $m){
            $user = $this->createIVRUser($orderInfo->Customer);
            return $user;
        }
    }

    /**
     * @param $orderInfo
     * @return string
     */
    public function checkIfTrial($orderInfo)
    {
        if($orderInfo['Billing']['Type'] == "TRIAL"){
            return true;
        }else{
            return false;
        }
    }

    public function checkIfLead($orderInfo)
    {
        //UNCOMMENT TO GO LIVE
        $status = $orderInfo['Billing']['STATUS'];
        switch($status){
            case "APPROVED":
                return false;
                break;
            case "DECLINED":
                return true;
                break;
            case "NOT AUTHED":
//                $authed = $this->payment->auth($orderInfo);
                return false;
                break;
            default:
                return true;
        }

//        return false;
    }

    public function makeIVRLead($orderInfo)
    {
        $l = new IVRLead;
        $l->name = $orderInfo['Customer']['First_Name'].' '. $orderInfo['Customer']['Last_Name'];
        $l->email =  $orderInfo['Customer']['First_Name'];
        $l->address =  $orderInfo['Customer']['Address_1'].' '. $orderInfo['Customer']['Address_2'];
        $l->city =  $orderInfo['Customer']['City'];
        $l->state =  $orderInfo['Customer']['State'];
        $l->zip =  $orderInfo['Customer']['Zip'];
        $l->country =  $orderInfo['Customer']['Country'];
        $l->phone =  $orderInfo['Customer']['Phone'];
        $l->reason = $orderInfo['Billing']['DECLINED_REASON'];
        $l->offer =  env('offer');

        $orderInfo = json_decode(json_encode($orderInfo));
        $productsOrdered = $this->makeFullProducts($orderInfo->Products);

        $l->products = json_encode($productsOrdered);
        $l->amount = $this->order->totalNoUser($l->products);
        $l->save();
        (new DeclinedCard)->withData(["reason" => $l->reason])->sendTo($l);
    }

    public function isNotADuplicate($orderInfo)
    {
        $ivr_id = $orderInfo['Customer']['IVR_ID'];

        try{
            Order::where('unique_ivr', $ivr_id)->firstorfail();
            return false;
        }catch(ModelNotFoundException $m) {
            return true;
        }


//        return true;

    }

    public function testJson(Request $request)
    {
        $data = $request->all();
        $orderInfo = $data[0]['Order'];
        if($orderInfo){
            return response()->json($orderInfo, 200);
//            return response()->json('IT IS WELL', 200);
        }
        else{
            return response()->json('Something is wrong', 500);
        }
    }
}
