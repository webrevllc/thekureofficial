<?php

namespace App\Http\Controllers;

use App\ChangeOrder;
use App\Contracts\Invisacloud;
use App\Order;
use App\Product;
use App\Serial;
use App\Subscription;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $orders = Order::with('subscription')->orderBy('created_at', 'desc')->get();

        return view('admin.order.orders', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $order = Order::with('user', 'serials')->find($id);
        $subscription = Subscription::with('change_orders')->where('unique_id', '=', $order->invoice_number)->first();
        $products = Product::find([2,3,4]);
        $serials =  Serial::where('subscription_id', $subscription->id)->get();
//        dd($serials);
//        $change_orders = ChangeOrder::where('subscription_id', $order->subscription->id)->get();

        return view('admin.order.show', compact('order', 'subscription', 'products', 'serials'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function uploadOrder($id, Invisacloud $i)
    {
        $order = Order::find($id);
        $i->uploadOrder($order);
        return redirect()->back()->withInput()->with('success', "Order has been uploaded");
    }

    public function uploadCustomer($id, Invisacloud $i)
    {
        $order = Order::with('user')->find($id);
        $i->makeUser($order->user, $order->auth_transaction, 'thekure');
        return redirect()->back()->withInput()->with('success', "Customer has been uploaded");
    }
}
