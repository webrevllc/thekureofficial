<?php

namespace App\Http\Controllers;

use App\Contracts\UserPaymentContract;
use App\Events\AdminOrder\UserFreeTrialOrder;
use App\Events\WebNew\TrialOrderComplete;
use App\Order;
use App\Product;
use App\Serial;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::with('orders','subscriptions', 'serials')->find($id);

        return view('admin.customers.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::with('serials')->find($id);
        $user->name = $request->username;
        $user->email = $request->email;
        $user->save();

        foreach($user->serials as $s){
            $s->name = $request->username;
            $s->save();
        }

        return redirect()->back()->withInput()->with('success', 'User has been updated');
    }

    public function sendReceipt(Request $request)
    {
        $order = Order::with('user', 'subscription')->find($request->order_id);
        $order->sendReceipt($order);
        return redirect()->back()->withInput()->with('success', 'Receipt has been sent');
    }

    public function resendCards(Request $request)
    {
        foreach($request->serial_id as $serial){
            $s = Serial::find($serial);
            $s->assigned_date = Carbon::now();
            $s->save();
        }
        return redirect()->back()->withInput()->with('success', 'Membership Cards will be shipped out tomorrow :)');
    }

    public function lookupAccount(Request $request)
    {
        try{
            $serial = Serial::where('serial', $request->serial)->firstorfail();
            try{
                $order = Order::with('user')->where('invoice_number', $request->transaction)->firstorfail();
                if($order->id == $serial->order_id){
                    return view('auth.register', compact('order'));
                }
            }catch (ModelNotFoundException $m){
                return redirect()->back()->withInput()->with('error', "That order was not found in our system, please try again.");
            }
            return $serial->name;
        }catch(ModelNotFoundException $m){
            return redirect()->back()->withInput()->with('error', "That serial number was not found in our system, please try again.");
        }
    }

    public function createAccount(Request $request)
    {
        $user = User::find($request->uid);
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        auth()->loginUsingId($user->id);
        return redirect('/mykure');
    }

    public function updateCC($userID, Request $request, UserPaymentContract $contract )
    {
        $user = User::find($userID);

        $response = $contract->updateUserPayment($user, $request);

        if($response == 'success'){
            return redirect()->back()->withInput()->with('success', "Profile Successfully Updated");
        }else{
            return redirect()->back()->withInput()->with('error', $response);
        }


    }

    public function makeNewOrder($id, Request $request, UserPaymentContract $userPaymentContract, Order $order)
    {
        $theKure = Product::find(1);
        $shipping = Product::find(10);
        $theKure['quantity'] = $request->quantity;
        $shipping['quantity'] = 1;
        $products = [];
        $products[] = $theKure;
        $products[] = $shipping;
        $user = User::find($id);

        if($request->addon){
            foreach($request->addon as $k => $v){
                $prod = Product::where('web_code', $v)->first();
                if($prod->ivr_code == 'PROC'){
                    $prod['quantity'] = 1;
                }else{
                    $prod['quantity'] = $request->quantity;
                }

                $products[] = $prod;
            }
        }

        $total = $order->total($products, $user);

        if($request->orderType == "trial"){
            $invoice_number = $order->newInvoiceNumber();
            event(new UserFreeTrialOrder($products, $user, $invoice_number, $total ));
            return redirect()->back()->withInput()->with('success', "Trial Order Created In The Amount of: " .$total);
        }else{
            $response = $userPaymentContract->createExistingUserOrder($user, $total);
            return redirect()->back()->withInput()->with('success', "Trial Order Created In The Amount of: " .$total);
        }

    }

}
