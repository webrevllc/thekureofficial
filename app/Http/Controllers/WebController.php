<?php

namespace App\Http\Controllers;

use App\Http\States;
use App\Order;
use App\Referral;
use App\Subscription;
use App\User;
use App\Product;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Contracts\WebPaymentContract;
use App\Http\Controllers\Traits\MakesOrderObjects;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;


class WebController extends Controller
{

    use MakesOrderObjects;
    /**
     * @var WebPaymentContract
     */
    private $payment;
    /**
     * @var User
     */
    private $user;
    /**
     * @var Product
     */
    private $product;
    /**
     * @var Order
     */
    private $order;

    public function __construct(WebPaymentContract $payment, User $user, Product $product, Order $order)
    {
        $this->payment = $payment;
        $this->user = $user;
        $this->product = $product;
        $this->order = $order;
    }

    public function makePayment(Request $request)
    {
        $request->flash();
        $payNow = $this->payNow($request);
        $productsOrdered = makeWebProductsArray($request, $payNow);
        $quantity = $productsOrdered[0]['quantity'];
        $user = $this->isUserUnique($request);
        $this->payment->makeWebPayment($request, $productsOrdered, $user, $payNow, $quantity);
    }

    public function showThanks(Request $request)
    {
        $transaction = $request->query('t');
        $order = Order::with('user')->whereAuth_transaction($transaction)->first();
        $sub = Subscription::whereUnique_id($order->invoice_number)->first();
        return view('checkout.thankyou', compact('order', 'sub'));
    }

    /**
     * @param Request $request
     * @return User
     */
    public function isUserUnique(Request $request)
    {

        if (!$this->user->exists($request->email)) {
            $user = $this->createWebUser($request);
            return $user;
        } else {
            $user = User::whereEmail($request->email)->first();
            return $user;
        }

    }

    private function createWebUser($request)
    {
        $user = new User;
        $user->name = $request->first_name. ' '. $request->last_name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password = bcrypt($request->password);
        if($request->state == "MO" || $request->state == "FL"){
            $user->taxable = true;
        }
        $user->save();
        return $user;
    }

    private function payNow($request)
    {
        if($request->PAYNOW == "true"){
            return 'payNow';
        }
        return 'authorizeOnly';
    }

    public function showThanks2(Request $request)
    {
        $data = [];
        $transaction = $request->query('t');
        $order = Order::with('user')->whereInvoice_number($transaction)->first();
        $sub = Subscription::whereUnique_id($transaction)->first();

        if (Cookie::has('AFFID') ) {
            $data = [
                "amount" => $order->status == 'trial' ? $sub->amount : $order->amount,
                "vsid" => Cookie::get('vsid')
            ];
        }
        return view('checkout2.thankyou', compact('order', 'sub', 'data'));
    }

    public function getState(Request $request)
    {
        $states = states::lookup($request->country);
        foreach($states as $k =>$v){
            echo "<option value='$k'>$v</option>";
        }

    }

    public function countdown()
    {
        $countdown = DB::table('countdown')->where('id', 1)->first();

        return $countdown->amount;
    }

    public function orderstatus( Request $request ) {

        try{
            $order = Order::with('user', 'subscription')->where('invoice_number', $request->trans)->firstOrFail();
            return view('mykure.status', compact('order'));
        }catch(ModelNotFoundException $m){
            return redirect()->back()->withInput()->with('error', "Sorry, transaction number not found");
        }

    }

    public function referral()
    {
        return view('mel4.referral');
    }

    public function referralPost(Request $request)
    {
        $source = $request->trans;
        try{
            Order::where('invoice_number', $source)->firstOrFail();
            if(Referral::where('referring_transaction_number', $source)->exists()){
                return back()->withInput()->with('error', 'That referral has already been used.');
            }else{
                $r = new Referral();
                $r->referring_transaction_number = $source;
                $r->save();
                return view('checkout2.purchase2', compact('source'));
            }

        }catch(ModelNotFoundException $m){
            return back()->withInput()->with('error', 'That order was not found');
        }
    }
}
