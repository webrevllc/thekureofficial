<?php

namespace App\Http\Controllers;


use App\Events\Question;
use App\Events\SupportIssue;
use Illuminate\Http\Request;
use App\Http\Requests;
class InquiryController extends Controller
{


    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        switch($request->department){
            case "questions":
                event(new Question($request));
                break;
            case "support":
                event(new SupportIssue($request));
                break;
        }
        return redirect()->back()->withInput()->with('success', "Thank you! Your message has been sent!");
    }



}
