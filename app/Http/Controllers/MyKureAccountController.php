<?php

namespace App\Http\Controllers;

use App\Contracts\Invisacloud;
use App\Contracts\Payment;
use App\Events\Refunds\TrialCancelled;
use App\Order;
use App\Serial;
use App\Subscription;
use App\User;
use Auth;
use Carbon\Carbon;
use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;
use DOMPDF_Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Input;

class MyKureAccountController extends Controller
{
    public function index(Payment $payment)
    {
        $billing = $payment->getCustomerPaymentProfile(auth()->user()->id);
        $shipping = $payment->getCustomerShippingProfile(auth()->user()->id);
        $user = User::with("orders", "subscriptions")->find(auth()->user()->id);
        $token = $payment->getHostedPage($user->auth_profile);
        $profile = $payment->getProfile($user->auth_profile);
        $defaultBilling = $payment->getCustomerPaymentProfile($user->id)->xml->paymentProfile;
//        $defaultShipping = $payment->getCustomerShippingProfile($user->id)->xml;
//        dd($defaultShipping);
        return view("mykure.home2", compact('user', 'billing', 'shipping', 'token', "profile", "defaultBilling"));
    }


    public function account(Payment $payment)
    {
        $billing = $payment->getCustomerPaymentProfile(auth()->user()->id);
        $shipping = $payment->getCustomerShippingProfile(auth()->user()->id);
        $user = User::with("orders", "subscriptions")->find(auth()->user()->id);
        return view("mykure.home2", compact('user', 'billing', 'shipping'));
    }


    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            "password" => "required|confirmed"
        ]);

        $password = bcrypt($request->password);

        $user = auth()->user();
        $user->password = $password;
        $user->save();

        flash()->success("Success", "Password updated successfully");

        return redirect()->back();
    }


    public function support(Invisacloud $invisacloud)
    {
        $users = User::with("subscriptions", "orders")->find(auth()->user()->id);
        $serials = [];
        foreach($users->orders as $order) {
            array_push($serials, Serial::where('order_id', $order->id)->get());
        }
        foreach($serials as $serial) {
            foreach($serial as $serialNum) {
                $serialNum->used = $invisacloud->isSerialUsed($serialNum->serial);
                $serialNum->friendlyName = $invisacloud->getFriendlyName($serialNum->serial);
            }
        }
//        $x = User::whereBetween("created_at", [Carbon::createFromDate(2015, 10, 20), Carbon::now()])->get();
        return view("mykure.help", compact('users', 'serials'));
    }

    public function getKurePassword(Request $request, Invisacloud $invisacloud) {
        $serial = $request->serial;

        return $invisacloud->getKurePassword($serial);
    }

    public function generatePDFReceipt()
    {

        try{
            $order = Order::with('user', 'address', 'subscription')->find(Input::query('order'));
            if(\Auth::user()->id != $order->user->id){
                die('That is not your order to view!');
            }
            $barcode = new BarcodeGenerator();
            $barcode->setText($order->invoice_number);
            $barcode->setType(BarcodeGenerator::Code128);
            $barcode->setScale(1);
            $barcode->setThickness(15);
            $code = $barcode->generate();
            $pdf = App::make('dompdf.wrapper');
            if ($order->status == 'failed') {

            } else {
                if ( ! is_null($order->user)) {
                    if(strtolower($order->user->name) == "hh"){

                    }else{
                        if ($order->amount == "Free Trial") {
                            return $pdf->loadView('pdf.lettertrial', compact('order', 'code'))->stream($order->id . '.pdf');
                        } else {
                            try {
                                if (count($order->address) > 0) {
                                    return $pdf->loadView('pdf.letter', compact('order', 'code'))->stream($order->id . '.pdf');
                                } else {
                                    echo "Something went wrong!";
                                }
                            } catch (DOMPDF_Exception $m) {

                            }
                        }
                    }
                }
            }
        }catch (ModelNotFoundException $m){

        }
        return "ERROR";
    }

    public function cancelSubscription()
    {
        try{
            $subscription = Subscription::findorfail(Input::query('sub'));
            $order = Order::findorfail($subscription->order_id);
        }catch(ModelNotFoundException $e){
            return redirect()->back()->withInput()->with('error', "That is not an active subscription");
        }

        if($subscription->status != 'active'){
            return redirect()->back()->withInput()->with('error', "That is not an active subscription");
        }

        if($order->status == "trial"){
            $subscription->status = 'cancelled';
            $subscription->next_process_date = Carbon::yesterday();
            $subscription->save();
            event(new TrialCancelled($subscription));
            return redirect()->back()->withInput()->with('success', "This subscription has been cancelled successfully, you will receive an email confirmation shortly.");
        }else{
            $subscription->status = 'cancelled';
            $subscription->save();
            event(new TrialCancelled($subscription));
            return redirect()->back()->withInput()->with('success', "Your subscription has been cancelled successfully, you will receive an email confirmation shortly.");

        }
    }

    public function updateBilling(Request $request)
    {
        $user = auth()->user();
        $user->default_billing = $request->billing;
        $user->save();
        return redirect()->back();

    }
}
