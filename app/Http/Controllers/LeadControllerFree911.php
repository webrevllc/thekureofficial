<?php

namespace App\Http\Controllers;

use App\Events\WebNew\CreateEmerSale;
use App\Events\WebNew\EasyPayOrderComplete;
use App\Events\WebNew\EmergencySaleMade;
use App\Events\WebNew\NewLifetimePaidOrderComplete;
use App\Events\WebNew\NewPaidOrderComplete;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use Input;
use App\User;
use App\Lead;
use App\Order;
use App\Product;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Contracts\WebPaymentContract;
use App\Events\WebNew\TrialOrderComplete;

class LeadControllerFree911 extends Controller
{
    /**
     * @var Order
     */
    private $order;
    /**
     * @var WebPaymentContract
     */
    private $webPaymentContract;

    /**
     * @param Order $order
     * @param WebPaymentContract $webPaymentContract
     */
    public function __construct(Order $order, WebPaymentContract $webPaymentContract)
    {
        $this->order = $order;
        $this->webPaymentContract = $webPaymentContract;
    }

    public function getHome(Response $response)
    {
        if(Input::has('AFFID')){
            Cookie::queue('AFFID', Input::get('AFFID'), 60);
            Cookie::queue('vsid', Input::get('vsid'), 60);
            return view('mel5.home');
        }else{
            return view('mel5.home');
        }

//        return view('mel5.home');
    }

    public function purchase2(Request $request)
    {
        if (Cookie::has('AFFID') ) {
            $source = Cookie::get('AFFID');
        }else{
            $source = "mel";
        }

        return view('checkout2.purchase2', compact('source'));

    }

    public function makeURL($step, $lead)
    {
        $lead->step = $step;
        $lead->url = "https://www.thekure.com/purchase/$step/?id=$lead->lead_id";
        $lead->save();
    }

    public function leadStepLookup($step)
    {
        $lead_id = Input::query('id');
        $lead = Lead::where('lead_id', $lead_id)->first();
//        if ( ! $lead_id || $step > $lead->step +1) {
        if ( 1 == 0) {
            return view('checkout2.purchase2');
        } else {
            $this->makeURL($step, $lead);

            // FREE TRIAL WITH 911
            switch ($step) {
                case '2':
                    return view('checkout2.quantity', compact('lead'));
                    break;
                case '3':
//                    dd('hi');
                    return view('checkout2.credit_card_notrial', compact('lead'));
//                    return view('checkout2.credit_card', compact('lead'));
                    break;
                case '4':
                    return view('checkout2.upsell1', compact('lead')); //HWMON
                    break;
                case '5':
//                    return view('checkout2.upsell2', compact('lead')); //PCOS
                    return view('checkout2.upsell911', compact('lead'));
                    break;
                case '6':
                    return view('checkout2.upsell911', compact('lead')); //OFFERS 911
                    break;
                case '7':
                    return view('checkout2.upsell4', compact('lead')); //OFFERS ONE-PAY
                    break;
                case '8':
                    return view('checkout2.cart', compact('lead'));
                    break;
                case '8a':
                    return view('checkout2.cart-paynow', compact('lead'));
                    break;

                case '5year':
                    return view('checkout2.5year', compact('lead'));
                    break;
                case 'one-year':
                    return view('checkout2.1year', compact('lead'));
                    break;
//                case 'avs':
//                    return view('checkout2.avs', compact('lead'));
//                    break;
//                case 'lifetime':
//                    return view('checkout2.lifetime', compact('lead'));
//                    break;
                case 'paid-rush':
                    return view('checkout2.paidrush', compact('lead'));
                    break;
                case 'free-rush':
                    return view('checkout2.upsell3', compact('lead'));
                    break;
                case 'qtyup':
                    return view('checkout2.quantity-upsell', compact('lead'));
                    break;
                default:
                    return view('checkout2.purchase2', compact('lead'));
                    break;
            }
        }
    }
    public function highestStepMaker($lead)
    {
        $highest_step = $lead->highest_step; // 2
        $current_step = $lead->step;  // 2

        $steps = [
            "1", "2", "3", "qtyup", "4", "5", "7", "paid-rush", "free-rush", "8", "8a"
        ];

        $current_key = array_search($current_step, $steps); // 1
        $highest_key = array_search($highest_step, $steps); // 1

//        echo $current_key . ' ' . $highest_key;
//        die();
        if($current_key > $highest_key){
            $theKey = array_search($current_step, $steps);
        }else{
            $theKey = array_search($highest_step, $steps);
        }

        return $steps[$theKey];

    }
//
//        }

    public function create(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'add1' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'phone' => 'required',
        ]);
//        $l = Lead::where('email', $request->email)->first();
//        if (count($l)){
//            return redirect("/purchase/$l->step?id=".$l->lead_id);
//        }else{
            $lead = new Lead;
            $lead->first_name = $request->first_name;
            $lead->last_name = $request->last_name;
            $lead->email = $request->email;
            $lead->add1 = $request->add1;
            $lead->add2 = $request->add2;
            $lead->city = $request->city;
            $lead->state = $request->state;
            $lead->country = $request->country;
            $lead->zip = $request->zip;
            $lead->source = $request->src;
            $lead->offer = env('offer');
            $lead->highest_Step = '2';
            if($request->state == "MO"){
                $lead->taxable = true;
            }
            $lead->password = bcrypt($request->password);
            $lead->phone = $request->phone;
            $lead->step = '2';

            $now = Carbon::now();
            $lead->lead_id = md5($now);
            $lead->save();
            return redirect("/purchase/$lead->step?id=".$lead->lead_id);
//        }
    }


    public function setQuantity(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->quantity = $request->quantity;
        $lead->highest_step = $this->highestStepMaker($lead);
        $lead->step = '3';
        $lead->save();

        $this->calculate($lead->lead_id);

        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

    public function leadLookup($lead_id)
    {
        return  Lead::where('lead_id', $lead_id)->first();
    }

    public function addHWMON(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->HWMON = $lead->quantity;
        $lead->highest_step = $this->highestStepMaker($lead);
        $lead->step = '5';
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

    public function addPCOS(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->PCOS = $lead->quantity;
        $lead->highest_step = $this->highestStepMaker($lead);
        $lead->step = '6'; //emergency 911
//        $lead->step = '5year';
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

    public function addPROC(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->PROC = true;
        $lead->highest_step = $this->highestStepMaker($lead);
        $lead->step = '8a';
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

    public function avs(Request $request, WebPaymentContract $payment)
    {
        $this->validate($request, [
            'card' => 'required',
            'month' => 'required',
            'year' => 'required',
        ]);

        $lead = $this->leadLookup($request->lead_id);

        $lead->add1 = $request->add1;
        $lead->add2 = $request->add2;
        $lead->city = $request->city;
        $lead->state = $request->state;
//        $lead->country = $request->country;
        $lead->zip = $request->zip;
        $lead->offer = env('offer');
        $lead->save();
        $ccresponse = $payment->leadAuth($request, $lead);
        if($ccresponse == 'success'){
            return redirect("/purchase/$lead->step?id=".$lead->lead_id);
        }else{
            if($ccresponse->response_reason_code == "27"){
                $lead->step = 'avs';
                $lead->save();
                return redirect("/purchase/$lead->step?id=".$lead->lead_id);
            }
            return redirect()->back()->withInput()->with('error', $ccresponse->response_reason_text);
        }
    }

    public function noToUpsell($product)
    {
        $lead = $this->leadLookup(Input::query('id'));
        $lead->highest_step = $this->highestStepMaker($lead);

        switch($product){
            case "HWMON":
                $lead->step = 5;
                $lead->HWMON = false;
                break;
            case "PCOS":
                $lead->step = '6'; //emergency 911
//                $lead->step = '5year';
                $lead->PCOS = false;
                break;
        }
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

    public function removeProduct($product)
    {
        $lead = $this->leadLookup(Input::query('id'));
        $lead->$product = false;
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }



    public function changeQty(Request $request)
    {
//        $this->cantExceedKureQty($request);
        $pcos = $request->PCOS;
        $hwmon = $request->HWMON;
        $lifetime = $request->lifetime;
        $emer = $request->EMER;

        if($request->PCOS > $request->KURE){
            $pcos = $request->KURE;
        }
        if($request->HWMON > $request->KURE){
            $hwmon = $request->KURE;
        }
        if($request->lifetime > $request->KURE){
            $lifetime = $request->KURE;
        }
        if($request->EMER > $request->KURE){
            $emer = $request->KURE;
        }

        $lead = $this->leadLookup($request->id);
        $lead->quantity = $request->KURE;
        $lead->PCOS = $pcos;
        $lead->HWMON = $hwmon;
        $lead->lifetime = $lifetime;
        $lead->EMER = $emer;
        $lead->save();
        $this->calculate($lead->lead_id);
    }

    private function calculate($lead_id)
    {
        $lead = $this->leadLookup($lead_id);

//        $kureTotal = $lead->quantity * 19.95;
        $kureTotal = $lead->five_year ? $lead->quantity * 79.80 : $lead->quantity * 49.95;
        $kureToday = $lead->quantity * 24.95;
        $pcosTotal = $lead->PCOS * 8.95;
        $hwmonTotal = $lead->HWMON * 9.90;
        $hwmonToday = $lead->HWMON * 4.95;
        $lifetime = $lead->lifetime * 39.95;
        $emerTotal = $lead->EMER * 19.95;
        $shipTotal = 5.95;
        $procTotal = $lead->pay_now ? 0 : 1.99;

        $sumitup = array();
        $sumitup[] = $kureTotal;
        if($lead->PCOS){
            $sumitup[] = $pcosTotal;
        }
        if($lead->HWMON){
            $sumitup[] = $hwmonTotal;
        }
        if($lead->PROC){
            $sumitup[] = $procTotal;
        }
        if($lead->lifetime){
            $sumitup[] = $lifetime;
        }
        $sumitup[] = $emerTotal;

        $todayArray = [];
        $nextMonth = [];
        $todayArray[] = $kureToday;
        $nextMonth[] = $kureToday;
        if($lead->HWMON){
            $todayArray[] = $hwmonToday;
            $nextMonth[] = $hwmonToday;
        }
        if($lead->PROC){
            $todayArray[] = $lead->pay_now ? 0 : $procTotal;
        }

        $todayArray[] = $emerTotal;

        $lead->today_total = array_sum($todayArray) + $shipTotal;
        $lead->emerTotal = $emerTotal;
        $lead->subtotal = array_sum($sumitup);
        $lead->total = array_sum($sumitup) + $shipTotal;
        $lead->next_month = array_sum($nextMonth);
        $lead->save();
    }

    public function switchFree(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->pay_now = true;
        $lead->step = '8a';
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }
    public function keepFree(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->pay_now = false;
        $lead->step = 'paid-rush';
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

    public function saleMade()
    {
        $lead = $this->leadLookup(Input::query('id'));
//        $lead->pay_now = true;
        $lead->highest_step = $this->highestStepMaker($lead);
        $lead->has_converted = true;
        $lead->save();
        $products = $this->createOrderProducts($lead);

        if($lead->pay_now){
           $tr = $this->webPaymentContract->pay_now_sale($lead, $products);
            if(is_array($tr)){
                $user = $this->convertLeadToUser($lead);
                $invoice_number = $this->order->newInvoiceNumber();
                    event(new NewPaidOrderComplete( $lead, $products, $user, $invoice_number, $tr['transactionID'] ));
                    die($invoice_number);
            }
            else{
                die($tr);
            }
        }else{

            $this->easyPaymentSale($lead, $products);
//            $this->free_trial_sale($lead, $products);
        }
    }

    public function easyPaymentSale($lead, $products)
    {
        $user = $this->convertLeadToUser($lead);
        $tr = $this->webPaymentContract->pay_now_sale($lead, $products);
        if($lead->EMER > 0){
            $emer = $this->removeAllButEmer($products);
            if(is_array($tr)){
                $invoice_number = $this->order->newInvoiceNumber();
                event(new CreateEmerSale($lead, $emer, $user, $invoice_number, $tr['transactionID']));
            }
        }
        $allButEmer = $this->removeEmer($products);
        $invoice_number = $this->order->newInvoiceNumber();
        event(new EasyPayOrderComplete( $lead, $allButEmer, $user, $invoice_number, $tr['transactionID'] ));
        die($invoice_number);
    }
    public function free_trial_sale($lead, $products)
    {
        $user = $this->convertLeadToUser($lead);
        if($lead->EMER > 0){
            $emer = $this->removeAllButEmer($products);
            $tr = $this->webPaymentContract->singleTimePayment($user, $lead->emerTotal);
            if(is_array($tr)){
                $invoice_number = $this->order->newInvoiceNumber();
                event(new CreateEmerSale($lead, $emer, $user, $invoice_number, $tr['transactionID']));
            }
        }
        $allButEmer = $this->removeEmer($products);
        $invoice_number = $this->order->newInvoiceNumber();
        event(new TrialOrderComplete( $lead, $allButEmer, $user, $invoice_number ));
        die($invoice_number);
    }

    public function convertLeadToUser($lead)
    {
        try{
            $user = User::where('email', $lead->email)->firstOrFail();
            return $user;
        }catch(ModelNotFoundException $m){
            $user = new User;
            $user->name = $lead->first_name . ' '. $lead->last_name;
            $user->email = $lead->email;
            $user->password = $lead->password;
            $user->auth_profile = $lead->auth_profile;
            $user->default_billing = $lead->default_billing;
            $user->default_shipping = $lead->default_shipping;
            $user->taxable = $lead->taxable;
            $user->phone = $lead->phone;
            $user->offer = $lead->offer;
            $user->save();
            return $user;
        }

    }

    public function createOrderProducts($lead)
    {
        $kure = Product::where('web_code', 'KURE')->first();
        $hwmon = Product::where('web_code', 'HWMON')->first();
        $cspc = Product::where('web_code', 'CSPC')->first();
        $pcos = Product::where('web_code', 'PCOS')->first();
        $ship = Product::where('web_code', 'SHIP')->first();
        $emer = Product::where('web_code', 'EMER')->first();
        $lifetime = Product::where('web_code', 'KLIFE')->first();

        $products = array();
        if($lead->quantity > 0){
            $kure['quantity'] = $lead->quantity;
            $cspc['quantity'] = $lead->quantity;
            $ship['quantity'] = '1';
            $products[] = $kure;
            $products[] = $cspc;
            $products[] = $ship;
        }

        if($lead->HWMON > 0){
            $hwmon['quantity'] = (string) $lead->HWMON;
            $products[] = $hwmon;
        }
        if($lead->PCOS > 0){
            $pcos['quantity'] = (string) $lead->PCOS;
            $products[] = $pcos;
        }
        if($lead->lifetime > 0){
            $lifetime['quantity'] = (string) $lead->lifetime;
            $products[] = $lifetime;
        }
        if($lead->EMER > 0){
            $emer['quantity'] = (string) $lead->EMER;
            $products[] = $emer;
        }

        return $products;

    }

    public function validateCard(Request $request, WebPaymentContract $payment)
    {
        $this->validate($request, [
            'card' => 'required',
            'month' => 'required',
            'year' => 'required',
        ]);


        $lead = $this->leadLookup($request->lead_id);
        $lead->highest_step = $this->highestStepMaker($lead);
        $ccresponse = $payment->leadCIM($request, $lead);
        $lead->save();


        if($ccresponse == 'success'){
            if($lead->quantity == "1"){
//                return redirect("/purchase/qtyup?id=".$lead->lead_id);
                return redirect("/purchase/$lead->step?id=".$lead->lead_id);
            }else{
                return redirect("/purchase/$lead->step?id=".$lead->lead_id);
            }
        }elseif(is_array($ccresponse)){
            if(array_key_exists('lead', $ccresponse)){
                $lead2 = $ccresponse['lead'];
                if($lead->quantity == "1"){
//                    return redirect("/purchase/qtyup?id=".$lead->lead_id);
                    return redirect("/purchase/$lead->step?id=".$lead->lead_id);
                }else{
                    return redirect("/purchase/4?id=".$lead2->lead_id);
                }
            }else{
                return redirect()->back()->withInput()->with('error', 'Sorry, something went wrong. Please try again.');
            }

        }else{
            return redirect()->back()->withInput()->with('error', $ccresponse);
        }
    }

    public function showPVM()
    {
        $pete = Lead::whereBetween('updated_at', [Carbon::today(), Carbon::now()])->where('source', 'pete')->count();
        $mel = Lead::whereBetween('updated_at', [Carbon::today(), Carbon::now()])->where('source', 'mel')->count();
        return view('admin.pvm', compact('pete', 'mel'));
    }

    public function getCustom(Request $request)
    {
        $request->flash();
        $fromDate = $request->fromDate . ' 00:00:00';
        if($request->toDate . ' 00:00:00' == Carbon::today()){
            $toDate = Carbon::today()->now();
        }else{
            $toDate = $request->toDate . ' 23:59:59';
        }

        $pete = Lead::whereBetween('updated_at', [$fromDate, $toDate])->where('source', 'pete')->count();
        $mel = Lead::where('source', 'mel')->whereBetween('updated_at', [$fromDate, $toDate])->count();
        return view('admin.pvm', compact('pete', 'mel'));
    }

    public function lifetime(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->lifetime = $lead->quantity;
        $lead->step = '8a';
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

    public function paidNoLifetime(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->step = 'paid-rush';
        $lead->lifetime = false;
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

    public function paidrushyes(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->PROC = true;
        $lead->highest_step = $this->highestStepMaker($lead);
        $lead->step = '8a';
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }
    public function paidrushno(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->highest_step = $this->highestStepMaker($lead);
        $lead->step = '8a';
        $lead->lifetime = 0;
        $lead->PROC = 0;
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

    public function freerushno(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->highest_step = $this->highestStepMaker($lead);
        $lead->step = '8a';
        $lead->lifetime = 0;
        $lead->PROC = 0;
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

    public function yes911(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->highest_step = $this->highestStepMaker($lead);
//        $lead->step = 'paid-rush';
        $lead->step = '7';
        $lead->EMER = $lead->quantity;
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

    public function no911(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->highest_step = $this->highestStepMaker($lead);
//        $lead->step = 'paid-rush';
        $lead->step = '7';
        $lead->EMER = 0;
        $lead->PROC = 0;
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

    public function qtyUpsell(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->quantity = $request->quantity;
        $lead->highest_step = $this->highestStepMaker($lead);
        $lead->step = '4';
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

    public function removeAllButEmer($products)
    {
        foreach($products as $p =>$v) {
            if ($v->id != 15) {
                unset($products[$p]);
            }
        }
        return $products;
    }

    public function removeEmer($products)
    {
        foreach($products as $p =>$v) {
            if ($v->id == 15) {
                unset($products[$p]);
            }
        }
        return $products;
    }

    public function yes5year(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->five_year = true;
        $lead->step = '7';
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }
    public function no5year(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->five_year = false;
        $lead->step = '7';
//        $lead->step = 'one-year';
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

}
