<?php

namespace App\Http\Controllers;

use App\Contracts\Payment;
use App\Events\OrderHasShipped;
use App\Order;
use App\Subscription;
use Carbon\Carbon;
use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;
use DOMPDF_Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ShippingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $orders = ordersThatNeedShipping();
        $trials = trialOrdersThatNeedShipping();
        return view('admin.shipments', compact('orders', 'trials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function getTrialReceipt($orderID, Payment $payment)
    {
        $order = Order::with('user')->find($orderID);
        $subscription = Subscription::where('unique_id', $order->invoice_number)->first();
        $address = $payment->getShippingAddress($order->user->auth_profile, $order->user->default_shipping);
        return view('pdf.trialinvoice', compact('order', 'address', 'subscription'));
    }

    public function getReceipt($orderID, Payment $payment)
    {
            $order = Order::with('user')->find($orderID);
            if($order->source == 'legacy'){
                $mysql2 = DB::connection('mysql2');
                $o = $mysql2->table('Transactions')->where('invoice_number', '=', $order->invoice_number)->first();
                $customerID = $o->customer_id;
                $customer =$mysql2->table('Customers')->where('id', '=',$customerID)->first();
                $address = new \stdClass();
                $address->address = $customer->address_1 . ' ' . $customer->address_2;
                $address->city = $customer->city;
                $address->state = $customer->state;
                $address->zip = $customer->zip;
            }else{
                $address = $payment->getShippingAddress($order->user->auth_profile, $order->user->default_shipping);
            }
            return view('pdf.invoice', compact('order', 'address'));
    }

    public function markAsShipped($invoice_number)
    {
        $orderUser = Order::with('user')->where('invoice_number', $invoice_number)->first();
        $orderUser->shipped = true;
        $orderUser->shipped_date = Carbon::now();
        $orderUser->shipped_by = \Auth::user()->name;
        $orderUser->save();
        event(new OrderHasShipped($orderUser));
        return redirect()->back();
    }

    public function newShipped()
    {
        try{
            $orderUser = Order::with('user')->where('invoice_number', Input::query('id'))->firstorfail();

            $orderUser->shipped = true;
            $orderUser->shipped_date = Carbon::now();
            $orderUser->shipped_by = Input::has('by') ? Input::query('by'): 'Shipping Computer';
            $orderUser->save();
            event(new OrderHasShipped($orderUser));
            return \Response::json($orderUser->user->name, 200);
        }catch(ModelNotFoundException $m){
            return \Response::json("ORDER NOT FOUND!", 400);
        }
    }

    public function newPrinted()
    {
        try{
            $order_id = Order::with('user')->where('invoice_number', Input::query('id'))->firstorfail();
            $order_id->printed = true;
            $order_id->printed_date = Carbon::now();
            $order_id->save();
//            event(new OrderHasShipped($order_id));
            return \Response::json($order_id->user->name, 200);
        }catch(ModelNotFoundException $m){
            return \Response::json("ORDER NOT FOUND!", 400);
        }
    }

    public function makePDF()
    {
        try{
            $order = Order::with('user', 'address', 'subscription')->find(Input::query('order'));

            $barcode = new BarcodeGenerator();
            $barcode->setText($order->invoice_number);
            $barcode->setType(BarcodeGenerator::Code128);
            $barcode->setScale(1);
            $barcode->setThickness(15);
            $code = $barcode->generate();
            $pdf = App::make('dompdf.wrapper');
            if ($order->status == 'failed') {

            } else {
                if ( ! is_null($order->user)) {
                    if(strtolower($order->user->name) == "hh"){

                    }else{
                        if ($order->amount == "Free Trial") {
                            return $pdf->loadView('pdf.lettertrial', compact('order', 'code'))->stream($order->id . '.pdf');
                        } else {
                            try {
                                if (count($order->address) > 0) {
                                    return $pdf->loadView('pdf.letter', compact('order', 'code'))->stream($order->id . '.pdf');
                                } else {
                                    echo "Something went wrong!";
                                }
                            } catch (DOMPDF_Exception $m) {

                            }
                        }
                    }
                }
            }
        }catch (ModelNotFoundException $m){

        }
        return "ERROR";
    }

}
