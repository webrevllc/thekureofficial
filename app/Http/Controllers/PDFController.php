<?php

namespace App\Http\Controllers;

use App\Order;
use App\Serial;
use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;
use DOMPDF_Exception;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Input;
use Maatwebsite\Excel\Excel;

class PDFController extends Controller
{
    public function makePDF()
    {
        $id = Input::query('id');
        $order = Order::with('user', 'address', 'subscription')->where('invoice_number', $id)->first();
//            $orders = Order::with('user', 'address', 'subscription')->whereBetween('created_at',[Carbon::createFromDate(2015,10,10), Carbon::today()])->get();
        $barcode = new BarcodeGenerator();
        $barcode->setText($order->invoice_number);
        $barcode->setType(BarcodeGenerator::Code128);
        $barcode->setScale(1);
        $barcode->setThickness(15);
        $code = $barcode->generate();
        $pdf = App::make('dompdf.wrapper');
        if ($order->status == 'failed') {

        } else {
            if ( ! is_null($order->user)) {
                if (strtolower($order->user->name) == "shane rosenthal"
                    || strtolower($order->user->name) == "peter spezza"
                    || strtolower($order->user->name) == "ugo spezza"
                    || strtolower($order->user->name) == "eddie talyor"
                    || strtolower($order->user->name) == "jeff rosenthal"
                    || strtolower($order->user->name) == "mel arthur"
                ) {

                } else {
                    if ($order->amount == "Free Trial") {
                        return $pdf->loadView('pdf.lettertrial', compact('order', 'code'))->stream('pdf.pdf');
                    } else {
                        try {
                            if (count($order->address) > 0) {
                                return $pdf->loadView('pdf.letter', compact('order', 'code'))->stream('pdf.pdf');
                            } else {
                            }
                        } catch (DOMPDF_Exception $m) {

                        }
                    }
                }
            }
        }
        return "Failed Order";
    }

    public function createCSV(Excel $excel)
    {
        $id = Input::query('id');
        $excel->create($id, function ($e) use($id){
        $e->sheet($id, function ($sheet) use ($id){
            $order = Order::where('invoice_number', $id)->first();
            $serials = Serial::where('order_id', $order->id)->get();
            $sheet->fromArray($serials);
        });
    })->download('csv');
    }
}
