<?php

namespace App\Http\Controllers;

use App\Contracts\LegacyPayment;
use App\Contracts\Payment;
use App\Contracts\SubscriptionContract;
use App\Events\Refunds\FullRefund;
use App\Events\Refunds\PartialRefund;
use App\Events\Refunds\TrialCancelled;
use App\Events\Subscription\FailedCancelled;
use App\Events\Subscription\SubscriptionModifiedEvent;
use App\Failedsub;
use App\Http\Controllers\Traits\IssuesRefunds;
use App\Order;
use App\Product;
use App\Subscription;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SubscriptionsController extends Controller
{
    use IssuesRefunds;
    /**
     * @var Payment
     */
    private $payment;
    /**
     * @var LegacyPayment
     */
    private $legacyPayment;

    /**
     * @param Payment $payment
     * @param LegacyPayment $legacyPayment
     */
    public function __construct(Payment $payment, LegacyPayment $legacyPayment)
    {
        $this->payment = $payment;
        $this->legacyPayment = $legacyPayment;
    }

    public function partialRefund($subscriptionId, Request $request)
    {
        $subscription = Subscription::find($subscriptionId);
//        $order = DB::table('orders')->orderBy('created_at', 'desc')->first();
        $order = Order::find($request->order_id);
        $user = User::find($order->user_id);
        $transId = $order->auth_transaction;
        $reason = $request->reason;

        //create new products array
        $products = $this->makeNewProductArray($request->product);

        //calculate new subscription total
        $newOrderTotal =  $this->calculateNewTotal($products);

        //calculate refund amount
        $refundAmount = $order->amount - $newOrderTotal;
        //try issuing refund to authorize
        if($order->source == 'legacy'){
//            $this->legacyPayment->refund($refundAmount, $order);
//            event(new PartialRefund($order, $refundAmount, $reason, $products, $newOrderTotal, $id));
            return redirect()->back()->withInput()->with('success', "CALL SHANE@407-312-9455");
//            return redirect()->back()->withInput()->with('success', "$$refundAmount has been refunded to the customer. They will receive it in 3-5 business days");
        }else{
            if($order->type == 'aim'){
                $response = $this->payment->refund($refundAmount, $order);
            }else{
                $response = $this->payment->CIMRefund($user, $refundAmount, $transId);
            }
            if(is_array($response)){
                $approvedRefundTransactionID = $response['id'];
                event(new PartialRefund($order, $refundAmount, $reason, $products, $newOrderTotal, $subscriptionId, $user, $approvedRefundTransactionID));
                event(new SubscriptionModifiedEvent($subscription, $request));
                return redirect()->back()->withInput()->with('success', '$'.number_format($refundAmount, 2).' has been refunded to the customer. They will receive it in 3-5 business days. ' .$approvedRefundTransactionID);
            }else{
                //BUILD A QUEUE FOR UNSETTLED TRANSACTIONS THAT NEED REFUNDING
                return redirect()->back()->withInput()->with('error', $response);
            }
        }
    }


    public function modifyTrial($id, Request $request)
    {
        $subscription = Subscription::with('user')->find($id);
//        $order = Order::where('invoice_number', '=', $subscription->unique_id)->first();
//        $reason = $request->reason;

        //create new products array
        $products = $this->makeNewProductArray($request->product);

        //calculate new subscription total
        $newOrderTotal =  $this->calculateNewTotal($products);

        $subscription->products = json_encode($products);
        $subscription->amount = $newOrderTotal;
        $subscription->save();
        event(new SubscriptionModifiedEvent($subscription, $request));
        return redirect()->back()->withInput()->with('success', 'Subscription updated. New Subscription Total: $' .$newOrderTotal);

    }

    public function fullRefund($id, Request $request)
    {
        $subscription = Subscription::find($id);
        $order = Order::with('user')->orderBy('created_at', 'desc')->find($request->order_id);
//        $refundAmount = $order->amount;
        $refundAmount = $order->amount;

        //try issuing refund to authorize
        if($order->source != 'legacy'){
            if($order->type == 'aim'){
                $response = $this->payment->refund($refundAmount, $order);
            }else{
                $response = $this->payment->CIMRefund($order->user, $refundAmount, $order->auth_transaction);
            }
            if(is_array($response)) {
                $approvedRefundTransactionID = $response['id'];
                event(new FullRefund($order, $refundAmount, $id, $approvedRefundTransactionID));
                return redirect()->back()->withInput()->with('success', "$$refundAmount has been refunded to the customer. They will receive it in 3-5 business days");
            }else{
//                if($this->payment->refund($refundAmount, $order)){
//                    event(new FullRefund($order, $refundAmount, $id, ''));
//                    return redirect()->back()->withInput()->with('success', "$$refundAmount has been refunded to the customer. They will receive it in 3-5 business days");
//                }
                return redirect()->back()->withInput()->with('error', $response);
            }
        }else{
            if($this->legacyPayment->refund($refundAmount - shipping($order), $order)){
                event(new FullRefund($order, $refundAmount, $id, '1'));
                return redirect()->back()->withInput()->with('success', "$$refundAmount has been refunded to the customer. They will receive it in 3-5 business days");
            }
            return redirect()->back()->withInput()->with('error', "An Error Has Occured");
           }

    }

    public function addOn($subscriptionId, Request $request)
    {
//        $subscription = Subscription::where('unique_id', '=', $order->invoice_number)->first();
        $order = Order::with('user')->find($request->order_id);
        $subscription = Subscription::find($subscriptionId);
        $prod = array();
        if($request->HWMON > 0)
        {
            $hw = Product::find(2);
            $hw['quantity'] = $request->HWMON;
            $prod[] = $hw;
        }
        if($request->PCOS > 0)
        {
            $hw = Product::find(4);
            $hw['quantity'] = $request->PCOS;
            $prod[] = $hw;
        }
        if($request->CSPC > 0)
        {
            $hw = Product::find(3);
            $hw['quantity'] = $request->CSPC;
            $prod[] = $hw;
        }
        return view('admin.order.addon', compact('prod', 'order', 'subscription'));
    }

    public function cancel(Request $request)
    {
        $request->flash();
        $order = Order::whereInvoice_number($request->trans)->first();

        try{
            $subscription = Subscription::whereUnique_id($request->trans)->firstorfail();
        }catch(ModelNotFoundException $e){
            return redirect()->back()->withInput()->with('error', "That is not an active subscription");
        }

        if($subscription->status != 'active'){
            return redirect()->back()->withInput()->with('error', "That is not an active subscription");
        }
        $subscription->status = 'cancelled';
        $subscription->save();
        return redirect()->back()->withInput()->with('success', "Your subscription has been cancelled successfully, you will receive an email confirmation shortly.");
    }

    public function cancelTrial($id, Request $request)
    {
        $request->flash();
        try{
            $subscription = Subscription::findorfail($id);
        }catch(ModelNotFoundException $e){
            return redirect()->back()->withInput()->with('error', "That is not an active subscription");
        }

        if($subscription->status != 'active'){
            return redirect()->back()->withInput()->with('error', "That is not an active subscription");
        }
        $subscription->status = 'cancelled';
        $subscription->save();
        event(new TrialCancelled($subscription));
        return redirect()->back()->withInput()->with('success', "Your subscription has been cancelled successfully, you will receive an email confirmation shortly.");
    }

    public function allFailed(  ) {
        $fs = Failedsub::where('processed', false)->with('user', 'subscription')->get();
        return view('admin.failed.all', compact('fs'));
    }

    public function payNow( $subID, SubscriptionContract $subscriptionContract ) {
        $subscription = Subscription::with('user')->find($subID);
        $subscriptionContract->chargeSubscription($subscription);
    }

    public function chargeFailed( $subID, SubscriptionContract $subscriptionContract ) {
        $fs = Failedsub::find($subID);
        $user = User::find($fs->user_id);

        $response = $subscriptionContract->chargeFailedSubscription($fs, $user);
        if($response == 'success'){
            $fs->processed = true;
            $fs->processed_by = \Auth::user()->name;
            $fs->save();
            return redirect()->back()->withInput()->with('success', "Profile Successfully Updated");
        }else{
            return redirect()->back()->withInput()->with('error', $response);
        }

    }

    public function cancelFailed($id)
    {
        try{
            $subscription = Subscription::findorfail($id);
            $failed = Failedsub::where('subscription_id', $id)->firstorfail();
            event(new FailedCancelled($subscription, $failed));
            return redirect()->back()->withInput()->with('success', "That subscription has been cancelled successfully, the user will receive an email confirmation shortly.");
        }catch(ModelNotFoundException $e){
            return redirect()->back()->withInput()->with('error', "That is not an active subscription");
        }

//        $subscription->status = 'cancelled';
//        $subscription->save();
//
//        $failed->processed = true;
//        $failed->cancelled = true;

    }

    public function removeFromFailedList($id)
    {
        $failed = Failedsub::where('subscription_id', $id)->firstorfail();
        $failed->processed = true;
        $failed->save();
        return redirect()->back()->withInput()->with('success', "Removed");
    }
}
