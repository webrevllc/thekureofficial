<?php

namespace App\Http\Controllers;

use App\Address;
use App\AirDate;
use App\Contracts\Payment;
use App\IVRLead;
use App\Lead;
use App\Order;
use App\Schedule;
use App\Subscription;
use App\Total;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use Storage;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller
{
    public function dashboard()
    {
        $schedules = Schedule::all();

        if(Input::get('schedule')){
            $airdates = AirDate::whereSchedule_id(Input::query('schedule'))->get();
        }else{
            $airdates = AirDate::whereSchedule_id(39)->get();
        }
        $totals = Total::whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()])->get();
        $dates = $totals->lists('date')->toJson();
        $allTotals = $totals->lists('total')->toJson();
        $dailyWebTotals = $totals->lists('web_total')->toJson();
        $dailyWebTrials = $totals->lists('web_trial')->toJson();
        $dailyWebPaid = $totals->lists('web_paid')->toJson();
        $dailyIVRTotal = $totals->lists('ivr_total')->toJson();
        $dailyIVRPaid = $totals->lists('ivr_paid')->toJson();
        $dailyIVRTrial = $totals->lists('ivr_trial')->toJson();

        // begin renewal tracking
        $renewals = [];
        $renewalDates = [];
        $amount = [];

        // get data for next 7 days
        for ($i = 0; $i < 35; $i++){
            $subscriptions = Subscription::where('status', 'active')->where('next_process_date', '=', Carbon::now()->addDay($i)->format('Y-m-d'))->get();
            $renewals[] = $subscriptions->sum('amount');
            $renewalDates[] = Carbon::now()->addDay($i)->format('Y-m-d');
            foreach ( $subscriptions as $subscription ) {
                $amount[] = $subscription->amount;
            }
        }



        $renewals = json_encode($renewals);
        $renewalDates = json_encode($renewalDates);
        $amount = json_encode($amount);

        // end renewal tracking


        return view('admin.home',
            compact('dates',
                'allTotals',
                'dailyWebTotals',
                'dailyIVRTotal',
                'dailyWebTrials',
                'dailyWebPaid',
                'dailyIVRPaid',
                'dailyIVRTrial',
                'schedules',
                'airdates',
                'renewals',
                'renewalDates',
                'amount'));
    }

    public function calcTrials($orders)
    {

        $t = array();
        foreach($orders as $order){
            if($order->amount == "Free Trial" || $order->amount == ".01"){
                $t[] = $order->subscription->amount;
            }
            $t[] = $order->amount;
        }
        return number_format(array_sum($t), 2);
    }

    public function lookup(Request $request)
    {
        $name = $request->pname;
        $users = User::with('orders')->where('name', 'LIKE', '%'.$name.'%')->get();
        if($users){
            return view("searchresults", compact('users', 'name'));
        }else{
            return redirect()->back()->withInput()->with('error', "No one found by that name");
        }
    }
    public function lookupOrder(Request $request)
    {
        $orderNumber = $request->ordernumber;
        $orders = Order::with('user')->where('invoice_number', 'LIKE', '%'.$orderNumber.'%')->get();

//        $user = User::with('orders')->where('name', 'LIKE', '%'.ordernumber.'%')->get();
        if($orders){
            return view("searchresults-order", compact('orders', 'orderNumber'));
        }else{
            return redirect()->back()->withInput()->with('error', "No one found by that name");
        }
    }

    public function failed()
    {
        $corders = customerNoOrder();
        $forders = failedOrders();
        return view('admin.order.failed', compact('corders', 'forders'));
    }


    public function financial()
    {
        $customDateDollars = '';
        return view('admin.financial', compact('customDateDollars'));
    }

    public function makeCustomDate(Request $request)
    {
        $request->flash();
        $fromDate = $request->fromDate . ' 00:00:00';
        if($request->toDate . ' 00:00:00' == Carbon::today()){
            $toDate = Carbon::today()->now();
        }else{
            $toDate = $request->toDate . ' 23:59:59';
        }
        $customDateDollars = getCustomDateDollars($fromDate, $toDate);
        $getCustomDateRefunds = getCustomDateRefunds($fromDate, $toDate);

        return view('admin.financial', compact('customDateDollars', 'getCustomDateRefunds'));
    }

    public function missingAddress(Payment $payment)
    {
        $x = DB::table('orders')->where('id', DB::raw("(select max(`id`) from orders)"))->first();
        $array = array();
        for ($i = 336;$i<=$x->id;$i++) {
            $order = Order::with('user', 'address')->find($i);
            if ($order != null) {
                if($order->user->name == "SHANE ROSENTHAL" || $order->user->name == "Shane Rosenthal" || $order->user->name == "Peter Spezza"){
                }else{
                    if($order->status != 'failed'){
                        if ($order->address == null) {
                            if($this->addAddress($order))
                                $array[] = $order;
                        }
                    }
                }
            }
        }
        return view('admin.ma', compact('array'));
    }

    public function addAddress($order)
    {
        $payment = App::make(Payment::class);
        $cimAdd = $payment->getShippingAddress($order->user->auth_profile, $order->user->default_shipping);
        if(!$cimAdd){
            return $order;
        }else{
            $address = new Address([
                'order_id' => $order->id,
                'name' => $cimAdd->firstName . ' ' . $cimAdd->lastName,
                'address' => $cimAdd->address,
                'city' => $cimAdd->city,
                'state' => $cimAdd->state,
                'zip' => $cimAdd->zip,
            ]);
            $order->address()->save($address);
            return false;
        }
    }

    public function uploadSchedule(Request $request)
    {
        $file = $request->file('schedule');
        $extension = $file->getClientOriginalExtension();
        Storage::disk('local')->put($file->getClientOriginalName(),  File::get($file));
        $entry = new Schedule();
        $entry->mime = $file->getClientMimeType();
        $entry->original_filename = $file->getClientOriginalName();
        $entry->filename = $request->nameoffile.'.'.$extension;
        $entry->save();

        $entry = Schedule::where('filename', $entry->filename)->firstOrFail();
        Excel::load('/storage/app/'.$entry->original_filename, function($reader) use ($entry) {
            $results = $reader->get();
//            dd($results);
            foreach($results as $r){
                if($r['market'] == ''){}else {
                    $date = new AirDate();
                    $date->market = $r['market'];
                    $date->station = $r['station'];
                    if(isset($r['airdate'])){
                        $date->airdate = $r['airdate'];
                    }elseif(isset($r['localairdate'])){
                        $date->airdate = $r['localairdate'];
                        $date->airdateday = $r['air_day'];
                    }else{
                        $date->airdate = $r['air_date'];
                        $date->airdateday = $r['air_day'];
                    }
                    $date->air_time = date('H:i', strtotime($r['local_time']));
                    if(isset($r['spend'])) {
                        $date->spend = preg_replace("[^0-9]", "", $r['spend']);
                    }else{
                        $date->spend =  $r['cnet'];
                    }
                    $entry->airdates()->save($date);
                }
           }
        });
        return redirect()->back();
    }

    public function showLeads()
    {
        $leads = DB::table('leads')
            ->orderBy('created_at', 'desc')
            ->get();
        return view('admin.leads', compact('leads'));
    }

    public function showOrderPage($id)
    {
        $user = User::find($id);
        return view('admin.customers.order', compact('user'));
    }

    public function showIVRLeads()
    {
        $leads = IVRLead::all();
        return view('admin.failed.ivr', compact('leads'));
    }

    public function deleteIVRLead($id)
    {
        $lead = IVRLead::find($id);
        $lead->delete();
        return redirect()->back();
    }
}
