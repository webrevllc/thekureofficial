<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CustomersManagementController extends Controller
{
    public function index()
    {
        $customers = User::whereAdmin(false)->get();
        return view('admin.customers.index', compact('customers'));
    }

    public function show($id)
    {
        $user = User::with('orders')->find($id);
        try{
            $address = $user->orders[0]->address;
        }catch (\ErrorException $e){
            $address = false;
        }

        if($user){
            return view('admin.customers.show', compact('user', 'address'));
        }
        return redirect()->back();
    }


}
