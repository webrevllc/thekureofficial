<?php

namespace App\Http\Controllers\Traits;

use App\Product;

trait IssuesRefunds{

    public function makeNewProductArray($products)
    {
        $editedSubscription = array();
        foreach($products as $productID => $quantity){
            if($quantity > 0) {
                $p = Product::where('id', '=', $productID)->first();
                $p->quantity = $quantity;
                $editedSubscription[] = $p;
            }
        }
        return $editedSubscription;
    }

    public function calculateNewTotal($products)
    {
        $price = array();
        foreach($products as $p){
            $x = $p->price * $p->quantity;
            $price[] = $x;
        }
        $productTotal = array_sum($price)/100;

        return number_format($productTotal, 2);
    }
}