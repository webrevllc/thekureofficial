<?php
namespace App\Http\Controllers\Traits;

use App\Product;

trait MakesOrderObjects{


    //WEB FUNCTIONS
    public function makeUserInfo($request)
    {
        $userInfo = new \stdClass();
        $userInfo->name = $request->first_name . ' ' . $request->last_name;
        $userInfo->email = $request->email;
        $userInfo->phone = $request->phone;
        $userInfo->password = bcrypt($request->password);
        return $userInfo;
    }

    public function makeBillingInfo($request)
    {
        $billingInfo = new \stdClass();
        $billingInfo->first_name = $request->first_name;
        $billingInfo->last_name = $request->last_name;
        $billingInfo->address = $request->add1 . ' ' . $request->add2;
        $billingInfo->city = $request->city;
        $billingInfo->state = $request->state;
        $billingInfo->zip = $request->zip;
        $billingInfo->country = $request->country;
        $billingInfo->phone = $request->phone;
        $billingInfo->email = $request->email;

        return $billingInfo;
    }

    public function makeShippingInfo($request)
    {
        $shippingInfo = new \stdClass();
        $shippingInfo->ship_to_first_name = $request->first_name;
        $shippingInfo->ship_to_last_name = $request->last_name;
        $shippingInfo->ship_to_address = $request->add1 . ' ' . $request->add2;
        $shippingInfo->ship_to_city = $request->city;
        $shippingInfo->ship_to_state = $request->state;
        $shippingInfo->ship_to_zip = $request->zip;
        $shippingInfo->country = $request->country;
        $shippingInfo->phone = $request->phone;
        return $shippingInfo;
    }

    public function makePaymentInfo($request)
    {
        $paymentInfo = new \stdClass();
        $paymentInfo->card = $request->card;
        $paymentInfo->expiration = $request->month . $request->year;
        $paymentInfo->cvc = $request->cvc;

        return $paymentInfo;
    }

    public function makeCustomer($request)
    {
        $customer = (object)array();
        $customer->first_name = $request->name;
        $customer->address = $request->street;
        $customer->city = $request->city;
        $customer->state = $request->state;
        $customer->zip = $request->zip;
        $customer->email = $request->email;
        $customer->ship_to_first_name = $request->name;
        $customer->ship_to_address = $request->street;
        $customer->ship_to_city = $request->city;
        $customer->ship_to_state = $request->state;
        $customer->ship_to_zip = $request->zip;
        return $customer;
    }


    //IVR FUNCTIONS
    public function makeIVRBillingInfo($orderInfo)
    {
        $customer = $orderInfo->Customer;
        if($customer->Email == ''){
            $email = 'thekure'.$customer->First_Name.$customer->Last_Name.'@gmail.com';
        }else{
            $email = $customer->Email;
        }
        $billingInfo = new \stdClass();
        $billingInfo->first_name = $customer->First_Name;
        $billingInfo->last_name = $customer->Last_Name;
        $billingInfo->email = $email;
        $billingInfo->address = $customer->Address_1 . ' ' . $customer->Address_2;
        $billingInfo->city = $customer->City;
        $billingInfo->state = $customer->State;
        $billingInfo->zip = $customer->Zip;
        $billingInfo->country = $customer->Country;
        $billingInfo->phone = $customer->Phone;
        return $billingInfo;
    }

    public function makeIVRShippingInfo($orderInfo)
    {
        if (isset($orderInfo->Shipping)) {
            $shippingCustomer = $orderInfo->Shipping;
        }else{
            $shippingCustomer = $orderInfo->Customer;
        }
        $shippingInfo = new \stdClass();
        $shippingInfo->ship_to_first_name = $shippingCustomer->First_Name;
        $shippingInfo->ship_to_last_name = $shippingCustomer->Last_Name;
        $shippingInfo->ship_to_address = $shippingCustomer->Address_1 . ' ' . $shippingCustomer->Address_2;
        $shippingInfo->ship_to_city = $shippingCustomer->City;
        $shippingInfo->ship_to_state = $shippingCustomer->State;
        $shippingInfo->ship_to_zip = $shippingCustomer->Zip;
        $shippingInfo->country = $shippingCustomer->Country;
        return $shippingInfo;
    }

    public function makeIVRPaymentInfo($orderInfo)
    {
        $payment = $orderInfo->Billing;
        $paymentInfo = new \stdClass();
        $paymentInfo->card = $payment->Card;
        $paymentInfo->expiration =  $payment->Expiration;
        $paymentInfo->cvc =  $payment->CVC;
        return $paymentInfo;
    }

    function makeFullProducts($sessionProducts)
    {
        $products = array();
        $pArray = (array) get_object_vars($sessionProducts);

        //Combine quantities of additional Kure units!!!
        if(array_key_exists('KUREA', $pArray)){
            $q1 = $pArray['KURE'];
            $q2 = $pArray['KUREA'];
            $kq = $q1 + $q2;
            unset($pArray['KUREA']);
            $pArray['KURE'] = $kq;
        }elseif(array_key_exists('KUREA30', $pArray)){
            $q1 = $pArray['KURE30'];
            $q2 = $pArray['KUREA30'];
            $kq = $q1 + $q2;
            unset($pArray['KUREA30']);
            $pArray['KURE30'] = $kq;
        }

        foreach($pArray as $k => $v){

            if($k == "KURE30" || $k == "KUREA" || $k == "KURE30" || $k == "KUREA30"){
                $k = "KURE";
            }elseif($k == "PCOS30"){
                $k = "PCOS";
            }elseif($k == "HWMONK30"){
                $k = "HWMONK";
            }elseif($k == "PROC30"){
                $k = "PROC";
            }
            if($k !== "EMER"){
                $p = Product::whereIvr_code($k)->first();
                $p->quantity = $v;
                $products[] = $p;
            }


            //Now that we have added all of the products for each KURE add AV
//            if($k == "KURE"){
//                $k = "AV";
//                $p = Product::whereIvr_code($k)->first();
//                $p->quantity = $v;
//                $products[] = $p;
//            }
        }
        return $products;
    }

    public function getRecurringProducts($eventProducts)
    {
        $products = array();
        foreach($eventProducts as $p){
            if($p->recurring){
                $products[] = $p;
            }
        }
        return $products;
    }

    public function getSubscriptionTotal($products, $qty)
    {
        $amount = array();
        foreach($products as $p){
            $amount[] = $p->price;
        }
        return (array_sum($amount) * $qty)/ 100;
    }
}