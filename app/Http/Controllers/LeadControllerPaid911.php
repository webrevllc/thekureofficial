<?php

namespace App\Http\Controllers;

use App\Events\WebNew\EmergencySaleMade;
use App\Events\WebNew\NewLifetimePaidOrderComplete;
use App\Events\WebNew\NewPaidOrderComplete;
use Input;
use App\User;
use App\Lead;
use App\Order;
use App\Product;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Contracts\WebPaymentContract;
use App\Events\WebNew\TrialOrderComplete;

class LeadControllerPaid911 extends Controller
{
    /**
     * @var Order
     */
    private $order;

    /**
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }
    public function purchase2(Request $request)
    {
        $r = $request->server('HTTP_REFERER');

        if (strpos($r,'/v1') !== false) {
            $source = "pete";
        }else{
            $source = "mel";
        }
        return view('checkout2.purchase2', compact('source'));

    }

    public function makeURL($step, $lead)
    {
        $lead->step = $step;
        $lead->url = "https://www.thekure.com/purchase/$step/?id=$lead->lead_id";
        $lead->save();
    }

    public function leadStepLookup($step)
    {
        $lead_id = Input::query('id');
        $lead = Lead::where('lead_id', $lead_id)->first();
//        if ( ! $lead_id || $step > $lead->step +1) {
        if ( 1 == 0) {
            return view('checkout2.purchase2');
        } else {
            $this->makeURL($step, $lead);
            switch ($step) {
                case '2':
                    return view('checkout2.quantity', compact('lead'));
                    break;
                case '3':
                    return view('checkout2.credit_card_notrial', compact('lead'));
//                    return view('checkout2.credit_card', compact('lead'));
                    break;
                case '4':
                    return view('checkout2.upsell1', compact('lead'));
                    break;
                case '5':
                    return view('checkout2.upsell2', compact('lead'));
                    break;
                case '6':
                    return view('checkout2.upsell911', compact('lead')); //OFFERS RUSH SHIPPING
//                    return view('checkout2.upsell4', compact('lead')); //OFFERS ONE-PAY
                    break;
                case '7':
                    return view('checkout2.upsell3', compact('lead'));
                    break;
                case '8':
                    return view('checkout2.cart', compact('lead'));
                    break;
                case '8a':
                    return view('checkout2.cart-paynow', compact('lead'));
                    break;
                case 'avs':
                    return view('checkout2.avs', compact('lead'));
                    break;
                case 'lifetime':
                    return view('checkout2.lifetime', compact('lead'));
                    break;
                case 'paid-rush':
                    return view('checkout2.paidrush', compact('lead'));
                    break;
                case 'free-rush':
                    return view('checkout2.upsell3', compact('lead'));
                    break;
                default:
                    return view('checkout2.purchase2', compact('lead'));
                    break;
            }
        }
    }
//
//        }

    public function create(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'add1' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'phone' => 'required',
        ]);
//        $l = Lead::where('email', $request->email)->first();
//        if (count($l)){
//            return redirect("/purchase/$l->step?id=".$l->lead_id);
//        }else{
            $lead = new Lead;
            $lead->first_name = $request->first_name;
            $lead->last_name = $request->last_name;
            $lead->email = $request->email;
            $lead->add1 = $request->add1;
            $lead->add2 = $request->add2;
            $lead->city = $request->city;
            $lead->state = $request->state;
            $lead->country = $request->country;
            $lead->zip = $request->zip;
            $lead->source = $request->src;
            if($request->state == "MO" || $request->state == "FL"){
                $lead->taxable = true;
            }
            $lead->password = bcrypt($request->password);
            $lead->phone = $request->phone;
            $lead->step = '2';

            $now = Carbon::now();
            $lead->lead_id = md5($now);
            $lead->save();
            return redirect("/purchase/$lead->step?id=".$lead->lead_id);
//        }
    }


    public function setQuantity(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->quantity = $request->quantity;
        $lead->step = '3';
        $lead->save();

        $this->calculate($lead->lead_id);

        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

    public function leadLookup($lead_id)
    {
        return  Lead::where('lead_id', $lead_id)->first();
    }

    public function addHWMON(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->HWMON = $lead->quantity;
        $lead->step = '5';
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

    public function addPCOS(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->PCOS = $lead->quantity;
        $lead->step = '6';
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

    public function addPROC(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->PROC = true;
        $lead->step = '8a';
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

    public function avs(Request $request, WebPaymentContract $payment)
    {
        $this->validate($request, [
            'card' => 'required',
            'month' => 'required',
            'year' => 'required',
        ]);

        $lead = $this->leadLookup($request->lead_id);

        $lead->add1 = $request->add1;
        $lead->add2 = $request->add2;
        $lead->city = $request->city;
        $lead->state = $request->state;
//        $lead->country = $request->country;
        $lead->zip = $request->zip;
        $lead->save();
        $ccresponse = $payment->leadAuth($request, $lead);
        if($ccresponse == 'success'){
            return redirect("/purchase/$lead->step?id=".$lead->lead_id);
        }else{
            if($ccresponse->response_reason_code == "27"){
                $lead->step = 'avs';
                $lead->save();
                return redirect("/purchase/$lead->step?id=".$lead->lead_id);
            }
            return redirect()->back()->withInput()->with('error', $ccresponse->response_reason_text);
        }
    }

    public function noToUpsell($product)
    {
        $lead = $this->leadLookup(Input::query('id'));
        $lead->step++;
        $lead->$product = false;
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

    public function removeProduct($product)
    {
        $lead = $this->leadLookup(Input::query('id'));
        $lead->$product = false;
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

    public function changeQty(Request $request)
    {
//        $this->cantExceedKureQty($request);
        $pcos = $request->PCOS;
        $hwmon = $request->HWMON;
        $lifetime = $request->lifetime;
        $emer = $request->EMER;

        if($request->PCOS > $request->KURE){
            $pcos = $request->KURE;
        }
        if($request->HWMON > $request->KURE){
            $hwmon = $request->KURE;
        }
        if($request->lifetime > $request->KURE){
            $lifetime = $request->KURE;
        }
        if($request->EMER > $request->KURE){
            $emer = $request->KURE;
        }

        $lead = $this->leadLookup($request->id);
        $lead->quantity = $request->KURE;
        $lead->PCOS = $pcos;
        $lead->HWMON = $hwmon;
        $lead->lifetime = $lifetime;
        $lead->EMER = $emer;
        $lead->save();
        $this->calculate($lead->lead_id);
    }

    private function calculate($lead_id)
    {
        $lead = $this->leadLookup($lead_id);

        $kureTotal = $lead->quantity * 49.95;
        $pcosTotal = $lead->PCOS * 8.95;
        $hwmonTotal = $lead->HWMON * 4.95;
        $lifetime = $lead->lifetime * 39.95;
        $emerTotal = $lead->EMER * 19.95;
        $shipTotal = 5.95;
        $procTotal = 1.99;

        $sumitup = array();
        $sumitup[] = $kureTotal;
        if($lead->PCOS){
            $sumitup[] = $pcosTotal;
        }
        if($lead->HWMON){
            $sumitup[] = $hwmonTotal;
        }
        if($lead->PROC){
            $sumitup[] = $procTotal;
        }
        if($lead->lifetime){
            $sumitup[] = $lifetime;
        }

        if($lead->EMER){
            $sumitup[] = $emerTotal;
        }
        $lead->subtotal = array_sum($sumitup);
        $lead->total = array_sum($sumitup) + $shipTotal;
        $lead->save();
    }

    public function switchFree(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->pay_now = true;
        $lead->step = '8a';
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }
    public function keepFree(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->pay_now = false;
        $lead->step = '8';
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

    public function saleMade(WebPaymentContract $web)
    {
        $lead = $this->leadLookup(Input::query('id'));
//        $lead->pay_now = true;
        $lead->save();
        $products = $this->createOrderProducts($lead);
//        if($lead->pay_now){
           $tr = $web->pay_now_sale($lead, $products);
            if(is_array($tr)){
                $user = $this->convertLeadToUser($lead);
                $invoice_number = $this->order->newInvoiceNumber();
                if($lead->EMER > 0) {
                    event(new EmergencySaleMade( $lead, $products, $user, $invoice_number, $tr['transactionID'] ));
//                    event(new NewLifetimePaidOrderComplete( $lead, $products, $user, $invoice_number, $tr['transactionID'] ));
                    $lead->has_converted = true;
                    $lead->save();
                    die($invoice_number);
                }else{
                    event(new NewPaidOrderComplete( $lead, $products, $user, $invoice_number, $tr['transactionID'] ));
                    $lead->has_converted = true;
                    $lead->save();
                    die($invoice_number);
                }
            }
            else{
                die($tr);
            }
//        }else{
//            $this->free_trial_sale($lead, $products);
//        }
    }

    public function free_trial_sale($lead, $products)
    {
        $user = $this->convertLeadToUser($lead);
        $invoice_number = $this->order->newInvoiceNumber();
        event(new TrialOrderComplete( $lead, $products, $user, $invoice_number ));
        $lead->has_converted = true;
        $lead->save();
        die($invoice_number);
//        $order = Order::where('lead_id', $lead->lead_id)->first();

    }

    public function convertLeadToUser($lead)
    {
        $user = new User;
        $user->name = $lead->first_name . ' '. $lead->last_name;
        $user->email = $lead->email;
        $user->password = $lead->password;
        $user->auth_profile = $lead->auth_profile;
        $user->default_billing = $lead->default_billing;
        $user->default_shipping = $lead->default_shipping;
        $user->taxable = $lead->taxable;
        $user->phone = $lead->phone;
        $user->save();

        return $user;
    }

    public function createOrderProducts($lead)
    {
        $kure = Product::where('web_code', 'KURE')->first();
        $hwmon = Product::where('web_code', 'HWMON')->first();
        $cspc = Product::where('web_code', 'CSPC')->first();
        $pcos = Product::where('web_code', 'PCOS')->first();
        $ship = Product::where('web_code', 'SHIP')->first();
        $emer = Product::where('web_code', 'EMER')->first();
        $lifetime = Product::where('web_code', 'KLIFE')->first();

        $products = array();
        if($lead->quantity > 0){
            $kure['quantity'] = $lead->quantity;
            $cspc['quantity'] = $lead->quantity;
            $ship['quantity'] = '1';
            $products[] = $kure;
            $products[] = $cspc;
            $products[] = $ship;
        }

        if($lead->HWMON > 0){
            $hwmon['quantity'] = (string) $lead->HWMON;
            $products[] = $hwmon;
        }
        if($lead->PCOS > 0){
            $pcos['quantity'] = (string) $lead->PCOS;
            $products[] = $pcos;
        }
        if($lead->lifetime > 0){
            $lifetime['quantity'] = (string) $lead->lifetime;
            $products[] = $lifetime;
        }
        if($lead->EMER > 0){
            $emer['quantity'] = (string) $lead->EMER;
            $products[] = $emer;
        }

        return $products;

    }

    public function validateCard(Request $request, WebPaymentContract $payment)
    {
        $this->validate($request, [
            'card' => 'required',
            'month' => 'required',
            'year' => 'required',
        ]);

        $lead = $this->leadLookup($request->lead_id);

        $ccresponse = $payment->leadCIM($request, $lead);

        if($ccresponse == 'success'){
            return redirect("/purchase/$lead->step?id=".$lead->lead_id);
        }elseif(is_array($ccresponse)){
            if(array_key_exists('lead', $ccresponse)){
//                dd($ccresponse);
//                Lead::destroy($lead->id);
                $lead2 = $ccresponse['lead'];
                return redirect("/purchase/4?id=".$lead2->lead_id);
            }else{
                return $ccresponse;
            }

        }else{
            return redirect()->back()->withInput()->with('error', $ccresponse);
        }
    }

    public function showPVM()
    {
        $pete = Lead::whereBetween('updated_at', [Carbon::today(), Carbon::now()])->where('source', 'pete')->count();
        $mel = Lead::whereBetween('updated_at', [Carbon::today(), Carbon::now()])->where('source', 'mel')->count();
        return view('admin.pvm', compact('pete', 'mel'));
    }

    public function getCustom(Request $request)
    {
        $request->flash();
        $fromDate = $request->fromDate . ' 00:00:00';
        if($request->toDate . ' 00:00:00' == Carbon::today()){
            $toDate = Carbon::today()->now();
        }else{
            $toDate = $request->toDate . ' 23:59:59';
        }

        $pete = Lead::whereBetween('updated_at', [$fromDate, $toDate])->where('source', 'pete')->count();
        $mel = Lead::where('source', 'mel')->whereBetween('updated_at', [$fromDate, $toDate])->count();
        return view('admin.pvm', compact('pete', 'mel'));
    }

    public function lifetime(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->lifetime = $lead->quantity;
        $lead->step = '8a';
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

    public function paidNoLifetime(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->step = 'paid-rush';
        $lead->lifetime = false;
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

    public function paidrushyes(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->PROC = true;
        $lead->step = '8a';
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }
    public function paidrushno(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->step = '8a';
        $lead->lifetime = 0;
        $lead->PROC = 0;
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

    public function freerushno(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->step = '8a';
        $lead->lifetime = 0;
        $lead->PROC = 0;
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

    public function yes911(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->step = '8a';
        $lead->EMER = $lead->quantity;
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

    public function no911(Request $request)
    {
        $lead = $this->leadLookup($request->lead_id);
        $lead->step = '7';
        $lead->EMER = 0;
        $lead->PROC = 0;
        $lead->save();
        $this->calculate($lead->lead_id);
        return redirect("/purchase/$lead->step?id=".$lead->lead_id);
    }

}
