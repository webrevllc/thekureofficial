<?php

namespace App\Http;

class States{
    protected static $us_states = array(
        '' => '--PLEASE SELECT--',
        'AL'=>'ALABAMA',
        'AK' => 'ALASKA',
        'AZ'=>'ARIZONA',
        'AR'=>'ARKANSAS',
        'CA'=>'CALIFORNIA',
        'CO'=>'COLORADO',
        'CT'=>'CONNECTICUT',
        'DE'=>'DELAWARE',
        'DC'=>'DISTRICT OF COLUMBIA',
        'FL'=>'FLORIDA',
        'GA'=>'GEORGIA',
        'HI' => 'HAWAII',
        'ID'=>'IDAHO',
        'IL'=>'ILLINOIS',
        'IN'=>'INDIANA',
        'IA'=>'IOWA',
        'KS'=>'KANSAS',
        'KY'=>'KENTUCKY',
        'LA'=>'LOUISIANA',
        'ME'=>'MAINE',
        'MD'=>'MARYLAND',
        'MA'=>'MASSACHUSETTS',
        'MI'=>'MICHIGAN',
        'MN'=>'MINNESOTA',
        'MS'=>'MISSISSIPPI',
        'MO'=>'MISSOURI',
        'MT'=>'MONTANA',
        'NE'=>'NEBRASKA',
        'NV'=>'NEVADA',
        'NH'=>'NEW HAMPSHIRE',
        'NJ'=>'NEW JERSEY',
        'NM'=>'NEW MEXICO',
        'NY'=>'NEW YORK',
        'NC'=>'NORTH CAROLINA',
        'ND'=>'NORTH DAKOTA',
        'OH'=>'OHIO',
        'OK'=>'OKLAHOMA',
        'OR'=>'OREGON',
        'PA'=>'PENNSYLVANIA',
        'RI'=>'RHODE ISLAND',
        'SC'=>'SOUTH CAROLINA',
        'SD'=>'SOUTH DAKOTA',
        'TN'=>'TENNESSEE',
        'TX'=>'TEXAS',
        'UT'=>'UTAH',
        'VT'=>'VERMONT',
        'VI'=>'VIRGIN ISLANDS',
        'VA'=>'VIRGINIA',
        'WA'=>'WASHINGTON',
        'WV'=>'WEST VIRGINIA',
        'WI'=>'WISCONSIN',
        'WY'=>'WYOMING'
    );

    protected static $canadian_states = array(
        '' => '--PLEASE SELECT--',
        "BC" => "British Columbia",
        "ON" => "Ontario",
        "NL" => "Newfoundland and Labrador",
        "NS" => "Nova Scotia",
        "PE" => "Prince Edward Island",
        "NB" => "New Brunswick",
        "QC" => "Quebec",
        "MB" => "Manitoba",
        "SK" => "Saskatchewan",
        "AB" => "Alberta",
        "NT" => "Northwest Territories",
        "NU" => "Nunavut",
        "YT" => "Yukon Territory"
    );

    public static function all()
    {
        return static::$us_states;

    }

    public static function lookup($country){
        if($country == "USA"){
            return static::$us_states;
        }else{
            return static::$canadian_states;
        }

    }
}