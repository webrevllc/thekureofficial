<?php

namespace App\Listeners\IVR\Success;

use App\Events\IVR\OrderPlacedByIVR;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Snowfire\Beautymail\Beautymail;

class EmailNotificationToBosses
{
    /**
     * @var Beautymail
     */
    private $beautymail;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Beautymail $beautymail)
    {
        //
        $this->beautymail = $beautymail;
    }

    /**
     * Handle the event.
     *
     * @param  OrderPlacedByIVR  $event
     * @return void
     */
    public function handle(OrderPlacedByIVR $event)
    {
        $order = Order::where('auth_transaction', '=', $event->transactionID)->first();
        $this->beautymail->send('emails.ivr.bosses', ['order' => $order, 'user' => $event->user, 'city' => $event->city, 'state' => $event->state], function ($m) {
            $m->cc('pspezza@centuriontech.com');
            $m->cc('shane@computercentersusa.com');
            $m->cc('melarthur4@gmail.com');
            $m->cc('jrosenthal@computercentersusa.com');
            $m->subject('The Kure - IVR Order!');
        });
    }
}
