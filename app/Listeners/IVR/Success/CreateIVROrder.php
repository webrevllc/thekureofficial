<?php

namespace App\Listeners\IVR\Success;

use App\Events\IVR\OrderPlacedByIVR;
use App\Order;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateIVROrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderPlacedByIVR  $event
     * @return void
     */
    public function handle(OrderPlacedByIVR $event)
    {
        $order = new Order;
        $order->amount = $event->total;
        $order->invoice_number = $event->invoiceNumber;
        $order->status = 'paid';
        $order->user_id = $event->user->id;
        $order->last_4 = $event->last4;
        $order->expiration = $event->orderData->Billing->Expiration;
        $order->source = 'ivr';
        $order->products = json_encode($event->products);
        $order->auth_transaction = $event->transactionID;
        $order->type = 'aim';
//        $order->unique_ivr = $event->orderData->Customer->IVR_ID;
        $order->save();
    }
}
