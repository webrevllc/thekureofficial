<?php

namespace App\Listeners\IVR\Success;

use App\Contracts\Payment;
use App\Events\IVR\OrderPlacedByIVR;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\App;

class SetIVRAuthNetProfileIDsOnUser
{

    /**
     * @var Payment
     */
    private $payment;

    /**
     * @param Payment $payment
     */
    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    /**
     * Handle the event.
     *
     * @param  OrderPlacedByIVR  $event
     * @return void
     */
    public function handle(OrderPlacedByIVR $event)
    {
        $response = $this->payment->createAuthIDs($event->transactionID);
        $customerProfileID = $response->xml->customerProfileId;
        $customerBillingID = $response->xml->customerPaymentProfileIdList->numericString;
        $customerShippingID = $response->xml->customerShippingAddressIdList->numericString;

        $user = User::where('email', '=', $event->user->email)->first();
        $user->auth_profile = $customerProfileID;
        $user->default_billing = $customerBillingID;
        $user->default_shipping = $customerShippingID;
        $user->save();
    }
}
