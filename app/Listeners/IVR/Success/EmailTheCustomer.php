<?php

namespace App\Listeners\IVR\Success;

use App\Events\IVR\OrderPlacedByIVR;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Snowfire\Beautymail\Beautymail;

class EmailTheCustomer
{
    /**
     * @var Beautymail
     */
    private $beautymail;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Beautymail $beautymail)
    {
        //
        $this->beautymail = $beautymail;
    }

    /**
     * Handle the event.
     *
     * @param  OrderPlacedByIVR  $event
     * @return void
     */
    public function handle(OrderPlacedByIVR $event)
    {
        $order = Order::where('auth_transaction', '=', $event->transactionID)->first();
        $email = $event->user->email;
        $this->beautymail->send('emails.ivr.customerreceipt', ['order' => $order, 'user' => $event->user], function ($m) use ($email){
            $m->to($email);
            $m->subject('The Kure - Receipt');
        });
    }

}
