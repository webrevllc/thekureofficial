<?php

namespace App\Listeners\IVR\Success;

use App\Events\IVR\OrderPlacedByIVR;
use App\Order;
use App\Subscription;
use App\Total;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateIVRSubscription
{
    /**
     * @var Total
     */
    private $total;


    /**
     * @param Total $total
     */
    public function __construct(Total $total)
    {

        $this->total = $total;
    }
    /**
     * Handle the event.
     *
     * @param  OrderPlacedByIVR  $event
     * @return void
     */
    public function handle(OrderPlacedByIVR $event)
    {
        $order = Order::where('invoice_number', $event->invoiceNumber)->first();
        $user = User::where('email', '=', $event->user->email)->first();
        $subscription = new Subscription;
        $subscription->unique_id = $event->invoiceNumber;
        $subscription->status = 'active';
        $subscription->next_process_date = Carbon::now()->addYear();
        $subscription->user_id = $user->id;
        $subscription->products = json_encode($event->products);
        $subscription->amount = $event->total;
        $order->subscription()->save($subscription);
        $this->total->updateTotals('ivr_paid', $subscription->amount);
    }
}
