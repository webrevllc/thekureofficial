<?php

namespace App\Listeners\IVR\Success;

use App\Contracts\Invisacloud;
use App\Events\IVR\OrderPlacedByIVR;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MakeInvisacloudProduct
{
    /**
     * Create the event listener.
     *
     * @param Invisacloud $invisacloud
     */
    public function __construct(Invisacloud $invisacloud)
    {
        //
        $this->invisacloud = $invisacloud;
    }

    /**
     * Handle the event.
     *
     * @param  OrderPlacedByIVR  $event
     * @return void
     */
    public function handle(OrderPlacedByIVR $event)
    {
        $order = Order::where('auth_transaction', '=', $event->transactionID)->first();
        $this->invisacloud->uploadOrder($order);
    }
}
