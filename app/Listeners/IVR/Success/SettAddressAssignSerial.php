<?php

namespace App\Listeners\IVR\Success;

use App\Address;
use App\Events\IVR\OrderPlacedByIVR;
use App\Order;
use App\Serial;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SettAddressAssignSerial
{

    /**
     * @var Address
     */
    private $address;
    /**
     * @var Serial
     */
    private $serial;

    /**
     * @param Address $address
     * @param Serial $serial
     */
    public function __construct(Address $address, Serial $serial)
    {
        $this->address = $address;
        $this->serial = $serial;
    }

    /**
     * Handle the event.
     *
     * @param  OrderPlacedByIVR  $event
     * @return void
     */
    public function handle(OrderPlacedByIVR $event)
    {
        //Set Address
        $order = Order::where('invoice_number',$event->invoiceNumber)->first();
        $this->address->makeAddress($order->id);

        //Assign Serial Number
        $this->serial->assignSerial($order->id);
    }
}
