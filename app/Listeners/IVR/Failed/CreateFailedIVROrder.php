<?php

namespace App\Listeners\IVR\Failed;

use App\Events\IVR\FailedIVROrder;
use App\Order;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateFailedIVROrder
{

    /**
     * Handle the event.
     *
     * @param  FailedIVROrder  $event
     * @return void
     */
    public function handle(FailedIVROrder $event)
    {
        $user = User::where('email', '=', $event->user->email)->first();
        $order = new Order;
        $order->amount = $event->total;
        $order->invoice_number = $event->invoice_number;
        $order->status = 'failed';
        $order->user_id = $user->id;
        $order->expiration = $event->orderInfo->Billing->Expiration;
        $order->source = 'ivr';
        $order->products = json_encode($event->productsOrdered);
        $order->auth_transaction = $event->reason;
        $order->type = 'aim';
        $order->save();
    }
}
