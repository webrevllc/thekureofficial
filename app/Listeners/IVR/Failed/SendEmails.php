<?php

namespace App\Listeners\IVR\Failed;

use App\Events\IVR\FailedIVROrder;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Snowfire\Beautymail\Beautymail;

class SendEmails
{
    /**
     * @var Beautymail
     */
    private $beautymail;

    /**
     * Create the event listener.
     *
     * @param Beautymail $beautymail
     */
    public function __construct(Beautymail $beautymail)
    {
        //
        $this->beautymail = $beautymail;
    }

    /**
     * Handle the event.
     *
     * @param  FailedIVROrder  $event
     * @return void
     */
    public function handle(FailedIVROrder $event)
    {
        $order = Order::where('invoice_number', '=', $event->invoice_number)->first();
        $this->beautymail->send('emails.ivr.failed', ['order' => $order, 'user' => $event->user], function ($m) {
            $m->to('pspezza@centuriontech.com');
            $m->to('cbennett@computercentersusa.com');
            $m->cc('shane@computercentersusa.com');
            $m->cc('jrosenthal@computercentersusa.com');
            $m->subject('The Kure - FAILED IVR!');
        });
    }
}
