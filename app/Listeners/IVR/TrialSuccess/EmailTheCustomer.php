<?php

namespace App\Listeners\IVR\TrialSuccess;

use App\Events\IVR\TrialOrderPlacedByIVR;
use App\Order;
use App\Subscription;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Snowfire\Beautymail\Beautymail;

class EmailTheCustomer
{
    /**
     * @var Beautymail
     */
    private $beautymail;

    /**
     * Create the event listener.
     *
     * @param Beautymail $beautymail
     */
    public function __construct(Beautymail $beautymail)
    {
        //
        $this->beautymail = $beautymail;
    }

    /**
     * Handle the event.
     *
     * @param  TrialOrderPlacedByIVR  $event
     * @return void
     */
    public function handle(TrialOrderPlacedByIVR $event)
    {
        $order = Order::whereAuth_transaction($event->transactionID)->first();
        $sub = Subscription::where('unique_id', '=', $order->invoice_number)->first();
        $email = $event->user->email;
        $this->beautymail->send('emails.ivr.trialcustomer', ['order' => $order, 'user' => $event->user, 'sub' => $sub], function ($m) use ($email){
            $m->to($email);
            $m->subject('The Kure - Receipt');
        });
    }
}
