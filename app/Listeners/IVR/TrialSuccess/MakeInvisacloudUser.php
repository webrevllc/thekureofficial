<?php

namespace App\Listeners\IVR\TrialSuccess;

use App\Contracts\Invisacloud;
use App\Events\IVR\TrialOrderPlacedByIVR;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MakeInvisacloudUser
{
    /**
     * @var Invisacloud
     */
    private $invisacloud;

    /**
     * Create the event listener.
     *
     * @param Invisacloud $invisacloud
     */
    public function __construct(Invisacloud $invisacloud)
    {
        $this->invisacloud = $invisacloud;
    }

    /**
     * Handle the event.
     *
     * @param  TrialOrderPlacedByIVR  $event
     * @return void
     */
    public function handle(TrialOrderPlacedByIVR $event)
    {
        $this->invisacloud->makeUser($event->user, $event->transactionID, 'ivr');
    }
}
