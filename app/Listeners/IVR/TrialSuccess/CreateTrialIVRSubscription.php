<?php

namespace App\Listeners\IVR\TrialSuccess;

use App\Events\IVR\TrialOrderPlacedByIVR;
use App\Order;
use App\Subscription;
use App\Total;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateTrialIVRSubscription
{
    /**
     * @var Order
     */
    private $order;
    /**
     * @var Total
     */
    private $total;

    /**
     * Create the event listener.
     *
     * @param Order $order
     * @param Total $total
     */
    public function __construct(Order $order, Total $total)
    {
        $this->order = $order;
        $this->total = $total;
    }

    /**
     * Handle the event.
     *
     * @param  TrialOrderPlacedByIVR  $event
     * @return void
     */
    public function handle(TrialOrderPlacedByIVR $event)
    {
        $order = Order::where('invoice_number', $event->invoiceNumber)->first();
        $user = User::where('email', '=', $event->user->email)->first();
        $subscription = new Subscription;
        $subscription->unique_id = $event->invoiceNumber;
        $subscription->status = 'active';
//        $subscription->next_process_date = Carbon::now()->addDays(30);
        $subscription->user_id = $user->id;
        $subscription->products = json_encode($event->products);
        $subscription->amount = $this->order->total($event->products, $event->user);
        $order->subscription()->save($subscription);
        $this->total->updateTotals('ivr_trial', $subscription->amount);
    }
}
