<?php

namespace App\Listeners\IVR\TrialSuccess;

use App\Events\IVR\TrialOrderPlacedByIVR;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateIVROrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TrialOrderPlacedByIVR  $event
     * @return void
     */
    public function handle(TrialOrderPlacedByIVR $event)
    {
        $order = new Order;
        $order->amount = 'Free Trial';
        $order->invoice_number = $event->invoiceNumber;
        $order->status = 'trial';
        $order->user_id = $event->user->id;
        $order->source = 'ivr';
        $order->products = json_encode($event->products);
        $order->auth_transaction = $event->transactionID;
        $order->type = 'aim';
//        $order->unique_ivr = $event->orderData->Customer->IVR_ID;
        $order->save();
    }
}
