<?php

namespace App\Listeners\IVR\TrialSuccess;

use App\Contracts\Invisacloud;
use App\Events\IVR\TrialOrderPlacedByIVR;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MakeInvisacloudProduct
{
    /**
     * @var Invisacloud
     */
    private $invisacloud;

    /**
     * Create the event listener.
     *
     * @param Invisacloud $invisacloud
     */
    public function __construct(Invisacloud $invisacloud)
    {
        //
        $this->invisacloud = $invisacloud;
    }

    /**
     * Handle the event.
     *
     * @param  TrialOrderPlacedByIVR  $event
     * @return void
     */
    public function handle(TrialOrderPlacedByIVR $event)
    {
        $order = Order::whereAuth_transaction($event->transactionID)->first();
        $this->invisacloud->uploadOrder($order);
    }
}
