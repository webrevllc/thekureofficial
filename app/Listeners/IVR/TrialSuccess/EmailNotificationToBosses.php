<?php

namespace App\Listeners\IVR\TrialSuccess;

use App\Events\IVR\TrialOrderPlacedByIVR;
use App\Order;
use App\Subscription;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Snowfire\Beautymail\Beautymail;

class EmailNotificationToBosses
{
    /**
     * @var Beautymail
     */
    private $beautymail;

    /**
     * Create the event listener.
     *
     * @param Beautymail $beautymail
     */
    public function __construct(Beautymail $beautymail)
    {
        $this->beautymail = $beautymail;
    }

    /**
     * Handle the event.
     *
     * @param  TrialOrderPlacedByIVR  $event
     * @return void
     */
    public function handle(TrialOrderPlacedByIVR $event)
    {
        $order = Order::where('auth_transaction', '=', $event->transactionID)->first();
        $sub = Subscription::where('unique_id', '=', $order->invoice_number)->first();
        $this->beautymail->send('emails.ivr.trialbosses',
            ['order' => $order, 'user' => $event->user, 'city' => $event->city, 'state' => $event->state, 'sub' => $sub], function ($m) {
            $m->cc('pspezza@centuriontech.com');
            $m->cc('shane@computercentersusa.com');
            $m->cc('melarthur4@gmail.com');
            $m->cc('jrosenthal@computercentersusa.com');
            $m->subject('The Kure - IVR Trial Order!');
        });
    }
}
