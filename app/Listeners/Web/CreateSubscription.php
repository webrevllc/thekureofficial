<?php
namespace App\Listeners\Web;

use App\Events\OrderPlacedOnWebsite;
use App\Http\Controllers\Traits\MakesOrderObjects;
use App\Order;
use App\Subscription;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateSubscription
{
    use MakesOrderObjects;
    /**
     * Handle the event.
     *
     * @param  OrderPlacedOnWebsite  $event
     * @return void
     */
    public function handle(OrderPlacedOnWebsite $event)
    {
        $order = Order::where('invoice_number', $event->invoiceNumber)->first();
        $subscription_products = $this->getRecurringProducts($event->products);
        $subscription_total = $this->getSubscriptionTotal($subscription_products, $event->quantity);
        $subscription = new Subscription;
        $subscription->unique_id = $event->invoiceNumber;
        $subscription->status = 'active';
        $subscription->next_process_date = Carbon::now()->addYear();
//        $subscription->next_process_date = Carbon::now()->addDays(30);
        $subscription->user_id = $event->user->id;
        $subscription->products = json_encode($subscription_products);
        $subscription->amount = $subscription_total;
        $order->subscription()->save($subscription);
    }
}
