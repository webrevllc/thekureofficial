<?php

namespace App\Listeners\Web;

use App\Events\OrderPlacedOnWebsite;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Snowfire\Beautymail\Beautymail;

class mailNotificationToBosses
{
    /**
     * @var Beautymail
     */
    private $beautymail;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Beautymail $beautymail)
    {
        //
        $this->beautymail = $beautymail;
    }

    /**
     * Handle the event.
     *
     * @param  OrderPlacedOnWebsite  $event
     * @return void
     */
    public function handle(OrderPlacedOnWebsite $event)
    {
        $order = Order::where('auth_transaction', '=', $event->transactionID)->first();
        $this->beautymail->send('emails.web.bosses', ['order' => $order, 'user' => $event->user, 'city' => $event->city, 'state' => $event->state], function ($m) {
            $m->to('pspezza@centuriontech.com');
            $m->cc('shane@computercentersusa.com');
            $m->cc('melarthur4@gmail.com');
            $m->cc('jrosenthal@computercentersusa.com');
            $m->subject('The Kure - Web Order!');
        });
    }
}
