<?php

namespace App\Listeners\Web;

use App\Events\OrderPlacedOnWebsite;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Snowfire\Beautymail\Beautymail;

class EmailCustomerReceipt
{
    /**
     * @var Beautymail
     */
    private $beautymail;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Beautymail $beautymail)
    {
        $this->beautymail = $beautymail;
    }

    /**
     * Handle the event.
     *
     * @param  OrderPlacedOnWebsite  $event
     * @return void
     */
    public function handle(OrderPlacedOnWebsite $event)
    {

        $order = Order::where('auth_transaction', '=', $event->transactionID)->first();
        $email = $event->user->email;
        $this->beautymail->send('emails.web.customerreceipt', ['order' => $order, 'user' => $event->user], function ($m) use ($email){
            $m->to($email);
            $m->subject('The Kure - Receipt');
        });

    }
}
