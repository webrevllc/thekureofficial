<?php

namespace App\Listeners\Web;

use App\Address;
use App\Events\OrderPlacedOnWebsite;
use App\Order;
use App\Serial;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SettAddressAssignSerial
{
    /**
     * @var Address
     */
    private $address;
    /**
     * @var Serial
     */
    private $serial;

    /**
     * Create the event listener.
     *
     * @param Address $address
     * @param Serial $serial
     */
    public function __construct(Address $address, Serial $serial)
    {
        //
        $this->address = $address;
        $this->serial = $serial;
    }

    /**
     * Handle the event.
     *
     * @param  OrderPlacedOnWebsite  $event
     * @return void
     */
    public function handle(OrderPlacedOnWebsite $event)
    {
        //Set Address
        $order = Order::where('invoice_number',$event->invoiceNumber)->first();
        $this->address->makeAddress($order->id);

        //Assign Serial Number
        $this->serial->assignSerial($order->id);
    }
}
