<?php

namespace App\Listeners\Web\TrialSuccess;

use App\Events\TrialOrderPlacedOnWebsite;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateTrialWebOrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TrialOrderPlacedOnWebsite  $event
     * @return void
     */
    public function handle(TrialOrderPlacedOnWebsite $event)
    {
        $order = new Order;
        $order->amount = 'Free Trial';
        $order->invoice_number = $event->invoice_number;
        $order->status = 'trial';
        $order->user_id = $event->user->id;
        $order->source = 'web';
        $order->products = json_encode($event->products);
        $order->auth_transaction = $event->transaction_id;
        $order->type = 'aim';
        $order->save();
    }
}
