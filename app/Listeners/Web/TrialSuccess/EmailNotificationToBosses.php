<?php

namespace App\Listeners\Web\TrialSuccess;

use App\Events\TrialOrderPlacedOnWebsite;
use App\Order;
use App\Subscription;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Snowfire\Beautymail\Beautymail;

class EmailNotificationToBosses
{
    /**
     * @var Beautymail
     */
    private $beautymail;

    /**
     * Create the event listener.
     *
     * @param Beautymail $beautymail
     */
    public function __construct(Beautymail $beautymail)
    {
        $this->beautymail = $beautymail;
    }

    /**
     * Handle the event.
     *
     * @param  TrialOrderPlacedOnWebsite  $event
     * @return void
     */
    public function handle(TrialOrderPlacedOnWebsite $event)
    {
        $order = Order::where('auth_transaction', '=', $event->transaction_id)->first();
        $sub = Subscription::where('unique_id', '=', $order->invoice_number)->first();
        $this->beautymail->send('emails.web.trialbosses', ['order' => $order, 'user' => $event->user, 'city' => $event->city, 'state' => $event->state, 'sub' => $sub], function ($m) {
            $m->cc('pspezza@centuriontech.com');
            $m->cc('shane@computercentersusa.com');
            $m->cc('melarthur4@gmail.com');
            $m->cc('jrosenthal@computercentersusa.com');
            $m->subject('The Kure - Web Trial Order!');
        });
    }
}
