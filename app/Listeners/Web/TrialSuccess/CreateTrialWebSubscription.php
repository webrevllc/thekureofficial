<?php

namespace App\Listeners\Web\TrialSuccess;

use App\Events\TrialOrderPlacedOnWebsite;
use App\Order;
use App\Subscription;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateTrialWebSubscription
{

    /**
     * @var Order
     */
    private $order;

    /**
     * Create the event listener.
     *
     * @param Order $order
     */
    public function __construct( Order $order)
    {
        $this->order = $order;
    }

    /**
     * Handle the event.
     *
     * @param  TrialOrderPlacedOnWebsite  $event
     * @return void
     */
    public function handle(TrialOrderPlacedOnWebsite $event)
    {
        $order = Order::where('invoice_number', $event->invoice_number)->first();
        $subscription = new Subscription;
        $subscription->unique_id = $event->invoice_number;
        $subscription->status = 'active';
//        $subscription->next_process_date = Carbon::now()->addDays(30);
        $subscription->user_id = $event->user->id;
        $subscription->products = json_encode($event->products);
        $subscription->amount = $this->order->total($event->products, $event->user);
        $order->subscription()->save($subscription);
    }
}
