<?php

namespace App\Listeners\Web\TrialSuccess;

use App\Events\TrialOrderPlacedOnWebsite;
use App\Order;
use App\Subscription;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Snowfire\Beautymail\Beautymail;

class EmailTheCustomer
{
    /**
     * @var Beautymail
     */
    private $beautymail;

    /**
     * Create the event listener.
     *
     * @param Beautymail $beautymail
     */
    public function __construct(Beautymail $beautymail)
    {
        $this->beautymail = $beautymail;
    }

    /**
     * Handle the event.
     *
     * @param  TrialOrderPlacedOnWebsite  $event
     * @return void
     */
    public function handle(TrialOrderPlacedOnWebsite $event)
    {
        $order = Order::where('auth_transaction', '=', $event->transaction_id)->first();
        $sub = Subscription::where('unique_id', '=', $order->invoice_number)->first();
        $email = $event->user->email;
        $this->beautymail->send('emails.web.trialcustomer', ['order' => $order, 'user' => $event->user, 'sub' => $sub], function ($m) use ($email){
            $m->to($email);
            $m->subject('The Kure - Receipt');
        });
    }
}
