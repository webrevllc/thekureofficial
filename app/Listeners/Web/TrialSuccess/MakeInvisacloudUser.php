<?php

namespace App\Listeners\Web\TrialSuccess;

use App\Contracts\Invisacloud;
use App\Events\TrialOrderPlacedOnWebsite;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MakeInvisacloudUser
{
    /**
     * @var Invisacloud
     */
    private $invisacloud;

    /**
     * Create the event listener.
     *
     * @param Invisacloud $invisacloud
     */
    public function __construct(Invisacloud $invisacloud)
    {
        //
        $this->invisacloud = $invisacloud;
    }

    /**
     * Handle the event.
     *
     * @param  TrialOrderPlacedOnWebsite  $event
     * @return void
     */
    public function handle(TrialOrderPlacedOnWebsite $event)
    {
        $this->invisacloud->makeUser($event->user, $event->transaction_id, 'trial');
    }
}
