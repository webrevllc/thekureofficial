<?php

namespace App\Listeners\Web;

use App\Events\OrderPlacedOnWebsite;
use App\Order;
use AuthorizeNetTD;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateTheOrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderPlacedOnWebsite  $event
     * @return void
     */
    public function handle(OrderPlacedOnWebsite $event)
    {
        $order = new Order;
        $order->amount = $event->total;
        $order->invoice_number = $event->invoiceNumber;
        $order->status = 'paid';
        $order->user_id = $event->user->id;
        $order->last_4 = $event->last4;
        $order->expiration = $event->request->input('month').$event->request->input('year');
        $order->source = 'web';
        $order->products = json_encode($event->products);
        $order->auth_transaction = $event->transactionID;
        $order->type = 'aim';
        $order->save();
    }
}
