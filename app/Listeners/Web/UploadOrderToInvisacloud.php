<?php

namespace App\Listeners\Web;

use App\Contracts\Invisacloud;
use App\Events\OrderPlacedOnWebsite;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UploadOrderToInvisacloud
{
    /**
     * @var Invisacloud
     */
    private $invisacloud;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Invisacloud $invisacloud)
    {
        //
        $this->invisacloud = $invisacloud;
    }

    /**
     * Handle the event.
     *
     * @param  OrderPlacedOnWebsite  $event
     * @return void
     */
    public function handle(OrderPlacedOnWebsite $event)
    {
        $order = Order::where('auth_transaction', '=', $event->transactionID)->first();
        $this->invisacloud->uploadOrder($order);
    }
}
