<?php

namespace App\Listeners\Web;

use App\Contracts\Payment;
use App\Events\OrderPlacedOnWebsite;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SetAuthNetProfileIDsOnUser
{
    /**
     * @var Payment
     */
    private $payment;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Payment $payment)
    {
        //
        $this->payment = $payment;
    }

    /**
     * Handle the event.
     *
     * @param  OrderPlacedOnWebsite  $event
     * @return void
     */
    public function handle(OrderPlacedOnWebsite $event)
    {
        $response = $this->payment->createAuthIDs($event->transactionID);
        $customerProfileID = $response->xml->customerProfileId;
        $customerBillingID = $response->xml->customerPaymentProfileIdList->numericString;
        $customerShippingID = $response->xml->customerShippingAddressIdList->numericString;

        $user = User::find($event->user->id);
        $user->auth_profile = $customerProfileID;
        $user->default_billing = $customerBillingID;
        $user->default_shipping = $customerShippingID;
        $user->save();
    }
}
