<?php

namespace App\Listeners;

use App\Contracts\Invisacloud;
use App\Events\OrderPlacedOnWebsite;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MakeInvisacloudUser
{
    /**
     * @var Invisacloud
     */
    private $invisacloud;

    /**
     * Create the event listener.
     */
    public function __construct(Invisacloud $invisacloud)
    {
        //
        $this->invisacloud = $invisacloud;
    }

    /**
     * Handle the event.
     *
     * @param  OrderPlacedOnWebsite  $event
     * @return void
     */
    public function handle(OrderPlacedOnWebsite $event)
    {
        $this->invisacloud->makeUser($event->request, $event->invoiceNumber);
    }
}
