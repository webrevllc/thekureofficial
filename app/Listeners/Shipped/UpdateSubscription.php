<?php

namespace App\Listeners\Shipped;

use App\Events\OrderHasShipped;
use App\Subscription;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateSubscription
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderHasShipped  $event
     * @return void
     */
    public function handle(OrderHasShipped $event)
    {
        if($event->orderUser->status == 'trial'){
            $subscription = Subscription::where('unique_id', $event->orderUser->invoice_number)->first();
            $subscription->next_process_date = Carbon::now()->addDays(35);
            $subscription->save();
        }
    }
}
