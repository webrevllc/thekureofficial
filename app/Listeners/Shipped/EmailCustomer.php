<?php

namespace App\Listeners\Shipped;


use App\Emails\EmailOrderHasShipped;
use App\Events\OrderHasShipped;

class EmailCustomer
{
    /**
     * Handle the event.
     *
     * @param  OrderHasShipped  $event
     * @return void
     */
    public function handle(OrderHasShipped $event)
    {
        (new EmailOrderHasShipped)->sendTo($event->orderUser->user);
    }
}
