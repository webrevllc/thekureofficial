<?php

namespace App\Listeners\Shipped;

use App\Contracts\Invisacloud;
use App\Events\OrderHasShipped;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateInvisacloudProducts
{
    /**
     * @var Invisacloud
     */
    private $invisacloud;

    /**
     * Create the event listener.
     *
     * @param Invisacloud $invisacloud
     */
    public function __construct(Invisacloud $invisacloud)
    {
        //
        $this->invisacloud = $invisacloud;
    }

    /**
     * Handle the event.
     *
     * @param  OrderHasShipped  $event
     * @return void
     */
    public function handle(OrderHasShipped $event)
    {
        $order = Order::whereInvoice_number($event->orderUser->invoice_number)->first();
        if($order->status == "trial"){
            $this->invisacloud->updateOrder($order);
        }
    }
}
