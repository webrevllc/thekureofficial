<?php

namespace App\Listeners\WebNew\EasyPayOrderComplete;

use App\Emails\Bosses;
use App\Emails\EmailBosses;
use App\Events\WebNew\EasyPayOrderComplete;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailNotificationToBosses
{
    /**
     * @var Bosses
     */
    private $bosses;

    /**
     * Create the event listener.
     *
     * @param Bosses $bosses
     */
    public function __construct(Bosses $bosses)
    {
        $this->bosses = $bosses;
    }

    /**
     * Handle the event.
     *
     * @param  EasyPayOrderComplete  $event
     * @return void
     */
    public function handle(EasyPayOrderComplete $event)
    {
        $order = Order::with('subscription')->where('invoice_number', $event->invoice_number)->first();
//        dd($order);
        $b = $this->bosses->returnBosses();
        foreach($b as $boss){
            (new EmailBosses)->withData([
                'status' => 'Easy Pay',
                'type' => 'Web',
                'order' => $order,
                'subscription' => $order->subscription,
                'orderInfo' => $event->lead])
                ->sendTo($boss);
        }
    }
}
