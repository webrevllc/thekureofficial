<?php

namespace App\Listeners\WebNew\EasyPayOrderComplete;

use App\Contracts\Invisacloud;
use App\Events\WebNew\EasyPayOrderComplete;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MakeInvisacloudUser
{
    /**
     * @var Invisacloud
     */
    private $invisacloud;

    /**
     * Create the event listener.
     *
     * @param Invisacloud $invisacloud
     */
    public function __construct(Invisacloud $invisacloud)
    {
        $this->invisacloud = $invisacloud;
    }

    /**
     * Handle the event.
     *
     * @param  EasyPayOrderComplete  $event
     * @return void
     */
    public function handle(EasyPayOrderComplete $event)
    {
        $this->invisacloud->makeNewUser($event->user, $event->invoice_number, 'trial');
    }
}
