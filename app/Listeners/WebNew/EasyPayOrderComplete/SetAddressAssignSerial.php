<?php

namespace App\Listeners\WebNew\EasyPayOrderComplete;

use App\Address;
use App\Events\WebNew\EasyPayOrderComplete;
use App\Order;
use App\Serial;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SetAddressAssignSerial
{
    /**
     * @var Address
     */
    private $address;
    /**
     * @var Serial
     */
    private $serial;

    /**
     * Create the event listener.
     *
     * @param Address $address
     * @param Serial $serial
     */
    public function __construct(Address $address, Serial $serial)
    {
        $this->address = $address;
        $this->serial = $serial;
    }

    /**
     * Handle the event.
     *
     * @param  EasyPayOrderComplete  $event
     * @return void
     */
    public function handle(EasyPayOrderComplete $event)
    {
        //Set Address
        $order = Order::where('invoice_number',$event->invoice_number)->first();
        $this->address->makeAddress($order->id);

        //Assign Serial Number
        $this->serial->assignSerial($order->id);
    }
}
