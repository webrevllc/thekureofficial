<?php

namespace App\Listeners\WebNew\EasyPayOrderComplete;

use App\Contracts\Invisacloud;
use App\Events\WebNew\EasyPayOrderComplete;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MakeInvisacloudProduct
{
    /**
     * @var Invisacloud
     */
    private $invisacloud;

    /**
     * Create the event listener.
     *
     * @param Invisacloud $invisacloud
     */
    public function __construct(Invisacloud $invisacloud)
    {
        $this->invisacloud = $invisacloud;
    }

    /**
     * Handle the event.
     *
     * @param  EasyPayOrderComplete  $event
     * @return void
     */
    public function handle(EasyPayOrderComplete $event)
    {
        $order = Order::where('invoice_number', '=', $event->invoice_number)->first();
        $this->invisacloud->uploadOrder($order);
    }
}
