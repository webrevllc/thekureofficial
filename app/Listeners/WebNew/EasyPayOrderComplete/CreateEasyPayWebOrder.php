<?php

namespace App\Listeners\WebNew\EasyPayOrderComplete;

use App\Events\WebNew\EasyPayOrderComplete;
use App\Order;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateEasyPayWebOrder
{
    /**
     * Handle the event.
     *
     * @param  EasyPayOrderComplete  $event
     * @return void
     */
    public function handle(EasyPayOrderComplete $event)
    {
        $order = new Order;
        $order->amount = $event->lead->today_total;
        $order->invoice_number = $event->invoice_number;
        $order->status = '2pay';
        $order->user_id = $event->user->id;
        $order->source = 'web';
        $order->products = json_encode($event->products);
        $order->auth_transaction = $event->transactionID;
        $order->type = 'cim';
        $order->offer = $event->lead->offer;
        $order->save();
    }
}
