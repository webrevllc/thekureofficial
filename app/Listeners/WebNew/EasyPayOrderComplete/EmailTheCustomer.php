<?php

namespace App\Listeners\WebNew\EasyPayOrderComplete;

use App\Emails\WelcomeEmail;
use App\Events\WebNew\EasyPayOrderComplete;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailTheCustomer
{
    /**
     * Handle the event.
     *
     * @param  EasyPayOrderComplete  $event
     * @return void
     */
    public function handle(EasyPayOrderComplete $event)
    {
        (new WelcomeEmail)->sendTo($event->user);
    }
}
