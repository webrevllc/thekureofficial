<?php

namespace App\Listeners\WebNew\EasyPayOrderComplete;

use App\Events\WebNew\EasyPayOrderComplete;
use App\Order;
use App\Subscription;
use App\Total;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateEasyPayWebSubscription
{
    /**
     * @var Order
     */
    private $order;
    /**
     * @var Total
     */
    private $total;

    /**
     * Create the event listener.
     *
     * @param Order $order
     * @param Total $total
     */
    public function __construct( Order $order, Total $total)
    {
        $this->order = $order;
        $this->total = $total;
    }
    /**
     * Handle the event.
     *
     * @param  EasyPayOrderComplete  $event
     * @return void
     */
    public function handle(EasyPayOrderComplete $event)
    {
        $order = Order::where('invoice_number', $event->invoice_number)->first();
        $subscription = new Subscription;
        $subscription->unique_id = $event->invoice_number;
        $subscription->status = 'active';
        $subscription->user_id = $event->user->id;
        $subscription->products = json_encode($event->products);
        $subscription->amount = $event->lead->total - $event->lead->today_total;
        $subscription->offer = $event->lead->offer;
        $subscription->next_process_date = Carbon::now()->addDays(30);
        $order->subscription()->save($subscription);
        $this->total->updateTotals('web_trial', $event->lead->total);
        $order->subscription_id = $subscription->id;
        $order->save();
    }
}
