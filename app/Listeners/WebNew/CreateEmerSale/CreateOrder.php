<?php

namespace App\Listeners\WebNew\CreateEmerSale;

use App\Events\WebNew\CreateEmerSale;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateOrder
{

    /**
     * Handle the event.
     *
     * @param  CreateEmerSale  $event
     * @return void
     */
    public function handle(CreateEmerSale $event)
    {
        $order = new Order;
        $order->amount = $event->lead->emerTotal;
        $order->invoice_number = $event->invoice_number;
        $order->status = 'paid';
        $order->user_id = $event->user->id;
        $order->source = 'web';
        $order->hasEMER = true;
        $order->shipped = true;
        $order->printed = true;
        $order->products = json_encode($event->emer);
        $order->auth_transaction = $event->transaction_id;
        $order->type = 'cim';
        $order->offer = $event->lead->offer;
        $order->save();
    }
}
