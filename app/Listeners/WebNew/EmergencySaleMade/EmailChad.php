<?php

namespace App\Listeners\WebNew\EmergencySaleMade;

use App\Emails\Bosses;
use App\Emails\SupportNotification;
use App\Events\WebNew\EmergencySaleMade;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class EmailChad
{

    /**
     * @var Bosses
     */
    private $bosses;

    /**
     * Create the event listener.
     *
     * @param Bosses $bosses
     */
    public function __construct(Bosses $bosses)
    {
        //
        $this->bosses = $bosses;
    }

    /**
     * Handle the event.
     *
     * @param  EmergencySaleMade  $event
     * @return void
     */
    public function handle(EmergencySaleMade $event)
    {
        $name = $event->lead->first_name.' ' .$event->lead->last_name;
        $email = $event->lead->email;
        $phone = $event->lead->phone;
        $quantity = $event->lead->EMER;

        $b = $this->bosses->returnBosses();
        foreach($b as $boss){
            (new SupportNotification())->withData(["name" => $name, "email" => $email, "phone" => $phone, "quantity" => $quantity])->sendTo($boss);
        }
        (new SupportNotification())->withData(["name" => $name, "email" => $email, "phone" => $phone, "quantity" => $quantity])->sendTo(User::find(5));

    }
}
