<?php

namespace App\Listeners\WebNew\EmergencySaleMade;

use App\Address;
use App\Events\WebNew\EmergencySaleMade;
use App\Order;
use App\Serial;


class SetAddressAssignSerial
{
    /**
     * @var Address
     */
    private $address;
    /**
     * @var Serial
     */
    private $serial;

    /**
     * Create the event listener.
     *
     * @param Address $address
     * @param Serial $serial
     */
    public function __construct(Address $address, Serial $serial)
    {
        //
        $this->address = $address;
        $this->serial = $serial;
    }


    public function handle(EmergencySaleMade $event)
    {
        //Set Address
        $order = Order::where('invoice_number',$event->invoice_number)->first();
        $this->address->makeAddress($order->id);

        //Assign Serial Number
        $this->serial->assignSerial($order->id);
    }
}
