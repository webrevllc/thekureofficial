<?php

namespace App\Listeners\WebNew\EmergencySaleMade;

use App\Contracts\Invisacloud;
use App\Events\WebNew\EmergencySaleMade;

class MakeInvisacloudUser
{
    /**
     * @var Invisacloud
     */
    private $invisacloud;

    /**
     * Create the event listener.
     *
     * @param Invisacloud $invisacloud
     */
    public function __construct(Invisacloud $invisacloud)
    {
        $this->invisacloud = $invisacloud;
    }


    public function handle(EmergencySaleMade $event)
    {
        $this->invisacloud->makeNewUser($event->user, $event->invoice_number, 'paid');
    }
}
