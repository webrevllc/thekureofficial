<?php

namespace App\Listeners\WebNew\EmergencySaleMade;

use App\Emails\Bosses;
use App\Emails\EmailBosses;
use App\Events\WebNew\EmergencySaleMade;
use App\Order;


class EmailNotificationToBosses
{
    /**
     * @var Bosses
     */
    private $bosses;

    /**
     * Create the event listener.
     *
     * @param Bosses $bosses
     */
    public function __construct(Bosses $bosses)
    {
        //
        $this->bosses = $bosses;
    }


    public function handle(EmergencySaleMade $event)
    {
        $order = Order::with('subscription')->where('invoice_number', $event->invoice_number)->first();

        $b = $this->bosses->returnBosses();
        foreach($b as $boss){
            (new EmailBosses)->withData([
                'status' => 'Paid',
                'type' => 'Web - With Emergency Upsell',
                'order' => $order,
                'subscription' => $order->subscription,
                'orderInfo' => $event->lead])
                ->sendTo($boss);
        }
    }
}
