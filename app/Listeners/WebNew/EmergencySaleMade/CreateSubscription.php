<?php

namespace App\Listeners\WebNew\EmergencySaleMade;

use App\Events\WebNew\EmergencySaleMade;
use App\Order;
use App\Subscription;
use App\Total;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateSubscription
{
    /**
     * @var Total
     */
    private $total;

    /**
     * Create the event listener.
     *
     * @param Total $total
     */
    public function __construct(Total $total)
    {
        $this->total = $total;
    }


    public function handle(EmergencySaleMade $event)
    {
        $order = Order::where('invoice_number', $event->invoice_number)->first();
        $subscription = new Subscription;
        $subscription->unique_id = $event->invoice_number;
        $subscription->status = 'active';
        $subscription->next_process_date = Carbon::now()->addDays(455);
        $subscription->user_id = $event->user->id;
        $subscription->products = json_encode($event->products);
        $subscription->amount = $this->calculateSubscriptionAmount($event->products);
        $subscription->offer = $event->lead->offer;
        $order->subscription()->save($subscription);
        $this->total->updateTotals('web_paid', $event->lead->total);

        $order->subscription_id = $subscription->id;
        $order->save();
    }

    private function calculateSubscriptionAmount($products)
    {
        $ps = json_decode(json_encode($products));
        $totalArray = [];
        foreach ($ps as $p) {
            if($p->recurring){
                $totalArray[] = $p->price * $p->quantity;
            }
        }
        $amount = array_sum($totalArray) / 100;

        return $amount;
    }

}
