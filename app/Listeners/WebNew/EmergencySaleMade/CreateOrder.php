<?php

namespace App\Listeners\WebNew\EmergencySaleMade;

use App\Events\WebNew\EmergencySaleMade;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateOrder
{

    public function handle(EmergencySaleMade $event)
    {
        $order = new Order;
        $order->amount = $event->lead->total;
        $order->invoice_number = $event->invoice_number;
        $order->status = 'paid';
        $order->user_id = $event->user->id;
        $order->source = 'web';
        $order->hasEMER = true;
        $order->products = json_encode($event->products);
        $order->auth_transaction = $event->transaction_id;
        $order->type = 'cim';
        $order->offer = $event->lead->offer;
        $order->save();
    }
}
