<?php

namespace App\Listeners\WebNew\EmergencySaleMade;

use App\Emails\WelcomeEmail;
use App\Events\WebNew\EmergencySaleMade;


class EmailTheCustomer
{


    public function handle(EmergencySaleMade $event)
    {
        (new WelcomeEmail)->sendTo($event->user);
    }
}
