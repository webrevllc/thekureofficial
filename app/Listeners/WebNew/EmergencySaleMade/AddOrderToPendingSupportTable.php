<?php

namespace App\Listeners\WebNew\EmergencySaleMade;

use App\Events\WebNew\EmergencySaleMade;
use App\Order;
use App\PendingSupport;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddOrderToPendingSupportTable
{


    /**
     * Handle the event.
     *
     * @param  EmergencySaleMade  $event
     * @return void
     */
    public function handle(EmergencySaleMade $event)
    {
        $order = Order::where('invoice_number', $event->invoice_number)->first();
        $support = new PendingSupport;
        $support->order_id = $order->id;
        $support->name = $event->lead->first_name.' ' .$event->lead->last_name;
        $support->phone = $event->lead->phone;
        $support->quantity = $event->lead->EMER;
        $support->save();

    }
}
