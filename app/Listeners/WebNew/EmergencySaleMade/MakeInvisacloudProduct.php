<?php

namespace App\Listeners\WebNew\EmergencySaleMade;

use App\Contracts\Invisacloud;
use App\Events\WebNew\EmergencySaleMade;
use App\Order;


class MakeInvisacloudProduct
{
    /**
     * @var Invisacloud
     */
    private $invisacloud;

    /**
     * Create the event listener.
     *
     * @param Invisacloud $invisacloud
     */
    public function __construct(Invisacloud $invisacloud)
    {
        //
        $this->invisacloud = $invisacloud;
    }

    public function handle(EmergencySaleMade $event)
    {
        $order = Order::where('auth_transaction', '=', $event->transaction_id)->first();
        $this->invisacloud->uploadOrder($order);
    }
}
