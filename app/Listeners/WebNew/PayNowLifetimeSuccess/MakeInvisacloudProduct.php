<?php

namespace App\Listeners\WebNew\PayNowLifetimeSuccess;

use App\Events\WebNew\NewLifetimePaidOrderComplete;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MakeInvisacloudProduct
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewLifetimePaidOrderComplete  $event
     * @return void
     */
    public function handle(NewLifetimePaidOrderComplete $event)
    {
        //
    }
}
