<?php

namespace App\Listeners\WebNew\PayNowSuccess;

use App\Emails\WelcomeEmail;
use App\Events\WebNew\NewPaidOrderComplete;
use App\Order;
use App\Subscription;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Snowfire\Beautymail\Beautymail;

class EmailTheCustomer
{

    /**
     * Handle the event.
     *
     * @param  NewPaidOrderComplete  $event
     * @return void
     */
    public function handle(NewPaidOrderComplete $event)
    {
        (new WelcomeEmail)->sendTo($event->user);
    }
}
