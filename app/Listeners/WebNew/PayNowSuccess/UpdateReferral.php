<?php

namespace App\Listeners\WebNew\PayNowSuccess;

use App\Events\WebNew\NewPaidOrderComplete;
use App\Referral;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateReferral
{

    /**
     * Handle the event.
     *
     * @param  NewPaidOrderComplete  $event
     * @return void
     */
    public function handle(NewPaidOrderComplete $event)
    {
        if($event->lead->source != 'mel'){
            $r = Referral::where('referring_transaction_number', $event->lead->source)->first();
            $r->completed_referral_transaction_number = $event->invoice_number;
            $r->save();
        }
    }
}
