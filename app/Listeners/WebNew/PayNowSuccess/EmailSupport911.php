<?php

namespace App\Listeners\WebNew\PayNowSuccess;

use App\Emails\Bosses;
use App\Emails\SupportNotification;
use App\Events\WebNew\NewPaidOrderComplete;
use App\User;

class EmailSupport911
{
    /**
     * @var Bosses
     */
    private $bosses;

    /**
     * Create the event listener.
     *
     * @param Bosses $bosses
     */
    public function __construct(Bosses $bosses)
    {
        $this->bosses = $bosses;
    }


    /**
     * @param NewPaidOrderComplete $event
     */
    public function handle(NewPaidOrderComplete $event)
    {
        if($event->lead->EMER){
            $name = $event->lead->first_name.' ' .$event->lead->last_name;
            $email = $event->lead->email;
            $phone = $event->lead->phone;
            $quantity = $event->lead->EMER;

            $b = $this->bosses->returnBosses();
            foreach($b as $boss){
                (new SupportNotification())->withData(["name" => $name, "email" => $email, "phone" => $phone, "quantity" => $quantity])->sendTo($boss);
            }
            (new SupportNotification())->withData(["name" => $name, "email" => $email, "phone" => $phone, "quantity" => $quantity])->sendTo(User::find(5));
        }

    }
}
