<?php

namespace App\Listeners\WebNew\PayNowSuccess;

use App\Emails\Bosses;
use App\Emails\EmailBosses;
use App\Events\WebNew\NewPaidOrderComplete;
use App\Order;
use App\Subscription;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Snowfire\Beautymail\Beautymail;

class EmailNotificationToBosses
{
    /**
     * @var Bosses
     */
    private $bosses;


    /**
     * EmailNotificationToBosses constructor.
     * @param Bosses $bosses
     */
    public function __construct(Bosses $bosses)
    {
        $this->bosses = $bosses;
    }

    /**
     * Handle the event.
     *
     * @param  NewPaidOrderComplete  $event
     * @return void
     */
    public function handle(NewPaidOrderComplete $event)
    {

        $order = Order::with('subscription')->where('invoice_number', $event->invoice_number)->first();

        $b = $this->bosses->returnBosses();
        foreach($b as $boss){
            (new EmailBosses)->withData([
                'status' => 'Paid',
                'type' => 'Web',
                'order' => $order,
                'subscription' => $order->subscription,
                'orderInfo' => $event->lead])
                ->sendTo($boss);
        }
    }
}
