<?php

namespace App\Listeners\WebNew\PayNowSuccess;

use App\Events\WebNew\NewPaidOrderComplete;
use App\Order;
use App\PendingSupport;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddOrderToPendingSupportTable
{


    /**
     * @param NewPaidOrderComplete $event
     */
    public function handle(NewPaidOrderComplete $event)
    {
        if($event->lead->EMER){
            $order = Order::where('invoice_number', $event->invoice_number)->first();
            $support = new PendingSupport;
            $support->order_id = $order->id;
            $support->name = $event->lead->first_name.' ' .$event->lead->last_name;
            $support->phone = $event->lead->phone;
            $support->quantity = $event->lead->EMER;
            $support->save();
        }
    }
}
