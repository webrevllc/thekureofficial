<?php

namespace App\Listeners\WebNew\PayNowSuccess;

use App\Events\WebNew\NewPaidOrderComplete;
use App\Order;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateTrialWebOrder
{
    /**
     * Handle the event.
     *
     * @param  NewPaidOrderComplete  $event
     * @return void
     */
    public function handle(NewPaidOrderComplete $event)
    {
        $order = new Order;
        $order->amount = $event->lead->total;
        if($event->lead->EMER > 0){
            $order->hasEmer = true;
        }
        $order->has_five_year = $event->lead->five_year ? true : false;
        $order->has_five_year_date = $event->lead->five_year ? Carbon::now()->addYears(5)->addMonths(3)->startOfDay() : null;
        $order->invoice_number = $event->invoice_number;
        $order->status = 'paid';
        $order->user_id = $event->user->id;
        $order->source = 'web';
        $order->products = json_encode($event->products);
        $order->auth_transaction = $event->transactionID;
        $order->type = 'cim';
        $order->offer = $event->lead->offer;
        $order->save();
    }
}
