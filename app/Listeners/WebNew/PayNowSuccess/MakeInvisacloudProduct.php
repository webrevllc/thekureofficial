<?php

namespace App\Listeners\WebNew\PayNowSuccess;

use App\Contracts\Invisacloud;
use App\Events\WebNew\NewPaidOrderComplete;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MakeInvisacloudProduct
{
    /**
     * @var Invisacloud
     */
    private $invisacloud;

    /**
     * Create the event listener.
     *
     * @param Invisacloud $invisacloud
     */
    public function __construct(Invisacloud $invisacloud)
    {
        //
        $this->invisacloud = $invisacloud;
    }

    /**
     * Handle the event.
     *
     * @param  NewPaidOrderComplete  $event
     * @return void
     */
    public function handle(NewPaidOrderComplete $event)
    {
        $order = Order::where('auth_transaction', '=', $event->transactionID)->first();
        $this->invisacloud->uploadOrder($order);
    }
}
