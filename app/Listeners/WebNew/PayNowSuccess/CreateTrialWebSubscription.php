<?php

namespace App\Listeners\WebNew\PayNowSuccess;

use App\Events\WebNew\NewPaidOrderComplete;
use App\Order;
use App\Subscription;
use App\Total;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateTrialWebSubscription
{
    /**
     * @var Total
     */
    private $total;

    /**
     * Create the event listener.
     *
     * @param Total $total
     */
    public function __construct(Total $total)
    {
        //
        $this->total = $total;
    }

    /**
     * Handle the event.
     *
     * @param  NewPaidOrderComplete  $event
     * @return void
     */
    public function handle(NewPaidOrderComplete $event)
    {
        $order = Order::where('invoice_number', $event->invoice_number)->first();
        $subscription = new Subscription;
        $subscription->unique_id = $event->invoice_number;
        $subscription->status = 'active';
        $subscription->next_process_date = $this->getSubDate($event->lead);
        $subscription->user_id = $event->user->id;
        $subscription->products = json_encode($event->products);
//        $subscription->amount = $event->lead->total - 2.95;
        $subscription->amount = 0;
//        $subscription->amount = $this->makeTotal($event->lead);
        $subscription->offer = $event->lead->offer;
//        if($event->lead->EMER){
//            $subscription->hasEMER = true;
//        }
        $order->subscription()->save($subscription);
        $this->total->updateTotals('web_paid', $event->lead->total);

        $order->subscription_id = $subscription->id;
        $order->save();
    }

    public function makeTotal($lead)
    {
        if($lead->HWMON || $lead->PCOS){
            $five_year_amount = $lead->five_year ? (59.85 * $lead->quantity) + (19.95 * $lead->quantity) : 0;
            $proc = $lead->PROC ? 1.99 : 0;
            $total = $lead->total - $five_year_amount - 5.95 - $proc - ($lead->quantity * 49.95);
        }else{
            $proc = $lead->PROC ? 1.99 : 0;
            $total = $lead->total - 5.95 - $proc - ($lead->quantity * 49.95);
        }

        return $total;
    }

    public function getSubDate($lead)
    {
        if($lead->HWMON || $lead->PCOS){
            return Carbon::now()->addDays(100000);
//            return Carbon::now()->addDays(455);
        }else{
            return $lead->five_year ? Carbon::now()->addYears(5)->addMonths(3) : Carbon::now()->addDays(100000) ;
//            return $lead->five_year ? Carbon::now()->addYears(5)->addMonths(3) : Carbon::now()->addDays(365) ;
        }
    }
}
