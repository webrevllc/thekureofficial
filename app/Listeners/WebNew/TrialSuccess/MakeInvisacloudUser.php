<?php

namespace App\Listeners\WebNew\TrialSuccess;

use App\Contracts\Invisacloud;
use App\Events\WebNew\TrialOrderComplete;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MakeInvisacloudUser
{
    /**
     * @var Invisacloud
     */
    private $invisacloud;

    /**
     * Create the event listener.
     *
     * @param Invisacloud $invisacloud
     */
    public function __construct(Invisacloud $invisacloud)
    {
        $this->invisacloud = $invisacloud;
    }

    /**
     * Handle the event.
     *
     * @param  TrialOrderComplete  $event
     * @return void
     */
    public function handle(TrialOrderComplete $event)
    {
        $this->invisacloud->makeNewUser($event->user, $event->invoice_number, 'trial');
    }
}
