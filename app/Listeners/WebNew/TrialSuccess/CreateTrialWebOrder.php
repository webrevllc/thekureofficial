<?php

namespace App\Listeners\WebNew\TrialSuccess;

use App\Events\WebNew\TrialOrderComplete;
use App\Order;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateTrialWebOrder
{
    /**
     * Handle the event.
     *
     * @param  TrialOrderComplete  $event
     * @return void
     */
    public function handle(TrialOrderComplete $event)
    {
        $order = new Order;
        $order->has_five_year = $event->lead->five_year ? true : false;
        $order->has_five_year_date = $event->lead->five_year ? Carbon::now()->addYears(5)->startOfDay() : null;
        $order->amount = 'Free Trial';
        $order->invoice_number = $event->invoice_number;
        $order->status = 'trial';
        $order->user_id = $event->user->id;
        $order->source = 'web';
        $order->products = json_encode($event->products);
        $order->auth_transaction = '';
        $order->type = 'cim';
        $order->offer = $event->lead->offer;
        $order->save();
    }
}
