<?php

namespace App\Listeners\WebNew\TrialSuccess;

use App\Address;
use App\Events\WebNew\TrialOrderComplete;
use App\Order;
use App\Serial;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SettAddressAssignSerial
{
    /**
     * @var Address
     */
    private $address;
    /**
     * @var Serial
     */
    private $serial;

    /**
     * Create the event listener.
     *
     * @param Address $address
     * @param Serial $serial
     */
    public function __construct(Address $address, Serial $serial)
    {
        //
        $this->address = $address;
        $this->serial = $serial;
    }

    /**
     * Handle the event.
     *
     * @param  TrialOrderComplete  $event
     * @return void
     */
    public function handle(TrialOrderComplete $event)
    {
        //Set Address
        $order = Order::where('invoice_number',$event->invoice_number)->first();
        $this->address->makeAddress($order->id);

        //Assign Serial Number
        $this->serial->assignSerial($order->id);
    }
}
