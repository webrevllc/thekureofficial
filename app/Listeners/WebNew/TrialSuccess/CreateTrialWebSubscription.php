<?php

namespace App\Listeners\WebNew\TrialSuccess;

use App\Events\WebNew\TrialOrderComplete;
use App\Order;
use App\Subscription;
use App\Total;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateTrialWebSubscription
{

    /**
     * @var Order
     */
    private $order;
    /**
     * @var Total
     */
    private $total;

    /**
     * Create the event listener.
     *
     * @param Order $order
     * @param Total $total
     */
    public function __construct( Order $order, Total $total)
    {
        $this->order = $order;
        $this->total = $total;
    }


    public function handle(TrialOrderComplete $event)
    {
        $order = Order::where('invoice_number', $event->invoice_number)->first();
        $subscription = new Subscription;
        $subscription->unique_id = $event->invoice_number;
        $subscription->status = 'active';
        $subscription->user_id = $event->user->id;
        $subscription->products = json_encode($event->products);
        $subscription->amount = $event->lead->total;
        $subscription->offer = $event->lead->offer;

        $order->subscription()->save($subscription);
        $this->total->updateTotals('web_trial', $event->lead->total);

        $order->subscription_id = $subscription->id;
        $order->save();

    }

}
