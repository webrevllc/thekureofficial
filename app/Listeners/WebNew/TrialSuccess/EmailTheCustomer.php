<?php

namespace App\Listeners\WebNew\TrialSuccess;

use App\Emails\WelcomeEmail;
use App\Events\WebNew\TrialOrderComplete;
use App\Order;
use App\Subscription;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Snowfire\Beautymail\Beautymail;

class EmailTheCustomer
{

    /**
     * Handle the event.
     *
     * @param  TrialOrderComplete  $event
     * @return void
     */
    public function handle(TrialOrderComplete $event)
    {
        (new WelcomeEmail)->sendTo($event->user);
    }
}
