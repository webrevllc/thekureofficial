<?php

namespace App\Listeners\WebNew\TrialSuccess;

use App\Events\WebNew\TrialOrderComplete;
use App\Referral;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateReferral
{

    /**
     * Handle the event.
     *
     * @param  TrialOrderComplete  $event
     * @return void
     */
    public function handle(TrialOrderComplete $event)
    {
        if($event->lead->source != 'mel'){
            $r = Referral::where('referring_transaction_number', $event->lead->source)->first();
            $r->completed_referral_transaction_number = $event->invoice_number;
            $r->save();
        }
    }
}
