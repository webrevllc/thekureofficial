<?php

namespace App\Listeners\WebNew\TrialSuccess;

use App\Emails\Bosses;
use App\Emails\EmailBosses;
use App\Events\WebNew\TrialOrderComplete;
use App\Order;
use App\Subscription;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Snowfire\Beautymail\Beautymail;

class EmailNotificationToBosses
{
    /**
     * @var Beautymail
     */
    private $beautymail;
    /**
     * @var Bosses
     */
    private $bosses;

    /**
     * Create the event listener.
     *
     * @param Beautymail $beautymail
     * @param Bosses $bosses
     */
    public function __construct(Beautymail $beautymail, Bosses $bosses)
    {

        $this->bosses = $bosses;
    }

    /**
     * Handle the event.
     *
     * @param  TrialOrderComplete  $event
     * @return void
     */
    public function handle(TrialOrderComplete $event)
    {

        $order = Order::with('subscription')->where('invoice_number', $event->invoice_number)->first();

        $b = $this->bosses->returnBosses();
        foreach($b as $boss){
            (new EmailBosses)->withData([
                'status' => 'Trial',
                'type' => 'Web',
                'order' => $order,
                'subscription' => $order->subscription,
                'orderInfo' => $event->lead])
                ->sendTo($boss);
        }
    }
}
