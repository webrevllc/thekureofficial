<?php

namespace App\Listeners\Refund\Trial;

use App\Emails\TrialCancelledEmail;
use App\Events\Refunds\TrialCancelled;
use App\User;

class EmailUser
{
    /**
     * Handle the event.
     *
     * @param  TrialCancelled  $event
     * @return void
     */
    public function handle(TrialCancelled $event)
    {
        $user = User::find($event->subscription->user_id);
        (new TrialCancelledEmail)->withData(['subscription' => $event->subscription])->sendTo($user);
    }
}
