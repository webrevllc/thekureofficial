<?php

namespace App\Listeners\Refund\Trial;

use App\Events\Refunds\TrialCancelled;
use App\Refund;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateRefund
{
    /**
     * Handle the event.
     *
     * @param  TrialCancelled  $event
     * @return void
     */
    public function handle(TrialCancelled $event)
    {
        $r = new Refund;
        $r->order_id = $event->subscription->order_id;
        $r->amount = $event->subscription->amount;
        $r->full = true;
        $r->offer = $event->subscription->offer;
        $r->transactionId = 'trial';
        $r->save();
    }
}
