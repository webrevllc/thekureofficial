<?php

namespace App\Listeners\Refund\Full;

use App\Events\Refunds\FullRefund;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Snowfire\Beautymail\Beautymail;

class EmailCustomer
{
    /**
     * @var Beautymail
     */
    private $beautymail;

    /**
     * Create the event listener.
     *
     * @param Beautymail $beautymail
     */
    public function __construct(Beautymail $beautymail)
    {
        //
        $this->beautymail = $beautymail;
    }

    /**
     * Handle the event.
     *
     * @param  FullRefund  $event
     * @return void
     */
    public function handle(FullRefund $event)
    {
        $email = $event->order->user->email;
        $name = $event->order->user->name;

        $refundInfo = new \stdClass();
        $refundInfo->amount = $event->refundAmount;
        $refundInfo->by = \Auth::user()->name;

        $this->beautymail->send('emails.fullrefund', ['refundInfo' => $refundInfo, 'name' => $name], function ($m) use ($email, $name){
//            $m->to('srosenthal82@gmail.com', 'Shane D Rosenthal');
            $m->to($email, $name);
            $m->subject('The Kure - Refund Issued');
        });
    }
}
