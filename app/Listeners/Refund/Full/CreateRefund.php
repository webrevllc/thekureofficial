<?php

namespace App\Listeners\Refund\Full;

use App\Events\Refunds\FullRefund;
use App\Refund;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateRefund
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FullRefund  $event
     * @return void
     */
    public function handle(FullRefund $event)
    {
        $order = $event->order;
        $refund = new Refund;
        $refund->amount = $event->refundAmount;
        $refund->refunded_by = \Auth::user()->name;
        $refund->full = true;
        $refund->offer = $order->offer;
        $refund->transactionId = $event->approvedRefundTransactionID;
        $order->refunds()->save($refund);
    }
}
