<?php

namespace App\Listeners\Refund\Full;

use App\Events\Refunds\FullRefund;
use App\Order;
use App\Subscription;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateSubscription
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FullRefund  $event
     * @return void
     */
    public function handle(FullRefund $event)
    {
        $subscription = Subscription::find($event->subscriptionID);
        $subscription->status = 'cancelled';
        $subscription->next_process_date = Carbon::yesterday();
        $subscription->save();
    }
}
