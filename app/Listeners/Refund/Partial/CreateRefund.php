<?php

namespace App\Listeners\Refund\Partial;

use App\Events\Refunds\PartialRefund;
use App\Refund;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateRefund
{


    /**
     * Handle the event.
     *
     * @param  PartialRefund  $event
     * @return void
     */
    public function handle(PartialRefund $event)
    {
        $order = $event->order;
        $refund = new Refund;
        $refund->amount = $event->refundAmount;
        $refund->reason = $event->reason;
        $refund->transactionId = $event->approvedRefundTransactionID;
        $refund->refunded_by = \Auth::user()->name;
        $refund->full = false;
        $refund->offer = $order->offer;
        $refund->order_id = $order->id;
        $refund->save();
    }
}
