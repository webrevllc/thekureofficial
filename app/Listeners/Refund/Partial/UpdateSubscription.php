<?php

namespace App\Listeners\Refund\Partial;

use App\Events\Refunds\PartialRefund;
use App\Subscription;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateSubscription
{


    /**
     * Handle the event.
     *
     * @param  PartialRefund  $event
     * @return void
     */
    public function handle(PartialRefund $event)
    {
        $subscription = Subscription::find($event->subscriptionId);
        $subscription->products = json_encode($event->products);
        $subscription->amount = $event->newOrderTotal;
        $subscription->save();
    }
}
