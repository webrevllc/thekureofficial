<?php

namespace App\Listeners\Refund\Partial;

use App\Emails\RefundEmail;
use App\Events\Refunds\PartialRefund;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Snowfire\Beautymail\Beautymail;

class EmailCustomer
{
    /**
     * @var Beautymail
     */
    private $beautymail;

    /**
     * Create the event listener.
     *
     * @param Beautymail $beautymail
     */
    public function __construct(Beautymail $beautymail)
    {
        //
        $this->beautymail = $beautymail;
    }

    /**
     * Handle the event.
     *
     * @param  PartialRefund  $event
     * @return void
     */
    public function handle(PartialRefund $event)
    {
//        $email = $event->order->user->email;
//        $name = $event->order->user->name;
//
//        $refundInfo = new \stdClass();
//        $refundInfo->reason = $event->reason;
//        $refundInfo->amount = $event->refundAmount;
//        $refundInfo->by = \Auth::user()->name;
//
//        $this->beautymail->send('emails.refund', ['refundInfo' => $refundInfo, 'name' => $name], function ($m) use ($email, $name){
////            $m->to('srosenthal82@gmail.com', 'Shane D Rosenthal');
//            $m->to($email, $name);
//            $m->subject('The Kure - Refund Issued!');
//        });

        (new RefundEmail)->withData(['amount' => $event->refundAmount])->sendTo($event->user);
    }
}
