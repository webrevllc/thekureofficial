<?php

namespace App\Listeners\Refund\Partial;

use App\Events\Refunds\PartialRefund;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MakeChangeOrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PartialRefund  $event
     * @return void
     */
    public function handle(PartialRefund $event)
    {
        //
    }
}
