<?php

namespace App\Listeners\Support;

use App\Emails\Support;
use App\Events\Question;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Snowfire\Beautymail\Beautymail;

class NewQuestion
{
    /**
     * @var Beautymail
     */
    private $beautymail;

    /**
     * Create the event listener.
     *
     * @param Beautymail $beautymail
     */
    public function __construct(Beautymail $beautymail)
    {
        //
        $this->beautymail = $beautymail;
    }

    /**
     * Handle the event.
     *
     * @param  Question  $event
     * @return void
     */
    public function handle(Question $event)
    {
//        dd($event);
        $name = $event->request->name;
        $email = $event->request->email;
        $phone = $event->request->phone;
        $message = $event->request->message;
//        (new Support)->withData([$name, $email, $phone, $message])->sendTo(User::find(1));
        (new Support)->withData([$name, $email, $phone, $message])->sendTo(User::find(2));
        (new Support)->withData([$name, $email, $phone, $message])->sendTo(User::find(5));
//        $this->beautymail->send('emails.support.csr', ['data' => $event->request], function ($m) use ($email){
//            $m->from($email);
////            $m->to('questions@thekure.com');
////            $m->cc('mel@thekure.com');
//            $m->to('srosenthal82@gmail.com');
//            $m->subject('The Kure - Customer Question!');
//        });
    }
}
