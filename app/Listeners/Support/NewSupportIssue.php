<?php

namespace App\Listeners\Support;

use App\Events\SupportIssue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Snowfire\Beautymail\Beautymail;

class NewSupportIssue
{
    /**
     * @var Beautymail
     */
    private $beautymail;

    /**
     * Create the event listener.
     *
     * @param Beautymail $beautymail
     */
    public function __construct(Beautymail $beautymail)
    {
        //
        $this->beautymail = $beautymail;
    }

    /**
     * Handle the event.
     *
     * @param  SupportIssue  $event
     * @return void
     */
    public function handle(SupportIssue $event)
    {
        $email = $event->request->email;
        $this->beautymail->send('emails.support.csr', ['data' => $event->request], function ($m) use ($email){
            $m->from($email);
            $m->to('support@thekure.com');
            $m->subject('The Kure - Customer Support!');
        });
    }
}
