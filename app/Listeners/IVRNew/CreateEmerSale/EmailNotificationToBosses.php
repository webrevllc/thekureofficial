<?php

namespace App\Listeners\IVRNew\CreateEmerSale;

use App\Emails\Bosses;
use App\Emails\SupportNotification;
use App\Events\IVRNew\CreateEmerSale;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailNotificationToBosses
{
    /**
     * @var Bosses
     */
    private $bosses;

    /**
     * Create the event listener.
     *
     * @param Bosses $bosses
     */
    public function __construct(Bosses $bosses)
    {
        $this->bosses = $bosses;
    }

    /**
     * Handle the event.
     *
     * @param  CreateEmerSale  $event
     * @return void
     */
    public function handle(CreateEmerSale $event)
    {
        $name = $event->user->name;
        $email = $event->user->email;
        $phone = $event->user->phone;
        $quantity = $event->emer->quantity;

        $b = $this->bosses->returnBosses();
        foreach ($b as $boss) {
            (new SupportNotification())->withData(["name" => $name, "email" => $email, "phone" => $phone, "quantity" => $quantity])->sendTo($boss);
        }
        (new SupportNotification())->withData(["name" => $name, "email" => $email, "phone" => $phone, "quantity" => $quantity])->sendTo(User::find(5));
    }
}
