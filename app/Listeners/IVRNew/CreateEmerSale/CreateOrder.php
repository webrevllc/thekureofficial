<?php

namespace App\Listeners\IVRNew\CreateEmerSale;

use App\Events\IVRNew\CreateEmerSale;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateOrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreateEmerSale  $event
     * @return void
     */
    public function handle(CreateEmerSale $event)
    {
        $order = new Order;
        $order->amount = $event->emerTotal;
        $order->invoice_number = $event->invoice_number;
        $order->status = 'paid';
        $order->user_id = $event->user->id;
        $order->source = 'ivr';
        $order->hasEMER = true;
        $order->shipped = true;
        $order->printed = true;
        $order->products = json_encode($event->emer);
        $order->auth_transaction = $event->transaction_id;
        $order->type = 'aim';
        $order->offer = env('offer');
        $order->save();
    }
}
