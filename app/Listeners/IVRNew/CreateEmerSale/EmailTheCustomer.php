<?php

namespace App\Listeners\IVRNew\CreateEmerSale;

use App\Events\IVRNew\CreateEmerSale;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailTheCustomer
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreateEmerSale  $event
     * @return void
     */
    public function handle(CreateEmerSale $event)
    {
        //
    }
}
