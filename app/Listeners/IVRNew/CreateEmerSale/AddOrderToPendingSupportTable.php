<?php

namespace App\Listeners\IVRNew\CreateEmerSale;

use App\Events\IVRNew\CreateEmerSale;
use App\Order;
use App\PendingSupport;


class AddOrderToPendingSupportTable
{
    /**
     * Handle the event.
     *
     * @param  CreateEmerSale  $event
     * @return void
     */
    public function handle(CreateEmerSale $event)
    {
        $order = Order::where('invoice_number', $event->invoice_number)->first();
        $support = new PendingSupport;
        $support->order_id = $order->id;
        $support->name = $event->user->name;
        $support->phone = $event->user->phone;
        $support->quantity = $event->emer->quantity;
        $support->save();
    }
}
