<?php

namespace App\Listeners\IVRNew\DeclinedIVROrder;

use App\Emails\DeclinedCard;
use App\Events\IVRNew\DeclinedIVROrder;
use App\Http\Controllers\Traits\MakesOrderObjects;
use App\IVRLead;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateIVRLead
{
    use MakesOrderObjects;
    /**
     * Handle the event.
     *
     * @param  DeclinedIVROrder  $event
     * @return void
     */
    public function handle(DeclinedIVROrder $event, Order $order)
    {
        $l = new IVRLead;

        $l->name = $event->orderInfo['Customer']['First_Name'] . ' ' . $event->orderInfo['Customer']['Last_Name'];
        $l->email = $event->orderInfo['Customer']['Email'];
        $l->address = $event->orderInfo['Customer']['Address_1'] . ' ' . $event->orderInfo['Customer']['Address_2'];
        $l->city = $event->orderInfo['Customer']['City'];
        $l->state = $event->orderInfo['Customer']['State'];
        $l->zip = $event->orderInfo['Customer']['Zip'];
        $l->country = $event->orderInfo['Customer']['Country'];
        $l->phone = $event->orderInfo['Customer']['Phone'];
//        $l->reason = $event->orderInfo['Billing']['DECLINED_REASON'];
        $l->offer = env('offer');

        $event->orderInfo = json_decode(json_encode($event->orderInfo));
        $productsOrdered = $this->makeFullProducts($event->orderInfo->Products);

        $l->products = json_encode($productsOrdered);
        $l->amount = $order->totalNoUser($l->products);
        $l->save();
        (new DeclinedCard)->withData(["reason" => $l->reason])->sendTo($l);
    }
}
