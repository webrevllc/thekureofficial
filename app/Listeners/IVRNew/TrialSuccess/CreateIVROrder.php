<?php

namespace App\Listeners\IVRNew\TrialSuccess;

use App\Events\IVRNew\TrialIVROrder;
use App\Order;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateIVROrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TrialIVROrder  $event
     * @return void
     */
    public function handle(TrialIVROrder $event)
    {
        $order = new Order;
        $order->has_five_year = $event->orderInfo->Billing->HAS_FIVE_YEAR == 'true' ? true : false;
        $order->has_five_year_date = $event->orderInfo->Billing->HAS_FIVE_YEAR == 'true' ? Carbon::now()->addYears(5)->addMonths(3)->startOfDay() : null;
        $order->amount = 'Free Trial';
        $order->invoice_number = $event->invoice_number;
        $order->status = 'trial';
        $order->user_id = $event->user->id;
        $order->source = 'ivr';
        $order->products = json_encode($event->productsOrdered);
        $order->type = 'cim';
        $order->unique_ivr = $event->orderInfo->Customer->IVR_ID;
        $order->offer = env('offer');
        $order->original_datetime = $event->orderInfo->Billing->ORIGINAL_DATETIME;
        $order->save();
    }
}
