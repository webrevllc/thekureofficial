<?php

namespace App\Listeners\IVRNew\TrialSuccess;

use App\Events\IVRNew\TrialIVROrder;
use App\Order;
use App\Subscription;
use App\Total;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateTrialIVRSubscription
{
    /**
     * @var Total
     */
    private $total;
    /**
     * @var Order
     */
    private $order;

    /**
     * Create the event listener.
     *
     * @param Order $order
     * @param Total $total
     */
    public function __construct(Order $order, Total $total)
    {
        $this->total = $total;
        $this->order = $order;
    }

    /**
     * Handle the event.
     *
     * @param  TrialIVROrder  $event
     * @return void
     */
    public function handle(TrialIVROrder $event)
    {
        $order = Order::where('invoice_number', $event->invoice_number)->first();
        $user = User::where('email', '=', $event->user->email)->first();
        $subscription = new Subscription;
        $subscription->unique_id = $event->invoice_number;
        $subscription->status = 'active';
//        $subscription->next_process_date = Carbon::now()->addDays(30);
        $subscription->user_id = $user->id;
        $subscription->products = json_encode($event->productsOrdered);
        $subscription->amount = $this->order->total($event->productsOrdered, $event->user);
        $subscription->offer = env('offer');
        $order->subscription()->save($subscription);
        $this->total->updateTotals('ivr_trial', $subscription->amount);

        $order->subscription_id = $subscription->id;
        $order->save();
    }

    public function getSubDate($orderInfo)
    {
        if(property_exists($orderInfo->Billing, 'HWMON') || property_exists($orderInfo->Billing, 'PCOS')){
            return Carbon::now()->addDays(455);
        }else{
            return $orderInfo->Billing->HAS_FIVE_YEAR == 'true' ? Carbon::now()->addYears(5)->addMonths(3) : Carbon::now()->addDays(455);
        }
    }
}
