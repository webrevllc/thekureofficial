<?php

namespace App\Listeners\IVRNew\TrialSuccess;

use App\Contracts\Invisacloud;
use App\Events\IVRNew\TrialIVROrder;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MakeInvisacloudUser
{
    /**
     * @var Invisacloud
     */
    private $invisacloud;

    /**
     * Create the event listener.
     *
     * @param Invisacloud $invisacloud
     */
    public function __construct(Invisacloud $invisacloud)
    {
        //
        $this->invisacloud = $invisacloud;
    }

    /**
     * Handle the event.
     *
     * @param  TrialIVROrder  $event
     * @return void
     */
    public function handle(TrialIVROrder $event)
    {
        $this->invisacloud->makeNewUser($event->user, $event->invoice_number, 'ivr');
    }
}
