<?php

namespace App\Listeners\IVRNew\TrialSuccess;

use App\Emails\WelcomeEmail;
use App\Events\IVRNew\TrialIVROrder;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailTheCustomer
{

    /**
     * Handle the event.
     *
     * @param  TrialIVROrder  $event
     * @return void
     */
    public function handle(TrialIVROrder $event)
    {
        (new WelcomeEmail)->sendTo($event->user);
    }
}
