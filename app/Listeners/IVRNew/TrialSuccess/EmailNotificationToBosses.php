<?php

namespace App\Listeners\IVRNew\TrialSuccess;

use App\Emails\Bosses;
use App\Emails\EmailBosses;
use App\Events\IVRNew\TrialIVROrder;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailNotificationToBosses
{
    /**
     * @var Bosses
     */
    private $bosses;

    /**
     * Create the event listener.
     *
     * @param Bosses $bosses
     */
    public function __construct(Bosses $bosses)
    {
        $this->bosses = $bosses;
    }

    /**
     * Handle the event.
     *
     * @param  TrialIVROrder  $event
     * @return void
     */
    public function handle(TrialIVROrder $event)
    {
        $order = Order::with('subscription')->where('invoice_number', $event->invoice_number)->first();

        $b = $this->bosses->returnBosses();
        foreach($b as $boss){
            (new EmailBosses)->withData([
                'status' => 'Trial',
                'type' => 'IVR',
                'order' => $order,
                'subscription' => $order->subscription,
                'orderInfo' => $event->orderInfo])
                ->sendTo($boss);
        }
    }
}
