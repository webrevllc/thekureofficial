<?php

namespace App\Listeners\IVRNew\PaidSuccess;

use App\Emails\WelcomeEmail;
use App\Events\IVRNew\PaidIVROrder;
use App\Events\IVRNew\TrialIVROrder;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailTheCustomer
{

    /**
     * Handle the event.
     *
     * @param  TrialIVROrder  $event
     * @return void
     */
    public function handle(PaidIVROrder $event)
    {
        (new WelcomeEmail)->sendTo($event->user);
    }
}
