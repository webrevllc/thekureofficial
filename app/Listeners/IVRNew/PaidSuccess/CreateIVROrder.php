<?php

namespace App\Listeners\IVRNew\PaidSuccess;

use App\Events\IVRNew\PaidIVROrder;
use App\Order;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateIVROrder
{

    public function handle(PaidIVROrder $event)
    {
        $order = new Order;
        $order->has_five_year = $event->orderInfo->Billing->HAS_FIVE_YEAR == 'true' ? true : false;
        $order->has_five_year_date = $event->orderInfo->Billing->HAS_FIVE_YEAR == 'true' ? Carbon::now()->addYears(5)->addMonths(3)->startOfDay() : null;
        $order->amount = $event->total;
        $order->invoice_number = $event->invoice_number;
        $order->status = $event->twoPay ? '2pay' : 'paid';
        $order->user_id = $event->user->id;
        $order->auth_transaction = $event->transactionId;
        $order->source = 'ivr';
        $order->products = json_encode($event->productsOrdered);
        $order->type = 'aim';
        $order->unique_ivr = $event->orderInfo->Customer->IVR_ID;
        $order->offer = env('offer');
        //Added 1/5 - ensures we have the right data to process refunds
        $order->last_4 = $event->orderInfo->Billing->LAST_4;
        $order->expiration = $event->orderInfo->Billing->EXP;
        $order->original_datetime = $event->orderInfo->Billing->ORIGINAL_DATETIME;
        $order->save();
    }
}
