<?php

namespace App\Listeners\IVRNew\PaidSuccess;

use App\Emails\Bosses;
use App\Emails\EmailBosses;
use App\Events\IVRNew\PaidIVROrder;
use App\Events\IVRNew\TrialIVROrder;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailNotificationToBosses
{
    /**
     * @var Bosses
     */
    private $bosses;

    /**
     * Create the event listener.
     *
     * @param Bosses $bosses
     */
    public function __construct(Bosses $bosses)
    {
        $this->bosses = $bosses;
    }

    /**
     * Handle the event.
     *
     * @param  TrialIVROrder  $event
     * @return void
     */
    public function handle(PaidIVROrder $event)
    {
        $order = Order::with('subscription')->where('invoice_number', $event->invoice_number)->first();

        $b = $this->bosses->returnBosses();
        foreach($b as $boss){
            (new EmailBosses)->withData([
                'status' => 'Paid',
                'type' => 'IVR',
                'order' => $order,
                'subscription' => $order->subscription,
                'orderInfo' => $event->orderInfo])
                ->sendTo($boss);
        }


//        'order_type' => $type,
//            'amount' => $subscription->amount,
//            'city' => 'Mount Dora',
//            'state' => 'FL',
//            'source' => 'Pete',
//            'name' => 'S. Rosenthal'
    }
}
