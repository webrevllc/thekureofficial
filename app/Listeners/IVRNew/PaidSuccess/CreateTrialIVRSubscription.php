<?php

namespace App\Listeners\IVRNew\PaidSuccess;

use App\Events\IVRNew\PaidIVROrder;
use App\Order;
use App\Subscription;
use App\Total;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateTrialIVRSubscription
{
    /**
     * @var Total
     */
    private $total;
    /**
     * @var Order
     */
    private $order;

    /**
     * Create the event listener.
     *
     * @param Order $order
     * @param Total $total
     */
    public function __construct(Order $order, Total $total)
    {
        $this->total = $total;
        $this->order = $order;
    }


    /**
     * @param PaidIVROrder $event
     */
    public function handle(PaidIVROrder $event)
    {
        $order = Order::where('invoice_number', $event->invoice_number)->first();
        $user = User::where('email', '=', $event->user->email)->first();
        $subscription = new Subscription;
        $subscription->unique_id = $event->invoice_number;
        $subscription->status = 'active';
//        $subscription->next_process_date = Carbon::now()->addDays(455);
        $subscription->next_process_date = $this->getSubDate($event->orderInfo);
        $subscription->user_id = $user->id;
        $subscription->products = json_encode($event->productsOrdered);
//        $total = $this->order->total($event->productsOrdered, $event->user);
        $subscription->amount = $this->makeTwoPayTotal($event->productsOrdered, $event->twoPay);
        $subscription->offer = env('offer');
        $order->subscription()->save($subscription);
        $this->total->updateTotals('ivr_paid', $subscription->amount);
        $order->subscription_id = $subscription->id;
        $order->save();
    }

    public function makeTotal($productsOrdered, $total)
    {
        $q = 0;
        foreach($productsOrdered as $p)
        {
            while($p->id == 1) {
                $q = $p->quantity;
            }
        }
        $amount = $q * 49.95;
        return ($total - $amount);
    }

    public function makeTwoPayTotal($productsOrdered, $twoPay)
    {
        if(!$twoPay){
            return 0;
        }else{
            $price = [];
            foreach($productsOrdered as $p){
                if($p->ivr_code == "KURE" || $p->ivr_code == "KUREA")
                {
                    $price[] = 2495 * $p->quantity;
                }
                elseif($p->api_code == "HWMON")
                {
                    $price[] = 495 * $p->quantity;
                }
            }
            return number_format(array_sum($price)/100, 2) ;
        }

    }

    public function getSubDate($orderInfo)
    {
        if(property_exists($orderInfo->Billing, 'HWMON') || property_exists($orderInfo->Billing, 'PCOS')){
            return Carbon::now()->addDays(455);
        }else{
            return $orderInfo->Billing->HAS_FIVE_YEAR == 'true' ? Carbon::now()->addYears(5)->addMonths(3) : Carbon::now()->addDays(455);
        }
    }
}
