<?php

namespace App\Listeners\IVRNew\PaidSuccess;

use App\Contracts\Invisacloud;
use App\Events\IVRNew\PaidIVROrder;
use App\Events\IVRNew\TrialIVROrder;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MakeInvisacloudProduct
{
    /**
     * @var Invisacloud
     */
    private $invisacloud;

    /**
     * Create the event listener.
     *
     * @param Invisacloud $invisacloud
     */
    public function __construct(Invisacloud $invisacloud)
    {
        $this->invisacloud = $invisacloud;
    }

    /**
     * Handle the event.
     *
     * @param  TrialIVROrder  $event
     * @return void
     */
    public function handle(PaidIVROrder $event)
    {
        $order = Order::whereInvoice_number($event->invoice_number)->first();
        $this->invisacloud->uploadOrder($order);
    }
}
