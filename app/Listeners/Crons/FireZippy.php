<?php

namespace App\Listeners\Crons;

use App\Events\Crons\PDFComplete;
use Artisan;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class FireZippy
{

    /**
     * Handle the event.
     *
     * @param  PDFComplete  $event
     * @return void
     */
    public function handle(PDFComplete $event)
    {
        Artisan::call('make:zippy');
    }
}
