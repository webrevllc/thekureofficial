<?php

namespace App\Listeners\Crons;

use App\Events\Crons\CSVComplete;
use Artisan;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class FirePDF
{

    /**
     * Handle the event.
     *
     * @param  CSVComplete  $event
     * @return void
     */
    public function handle(CSVComplete $event)
    {
        Artisan::call('make:pdf');
    }
}
