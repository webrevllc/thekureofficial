<?php

namespace App\Listeners\Crons;

use App\Emails\Bosses;
use App\Emails\ShippingReportEmail;
use App\Events\Crons\ZippyComplete;
use App\Order;
use App\Serial;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class NotifyComplete
{
    /**
     * @var Bosses
     */
    private $bosses;

    /**
     * Create the event listener.
     *
     * @param Bosses $bosses
     */
    public function __construct(Bosses $bosses)
    {
        $this->bosses = $bosses;
    }

    /**
     * Handle the event.
     *
     * @param  ZippyComplete  $event
     * @return void
     */
    public function handle(ZippyComplete $event)
    {
        $serials = Serial::where('name', '!=', 'Jeffrey Rosenthal')
            ->where('name', '!=', 'EDDIE TAYLOR')
            ->where('name', '!=', 'Ugo Spezza')
            ->where('name', '!=', 'peter spezza')
            ->whereBetween('assigned_date',[Carbon::yesterday()->startOfDay(), Carbon::today()->startOfDay()])->count();
        $orders = Order::with('user', 'address', 'subscription')
            ->whereBetween('created_at',[Carbon::yesterday()->startOfDay(), Carbon::yesterday()->endOfDay()])
            ->where('source', '!=', 'renewal')
            ->count();
        $yesterday = date('F j, Y', strtotime(Carbon::yesterday()));

        (new ShippingReportEmail)->withData(['date' => $yesterday, 'serials' => $serials, 'orders' => $orders])->sendTo(User::find(3));
//        foreach($this->bosses->shipping() as $user){
//            (new ShippingReportEmail)->withData(['date' => $yesterday, 'serials' => $serials, 'orders' => $orders])->sendTo($user);
//        }
    }
}
