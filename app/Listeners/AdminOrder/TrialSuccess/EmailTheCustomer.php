<?php

namespace App\Listeners\AdminOrder\TrialSuccess;

use App\Emails\WelcomeEmail;
use App\Events\AdminOrder\UserFreeTrialOrder;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailTheCustomer
{


    /**
     * Handle the event.
     *
     * @param  UserFreeTrialOrder  $event
     * @return void
     */
    public function handle(UserFreeTrialOrder $event)
    {
        (new WelcomeEmail)->sendTo($event->user);
    }
}
