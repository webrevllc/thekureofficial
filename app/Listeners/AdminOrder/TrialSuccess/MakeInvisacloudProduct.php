<?php

namespace App\Listeners\AdminOrder\TrialSuccess;

use App\Contracts\Invisacloud;
use App\Events\AdminOrder\UserFreeTrialOrder;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MakeInvisacloudProduct
{
    /**
     * @var Invisacloud
     */
    private $invisacloud;

    /**
     * Create the event listener.
     *
     * @param Invisacloud $invisacloud
     */
    public function __construct(Invisacloud $invisacloud)
    {
        //
        $this->invisacloud = $invisacloud;
    }

    /**
     * Handle the event.
     *
     * @param  UserFreeTrialOrder  $event
     * @return void
     */
    public function handle(UserFreeTrialOrder $event)
    {
        $order = Order::where('invoice_number', '=', $event->invoice_number)->first();
        $this->invisacloud->uploadOrder($order);
    }
}
