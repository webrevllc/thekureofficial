<?php

namespace App\Listeners\AdminOrder\TrialSuccess;

use App\Events\AdminOrder\UserFreeTrialOrder;
use App\Order;
use App\Subscription;
use App\Total;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateTrialWebSubscription
{
    /**
     * @var Order
     */
    private $order;
    /**
     * @var Total
     */
    private $total;

    /**
     * Create the event listener.
     *
     * @param Order $order
     * @param Total $total
     */
    public function __construct( Order $order, Total $total)
    {
        $this->order = $order;
        $this->total = $total;
    }

    /**
     * Handle the event.
     *
     * @param  UserFreeTrialOrder  $event
     * @return void
     */
    public function handle(UserFreeTrialOrder $event)
    {
        $order = Order::where('invoice_number', $event->invoice_number)->first();
        $subscription = new Subscription;
        $subscription->unique_id = $event->invoice_number;
        $subscription->status = 'active';
        $subscription->user_id = $event->user->id;
        $subscription->products = json_encode($event->products);
        $subscription->amount = $event->total;
        $order->subscription()->save($subscription);
        $this->total->updateTotals('web_trial', $event->total);
    }
}
