<?php

namespace App\Listeners\AdminOrder\TrialSuccess;

use App\Contracts\Invisacloud;
use App\Events\AdminOrder\UserFreeTrialOrder;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MakeInvisacloudUser
{
    /**
     * @var Invisacloud
     */
    private $invisacloud;

    /**
     * Create the event listener.
     *
     * @param Invisacloud $invisacloud
     */
    public function __construct(Invisacloud $invisacloud)
    {
        $this->invisacloud = $invisacloud;
    }

    /**
     * Handle the event.
     *
     * @param  UserFreeTrialOrder  $event
     * @return void
     */
    public function handle(UserFreeTrialOrder $event)
    {
        $this->invisacloud->makeNewUser($event->user, $event->invoice_number, 'trial');
    }
}
