<?php

namespace App\Listeners\AdminOrder\TrialSuccess;

use App\Events\AdminOrder\UserFreeTrialOrder;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateTrialWebOrder
{
    /**
     * Handle the event.
     *
     * @param  UserFreeTrialOrder  $event
     * @return void
     */
    public function handle(UserFreeTrialOrder $event)
    {
        $order = new Order;
        $order->amount = 'Free Trial';
        $order->invoice_number = $event->invoice_number;
        $order->status = 'trial';
        $order->user_id = $event->user->id;
        $order->source = 'admin';
        $order->products = json_encode($event->products);
        $order->auth_transaction = '';
        $order->type = 'cim';
        $order->save();
    }
}
