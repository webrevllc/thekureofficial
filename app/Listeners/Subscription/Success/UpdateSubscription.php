<?php

namespace App\Listeners\Subscription\Success;

use App\Contracts\Invisacloud;
use App\Events\Subscription\SuccessfulSubscriptionRenewal;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateSubscription
{
    /**
     * @var Invisacloud
     */
    private $invisacloud;

    /**
     * UpdateSubscription constructor.
     * @param Invisacloud $invisacloud
     */
    public function __construct(Invisacloud $invisacloud)
    {

        $this->invisacloud = $invisacloud;
    }

    /**
     * Handle the event.
     *
     * @param  SuccessfulSubscriptionRenewal  $event
     * @return void
     */
    public function handle(SuccessfulSubscriptionRenewal $event)
    {
        $event->subscription->next_process_date = Carbon::now()->addYear(1);
        $products = removeNonRecurring($event->subscription);
        $event->subscription->products = $products;
        $event->subscription->converted_trial = true;
        $event->subscription->amount = calculateNewTotal(json_decode($products));
        $event->subscription->save();

        $this->invisacloud->subscriptionRenewal($event->subscription, 365);
    }
}
