<?php

namespace App\Listeners\Subscription\Success;

use App\Emails\SubscriptionRenewalSuccess;
use App\Events\Subscription\SuccessfulSubscriptionRenewal;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendSuccessEmails
{

    /**
     * Handle the event.
     *
     * @param  SuccessfulSubscriptionRenewal  $event
     * @return void
     */
    public function handle(SuccessfulSubscriptionRenewal $event)
    {
        (new SubscriptionRenewalSuccess)->withData(['subscription' => $event->subscription])->sendTo($event->subscription->user);
    }
}
