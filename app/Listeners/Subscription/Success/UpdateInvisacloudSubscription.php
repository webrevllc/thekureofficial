<?php

namespace App\Listeners\Subscription\Success;

use App\Contracts\Invisacloud;
use App\Events\Subscription\SuccessfulSubscriptionRenewal;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateInvisacloudSubscription
{
    /**
     * @var Invisacloud
     */
    private $invisacloud;

    /**
     * Create the event listener.
     *
     * @param Invisacloud $invisacloud
     */
    public function __construct(Invisacloud $invisacloud)
    {
        $this->invisacloud = $invisacloud;
    }

    /**
     * Handle the event.
     *
     * @param  SuccessfulSubscriptionRenewal  $event
     * @return void
     */
    public function handle(SuccessfulSubscriptionRenewal $event)
    {
        $subscription = $event->subscription;
        $this->invisacloud->subscriptionRenewal($subscription, 365);
    }
}
