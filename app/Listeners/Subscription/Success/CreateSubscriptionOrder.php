<?php

namespace App\Listeners\Subscription\Success;

use App\Contracts\Invisacloud;
use App\Events\Subscription\SuccessfulSubscriptionRenewal;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateSubscriptionOrder
{

    /**
     * Handle the event.
     *
     * @param  SuccessfulSubscriptionRenewal  $event
     * @return void
     */
    public function handle(SuccessfulSubscriptionRenewal $event)
    {
        $order = new Order;
        $order->invoice_number = $event->subscription->unique_id;
        $order->amount = $event->subscription->amount;
        $order->status = 'paid';
        $order->auth_transaction = $event->transactionId;
        $order->source = 'renewal';
        $order->products = $event->subscription->products;
        $order->type = 'cim';
        $order->shipped = 1;
        $order->user_id = $event->subscription->user->id;
        $order->shipped_by = 'RENEWAL ORDER';
        $order->save();

    }
}
