<?php

namespace App\Listeners\Subscription\CancelFailed;

use App\Emails\CancelledSubscription;
use App\Events\Subscription\FailedCancelled;
use App\User;

class EmailUser
{

    /**
     * Handle the event.
     *
     * @param  FailedCancelled  $event
     * @return void
     */
    public function handle(FailedCancelled $event)
    {
        $user = User::find($event->subscription->user_id);
        (new CancelledSubscription)->withData(['order_id' => $event->subscription->unique_id])->sendTo($user);
    }
}
