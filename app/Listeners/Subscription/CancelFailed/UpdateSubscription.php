<?php

namespace App\Listeners\Subscription\CancelFailed;

use App\Events\Subscription\FailedCancelled;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateSubscription
{

    /**
     * Handle the event.
     *
     * @param  FailedCancelled  $event
     * @return void
     */
    public function handle(FailedCancelled $event)
    {
        $event->subscription->status = 'cancelled';
        $event->subscription->next_process_date = Carbon::yesterday()->startOfDay();
        $event->subscription->save();
    }
}
