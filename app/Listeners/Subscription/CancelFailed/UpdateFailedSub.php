<?php

namespace App\Listeners\Subscription\CancelFailed;

use App\Events\Subscription\FailedCancelled;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateFailedSub
{

    /**
     * Handle the event.
     *
     * @param  FailedCancelled  $event
     * @return void
     */
    public function handle(FailedCancelled $event)
    {
        $event->failed->processed = true;
        $event->failed->cancelled = true;
        $event->failed->save();
    }
}
