<?php

namespace App\Listeners\Subscription\Modified;

use App\Emails\SubscriptionModified;
use App\Events\Subscription\SubscriptionModifiedEvent;

class EmailCustomer
{

    /**
     * Handle the event.
     *
     * @param  SubscriptionModified  $event
     * @return void
     */
    public function handle(SubscriptionModifiedEvent $event)
    {
        $user = $event->subscription->user;
        (new SubscriptionModified)->sendTo($user);
    }
}
