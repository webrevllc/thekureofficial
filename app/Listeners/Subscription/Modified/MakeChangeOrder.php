<?php

namespace App\Listeners\Subscription\Modified;

use App\ChangeOrder;
use App\Events\Subscription\SubscriptionModifiedEvent;

class MakeChangeOrder
{

    /**
     * Handle the event.
     *
     * @param  SubscriptionModifiedEvent  $event
     * @return void
     */
    public function handle(SubscriptionModifiedEvent $event)
    {
        $subscription = $event->subscription;
        $co = new ChangeOrder;

        $co->user = \Auth::user()->name;
        $co->reason = isset($event->requestData->reason) ? $event->requestData->reason : '';

        //WHAT WAS CHANGED THOUGH?

        $subscription->change_orders()->save($co);
    }
}
