<?php

namespace App\Listeners\AddOnSale;

use App\Events\Sale\AddOnSale;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateOrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddOnSale  $event
     * @return void
     */
    public function handle(AddOnSale $event)
    {
        $order = new Order;
        $order->amount = $event->amount;
        $order->invoice_number = $event->invoiceNumber;
        $order->status = 'paid';
        $order->user_id = $event->user->id;
        $order->source = 'add-on';
        $order->products = json_encode($event->products);
        $order->auth_transaction = $event->transactionId;
        $order->type = 'cim';
        $order->save();
    }
}
