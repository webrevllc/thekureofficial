<?php

namespace App\Listeners\AddOnSale;

use App\Events\Sale\AddOnSale;
use App\Order;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Snowfire\Beautymail\Beautymail;

class EmailBosses
{
    /**
     * @var Beautymail
     */
    private $beautymail;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Beautymail $beautymail)
    {
        //
        $this->beautymail = $beautymail;
    }

    /**
     * Handle the event.
     *
     * @param  AddOnSale  $event
     * @return void
     */
    public function handle(AddOnSale $event)
    {

    }
}
