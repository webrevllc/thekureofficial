<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AirDate extends Model
{
    protected $table = 'airdates';


    public function schedule()
    {
        return $this->belongsTo('App\Schedule');
    }
}
