<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inquiry extends Model
{
    protected $table ='inquiries';
    protected $fillable = ['name', 'phone', 'email', 'message', 'status'];

    public function is_pending($id)
    {

    }
}
