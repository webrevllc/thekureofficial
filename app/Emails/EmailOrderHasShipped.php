<?php

namespace App\Emails;


class EmailOrderHasShipped extends Emails
{

    public function getEmailId()
    {
        return '18b6ebae-64a6-496a-958e-84ee1a838bfb';
    }

    public function variables($user)
    {
        return [
            'name' => ucwords(strtolower($user->name))
        ];
    }
}