<?php

namespace App\Emails;


class ShippingReportEmail extends Emails
{

    public function getEmailId()
    {
        return 'cdf59f4a-fa10-45e2-9b34-c8624b8473dc';
    }

    public function variables($user, $date, $serials, $orders)
    {
        return [
            'name' => ucwords(strtolower($user->name)),
            'date' => $date,
            'serials' => $serials,
            'orders' => $orders
        ];
    }
}