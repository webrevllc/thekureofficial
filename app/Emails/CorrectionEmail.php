<?php

namespace App\Emails;

class CorrectionEmail extends Emails
{

    public function getEmailId()
    {
        return 'b2ffe204-e49a-4c08-aa35-79b20c584184';
    }

    public function variables($user)
    {
        return [
            'name' => $user->name,
        ];
    }
}