<?php

namespace App\Emails;

class RefundEmail extends Emails
{

    public function getEmailId()
    {
        return 'd4773868-de5b-4c6a-ab62-af8bb765ce85';
    }

    public function variables($user, $amount)
    {
        return [
            'name' => $user->name,
            'amount' => number_format($amount, 2),
        ];
    }
}