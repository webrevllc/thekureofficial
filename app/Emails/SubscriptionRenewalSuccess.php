<?php

namespace App\Emails;

class SubscriptionRenewalSuccess extends Emails
{

    public function getEmailId()
    {
        return 'e10800b1-5197-4b46-94fa-1f1cca872c5b';
    }

    public function variables($user, $subscription)
    {
        return [
            'amount' => $subscription->amount,
            'url' => 'https://www.thekure.com/mykure?pdf='. $subscription->unique_id
        ];
    }
}