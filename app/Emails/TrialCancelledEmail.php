<?php

namespace App\Emails;

class TrialCancelledEmail extends Emails
{

    public function getEmailId()
    {
        return 'f7b10a6a-d12f-463b-9ba7-27c0e4ff1424';
    }

    public function variables($user, $subscription)
    {
        return [
            'name' => $user->name,
            'order_id' => $subscription->unique_id
        ];
    }
}