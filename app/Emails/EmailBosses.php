<?php


namespace App\Emails;


class EmailBosses extends Emails
{

    public function getEmailId()
    {
        return '654bf490-b0a3-4a23-9eb8-af4cf13910e4';
    }

    public function variables($user, $status, $type, $order, $subscription, $orderInfo)
    {
        $x = [
            'type' => $type,
            'status' => $status,
            'date' => $order->original_datetime == '' ? date("F j, Y, g:i a", strtotime($order->created_at)) : $order->original_datetime,
            'amount' => number_format($order->amount, 2),
            'city' => $type == "IVR" ? $orderInfo->Customer->City : $orderInfo->city,
            'state' => $type == "IVR" ? $orderInfo->Customer->State : $orderInfo->state,
            'source' => $type == "IVR" ? 'IVR' : $orderInfo->source,
            'name' => $type == "IVR" ? $orderInfo->Customer->First_Name . ' '. $orderInfo->Customer->Last_Name : $orderInfo->first_name .' '. $orderInfo->last_name
        ];
        return $x;
    }
}