<?php

namespace App\Emails;


class EmailReceipt extends Emails
{

    public function getEmailId()
    {
        return '4ee04dce-1ed2-43bd-bdc2-c2ccaac9590c';
    }

    public function variables($user, $order)
    {
        return [
            'name' => ucwords(strtolower($user->name)),
            'order_id' => $order->id,
            'date' => date('F j, Y, g:i a', strtotime($order->created_at))
        ];
    }
}