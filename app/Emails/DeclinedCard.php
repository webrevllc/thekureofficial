<?php

namespace App\Emails;


class DeclinedCard extends Emails
{

    public function getEmailId()
    {
        return 'eb10c08e-0b0c-49a3-9efa-aa383e15ef47';
    }

    public function variables($user, $reason)
    {
        return [
            'name' => ucfirst($user->name),
            'reason' => $reason,
        ];
    }
}