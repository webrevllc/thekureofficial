<?php

namespace App\Emails;

class CancelledSubscription extends Emails
{

    public function getEmailId()
    {
        return 'a22e2d45-ec66-43b5-aec5-34b4e45e3629';
    }

    public function variables($user, $order_id)
    {
        return [
            'name' => $user->name,
            'order_id' => $order_id,
        ];
    }
}