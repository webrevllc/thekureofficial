<?php

namespace App\Emails;


class AbandonedCart extends Emails
{

    public function getEmailId()
    {
        return '1017ebc3-6df1-4f73-9039-5a01336bc1f7';
    }

    public function variables($user)
    {
        return [
            'name' => ucfirst($user->first_name),
            'url' => $user->url
        ];
    }
}