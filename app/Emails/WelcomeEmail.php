<?php

namespace App\Emails;

class WelcomeEmail extends Emails
{

    public function getEmailId()
    {
        if(env('offer_status') == 'free trial'){
            return '61a2a89b-0c72-44bd-b92a-f76df8f4b7b0';
        }
        else{
            return '1e8c60a3-4d5b-4d55-8c54-e7803ac1993d';
        }
    }

    public function variables($user)
    {
        return [
            'name' => $user->name,
        ];
    }
}