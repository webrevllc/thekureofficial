<?php

namespace App\Emails;


class SupportNotification extends Emails
{

    public function getEmailId()
    {
        return 'e2335c95-1c08-4251-95cd-e7098acb28f1';
    }

    public function variables($user, $name, $email, $phone, $quantity)
    {
        return [
            'name' => $name,
            'email' => $email,
            'phone' => $phone,
            'quantity' => $quantity,
        ];
    }
}