<?php

namespace App\Emails;


class SubscriptionModified extends Emails
{

    public function getEmailId()
    {
        return '5ee59387-65bf-4a54-9fef-d5ef8f1adffb';
    }

    public function variables($user)
    {
        return [
            'name' => ucwords(strtolower($user->name)),
            'url' => 'https://www.thekure.com/mykure'
        ];
    }
}