<?php

namespace App\Emails;


class Support extends Emails
{

    public function getEmailId()
    {
        return '1937bcb4-e90b-4ab7-b8c0-8eb548d8fc7e';
    }

    public function variables($user, $name, $email, $phone, $message)
    {
        return [
            'name' => $name,
            'email' => $email,
            'phone' =>$phone,
            'message' => $message,

        ];
    }
}