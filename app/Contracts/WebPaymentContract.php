<?php
/**
 * Created by PhpStorm.
 * User: srose
 * Date: 9/28/2015
 * Time: 4:39 PM
 */

namespace App\Contracts;


interface WebPaymentContract extends Payment
{
    public function makeWebPayment($request, $products, $user, $type, $quantity);
    public function leadAuth($request, $lead);
    public function pay_now_sale($lead, $products);
    public function leadCIM($request, $lead);
    public function singleTimePayment($user, $products);
}