<?php

namespace App\Contracts;


use App\User;
use Illuminate\Http\Request;

interface Invisacloud
{
    public function getToken();
    public function makeUser($user, $transaction_id, $campaign);
    public function uploadOrder($order);
    public function updateOrder($order);
    public function createSerials($quantity);
    public function makeNewUser($user, $invoice_number, $campaign);
    public function isSerialUsed($serial);
    public function getKurePassword($serial);
    public function getFriendlyName($serial);
    public function subscriptionRenewal($subscription, $numberOfDays);

}