<?php

namespace App\Contracts;

interface Payment
{
    public function refund($amount, $order);
    public function voidTransaction($trans_id);
    public function createCIMProfile($transaction);
    public function createAuthIDs($transactionID);
    public function getAuthIDs($customerProfileID);
    public function capturePriorAuth($trans_id, $amount);
    public function makeSubscriptionPayment($subscriptions);
    public function makeAIMPayment($type, $amount, $card, $exp);
    public function addOnSale($user, $amount, $quantity, $products);
    public function getShippingAddress($auth_profile, $default_shipping);
    public function CIMRefund($user, $amount, $transId);
    public function getCustomerPaymentProfile($userID);
    public function getCustomerShippingProfile($userID);
    public function getHostedPage($profileID);
    public function getProfile($profile);
}



