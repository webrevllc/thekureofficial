<?php

namespace App\Contracts;


use App\Order;
use App\User;
use Illuminate\Http\Request;

interface LegacyPayment
{
    public function refund($amount, $order);
    public function addBillingInfoToRefunds($userID);
}