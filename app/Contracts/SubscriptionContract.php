<?php
/**
 * Created by PhpStorm.
 * User: srose
 * Date: 9/28/2015
 * Time: 4:40 PM
 */

namespace App\Contracts;


interface SubscriptionContract extends UserPaymentContract
{
    public function userHasValidCIM($subscription);
    public function theUserHasValidCIM($user);
    public function chargeSubscription($subscription);
    public function chargeFailedSubscription($subscription, $user);
}