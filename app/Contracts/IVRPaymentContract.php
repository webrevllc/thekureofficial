<?php
/**
 * Created by PhpStorm.
 * User: srose
 * Date: 9/28/2015
 * Time: 4:40 PM
 */

namespace App\Contracts;


interface IVRPaymentContract extends WebPaymentContract
{
    public function makeIVRPayment($orderInfo, $user, $type);
    public function newCIMSale($orderInfo, $user, $type);
    public function updateCustomerWithJSON($user, $orderInfo, $type);
    public function makeProfileOnANet($trans);
}