<?php
/**
 * Created by PhpStorm.
 * User: srose
 * Date: 9/28/2015
 * Time: 4:39 PM
 */

namespace App\Contracts;


interface UserPaymentContract extends Payment
{
	public function updateUserPayment( $user, $requestData );
	public function createExistingUserOrder($user, $total);
}