<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IVRLead extends Model
{
    protected $fillable = ['name', 'email', 'address', 'city', 'state', 'zip', 'phone', 'country', 'products'];
    protected $table = 'ivr_leads';
}
