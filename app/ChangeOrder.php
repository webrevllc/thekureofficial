<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChangeOrder extends Model
{
    protected $table = 'change_orders';
    protected $fillable = ['user', 'reason'];

    public function subscription()
    {
        return $this->belongsTo('App\Subscription');
    }
}
