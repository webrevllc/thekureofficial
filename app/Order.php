<?php

namespace App;

use App;
use App\Contracts\Payment;
use App\Emails\EmailReceipt;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    protected $fillable = ['invoice_number', 'amount', 'status', 'user_id', 'last_4', 'expiration', 'auth_transaction', 'auth_billing', 'auth_shipping', 'products'];

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    protected $table= "orders";

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function subscription()
    {
        return $this->hasOne('App\Subscription');
    }
    public function serials()
    {
        return $this->hasMany('App\Serial');
    }

    public function address()
    {
        return $this->hasOne('App\Address');
    }

    public function refunds()
    {
        return $this->hasMany('App\Refund');
    }

    public function last4()
    {
        return $this->last_4;
    }

    public function total($products, $user)
    {
        $price = [];
        foreach($products as $p){
            $x = $p->price * $p->quantity;
            $price[] = $x;
        }
        $productTotal = array_sum($price)/100;
        if($user->taxable){
            $subtotal = number_format(($productTotal * $user->tax_rate) + $productTotal, 2);
//            $subtotal = ($productTotal * .07613) + $productTotal;
        }else{
            $subtotal = $productTotal;
        }
        return number_format($subtotal, 2);
    }

    public function twoPayTotal($products, $user, $twoPay)
    {

        $price = [];
        foreach($products as $p){
            if($p->ivr_code == "KURE" || $p->ivr_code == "KUREA")
            {
                $x = $twoPay ? 2495 : 4995;
                $price[] = $x * $p->quantity;
            }
            elseif($p->api_code == "HWMON")
            {
                $x = $twoPay ? 495 : 995;
                $price[] = $x * $p->quantity;
            }
            elseif($p->ivr_code == "EMER")
            {
                $price[] = 1995  * $p->quantity;
            }
            elseif($p->ivr_code == "SHIP")
            {
                $price[] = 595  * $p->quantity;
            }
            elseif($p->ivr_code == "PROC")
            {
                $x = $twoPay ? 199 : 0;
                $price[] = $x * $p->quantity;
            }

        }
        $productTotal = array_sum($price)/100;
        if($user->taxable){
            $subtotal = number_format(($productTotal * $user->tax_rate) + $productTotal, 2);
        }else{
            $subtotal = $productTotal;
        }
        return number_format($subtotal, 2);
    }

    public function totalNoUser($products)
    {
        $price = array();
        foreach(json_decode($products) as $p){
            $x = $p->price * $p->quantity;
            $price[] = $x;
        }
        $productTotal = array_sum($price)/100;

        return number_format($productTotal, 2);
    }
    public function recurringProductsTotal($products)
    {
        $price = array();
        foreach($products as $p){
            if($p->recurring){
                $x = $p->price * $p->quantity;
                $price[] = $x;
            }

        }
        $productTotal = array_sum($price)/100;
        if(\Auth::check() && \Auth::user()->is_taxable()){
            $subtotal = ($productTotal * .07) + $productTotal;
        }else{
            $subtotal = $productTotal;
        }
        return number_format($subtotal + $this->shippingAmount(), 2);
    }

    public function newInvoiceNumber()
    {
        $validCharacters = "23456789ABCDEFGHJKMNPRSTUXYVWZ";
        $validCharNumber = strlen($validCharacters);
        $result = "";
        for ($i = 0; $i < 8; $i++) {
            $index = mt_rand(0, $validCharNumber - 1);
            $result .= $validCharacters[$index];
        }
        if($this->where('invoice_number', '=', $result)->exists()){
            $this->newInvoiceNumber();
        }
        return $result;
    }

    public function failed()
    {
        return $this->where('status', '=', 'paid')->all();
    }

    private function shippingAmount()
    {
        return 0;
//        return 5.95;
    }

    public function calculateTax($total)
    {
        return number_format($total * .07613, 2);
    }

    public function sendReceipt($order)
    {
        (new App\Emails\WelcomeEmail())->sendTo($order->user);
//        (new EmailReceipt)->withData(['order' => $order])->sendTo($order->user);
    }
}
