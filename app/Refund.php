<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Refund extends Model
{
    protected $fillable = ['order_id', 'amount', 'refunded_by', 'reason', 'full', 'new_products'];

    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}
